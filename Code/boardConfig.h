#ifndef __HYDRACONFIG__
#define __HYDRACONFIG__

// LCD DATA PIN
#define LCD_DATA_PORT		GPIO_PORT_2
#define LCD_DATA_0			GPIO_PIN_0
#define LCD_DATA_1			GPIO_PIN_1
#define LCD_DATA_2			GPIO_PIN_2
#define LCD_DATA_3			GPIO_PIN_3
#define LCD_DATA_4			GPIO_PIN_4
#define LCD_DATA_5			GPIO_PIN_5
#define LCD_DATA_6			GPIO_PIN_6
#define LCD_DATA_7			GPIO_PIN_7
// LCD CS PIN	
#define LCD_CS_PORT			GPIO_PORT_1
#define LCD_CS_PIN			GPIO_PIN_29
// LCD WR PIN
#define LCD_WR_PORT			GPIO_PORT_0
#define LCD_WR_PIN			GPIO_PIN_22
// LCD RD PIN
#define LCD_RD_PORT			GPIO_PORT_0
#define LCD_RD_PIN			GPIO_PIN_21
// LCD RS pin
#define LCD_RS_PORT			GPIO_PORT_4
#define LCD_RS_PIN			GPIO_PIN_29
// LCD RST pin
#define LCD_RST_PORT		GPIO_PORT_1
#define LCD_RST_PIN			GPIO_PIN_28


#define SD_SPI_CHANNEL		GPIO_PORT_0
#define TP_SPI_CHANNEL		GPIO_PIN_0

#define SD_SPI_CS_PORT		GPIO_PORT_0
#define SD_SPI_CS_PIN		GPIO_PIN_16

#define TP_SPI_CS_PORT		GPIO_PORT_0
#define TP_SPI_CS_PIN		GPIO_PIN_20

#define TP_INT_PORT			GPIO_PORT_2
#define TP_INT_PIN			GPIO_PIN_11

#define TP_BUSY_PORT		GPIO_PORT_0
#define TP_BUSY_PIN			GPIO_PIN_19

#define KEY_1_PORT			GPIO_PORT_1
#define KEY_1_PIN			GPIO_PIN_14

#define KEY_2_PORT			GPIO_PORT_1
#define KEY_2_PIN			GPIO_PIN_15

#define KEY_3_PORT			GPIO_PORT_1
#define KEY_3_PIN			GPIO_PIN_4

#define KEY_4_PORT			GPIO_PORT_1
#define KEY_4_PIN			GPIO_PIN_0

#define KEY_5_PORT			GPIO_PORT_1
#define KEY_5_PIN			GPIO_PIN_1

#define LED_1_PORT			GPIO_PORT_1
#define LED_1_PIN			GPIO_PIN_8

#define LED_2_PORT			GPIO_PORT_1
#define LED_2_PIN			GPIO_PIN_9

#define LED_3_PORT			GPIO_PORT_1
#define LED_3_PIN			GPIO_PIN_10

#define SD_MISO_PORT			GPIO_PORT_0
#define SD_MISO_PIN				GPIO_PIN_17

#define SD_MOSI_PORT			GPIO_PORT_0
#define SD_MOSI_PIN				GPIO_PIN_18

#define SD_SCK_PORT				GPIO_PORT_0
#define SD_SCK_PIN				GPIO_PIN_15

#define SD_CS_PORT			GPIO_PORT_0
#define SD_CS_PIN			GPIO_PIN_16


// Macros for control line st//
void pfBoardInit(void);
void TimerCallback(void);

#endif //__HYDRACONFIG__
