#include "prime_framework.h"
#include "prime_sysclk.h"
#include "prime_gpio.h"
#include "prime_uart0.h"
#include "prime_spi0.h"
#include "prime_lcd.h"
#include "prime_graphics.h"
#include "prime_mmc.h"
#include "prime_diskio.h"
#include "prime_fatfs.h"
#include "prime_touch.h"
#include "boardConfig.h"
#include "logo.h"
#include "main.h"
#include "ptm.h"

PFGpioPortPin keyPortPin[5] = 
{
	{KEY_1_PORT, KEY_1_PIN},
	{KEY_2_PORT, KEY_2_PIN},
	{KEY_3_PORT, KEY_3_PIN},
	{KEY_4_PORT, KEY_4_PIN},
	{KEY_5_PORT, KEY_5_PIN}
};

void showLogo(void);
void BlurOut(void);
void showText(void);
PFEnBoolean pfButtonPressed(PFbyte keyNum);

int main(void)
{
	PFdword x = 0 ,y = 0;
	pfBoardInit();
	
	showLogo();
	pfTickDelayMs(500);
	BlurOut();
	//showText();
	
	while(1)
	{
			pfRunPtm();
	}
}

void showLogo(void)
{
	PFdword logoWidth, logoHeight, logoLoop;
	
	logoWidth = bitmap_file[0];
	logoHeight = bitmap_file[1];
	
	pfILI9320SetAreaMax();
	pfILI9320FillRGB(WHITE);
	
	pfILI9320SetWindow(120-(logoWidth/2), 160-(logoHeight/2) , 119+(
	logoWidth/2), 159+(logoHeight/2));
	pfILI9320SetCursor(120-(logoWidth/2), 160-(logoHeight/2));
	pfILI9320WriteCmd(0x0022);
	for(logoLoop = 0; logoLoop < (logoWidth * logoHeight); logoLoop++)
	{
		pfILI9320WriteData(bitmap_file[logoLoop + 2]);
	}
	pfILI9320SetAreaMax();
}

void BlurOut()
{
	pfGfxDrawSolidRectangle(64,177,64+37,177+34,WHITE);		//1 
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(101,75,101+37,75+34,WHITE);		//2
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(138,143,138+37,143+34,WHITE);	//3
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(27,109,27+37,109+34,WHITE);		//4
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(175,75,175+37,75+34,WHITE);		//5	
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(101,211,101+37,211+34,WHITE);	//6
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(64,75,64+37,75+34,WHITE);			//7
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(27,211,27+37,211+34,WHITE);		//8
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(138,75,138+37,75+34,WHITE);		//9
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(101,143,101+37,143+34,WHITE);	//10
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(27,143,27+37,143+34,WHITE);		//11
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(27,75,27+37,75+34,WHITE);			//12
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(101,109,101+37,109+34,WHITE);	//13
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(175,211,175+37,211+34,WHITE);	//14
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(175,109,175+37,109+34,WHITE);	//15
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(138,211,138+37,211+34,WHITE);	//16
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(64,109,64+37,109+34,WHITE);		//17
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(138,177,138+37,177+34,WHITE);	//18	
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(138,109,138+37,109+34,WHITE);	//19	
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(64,211,64+37,211+34,WHITE);		//20
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(64,143,64+37,143+34,WHITE);		//21
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(175,177,175+37,177+34,WHITE);	//22
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(27,177,27+37,177+34,WHITE);		//23
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(101,177,101+37,177+34,WHITE);	//24
	pfTickDelayMs(50);
	pfGfxDrawSolidRectangle(175,143,175+37,143+34,WHITE);	//25
	pfTickDelayMs(50);
}

PFEnBoolean pfButtonPressed(PFbyte keyNum)
{
	PFdword temp=0 ;
	PFdword i=0; 
	keyNum--;
	temp = ((pfGpioPortRead(keyPortPin[keyNum].port)) & (keyPortPin[keyNum].pin));
	if(temp == 0)
	{
		return enBooleanTrue;
	}
	else
	{
		return enBooleanFalse;
	}
}