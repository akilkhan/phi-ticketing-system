#include "prime_framework.h"
#include "prime_sysclk.h"
#include "prime_utils.h"
#include "prime_i2s.h"
#include "prime_sgtl5000.h"

#define SGTL5000_SlaveAddr 0x14		//SGTL5000 Slave Address. if AD0-0 Slave address is 14H if AD0-1 Slave address is 54H
#define AA 0x04
#define SI 0x08
#define STO 0x10
#define STA 0x20
PFCfgSGTL5000 sgtlConfig;
PFword muted = 1;
PFEnStatus pfSgtl5000Write(PFword RegAdd,PFword data);
PFbyte sgtlConfigInit=0;
PFEnStatus pfSgtl5000Read(PFbyte RegAdd,PFbyte *data,PFbyte size,PFdword *readBytes);
 PFchar pfCalcVol(PFfloat n, PFbyte range);

PFEnStatus pfSgtl5000Write(PFword RegAdd,PFword data)
{
	PFbyte temp[4]={0};
	temp[0]= (RegAdd>>8);
	temp[1]= (RegAdd);
	temp[2]= (data>>8);
	temp[3]= (data);
#if (PF_SGTL5000_DEBUG == 1)
	if(sgtlConfigInit == 0)
		return enStatusNotConfigured;
#endif
    sgtlConfig.sgtl5000I2CWrite(temp,enBooleanTrue,SGTL5000_SlaveAddr,4); 
return enStatusSuccess;
}

PFEnStatus pfSgtl5000Read(PFbyte RegAdd,PFbyte *data,PFbyte size,PFdword *readBytes)
{
	PFbyte temp[2]={0};
    temp[0]= (RegAdd>>8);
	temp[1]= (RegAdd);
#if (PF_SGTL5000_DEBUG == 1)
	if(sgtlConfigInit == 0)
		return enStatusNotConfigured;
#endif
	sgtlConfig.sgtl5000I2CWrite(temp,enBooleanTrue,SGTL5000_SlaveAddr,4); 
	sgtlConfig.sgtl5000I2CRead(data,enBooleanTrue,SGTL5000_SlaveAddr, size,readBytes);
 return enStatusSuccess; 
}

PFEnStatus pfSgtl5000open(PFpCfgSGTL5000 config)
{
	
#if(PF_SGTL5000_DEBUG==1)
	if(config == NULL)
		return enStatusNotConfigured;
#endif
    PFdword clkctrl=0;
    PFdword i2sctrl=0;
	pfMemCopy(&sgtlConfig,config,sizeof(PFCfgSGTL5000));
    pfSgtl5000Write(SGTL5000_CHIP_DIG_POWER,0x73);
	clkctrl= (clkctrl | (config->sampleSel<<2) | (config->divMode<<4));
	pfSgtl5000Write(SGTL5000_CHIP_CLK_CTRL,clkctrl);
	i2sctrl = (1<<8|config->dataLength<<4);
	pfSgtl5000Write(SGTL5000_CHIP_I2S_CTRL,i2sctrl);
	pfSgtl5000Write(SGTL5000_CHIP_ADCDAC_CTRL,config->hpVolume);
	pfSgtl5000Write(SGTL5000_CHIP_DAC_VOL,config->dacVolume);
	pfSgtl5000Write(SGTL5000_CHIP_ANA_HP_CTRL,0);
	pfSgtl5000Write(SGTL5000_CHIP_ANA_CTRL,0);
	pfSgtl5000Write(SGTL5000_CHIP_ANA_POWER,0x70f8);
	sgtlConfigInit=1; 
	return enStatusSuccess;
}


PFEnStatus pfSgtl5000SetHeadPhoneVolume(PFword volume)
 {
#if (PF_SGTL5000_DEBUG == 1)
	if(sgtlConfigInit == 0)
		return enStatusNotConfigured;
#endif	
	if (volume > 0x80) 
	{
		volume = 0;
	}
	else
	{
		volume = 0x80 - volume;
	}
	 volume= volume | (volume << 8);
	 pfSgtl5000Write(SGTL5000_CHIP_ANA_HP_CTRL, volume);  // set volume 
	return enStatusSuccess;
 }
 
 PFEnStatus pfSgtl5000SetDacVolume(PFword volume)
 {
#if (PF_SGTL5000_DEBUG == 1)
	if(sgtlConfigInit == 0)
		return enStatusNotConfigured;
#endif	
	 pfSgtl5000Write(SGTL5000_CHIP_DAC_VOL, volume);  // set volume 
	return enStatusSuccess;
 }
 
 PFEnStatus pfSgtl5000MuteHeadphone()
 { 
 #if (PF_SGTL5000_DEBUG == 1)
	if(sgtlConfigInit == 0)
		return enStatusNotConfigured;
#endif
	 PFbyte mute=0;
	 mute|=(1<<4); 
	 pfSgtl5000Write(SGTL5000_CHIP_ANA_CTRL,mute);  // set mute
	return enStatusSuccess;
  }
  
 PFchar pfCalcVol(PFfloat n, PFbyte range)
{
	n=(n*(PFfloat)range)+0.499;
	if ((PFchar)n>range) n=range;
	return (PFchar)n;
}

PFEnStatus pfSgtl5000Close(void)
{
 #if (PF_SGTL5000_DEBUG == 1)
	if(sgtlConfigInit == 0
		return enStatusNotConfigured;
#endif 
	sgtlConfigInit =0 ;
	return enStatusSuccess;
}
