#ifndef __PTM__
#define __PTM__

void pfPtmCleanup(void);				//screen clean-up
void pfPtmDrawMenu(void);				//show cal menu
PFbyte pfPtmSelectRoute(const char* FILENAME);
PFEnStatus pfPtmProcessRoute(PFbyte route);
PFbyte pfPtmRoutesGetTouchBlock(void);	//returns block number, if any block sensed touch
PFbyte pfPtmRoutesGetMenuBlock(void);		//returns menu block 
void pfPtmDataEntry(PFbyte field);
PFbyte pfPtmGetTouchNextBlock(void);
PFbyte pfPtmRoutesGetDestStopsBlock(void);
PFbyte pfPtmRoutesGetSrcStopsBlock(void);
PFbyte pfPtmGetNumberBlock(void);
PFbyte pfPtmGetConfirmTouch(void);
PFbyte pfPtmGetNextTicket(void);
void pfPtmCalculateTicket(void);
void pfPtmDisplayTicket(void);
void pfRunPtm(void);

#endif	// __PTM__

