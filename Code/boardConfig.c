#include "prime_framework.h"
#include "prime_sysClk.h"
#include "boardConfig.h"
#include "prime_gpio.h"
#include "prime_tick.h"
#include "prime_uart0.h"
#include "prime_rit.h"
#include "prime_timer0.h"
#include "prime_spi0.h"
#include "prime_lcd.h"
#include "prime_graphics.h"
#include "prime_mmc.h"
#include "prime_diskio.h"
#include "prime_fatfs.h"
#include "prime_touch.h"
#include "prime_rtc.h"

//extern PFdword delayTick;
PFbyte IDArray=0;

#define GPIO_PIN_COUNT		30
#define DRIVE_NUM	0
#define FAT_TIME	1412094657
PFCfgGpio gpioPins[] = 
{
	// uart 0 Tx
	{GPIO_PORT_0, GPIO_PIN_2, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_2_TXD0},
	// uart 0 Rx
	{GPIO_PORT_0, GPIO_PIN_3, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_3_RXD0},
	
	// LCD data pins
	{LCD_DATA_PORT, (LCD_DATA_0 | LCD_DATA_1 | LCD_DATA_2 | LCD_DATA_3 | LCD_DATA_4 | LCD_DATA_5 | LCD_DATA_6 | LCD_DATA_7), enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},

	// LCD RS pin
	{LCD_RS_PORT, LCD_RS_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},

	// LCD WR pin
	{LCD_WR_PORT, LCD_WR_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},

	// LCD RD pin
	{LCD_RD_PORT, LCD_RD_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},

	// LCD CS pin
	{LCD_CS_PORT, LCD_CS_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},

	// LCD RST pin
	{LCD_RST_PORT, LCD_RST_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},

	// Touch panel 
	{TP_SPI_CS_PORT, TP_SPI_CS_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	{TP_INT_PORT, TP_INT_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	{TP_BUSY_PORT, TP_BUSY_PIN, enGpioDirInput, enGpioPinModePullDown, enGpioOpenDrainDisable, enGpioFunctionGpio},
	
	// Keypad keys
	{KEY_1_PORT, KEY_1_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	{KEY_2_PORT, KEY_2_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	{KEY_3_PORT, KEY_3_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	{KEY_4_PORT, KEY_4_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	{KEY_5_PORT, KEY_5_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	
	// Keypad LEDs
	{LED_1_PORT, LED_1_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	{LED_2_PORT, LED_2_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	{LED_3_PORT, LED_3_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	
	// I2C pins
	{GPIO_PORT_0, GPIO_PIN_27, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainEnable, GPIO_0_27_SDA0},
	{GPIO_PORT_0, GPIO_PIN_28, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainEnable, GPIO_0_28_SCL0},
	
	// SPI pins
	{SD_SCK_PORT, SD_SCK_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_15_SCK0},
	{SD_MISO_PORT, SD_MISO_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_17_MISO0},
	{SD_MOSI_PORT, SD_MOSI_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_18_MOSI0},
	
	// SD card chip select
	{SD_CS_PORT, SD_CS_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	
	//I2S PINS
	
	{GPIO_PORT_0, GPIO_PIN_6,  enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionAlt1},
	{GPIO_PORT_0, GPIO_PIN_7,  enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionAlt1},
	{GPIO_PORT_0, GPIO_PIN_8,  enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionAlt1},
	{GPIO_PORT_0, GPIO_PIN_9,  enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionAlt1},
	
	{GPIO_PORT_4, GPIO_PIN_28, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionAlt1}
};





PFCfgClk clkConfig = {	
							100000000,
							12000000,
							enPllClkSrcMainOSC
						};

static const PFCfgSpi0 spi0Cfg =
{
	enPclkDiv_4,		// Peripheral clock divider for SPI0 module		
	enBooleanTrue,		// Select master or slave mode for SPI0			
	enSpi0Databits_8,	// Set datasize for SPI0 packet					
	enSpi0Mode_0,		// Select SPI0 mode								
	25000000,			 // SPI0 channel baudrate in bits/second`		
	enSpi0IntNone		// Select SPI0 interrupts to enable				
};


/*********************************RIT Configuration**********************/
PFCfgRit ritConfig = 
{
	25000,// configure RIT for 1ms
    0x00,			// compare mask
	pfTickUpdate,	// callback
	enPclkDiv_4,	// pclk divider
	enBooleanTrue,	// halt on break
	enBooleanTrue	// reset on match
};
/*********************************UART0 Configuration**********************/
PFCfgUart0 uart0Config = 
{
	enPclkDiv_4, 			
	enUart0Baudrate_9600, 	
	enUart0Databits_8, 		
	enUart0ParityNone, 		
	enUart0StopBits_1, 
	enUart0IntRx
};


PFCfgILI9320 lcddisplayConfig  = 
{  
	{
		{LCD_DATA_PORT,LCD_DATA_0},
		{LCD_DATA_PORT,LCD_DATA_1},
		{LCD_DATA_PORT,LCD_DATA_2},
		{LCD_DATA_PORT,LCD_DATA_3},
		{LCD_DATA_PORT,LCD_DATA_4},
		{LCD_DATA_PORT,LCD_DATA_5},
		{LCD_DATA_PORT,LCD_DATA_6},
		{LCD_DATA_PORT,LCD_DATA_7}	    },//data
		{LCD_CS_PORT,LCD_CS_PIN},//CS
		{LCD_RS_PORT,LCD_RS_PIN},//RS
		{LCD_WR_PORT,LCD_WR_PIN},//WR
		{LCD_RD_PORT,LCD_RD_PIN},//RD
		{LCD_RST_PORT,LCD_RST_PIN},//Reset
		enLcdOrientation_0,
		320,
		240				
};

static const PFCfgMmc mmcCfg = 
{
	{SD_CS_PORT, SD_CS_PIN},			// GPIO pin used for SPI chip select
	{SD_CS_PORT, SD_CS_PIN},			// GPIO pin used for power control			
	 {0,0},			// GPIO pin used for device detection			
	enBooleanFalse,					// Use power control option for MMC card		
	enBooleanFalse,						// Use card detect option for MMC card	
	pfSpi0RegisterDevice,				// Poiner to SPI device register function		
	pfSpi0UnregisterDevice,				// Poiner to SPI device unregister function	
	pfSpi0ChipSelect,					// Poiner to SPI chip select function		
	pfSpi0ExchangeByte,					// Poiner to SPI exchange byte function		
	pfSpi0Write,							// Poiner to SPI read function					
	pfSpi0Read								// Poiner to SPI write function				
};

static const PFCfgDisk diskCfg =
{
 (PFpCfgMmc)&mmcCfg,						// Device configuration structure		
	pfMmcOpen,							// Pointer to device open function		
	pfMmcGetStatus,						// Pointer to device get status function	
	pfMmcRead,							// Pointer to device read function	
	pfMmcWrite,							// Pointer to device write function	
	pfMmcIoCtrl,							// Pointer to device control function
	enBooleanTrue,						// Set device write permission			
	enBooleanTrue						// Set device IO control permission		

};


PFCfgTouch touchConfig = {
							{TP_INT_PORT, TP_INT_PIN},
							{TP_BUSY_PORT, TP_BUSY_PIN},
						    {TP_SPI_CS_PORT, TP_SPI_CS_PIN},
						    enTouchReferenceSelect_Differential,
						    enTouchPrecision_12bit,
							pfSpi0RegisterDevice,
							pfSpi0ChipSelect,
							pfSpi0Write,
							pfSpi0Read
                           };

static PFFatFs fat;

void rtc_isr(void);

PFCfgRtc conf =
{
	1,
	0,
	0,
	rtc_isr,
};

void rtc_isr()
{
	
}

PFRtcTime con = 
{
	12,		/**< Seconds value in the range of 0 to 59 */
	15,		/**< Minutes value in the range of 0 to 59 */
	10,	/**< Hours value in the range of 0 to 23 */
	27,		/**< Day of month value in the range of 1 to 28, 29, 30, or 31 */
	0,		/**< Day of week value in the range of 0 to 6*/
	33,		/**< Day of year value in the range of 1 to 365 (366 for leap years) */
	4,		/**< Month value in the range of 1 to 12*/
	2015,	/**< Year value in the range of 0 to 4095 */	
};

void pfBoardInit()
{
	PFEnStatus status;
	PFdword x, y,i;
	PFbyte diskId;
	pfSysSetCpuClock(&clkConfig); 					//System Clock Init
	status = pfGpioInit(gpioPins, GPIO_PIN_COUNT);	//GPIO Init
	if(status != enStatusSuccess)
	{
		while(1);
	}
	status = pfUart0Open(&uart0Config);				//UART0 Init
	if(status != enStatusSuccess)
	{
		while(1);
	}
	status = pfRitOpen(&ritConfig);
	if(status != enStatusSuccess)
	{
		while(1);
	}
    pfRitStart();
	pfTickSetTimerPeriod(1);
	status = pfSpi0Open((PFpCfgSpi0)&spi0Cfg);
	if(status != enStatusSuccess)
	{
 		while(1);
 	}
	status  = pfILI9320Open(&lcddisplayConfig);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	status = pfTouchOpen(&touchConfig);
   if(status != enStatusSuccess)
	{
		 while(1);
	}
	status = pfDiskOpen(&diskId, (PFpCfgDisk)&diskCfg);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	status = pfFsMount(DRIVE_NUM, &fat);
	if(status!= enStatusSuccess)
 	{
		while(1);
	}	
	
	status = pfRtcOpen(&conf);
	if(status!= enStatusSuccess)
 	{
		while(1);
	}
	pfRtcSetTime(&con);
	pfRtcStart();
}

PFdword get_fattime(void)
{
	return FAT_TIME;
}

//void TimerCallback(void)
//{
//	PFdword status;
//	++delayTick;
//	pfTimer0GetIntStatus(&status);
//	pfTimer0ClearIntStatus(status);
//}


