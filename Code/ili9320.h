/*----------------------------------------------------------------------------
 * Name:    ILI9320.H
 * Purpose: Low level function prototypes and definitions to interface
 * 			ILI9320 based TFT display
 * Note(s):
 * Development Board Name : LPC1768 TFT Board
 *----------------------------------------------------------------------------
 	 	 Created By : G.V.Raghavendra Prasad
 	 	 	 	 	 Phi-Robotics Technologies Pvt. Ltd.
 	 	 	 	 	 Thane
 	 	 	 	e-mail: rprasad@phi-robotics.com
 	 	 	 	Date : 10-09-12
 *----------------------------------------------------------------------------*/



//#include "prime_gpio.h"


// Color definitions


// // Data pins
// // Note: data pins must be consecutive and on the same port
// #define ILI9320_DATA_PORT       (LPC_GPIO2->FIODIR0)
// #define ILI9320_DATA_PIN		(LPC_GPIO2->FIOPIN0)

// // Macros for setting control line directions
// #define ILI9340_CD_OUT	(LPC_GPIO4->FIODIR |= (1<<29))
// #define ILI9340_CS_OUT	(LPC_GPIO1->FIODIR |= (1<<29))
// #define ILI9340_WR_OUT	(LPC_GPIO0->FIODIR |= (1<<22))
// #define ILI9340_RD_OUT	(LPC_GPIO0->FIODIR |= (1<<21))
// #define ILI9340_RESET_OUT	(LPC_GPIO1->FIODIR |= (1<<28))


#define LCD_SET_DATA(A)		pfGpioPortWriteByte(LCD_DATA_PORT, A, 0)

// Macros for control line state
#define CLR_CD()          PERIPH_GPIO4->FIOPIN &= ~(1<<29)	//pfGpioPinsClear(LCD_RS_PORT, LCD_RS_PIN)		//	LPC_GPIO4->FIOPIN &= ~(1<<29)
#define SET_CD()          PERIPH_GPIO4->FIOPIN |= (1<<29)	//pfGpioPinsSet(LCD_RS_PORT, LCD_RS_PIN)		//	LPC_GPIO4->FIOPIN |= (1<<29)
                          
#define CLR_CS()          PERIPH_GPIO1->FIOPIN &= ~(1<<29)	//pfGpioPinsClear(LCD_CS_PORT, LCD_CS_PIN)		//	LPC_GPIO1->FIOPIN &= ~(1<<29)
#define SET_CS()          PERIPH_GPIO1->FIOPIN |= (1<<29)	//pfGpioPinsSet(LCD_CS_PORT, LCD_CS_PIN)		//	LPC_GPIO1->FIOPIN |= (1<<29)
                          
#define CLR_WR()          PERIPH_GPIO0->FIOPIN &= ~(1<<22)	//pfGpioPinsClear(LCD_WR_PORT, LCD_WR_PIN)		//	LPC_GPIO0->FIOPIN &= ~(1<<22)
#define SET_WR()          PERIPH_GPIO0->FIOPIN |= (1<<22)	//pfGpioPinsSet(LCD_WR_PORT, LCD_WR_PIN)		//	LPC_GPIO0->FIOPIN |= (1<<22)
                          
#define CLR_RD()          PERIPH_GPIO0->FIOPIN &= ~(1<<21)	//pfGpioPinsClear(LCD_RD_PORT, LCD_RD_PIN)		//	LPC_GPIO0->FIOPIN &= ~(1<<21)
#define SET_RD()          PERIPH_GPIO0->FIOPIN |= (1<<21)	//pfGpioPinsSet(LCD_RD_PORT, LCD_RD_PIN)		//	LPC_GPIO0->FIOPIN |= (1<<21)
                          
#define CLR_RESET()       PERIPH_GPIO1->FIOPIN &= ~(1<<28)	//pfGpioPinsClear(LCD_RST_PORT, LCD_RST_PIN)	//	LPC_GPIO1->FIOPIN &= ~(1<<28)
#define SET_RESET()       PERIPH_GPIO1->FIOPIN |= (1<<28) 	//pfGpioPinsSet(LCD_RST_PORT, LCD_RST_PIN)		//	LPC_GPIO1->FIOPIN |= (1<<28)   

//Function Prototypes
void ili9320WriteCmd(PFword command);
void ili9320WriteData(PFword data);
void ili9320Command(PFword command, PFword data);
void ili9320Delay(PFdword t);
void ili9320InitDisplay(void);
void ili9320Home(void);
void ili9320SetWindow(PFword x, PFword y, PFword x1, PFword y1);
void ili9320SetCursor(PFword x, PFword y);
void lcdInit(void);
void lcdTest(void);
void lcdFillRGB(PFword data);
void lcdDrawPixel(PFword x, PFword y, PFword color);
void lcdFillArea(PFword xStart, PFword yStart, PFword xEnd, PFword yEnd, PFword clr);
void lcdSetAreaMax(void);


