#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "prime_framework.h"
#include "prime_sysclk.h"
#include "prime_gpio.h"
#include "prime_uart0.h"
#include "prime_spi0.h"
#include "prime_lcd.h"
#include "prime_graphics.h"
#include "prime_touch.h"
#include "prime_mmc.h"
#include "prime_diskio.h"
#include "prime_fatfs.h"
#include "boardConfig.h"
#include "ptm.h"
#include "prime_rtc.h"
#include "buttons.h"

/* Variable Declarations */

static PFword Next_Page[4] = {200,120,220,200}; 	// Rectangle co-ordinates for Next Page symbol

static PFbyte selected_src_stop = 0xFF; 	// Any Src Stop	
static PFbyte selected_dest_stop = 0xFF;	// Any Dest Stop
static PFbyte select_confirm = 0xFF;
static PFbyte new_entry = 0xFF;

static PFbyte next = 0xFF;
static PFdword cost_price = 0;
static PFword chunk = 10;
static PFword var = 0;
PFword src_page = 0;
PFword dest_page = 0;
static PFfloat *dest_dist = 0;
static PFfloat *src_dist = 0;


static PFdword BytesRead;
static PFdword total_no_routes;
static PFdword total_no_stops;

PFbyte **route_arr; 			// Displaying all the routes from file
PFword **RouteBlkOrigins; // Making touch blocks for all the routes	 
PFbyte **route_stops;			// Storing the route stops
PFbyte **stops_distance;	// Storing the stops distance from source
PFfloat **distance_value;	// Converting string into Float
PFbyte **print_stops;			// Only stops of selected route
PFword **stops_Blk;				// Storing x,y coordinates of rectangle stops
PFword **rem_stops;				// Storing stops of last page
PFword **mid_stops;				// Storing stops between first and last page ( middle 10 pages)

/* Flags for printing data */

PFEnBoolean print_stop1 = enBooleanFalse;	
PFEnBoolean print_stop2 = enBooleanFalse;
PFEnBoolean print_ad = enBooleanFalse;
PFEnBoolean print_ch = enBooleanFalse;
PFEnBoolean num_grt_9 = enBooleanFalse;

#define ROUTE_FILE			"/PTM/Routes.txt"

static PFword MenuBlockOrigins[4][4] = 
{
		{68, 60, 218, 78},
		{52, 98, 202, 116},
		{84,136,118,154},
		{100,174,134,192}
};

static PFword NumberBlockOrigins[16][4] =
{
	{20, 60, 60, 100},
	{20, 110, 60, 150},
	{20, 160,60, 200},
	{20, 210,60, 250},

	{70, 60, 110, 100},
	{70, 110, 110, 150},
	{70, 160, 110, 200},
	{70, 210, 110,250},

	{120, 60, 160, 100},
	{120, 110, 160, 150},
	{120, 160, 160, 200},
	{120, 210, 160,250},

	{170, 60, 210, 100},
	{170, 110, 210, 150},
	{170, 160, 210, 200},
	{170, 210, 210,250},

};

static PFbyte disp[10] = {'0','1','2','3','4','5','6','7','8','9'};
static PFbyte selected_ad_no=0;
static PFbyte selected_ch_no=0;

static PFbyte numbers[7][3] = {"10\0","11\0","12\0","13\0","14\0","15\0","16\0"};
static PFword confirm_blk[4] = {52,215,182,255};
static PFword New_Ticket_blk[4] = {64,268,164,298};
static PFbyte cost_arr[2] = {0};
static PFbyte amt[10] = {0};
static PFbyte dis[5] = {0};
static PFbyte info[41] = {0};

void pfShowConfirmButton(void);
void pfShowRoutesButton(PFdword x, PFdword y);
void pfPtmShowStopsButton(PFword x, PFword y);
void pfPtmShowNumbersButton(PFword x, PFword y);
void pfPtmShowNewTktButton(PFword x, PFword y); 

void pfRunPtm(void)
{
	PFbyte select_field;
	PFbyte route;
	PFEnStatus status;
	PFEnBoolean disable_result = 0;
	PFbyte i;

	pfPtmCleanup();

	route = pfPtmSelectRoute(ROUTE_FILE); 
	if(route == 0xFF)
	{
		while(1);		
	}
	
	status = pfPtmProcessRoute(route);
	if(status != enStatusSuccess)
	{
		while(1);		
	}
	
	stops_Blk = (PFword**)malloc(total_no_stops * sizeof(PFword*));

	for(i=0; i < total_no_stops; i++)
			stops_Blk[i] = (PFword*)malloc(4 * sizeof(PFword));
	
	while(1)
	{
		pfPtmDrawMenu();
		
		do
		{		
			select_field = pfPtmRoutesGetMenuBlock();
			if(select_field != 0xFF)
			{	
				pfPtmDataEntry(select_field);
			}

			pfGfxDrawRectangle(confirm_blk[0]-1, confirm_blk[1]-1 , confirm_blk[2] + 1 ,confirm_blk[3] + 1);
			pfShowConfirmButton();
			pfGfxDrawString(confirm_blk[0] + 6, confirm_blk[1] + 12, "CONFIRM", 2, 0x000000, 0xFFFFFF);// CONFIRM			
			
			select_confirm = pfPtmGetConfirmTouch();
		}while(select_confirm == 0xFF);
		
		if(print_stop1 == enBooleanFalse || print_stop2 == enBooleanFalse || print_ad == enBooleanFalse || print_ch == enBooleanFalse)
		{
			  disable_result = 1; 
				pfILI9320FillRGB(WHITE);
				
				pfGfxDrawString(20, 100, "Incomplete Informataion", 1, RED, 0xFFFFFF);
				pfGfxDrawString(30, 120, "Please Try again", 1, RED, 0xFFFFFF);
				pfTickDelayMs(2500);	
		}
		
		if(*dest_dist - *src_dist < 0 || *dest_dist == *src_dist && disable_result == 0) // VALIDITY
		{
			  disable_result = 1; 
				pfILI9320FillRGB(WHITE);
				
				pfGfxDrawString(20, 100, "Invalid Source/Destination", 1, RED, 0xFFFFFF);
				pfGfxDrawString(30, 120, "Please Try again", 1, RED, 0xFFFFFF);
				pfTickDelayMs(2500);			
		}
		
		if(selected_ad_no == 0 && selected_ch_no == 0 && disable_result == 0)
		{		
				disable_result = 1; 
				pfILI9320FillRGB(WHITE);
				
				pfGfxDrawString(20, 100, "Invalid Number entry ", 1, RED, 0xFFFFFF);
				pfGfxDrawString(30, 120, "Please Try again", 1, RED, 0xFFFFFF);
				pfTickDelayMs(2500);						
		}
		    
		if(disable_result == 0)
		{		
			pfPtmCalculateTicket();
			pfPtmDisplayTicket();
		
			do
			{
 			new_entry = pfPtmGetNextTicket();
			}while(new_entry == 0xFF);
 		}
		/* Set all the variables to default for next ticket */
		print_stop1 = enBooleanFalse;
		print_stop2 = enBooleanFalse;
		print_ad = enBooleanFalse;
		print_ch = enBooleanFalse;
		num_grt_9 = enBooleanFalse;
		selected_src_stop = 0xFF;
		selected_dest_stop = 0xFF;
		src_page = 0;
		dest_page = 0;
		disable_result=0;
		memset(amt,0,sizeof(amt));
		memset(dis,0,sizeof(dis));
		memset(info,0,sizeof(info));
	}
}

PFbyte pfPtmSelectRoute(const char* FILENAME)
{
	PFEnStatus file_status;
	PFFsFile fd;
	PFbyte i=0;
	PFbyte deb[20];
	static PFbyte no_of_routes[2] = {0};
	PFbyte selected_route = 0;
	
	pfILI9320FillRGB(0xFFFF);//set complete background colour
	
	pfGfxDrawString(20, 10, "SELECT ROUTE: ", enFont_8X16, RED, WHITE);
	pfGfxDrawLine(20, 25, 120, 25); // Underline
	
	//Display all routes from file
	file_status = pfFsFileOpen(&fd, FILENAME ,FA_READ);
	
	if(file_status == enStatusSuccess)
	{
		file_status = pfFsFileRead(&fd, (PFbyte*)no_of_routes, 2, &BytesRead);
		if((file_status != enStatusSuccess) || (BytesRead == 0))
		{
			pfFsFileClose(&fd);
			return enStatusError;
		}
		
		pfAtoI(no_of_routes, &total_no_routes);
		pfFsFileSeek(&fd,4); //Skipping Number of routes, Carriage return, new line char
		
				
		if(total_no_routes != 0)
		{		
			route_arr = (PFbyte**)malloc(total_no_routes * sizeof(PFbyte*));
			for(i = 0 ; i < total_no_routes; i++)
				route_arr[i] = (PFbyte*)malloc(21 * sizeof(PFbyte));

			for(i = 0 ; i < total_no_routes; i++)
			{
				file_status = pfFsFileRead(&fd, (PFbyte*)route_arr[i], 20, &BytesRead);
				pfFsFileSeek(&fd,22 *(i+1) + 4);
				if((file_status != enStatusSuccess) || (BytesRead == 0))
				{	
					pfFsFileClose(&fd);
					return enStatusError;
				}
			}
			
			//Display of routes
			for(i = 0 ; i < total_no_routes; i++)
			{
				pfGfxDrawSolidCircle(20, 50+i*32 + 5, 2.5, 0x000000);
				pfGfxDrawRectangle(28 -1 , 50 + i*32 - 8 -1, 196 +1,50 + i*32 +18);			
				pfShowRoutesButton(28,50 + i*32 - 8);
				pfGfxDrawString( 30, 50+i*32, route_arr[i], 1, 0x000000, 0xFFFFFF);// Routes
			}

			RouteBlkOrigins = (PFword**)malloc((total_no_routes) * sizeof(PFword*));
			for(i = 0 ; i < total_no_routes; i++)
				RouteBlkOrigins[i] = (PFword*)malloc(4*sizeof(PFword));	
		
			//Making touch blocks for routes
			for(i = 0 ; i < total_no_routes; i++)
			{	
				RouteBlkOrigins[i][0] =  28 - 1;
			}	
			
			for(i = 0 ; i < total_no_routes; i++)
			{	
				RouteBlkOrigins[i][2] =  197;
			}

			for(i = 0 ; i < total_no_routes; i++)
			{	
				RouteBlkOrigins[i][1] =  50 + i*32 - 9;
			}

			for(i = 0 ; i < total_no_routes; i++)
			{	
				RouteBlkOrigins[i][3] =  68 + i*32;
			}			
			 
			do 
			{
				selected_route = pfPtmRoutesGetTouchBlock();
			}while(selected_route == 0xFF);	
		}
		pfFsFileClose(&fd);
		return selected_route;
	}
	else
	{
		sprintf(deb,"File stat: %d \r",file_status);
		pfUart0WriteString(deb);
		pfUart0WriteString("SD card not inserted\r");	
		pfGfxDrawString(25, 130, "Insert SD Card and ", enFont_8X16, RED, WHITE);
		pfGfxDrawString(25, 146, "Press Reset ", enFont_8X16, RED, WHITE);
		pfFsFileClose(&fd);
		return 0xFF;
	}	
}


PFEnStatus pfPtmProcessRoute(PFbyte route)
{
	PFEnStatus file_status;
	PFFsFile fd;
	PFbyte no_of_stops[2] = {0};
	PFbyte route_file[15] = {0};	//Storing the name of the file corresponding to the path
	PFbyte i=0;
	
	sprintf(route_file,"/PTM/Route%d.txt", route + 1);
	pfUart0WriteString(route_file);

	file_status = pfFsFileOpen(&fd, route_file ,FA_READ);
	if(file_status == enStatusSuccess)
	{
		file_status = pfFsFileRead(&fd, (PFbyte*)no_of_stops, 2, &BytesRead);
		if((file_status != enStatusSuccess) || (BytesRead == 0))
		{
			pfFsFileClose(&fd);
			return enStatusError;
		}
	
		pfAtoI(no_of_stops, &total_no_stops);
		pfFsFileSeek(&fd,4); //Skipping number of stops, carriage return and new line char
		
		if(total_no_stops != 0)
		{
			route_stops = (PFbyte**)malloc(total_no_stops * sizeof(PFbyte*)); // For storing all the stops on the route
			for(i = 0 ; i < total_no_stops; i++)
				route_stops[i] = (PFbyte*)malloc(17 * sizeof(PFbyte));  //Maximum stop character size - 12 bytes + 4 byte distance
			
			for(i = 0 ; i < total_no_stops; i++)
			{
				file_status = pfFsFileRead(&fd, (PFbyte*)route_stops[i], 16, &BytesRead);
				pfFsFileSeek(&fd,18 *(i+1) + 4);
				if((file_status != enStatusSuccess) || (BytesRead == 0))
				{	
					pfFsFileClose(&fd);
					return enStatusError;
				}
			}
			
			file_status = pfFsFileRead(&fd, (PFbyte*)cost_arr, 2, &BytesRead); //Storing the per km ticket cost
			if((file_status != enStatusSuccess) || (BytesRead == 0))
			{	
					pfFsFileClose(&fd);
					return enStatusError;
			}
			
			pfAtoI(cost_arr, &cost_price);			
			
			stops_distance = (PFbyte**)malloc(total_no_stops * sizeof(PFbyte*)); // For distance
			
			for(i = 0 ; i < total_no_stops; i++)
					stops_distance[i] = (PFbyte*)malloc(5 * sizeof(PFbyte)); 

			for(i = 0 ; i < total_no_stops; i++)
			{
					pfMemCopy(stops_distance[i], route_stops[i]+12,4);			
			}
			
			distance_value = (PFfloat**)malloc(total_no_stops * sizeof(PFfloat*));
			for(i = 0 ; i < total_no_stops; i++)
					distance_value[i] = (PFfloat*)malloc(sizeof(PFfloat));
			
			for(i = 0 ; i < total_no_stops; i++)
				pfAtoF(stops_distance[i],distance_value[i]);
			
			print_stops = (PFbyte**)malloc(total_no_stops * sizeof(PFbyte*));  //Maximum stop character size - 12 bytes
			for(i = 0 ; i < total_no_stops; i++)
					print_stops[i] = (PFbyte*)malloc(13 * sizeof(PFbyte));
			
			for(i = 0 ; i < total_no_stops; i++)
					pfMemCopy(print_stops[i], route_stops[i],12);
		}
		pfFsFileSeek(&fd,0);
		pfFsFileClose(&fd);		
	}
	else
	{
		pfUart0WriteString("Route file missing\n");
		pfFsFileClose(&fd);
		return enStatusError;
	}
	return enStatusSuccess;
}

void pfPtmDataEntry(PFbyte field)
{
	PFbyte i;
	
	switch(field)	
	{
			case 0:
								pfILI9320FillRGB(WHITE);			
							
								if( chunk - total_no_stops < 10)
								{					
												
									for(i=0; i < total_no_stops; i++)
									{
										pfGfxDrawRectangle(64 -1, 8 + i*32 -1 , 184 + 1, 27 + i*32);		
										pfPtmShowStopsButton(64,8+i*32);
										pfGfxDrawString(80, 8 + i*32 + 2, print_stops[i], enFont_8X16, RED, WHITE);											
										stops_Blk[i][0] = 64;
										stops_Blk[i][1] = 8 + i*32;
										stops_Blk[i][2] = 184;
										stops_Blk[i][3] = 27 + i*32;
									}
								do
								{
									selected_src_stop = pfPtmRoutesGetSrcStopsBlock();
								}while(selected_src_stop == 0xFF);
								
								src_dist = distance_value[selected_src_stop];
										
								print_stop1 = enBooleanTrue;
								pfPtmDrawMenu();											
								}
								else 
								{										
									for(i=0; i < 10; i++)
									{
										pfGfxDrawRectangle(64 - 1, 8 + i*32 -1 , 184 + 1, 27 + i*32);		
										pfPtmShowStopsButton(64,8+i*32);
										pfGfxDrawString(80, 8 + i*32 + 2, print_stops[i], enFont_8X16, RED, WHITE);
										stops_Blk[i][0] = 64;
										stops_Blk[i][1] = 8 + i*32;
										stops_Blk[i][2] = 184;
										stops_Blk[i][3] = 27 + i*32;												
									}
											
								pfGfxDrawFilledTriangle(220,160,200,120,200,200); // NEXT PAGE										
								
								var = total_no_stops;			
								do
								{													
								next = pfPtmGetTouchNextBlock();
																					
								if(next == 1)
								{	
									src_page++;
									pfILI9320FillRGB(WHITE);
									var = var - chunk;		
								
									if(var > chunk)
									{
										mid_stops = (PFword**)malloc(10 * sizeof(PFword*));
										for(i=0; i<10; i++)
											mid_stops[i] = (PFword*)malloc(4 * sizeof(PFword));
										
										for(i=10*src_page; i < (10*src_page+10); i++)
										{
											pfGfxDrawRectangle(64 - 1, 8 + (i%10*src_page)*32 -1  , 184 +1, 27 + (i%10*src_page)*32);		
											pfPtmShowStopsButton(64,8 + (i%10*src_page)*32);
											pfGfxDrawString(80, 8 + (i%10*src_page)*32 + 2, print_stops[i], enFont_8X16, RED, WHITE);						
										}										
										for(i=0; i < 10; i++)
										{
											mid_stops[i][0] = 64;		
											mid_stops[i][1] = 8 + i*32;
											mid_stops[i][2] = 184;
											mid_stops[i][3] = 27 + i*32;
										}
										pfGfxDrawFilledTriangle(220,160,200,120,200,200); // NEXT PAGE										
										
									}
									else
									{
										rem_stops = (PFword**)malloc(var * sizeof(PFword*));
										for(i=0; i < var; i++)
											rem_stops[i] = (PFword*)malloc(4 * sizeof(PFword));
										
										for(i=0; i < var; i++)
										{										
											pfGfxDrawRectangle(64 - 1, 8 + i*32 - 1, 184 + 1, 27 + i*32);
											pfPtmShowStopsButton(64,8 + i*32);
											rem_stops[i][0] = 64; 
											rem_stops[i][1] = 8+i*32; 
											rem_stops[i][2] = 183;
											rem_stops[i][3] = 27+i*32; 
										}
																			
										
										for(i=10*src_page; i < (10*src_page + var); i++)
										{													
											pfGfxDrawString(80, 8 + (i%10*src_page)*32 + 2, print_stops[i], enFont_8X16, RED, WHITE);
										}

									}
								}
								selected_src_stop = pfPtmRoutesGetSrcStopsBlock();	
							}while(selected_src_stop == 0xFF);
							
							if(selected_src_stop != 0xFF)
								src_dist = distance_value[selected_src_stop];														
							
							print_stop1 = enBooleanTrue;
							pfPtmDrawMenu();
									
							}
				break;
							
		case 1:		
								pfILI9320FillRGB(WHITE);

								if( chunk - total_no_stops < 10)
								{					
												
									for(i=0; i < total_no_stops; i++)
									{
										pfGfxDrawRectangle(64 - 1, 8 + i*32-1 , 184 + 1, 27 + i*32);		
										pfPtmShowStopsButton(64,8 + i*32);
										pfGfxDrawString(80, 8 + i*32 + 2, print_stops[i], enFont_8X16, RED, WHITE);											
										stops_Blk[i][0] = 64;
										stops_Blk[i][1] = 8 + i*32;
										stops_Blk[i][2] = 184;
										stops_Blk[i][3] = 27 + i*32;
									}
											
								do
								{
									selected_dest_stop = pfPtmRoutesGetDestStopsBlock();
								}while(selected_dest_stop == 0xFF);
								
								dest_dist = distance_value[selected_dest_stop];
										
								print_stop2 = enBooleanTrue;
								pfPtmDrawMenu();											
								}
								else 
								{										
									for(i=0; i < 10; i++)
									{
										pfGfxDrawRectangle(64 - 1, 8 + i*32 - 1 , 184 + 1, 27 + i*32);		
										pfPtmShowStopsButton(64,8 + i*32);
										pfGfxDrawString(80, 8 + i*32 + 2, print_stops[i], enFont_8X16, RED, WHITE);
										stops_Blk[i][0] = 64;
										stops_Blk[i][1] = 8 + i*32;
										stops_Blk[i][2] = 184;
										stops_Blk[i][3] = 27 + i*32;												
									}
											
								pfGfxDrawFilledTriangle(220,160,200,120,200,200); // NEXT PAGE									
								
								var = total_no_stops;			
								do
								{
								next = pfPtmGetTouchNextBlock();
																					
								if(next == 1)
								{	
									dest_page++;
									pfILI9320FillRGB(WHITE);
									var = var - chunk;		
								
									if(var > chunk)
									{
										mid_stops = (PFword**)malloc(10 * sizeof(PFword*));
										for(i=0; i<10; i++)
											mid_stops[i] = (PFword*)malloc(4 * sizeof(PFword));
										
										for(i=10*dest_page; i < (10*dest_page+10); i++)
										{
											pfGfxDrawRectangle(64 - 1, 8 + (i%10*dest_page)*32 - 1 , 184 + 1, 27 + (i%10*dest_page)*32);		
											pfPtmShowStopsButton(64,8 + (i%10*dest_page)*32);
											pfGfxDrawString(80, 8 + (i%10*dest_page)*32 + 2, print_stops[i], enFont_8X16, RED, WHITE);						
										}										
										for(i=0; i < 10; i++)
										{
											mid_stops[i][0] = 64;		
											mid_stops[i][1] = 8 + i*32;
											mid_stops[i][2] = 184;
											mid_stops[i][3] = 27 + i*32;
										}
										pfGfxDrawFilledTriangle(220,160,200,120,200,200); // NEXT PAGE								
										
									}
									else
									{
										rem_stops = (PFword**)malloc(var * sizeof(PFword*));
										for(i=0; i < var; i++)
											rem_stops[i] = (PFword*)malloc(4 * sizeof(PFword));
										
										for(i=0; i < var; i++)
										{										
											pfGfxDrawRectangle(64 - 1, 8 + i*32 - 1 , 184 + 1, 27 + i*32);
											pfPtmShowStopsButton(64,8 + i*32);
											rem_stops[i][0] = 64; 
											rem_stops[i][1] = 8+i*32; 
											rem_stops[i][2] = 183;
											rem_stops[i][3] = 27+i*32; 
										}
																			
										
										for(i=10*dest_page; i < (10*dest_page + var); i++)
										{													
											pfGfxDrawString(80, 8 + (i%10*dest_page)*32 + 2, print_stops[i], enFont_8X16, RED, WHITE);
										}

									}
								}
								selected_dest_stop = pfPtmRoutesGetDestStopsBlock();						
								
							}while(selected_dest_stop == 0xFF);
							
							if(selected_dest_stop != 0xFF)
								dest_dist = distance_value[selected_dest_stop];																							
												
							print_stop2 = enBooleanTrue;
							pfPtmDrawMenu();
											
							}
							break;
							
		case 2: 	pfILI9320FillRGB(WHITE);
							for(i=0; i < 10; i++)
							{
								pfGfxDrawRectangle(NumberBlockOrigins[i][0] - 1 ,NumberBlockOrigins[i][1] - 1,NumberBlockOrigins[i][2] + 1,NumberBlockOrigins[i][3]);
								pfPtmShowNumbersButton(NumberBlockOrigins[i][0],NumberBlockOrigins[i][1]);
								pfGfxDrawChar( (NumberBlockOrigins[i][0] + NumberBlockOrigins[i][2])/2 - 5 , (NumberBlockOrigins[i][1] + NumberBlockOrigins[i][3])/2 - 5, disp[i], enFont_8X16, RED, WHITE);
							}
							
							for(i=10; i<16; i++)
							{
								pfPtmShowNumbersButton(NumberBlockOrigins[i][0],NumberBlockOrigins[i][1]);
								pfGfxDrawRectangle(NumberBlockOrigins[i][0]-1,NumberBlockOrigins[i][1] - 1,NumberBlockOrigins[i][2]+1,NumberBlockOrigins[i][3]);
							}
							pfGfxDrawString(NumberBlockOrigins[10][0]+10,NumberBlockOrigins[10][1]+10, "10", enFont_8X16,RED, WHITE);
							pfGfxDrawString(NumberBlockOrigins[11][0]+10,NumberBlockOrigins[11][1]+10, "11", enFont_8X16,RED, WHITE);
							pfGfxDrawString(NumberBlockOrigins[12][0]+10,NumberBlockOrigins[12][1]+10, "12", enFont_8X16,RED, WHITE);
							pfGfxDrawString(NumberBlockOrigins[13][0]+10,NumberBlockOrigins[13][1]+10, "13", enFont_8X16,RED, WHITE);
							pfGfxDrawString(NumberBlockOrigins[14][0]+10,NumberBlockOrigins[14][1]+10, "14", enFont_8X16,RED, WHITE);
							pfGfxDrawString(NumberBlockOrigins[15][0]+10,NumberBlockOrigins[15][1]+10, "15", enFont_8X16,RED, WHITE);

							do
							{	
								selected_ad_no = pfPtmGetNumberBlock();
							}while(selected_ad_no == 0xFF);
							
							print_ad = enBooleanTrue;
							if(selected_ad_no > 9)
								num_grt_9 = enBooleanTrue;
							pfPtmDrawMenu();
							
							break;
							
		case 3:		pfILI9320FillRGB(WHITE);
							for(i=0; i < 10; i++)
							{
								pfGfxDrawRectangle(NumberBlockOrigins[i][0]-1,NumberBlockOrigins[i][1]-1,NumberBlockOrigins[i][2]+1,NumberBlockOrigins[i][3]);
								pfPtmShowNumbersButton(NumberBlockOrigins[i][0],NumberBlockOrigins[i][1]);
								pfGfxDrawChar( (NumberBlockOrigins[i][0] + NumberBlockOrigins[i][2])/2 - 5 , (NumberBlockOrigins[i][1] + NumberBlockOrigins[i][3])/2 - 5, disp[i], enFont_8X16, RED, WHITE);
							}
							
							for(i=10; i<16; i++)
							{
								pfGfxDrawRectangle(NumberBlockOrigins[i][0]-1,NumberBlockOrigins[i][1]-1,NumberBlockOrigins[i][2]+1,NumberBlockOrigins[i][3]);
								pfPtmShowNumbersButton(NumberBlockOrigins[i][0],NumberBlockOrigins[i][1]);
							}
														
							pfGfxDrawString(NumberBlockOrigins[10][0]+10,NumberBlockOrigins[10][1]+10, "10", enFont_8X16,RED, WHITE);
							pfGfxDrawString(NumberBlockOrigins[11][0]+10,NumberBlockOrigins[11][1]+10, "11", enFont_8X16,RED, WHITE);
							pfGfxDrawString(NumberBlockOrigins[12][0]+10,NumberBlockOrigins[12][1]+10, "12", enFont_8X16,RED, WHITE);
							pfGfxDrawString(NumberBlockOrigins[13][0]+10,NumberBlockOrigins[13][1]+10, "13", enFont_8X16,RED, WHITE);
							pfGfxDrawString(NumberBlockOrigins[14][0]+10,NumberBlockOrigins[14][1]+10, "14", enFont_8X16,RED, WHITE);
							pfGfxDrawString(NumberBlockOrigins[15][0]+10,NumberBlockOrigins[15][1]+10, "15", enFont_8X16,RED, WHITE);
						
							do
							{	
								selected_ch_no = pfPtmGetNumberBlock();
							}while(selected_ch_no == 0xFF);
							
							print_ch = enBooleanTrue;
							if(selected_ch_no > 9)
								num_grt_9 = enBooleanTrue;
							pfPtmDrawMenu();
							
							break;
	}
}

PFfloat total_distance = 0;

#define RECORD_FILE		"/PTM/RECORD.CSV"

void pfPtmCalculateTicket(void)
{	
	PFword final_amount = 0;
	
	total_distance = *dest_dist - *src_dist;
	final_amount = (PFword)(total_distance*selected_ad_no * cost_price  + selected_ch_no * total_distance * (cost_price/2));	
	pfItoA(final_amount,amt);
	pfFtoA(total_distance,dis);
	dis[4]='\0';
}

static PFword count = 0;

void pfPtmDisplayTicket(void)
{
	PFFsFile fp;
	PFdword bw;
	PFEnStatus file_status;
	PFRtcTime ref;
	PFbyte i;

	PFbyte time[9] = {0};
	PFbyte daat[9] = {0};
	PFbyte deb[41] = {0};
	PFdword putty;

	pfILI9320FillRGB(WHITE);

	pfGfxDrawRectangle(20,20,220,258);	//Outer Box
	pfGfxDrawString( 48, 30, "Phi Transport System", 0, 0x000000, 0xFFFFFF);// 

	pfRtcGetTime(&ref);
	sprintf(time,"%d:%d:%d",ref.rtcHour,ref.rtcMin,ref.rtcSec);
	sprintf(daat,"%d/%d/%d",ref.rtcDom,ref.rtcMon,ref.rtcYear);
	
	pfGfxDrawString( 148, 50, time, 0, 0x000000, 0xFFFFFF);	// Time
	pfGfxDrawString( 28, 50, daat, 0, 0x000000, 0xFFFFFF);	// Date
	
	if(src_page == 0 && print_stop1 == enBooleanTrue)
	{
		pfGfxDrawString( 78, 70, print_stops[selected_src_stop], 1, RED, 0xFFFFFF);		// From
		pfMemCopy(info, print_stops[selected_src_stop],12);
	}
	else if(src_page > 0 && src_page < (total_no_stops - 1)/10 && print_stop1 == enBooleanTrue)
	{
		pfGfxDrawString( 78, 70, print_stops[selected_src_stop], 1, RED, 0xFFFFFF);		// From
		pfMemCopy(info, print_stops[selected_src_stop],12);
	}
	else if(src_page == (total_no_stops-1)/10 && print_stop1 == enBooleanTrue)
	{
		pfGfxDrawString( 78, 70, print_stops[selected_src_stop], 1, RED, 0xFFFFFF);		// From
		pfMemCopy(info, print_stops[selected_src_stop],12);
	}
	info[12] = ',';
	
	pfGfxDrawString( 108, 90, "To", 1, RED, 0xFFFFFF);		// From
	
	if(dest_page == 0 && print_stop2 == enBooleanTrue)
	{
		pfGfxDrawString( 78, 108, print_stops[selected_dest_stop], 1, RED, 0xFFFFFF);		// To
		pfMemCopy(info+13, print_stops[selected_dest_stop],12);
	}
	else if(dest_page > 0 && dest_page < (total_no_stops - 1)/10 && print_stop2 == enBooleanTrue)
	{
		pfGfxDrawString( 78, 108, print_stops[selected_dest_stop], 1, RED, 0xFFFFFF);		// To
		pfMemCopy(info+13, print_stops[selected_dest_stop],12);
	}
	else if(dest_page == (total_no_stops-1)/10 && print_stop2 == enBooleanTrue)
	{
		pfGfxDrawString( 78, 108, print_stops[selected_dest_stop], 1, RED, 0xFFFFFF);		// To
		pfMemCopy(info+13, print_stops[selected_dest_stop],12);
	}
	info[25] = ',';
	
	pfGfxDrawString( 34, 138, "AD x", 1, BLACK, 0xFFFFFF);	// 10-16
	
	if(selected_ad_no > 9 && print_ad == enBooleanTrue)
	{	
		pfGfxDrawString( 74, 138, numbers[selected_ad_no%10], 1, RED, 0xFFFFFF);	// 10-16
		pfMemCopy(info+26, numbers[selected_ad_no%10],2);
		info[28] = ',';
	}
	else if(selected_ad_no < 10 && print_ad == enBooleanTrue)
	{
		pfGfxDrawChar( 74, 138, disp[selected_ad_no], 1, RED, 0xFFFFFF);	// 1-9
		info[26] = disp[selected_ad_no];
		info[27] = ',';
	}
		
	pfGfxDrawString( 34, 158, "CH x", 1, BLACK, 0xFFFFFF);	// 10-16
	if(selected_ch_no > 9 && print_ch == enBooleanTrue)
	{	
		pfGfxDrawString( 74, 158, numbers[selected_ch_no%10], 1, RED, 0xFFFFFF);	// 10-16
		pfMemCopy(info+28, numbers[selected_ch_no%10],2);
		info[30] = ',';
	}
	else if(selected_ch_no < 10 && print_ch == enBooleanTrue)
	{
		pfGfxDrawChar( 74, 158, disp[selected_ch_no], 1, RED, 0xFFFFFF);	// 1-9
		info[28] = disp[selected_ch_no];
		info[29] = ',';
	}
	
	pfGfxDrawString( 130, 158, "Dist:", 1, BLACK, 0xFFFFFF);	// 10-16
	pfGfxDrawString( 170, 158, dis, 1, RED, 0xFFFFFF);	// 10-16
	pfGfxDrawString( 202, 158, "km", 1, BLACK, 0xFFFFFF);	// 10-16
	for(i=0; i < 3; i++)
		info[30+i] = dis[i];	
	
		info[34] = ',';	
	
	pfGfxDrawString( 70, 180, "Total Fare:", 1, BLACK, 0xFFFFFF);	// 10-16
	pfGfxDrawString(160, 180, amt, 1, RED, 0xFFFFFF);	// 10-16
	pfGfxDrawString(192, 180, "/-", 1, BLACK, 0xFFFFFF);	// 10-16
	for(i=0; i < 3; i++)
		info[35+i] = amt[i];	
	
	info[38] = ',';
	info[39] = '\r';
	info[40] = '\n';
	pfGfxDrawString( 72, 204, "Happy", 2, BLACK, 0xFFFFFF);	// 10-16
	pfGfxDrawString( 50, 230, "Journey", 2, BLACK, 0xFFFFFF);	// 10-16
	
	//New Ticket Box
	pfGfxDrawRectangle(64 - 1,268 - 1,164 + 1,298);
	pfPtmShowNewTktButton(64,268);
	pfGfxDrawString(76, 274, "New Ticket", 1, BLACK, 0xFFFFFF);	// 10-16
	
	//Write into a file

	file_status = pfFsFileOpen(&fp, RECORD_FILE, FA_OPEN_ALWAYS | FA_WRITE);
	if(file_status == enStatusSuccess)
	{
		pfFsFileSeek(&fp,40*count);
		sprintf(deb, " Create status : %d", file_status);
		pfUart0WriteString(deb);

		file_status = pfFsFileWrite(&fp, info, 41, &bw);
		sprintf(deb, " Write status : %d Bw: %d\r", file_status, bw);
		pfUart0WriteString(deb);
		
		count++;
	}
	else
	{
		sprintf(deb, " Open status : %d \r", file_status);
		pfUart0WriteString(deb);
	}
	pfFsFileClose(&fp);

	
}
  
void pfPtmCleanup(void)
{
	pfILI9320SetAreaMax();
	pfILI9320FillRGB(WHITE);
	pfGfxDrawString(25, 130, "Phi Ticketing Machine", enFont_8X16, RED, WHITE);
	pfGfxDrawString(100, 152, "Loading...", enFont_8X8, BLUE, WHITE);
		
	pfTickDelayMs(1000);
	
	pfFsFileDelete(RECORD_FILE); //Delete the record file for New entry
}

void pfPtmShowFromButton(PFword x,PFword y);
void pfPtmShowAdButton(PFword x, PFword y);

void pfPtmDrawMenu(void)
{
	PFRtcTime timy;
	static PFbyte daz[9];
	static PFbyte date[9];

	pfILI9320FillRGB(0xFFFF);//set complete background colour

/******************   result display window printing  *******************/
	
	// Bus Transport Title

	pfGfxDrawString( 48, 15, "Phi Transport System", 0, 0x000000, 0xFFFFFF);// 
	
	// Date and time
	pfRtcGetTime(&timy);
	sprintf(daz,"%d:%d:%d",timy.rtcHour,timy.rtcMin,timy.rtcSec);
	sprintf(date,"%d/%d/%d",timy.rtcDom,timy.rtcMon,timy.rtcYear);
	
	pfGfxDrawString( 20, 33, daz, 0, 0x000000, 0xFFFFFF);// Time
	pfGfxDrawString( 160, 33, date, 0, 0x000000, 0xFFFFFF);// Date
	
	// Input information (From, To, Number of Adults and Childs)
	
	pfGfxDrawString( 20, 60, "FROM:", 1, 0x000000, 0xFFFFFF);	// From
	pfGfxDrawString( 20, 98, "TO:", 1, 0x000000, 0xFFFFFF);		// To
		
	pfGfxDrawRectangle(68 - 1,60 - 1 ,218 + 1,78);	//From Box 
	pfPtmShowFromButton(68,60);
	
	pfGfxDrawRectangle(52 - 1,98 -1,202 +1,116);	// To Box
	pfPtmShowFromButton(52,98);
	
	if(src_page == 0 && print_stop1 == enBooleanTrue)
	{
		pfGfxDrawString( 70, 62, print_stops[selected_src_stop], 1, RED, 0xFFFFFF);		// From
	}
	else if(src_page > 0 && src_page < (total_no_stops - 1)/10 && print_stop1 == enBooleanTrue)
	{
		pfGfxDrawString( 70, 62, print_stops[selected_src_stop], 1, RED, 0xFFFFFF);		// From
	}
	else if(src_page == (total_no_stops-1)/10 && print_stop1 == enBooleanTrue)
	{
		pfGfxDrawString( 70, 62, print_stops[selected_src_stop], 1, RED, 0xFFFFFF);		// From
	} 
	
	if(dest_page == 0 && print_stop2 == enBooleanTrue)
	{
		pfGfxDrawString( 54, 100, print_stops[selected_dest_stop], 1, RED, 0xFFFFFF);		// To
	}
	else if(dest_page > 0 && dest_page < (total_no_stops - 1)/10 && print_stop2 == enBooleanTrue)
	{
		pfGfxDrawString( 54, 100, print_stops[selected_dest_stop], 1, RED, 0xFFFFFF);		// To
	}
	else if(dest_page == (total_no_stops-1)/10 && print_stop2 == enBooleanTrue)
	{
		pfGfxDrawString( 54, 100, print_stops[selected_dest_stop], 1, RED, 0xFFFFFF);		// To
	}

	pfGfxDrawString( 20, 136, "ADULTS:", 1, 0x000000, 0xFFFFFF);	// AD
	pfGfxDrawString( 20, 174, "CHILDREN:", 1, 0x000000, 0xFFFFFF);		// CH
	
 	pfGfxDrawRectangle(84 - 1,136 - 1,118 + 1,154);	//AD Box 
	pfPtmShowAdButton(84,136);
	pfGfxDrawRectangle(100 - 1,174 - 1,134 +1,192);	// CH Box
	pfPtmShowAdButton(100,174);
		
	if(selected_ad_no > 9 && print_ad == enBooleanTrue)
	{
			pfGfxDrawString( 86, 138, numbers[selected_ad_no%10], 1, RED, 0xFFFFFF);	// 10-16
	}
	else if(selected_ad_no < 10 && print_ad == enBooleanTrue)
	{
			pfGfxDrawChar( 86, 138, disp[selected_ad_no], 1, RED, 0xFFFFFF);	// 1-9
	}
		
	if(selected_ch_no > 9 && print_ch == enBooleanTrue)
	{
			pfGfxDrawString( 102, 176, numbers[selected_ch_no%10], 1, RED, 0xFFFFFF);	// 10-16
	}
	else if(selected_ch_no < 10 && print_ch == enBooleanTrue)
	{
			pfGfxDrawChar( 102, 176, disp[selected_ch_no], 1, RED, 0xFFFFFF);	// 1-9
	}

}

PFbyte pfPtmRoutesGetTouchBlock(void)
{
	PFdword touchX, touchY;
	PFbyte i;
	
	pfGpioPinsSet(GPIO_PORT_0, GPIO_PIN_16);
	pfGpioPinsSet(TP_SPI_CS_PORT, TP_SPI_CS_PIN);
	if(pfTouchGetCoordinates(&touchX, &touchY))
	{
		for(i = 0; i < total_no_routes; i++)
		{
			if( (touchX > RouteBlkOrigins[i][0]) && (touchX < RouteBlkOrigins[i][2] ))
			{
				if( (touchY > RouteBlkOrigins[i][1]) && (touchY < RouteBlkOrigins[i][3]) )
				{
					return i;
				}
			}
		}
	}
	return 0xFF;
}

PFbyte pfPtmRoutesGetMenuBlock(void)
{
	PFdword touchX, touchY;
	PFbyte i;
	
	pfGpioPinsSet(GPIO_PORT_0, GPIO_PIN_16);
	pfGpioPinsSet(TP_SPI_CS_PORT, TP_SPI_CS_PIN);
	if(pfTouchGetCoordinates(&touchX, &touchY))
	{
		for(i = 0; i < 4; i++)
		{
				if( (touchX > MenuBlockOrigins[i][0]) && (touchX < MenuBlockOrigins[i][2] ))
				{
					if( (touchY > MenuBlockOrigins[i][1]) && (touchY < MenuBlockOrigins[i][3]) )
					{
						return i;
					}
				}
		}
	}
	return 0xFF;
}

PFbyte pfPtmGetTouchNextBlock(void)
{
	PFdword touchX, touchY;
	
	pfGpioPinsSet(GPIO_PORT_0, GPIO_PIN_16);
	pfGpioPinsSet(TP_SPI_CS_PORT, TP_SPI_CS_PIN);
	if(pfTouchGetCoordinates(&touchX, &touchY))
	{
			if( (touchX > Next_Page[0]) && (touchX < Next_Page[2] ))
			{			
				if( (touchY > Next_Page[1]) && (touchY < Next_Page[3]) )
				{
					return 1;
				}
			}	
	}
	return 0xFF;
}

PFbyte pfPtmRoutesGetSrcStopsBlock(void)
{

	PFdword touchX, touchY;
	PFbyte i;
		
	if(src_page == 0)
	{
	
		pfGpioPinsSet(GPIO_PORT_0, GPIO_PIN_16);
		pfGpioPinsSet(TP_SPI_CS_PORT, TP_SPI_CS_PIN);
		if(pfTouchGetCoordinates(&touchX, &touchY))
		{
			for(i = 0; i < total_no_stops; i++)
			{
				if( (touchX > stops_Blk[i][0]) && (touchX < stops_Blk[i][2] ))
				{
					if( (touchY > stops_Blk[i][1]) && (touchY < stops_Blk[i][3]) )
					{
						return i;
					}
				}
			}
		}
	}
	else if(src_page > 0 && src_page < (total_no_stops-1)/10)
	{	
		pfGpioPinsSet(GPIO_PORT_0, GPIO_PIN_16);
		pfGpioPinsSet(TP_SPI_CS_PORT, TP_SPI_CS_PIN);
		if(pfTouchGetCoordinates(&touchX, &touchY))
		{
		for(i = 0; i < 10; i++)
		{
				if( (touchX > mid_stops[i][0]) && (touchX < mid_stops[i][2] ))
				{
					if( (touchY > mid_stops[i][1]) && (touchY < mid_stops[i][3]) )
					{
						return (i + 10*src_page);
					}
				}
			}
		}
	}
	else if(src_page == (total_no_stops-1)/10)
	{
		pfGpioPinsSet(GPIO_PORT_0, GPIO_PIN_16);
		pfGpioPinsSet(TP_SPI_CS_PORT, TP_SPI_CS_PIN);
		if(pfTouchGetCoordinates(&touchX, &touchY))
		{
			for(i = 0; i < var; i++)
			{
				if( (touchX > rem_stops[i][0]) && (touchX < rem_stops[i][2] ))
				{
					if( (touchY > rem_stops[i][1]) && (touchY < rem_stops[i][3]) )
					{
						return (i + 10*src_page);
					}
				}
			}
		}
	}
	return 0xFF;	
}

PFbyte pfPtmRoutesGetDestStopsBlock(void)
{

	PFdword touchX, touchY;
	PFbyte i;
		
	if(dest_page == 0)
	{
	
		pfGpioPinsSet(GPIO_PORT_0, GPIO_PIN_16);
		pfGpioPinsSet(TP_SPI_CS_PORT, TP_SPI_CS_PIN);
		if(pfTouchGetCoordinates(&touchX, &touchY))
		{
			for(i = 0; i < total_no_stops; i++)
			{
				if( (touchX > stops_Blk[i][0]) && (touchX < stops_Blk[i][2] ))
				{
					if( (touchY > stops_Blk[i][1]) && (touchY < stops_Blk[i][3]) )
					{
						return i;
					}
				}
			}
		}
	}
	else if(dest_page > 0 && dest_page < (total_no_stops-1)/10)
	{	
		pfGpioPinsSet(GPIO_PORT_0, GPIO_PIN_16);
		pfGpioPinsSet(TP_SPI_CS_PORT, TP_SPI_CS_PIN);
		if(pfTouchGetCoordinates(&touchX, &touchY))
		{
		for(i = 0; i < 10; i++)
		{
				if( (touchX > mid_stops[i][0]) && (touchX < mid_stops[i][2] ))
				{
					if( (touchY > mid_stops[i][1]) && (touchY < mid_stops[i][3]) )
					{
						return (i + 10*dest_page);
					}
				}
			}
		}
	}
	else if(dest_page == (total_no_stops-1)/10)
	{
		pfGpioPinsSet(GPIO_PORT_0, GPIO_PIN_16);
		pfGpioPinsSet(TP_SPI_CS_PORT, TP_SPI_CS_PIN);
		if(pfTouchGetCoordinates(&touchX, &touchY))
		{
			for(i = 0; i < var; i++)
			{
				if( (touchX > rem_stops[i][0]) && (touchX < rem_stops[i][2] ))
				{
					if( (touchY > rem_stops[i][1]) && (touchY < rem_stops[i][3]) )
					{
						return (i + 10*dest_page);
					}
				}
			}
		}
	}
	return 0xFF;	
}

PFbyte pfPtmGetNumberBlock(void)
{
	PFdword touchX, touchY;
	PFbyte i;
	
	pfGpioPinsSet(GPIO_PORT_0, GPIO_PIN_16);
	pfGpioPinsSet(TP_SPI_CS_PORT, TP_SPI_CS_PIN);		
	if(pfTouchGetCoordinates(&touchX, &touchY))
	{
		for(i = 0; i < 16; i++)
		{
			if( (touchX > NumberBlockOrigins[i][0]) && (touchX < NumberBlockOrigins[i][2] ))
			{
				if( (touchY > NumberBlockOrigins[i][1]) && (touchY < NumberBlockOrigins[i][3]) )
				{
					return i;
				}
			}
		}
	}
	return 0xFF;
}

PFbyte pfPtmGetConfirmTouch(void)
{
	PFdword touchX, touchY;
	
	pfGpioPinsSet(GPIO_PORT_0, GPIO_PIN_16);
	pfGpioPinsSet(TP_SPI_CS_PORT, TP_SPI_CS_PIN);		
	if(pfTouchGetCoordinates(&touchX, &touchY))
	{
		if( (touchX > confirm_blk[0]) && (touchX < confirm_blk[2] ))
		{
			if( (touchY > confirm_blk[1]) && (touchY < confirm_blk[3]) )
			{
				return 1;
			}
		}
	}
	return 0xFF;
}

PFbyte pfPtmGetNextTicket(void)
{
	PFdword touchX, touchY;

	pfGpioPinsSet(GPIO_PORT_0, GPIO_PIN_16);
	pfGpioPinsSet(TP_SPI_CS_PORT, TP_SPI_CS_PIN);		
	if(pfTouchGetCoordinates(&touchX, &touchY))
	{
		if( (touchX > New_Ticket_blk[0]) && (touchX < New_Ticket_blk[2] ))
		{
			if( (touchY > New_Ticket_blk[1]) && (touchY < New_Ticket_blk[3]) )
			{
				return 1;
			}
		}
	}
	return 0xFF;
}

void pfShowConfirmButton(void)
{
	PFdword logoWidth, logoHeight, logoLoop;
	
	logoWidth = bitmap_file_confirm[0];
	logoHeight = bitmap_file_confirm[1];
	
	pfILI9320SetAreaMax();
	//pfILI9320FillRGB(WHITE);
	
	pfILI9320SetWindow(52, 215 , 52 + logoWidth, 215 + logoHeight);
	pfILI9320SetCursor(52, 215);
	pfILI9320WriteCmd(0x0022);
	for(logoLoop = 0; logoLoop < (logoWidth * logoHeight); logoLoop++)
	{
		pfILI9320WriteData(bitmap_file_confirm[logoLoop + 2]);
	}
	pfILI9320SetAreaMax();
}


void pfShowRoutesButton(PFdword x, PFdword y)
{
	PFdword logoWidth, logoHeight, logoLoop;
	
	logoWidth = bitmap_file_routes[0];
	logoHeight = bitmap_file_routes[1];
	
	pfILI9320SetAreaMax();
	
	pfILI9320SetWindow(x, y , x + logoWidth, y + logoHeight);
	pfILI9320SetCursor(x, y);
	pfILI9320WriteCmd(0x0022);
	for(logoLoop = 0; logoLoop < (logoWidth * logoHeight); logoLoop++)
	{
		pfILI9320WriteData(bitmap_file_routes[logoLoop + 2]);
	}
	pfILI9320SetAreaMax();
}

void pfPtmShowFromButton(PFword x,PFword y)
{
	PFdword logoWidth, logoHeight, logoLoop;
	
	logoWidth = bitmap_file_from[0];
	logoHeight = bitmap_file_from[1];
	
	pfILI9320SetAreaMax();
	
	pfILI9320SetWindow(x, y , x + logoWidth, y + logoHeight);
	pfILI9320SetCursor(x, y);
	pfILI9320WriteCmd(0x0022);
	for(logoLoop = 0; logoLoop < (logoWidth * logoHeight); logoLoop++)
	{
		pfILI9320WriteData(bitmap_file_from[logoLoop + 2]);
	}
	pfILI9320SetAreaMax();
}

void pfPtmShowAdButton(PFword x, PFword y)
{
	PFdword logoWidth, logoHeight, logoLoop;
	
	logoWidth = bitmap_file_adch[0];
	logoHeight = bitmap_file_adch[1];
	
	pfILI9320SetAreaMax();
	
	pfILI9320SetWindow(x, y , x + logoWidth, y + logoHeight);
	pfILI9320SetCursor(x, y);
	pfILI9320WriteCmd(0x0022);
	for(logoLoop = 0; logoLoop < (logoWidth * logoHeight); logoLoop++)
	{
		pfILI9320WriteData(bitmap_file_adch[logoLoop + 2]);
	}
	pfILI9320SetAreaMax();
}

void pfPtmShowStopsButton(PFword x, PFword y)
{
	PFdword logoWidth, logoHeight, logoLoop;
	
	logoWidth = bitmap_file_stops[0];
	logoHeight = bitmap_file_stops[1];
	
	pfILI9320SetAreaMax();
	
	pfILI9320SetWindow(x, y , x + logoWidth, y + logoHeight);
	pfILI9320SetCursor(x, y);
	pfILI9320WriteCmd(0x0022);
	for(logoLoop = 0; logoLoop < (logoWidth * logoHeight); logoLoop++)
	{
		pfILI9320WriteData(bitmap_file_stops[logoLoop + 2]);
	}
	pfILI9320SetAreaMax();
}
void pfPtmShowNumbersButton(PFword x, PFword y)
{	
	PFdword logoWidth, logoHeight, logoLoop;
	
	logoWidth = bitmap_file_numbers[0];
	logoHeight = bitmap_file_numbers[1];
	
	pfILI9320SetAreaMax();
	
	pfILI9320SetWindow(x, y , x + logoWidth, y + logoHeight);
	pfILI9320SetCursor(x, y);
	pfILI9320WriteCmd(0x0022);
	for(logoLoop = 0; logoLoop < (logoWidth * logoHeight); logoLoop++)
	{
		pfILI9320WriteData(bitmap_file_numbers[logoLoop + 2]);
	}
	pfILI9320SetAreaMax();	
}

void pfPtmShowNewTktButton(PFword x, PFword y)
{
	PFdword logoWidth, logoHeight, logoLoop;
	
	logoWidth = bitmap_file_new_Tkt[0];
	logoHeight = bitmap_file_new_Tkt[1];
	
	pfILI9320SetAreaMax();
	
	pfILI9320SetWindow(x, y , x + logoWidth, y + logoHeight);
	pfILI9320SetCursor(x, y);
	pfILI9320WriteCmd(0x0022);
	for(logoLoop = 0; logoLoop < (logoWidth * logoHeight); logoLoop++)
	{
		pfILI9320WriteData(bitmap_file_new_Tkt[logoLoop + 2]);
	}
	pfILI9320SetAreaMax();	
}

