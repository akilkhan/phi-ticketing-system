#include "prime_framework.h"
#include "prime_sysclk.h"
#include "prime_gpio.h"
#include "prime_uart0.h"
#include "prime_spi0.h"
#include "prime_lcd.h"
#include "prime_graphics.h"
#include "prime_mmc.h"
#include "prime_diskio.h"
#include "prime_fatfs.h"
#include "prime_i2c0.h"
#include "prime_i2s.h"
#include "prime_touch.h"
#include "logo.h"
#include "hydraConfig.h"


#define FBUFF_SIZE		128
#define RXFIFO_EMPTY		0
#define TXFIFO_FULL			8
#define FILE_BUFF_SIZE		1023
#define FILE1_NAME	"/HYDRAC~1.c"
//#define FILE1_NAME	"/main.c"
PFbyte buff[FILE_BUFF_SIZE] = {0};
//static PFbyte fBuff[FBUFF_SIZE] = {0};
static PFbyte cwd[128] = "/";

// volatile PFdword I2SReadLength = 0;
// volatile PFdword I2SWriteLength = 0;
// PFEnI2sMode modeinit = enI2sModeTx;
// volatile PFdword touchX = 0xFFFF, touchY = 0xFFFF, prevX = 0xFFFF, prevY = 0xFFFF;
void filopDisplayList(void);
void showLogo(void);
int main(void)
{
	PFEnStatus status;
	PFbyte mmcId;
	PFFsFile file;
	PFEnStatus stat;
	PFFsDir dir;
	PFFsFileInfo fileInfo;
	PFbyte* fn;
	PFdword tempm, bytesWritten, bytesRead; 
//	PFbyte ch, ind;
	
	PFbyte	prBuff[64] = {0};
	PFdword prLen=0;
	PFdword mOpt;	// bytesWritten, bytesRead
	boardInit();    //Board Initialization);
	pfUart0Write("\r\nInitok",10);
	pfILI9320FillRGB(GREEN);	
	pfILI9320DrawPixel(10,10,BLUE);
	showLogo();
/*****************File Operation start ******************************/
//filopDisplayList();

// status = pfFsFileOpen(&file, FILE1_NAME, FA_READ);
// 	if(status != enStatusSuccess)				
// 	{
// 		prLen = sprintf(prBuff, "\r\nErr: File open [%d]", status);
// 		pfUart0Write(prBuff, prLen);
// 		while(1);
// 	}
// 	pfUart0Write("File Opened" ,11);
// 	
// 	prLen = sprintf(prBuff, "\r\nreading file...\r\n");
// 	pfUart0Write(prBuff, prLen);
// 		

// for(mOpt = 0; mOpt < 20; mOpt++)
// 	{
// 		status = pfFsFileRead(&file, buff, FILE_BUFF_SIZE, &bytesRead);
// 		pfUart0Write(buff, bytesRead);
// 			
// 			if(status != enStatusSuccess)
// 		{
// 			prLen = sprintf(prBuff, "Err: File Read [%d]", status);
// 			pfUart0Write(prBuff, prLen);
// 			while(1);
// 		}	
// 	}

// 	status = pfFsFileOpen(&file, FILE2_NAME, FA_READ);
// 	if(status != enStatusSuccess)				
// 	{
// 		prLen = sprintf(prBuff, "\r\nErr: File open [%d]", status);
// 		pfUart0Write(prBuff, prLen);
// 		while(1);
// 	}
// 	
// 	pfUart0Write("File Opened" ,11);
// 		status = pfFsFileRead(&file, buff, 1024, &bytesRead);
// 		pfUart0Write(buff, bytesRead);
// 			
// 			if(status != enStatusSuccess)
// 		{
// 			prLen = sprintf(prBuff, "Err: File Read [%d]", status);
// 			pfUart0Write(prBuff, prLen);
// 			while(1);
// 		}
	
// 	stat = pfFsDirOpen(&dir, cwd);
// 	if(stat != enStatusSuccess)
// 	{
// 		pfUart0WriteString(pfGetErrorString(stat));
// 		return;
// 	}
	
	
	
/*************** While(1)************************************************/	
	//pfUart0Write("\r\nsending data to audio codec",31);
     while (1);
  //{

   //for(tempm=0;tempm<475990;tempm++)
  //  {
	
	//   pfI2SGetFifoLevel(modeinit,&curLevel);
		//while(((I2S_CHANNEL->I2SSTATE>>16)&0x0f)>6);
		//while(pfI2SGetFifoLevel(enI2sModeTx)>6);
		//data=(audio[tempm]);

        //   pfI2sWrite((PFbyte *)audio,5446656);  
		     
	//	I2S_CHANNEL->I2STXFIFO = (audio[tempm]);
//     	//while(curLevel>6);	//Wait for the buffer becomes atleast 2 bytes empty
//     	if(tempm<200000)
// 		{
// 			dataRight=((audio[tempm])<<6);
// 			I2S_CHANNEL->I2STXFIFO =  dataRight;  
//     		//pfI2sWrite(&dataRight,1);   //Right Channel
// 		}
//     	else
// 		{
// 			dataLeft=((audio[tempm])<<22);
// 			I2S_CHANNEL->I2STXFIFO = dataLeft;
//     		//pfI2sWrite(&dataLeft,1);  //Left Channel
// 		}
  // }
    
 // }
 	
}



void filopDisplayList(void)
{
	PFEnStatus stat;
	PFFsDir dir;
	PFFsFileInfo fileInfo;
	PFbyte* fn;
#if (PF_FS_USE_LFN != 0)	
	PFbyte fname[PF_FS_MAX_LFN + 1] = {0};
	fileInfo.lfname = fname;
	fileInfo.lfsize = PF_FS_MAX_LFN + 1;
#endif
		
	stat = pfFsDirOpen(&dir, cwd);
	if(stat != enStatusSuccess)
	{
		pfUart0WriteString(pfGetErrorString(stat));
		return;
	}
	
	while(1)
	{
		stat = pfFsDirReadEntry(&dir, &fileInfo);
		if(stat != enStatusSuccess)
		{
			pfUart0WriteString(pfGetErrorString(stat));
			return;
		}
		if(fileInfo.fname[0] == 0)
		{
			pfUart0WriteString("\r\n---");
			return;
		}
		
		if(fileInfo.fname[0] != '.')
		{
		
		#if (PF_FS_USE_LFN != 0)
			fn = *fileInfo.lfname ? fileInfo.lfname : fileInfo.fname;
		#else
            fn = fileInfo.fname;
		#endif
			pfUart0WriteString("\r\n");
			pfUart0WriteString(fn);
		}	
	}
}


void showLogo(void)
{
	PFdword logoWidth, logoHeight, logoLoop;
	
	logoWidth = bitmap_file[0];
	logoHeight = bitmap_file[1];
	
	pfILI9320SetAreaMax();
	pfILI9320FillRGB(WHITE);
	
	pfILI9320SetWindow(120-(logoWidth/2), 160-(logoHeight/2) , 119+(logoWidth/2), 159+(logoHeight/2));
	pfILI9320SetCursor(120-(logoWidth/2), 160-(logoHeight/2));
	pfILI9320WriteCmd(0x0022);
	for(logoLoop = 0; logoLoop < (logoWidth * logoHeight); logoLoop++)
	{
		pfILI9320WriteData(bitmap_file[logoLoop + 2]);
	}
	pfILI9320SetAreaMax();
	//pfGfxDrawString(40, 280, "www.phi-robotics.com", enFont_8X16, BLACK, WHITE);
}


PFEnStatus pfCreateBorder(void)
{ 
	pfGfxDrawLine(0,0,240,0);
	pfGfxDrawLine(240,0,240,320);
	pfGfxDrawLine(240,320,0,320);
	pfGfxDrawLine(0,320,0,0);
	return  enStatusSuccess;
			
}
