#include "prime_framework.h"
#include "prime_types.h"
#include "prime_gpio.h"
#include "prime_lcd.h"
#include "prime_graphics.h"
#include "prime_mmc.h"
#include "prime_diskio.h"
#include "prime_fatfs.h"
#include "prime_touch.h"
#include "bitmap.h"


#define TTC_KEY_DEBOUNCE		200
#define TTC_BLOCK_ORIGIN_X		1
#define TTC_BLOCK_ORIGIN_Y		1
#define TTC_BLOCK_COUNT			9
#define TTC_MARGIN_SIZE			30
#define TTC_GRID_SIZE			80
#define TTC_SIDE_MARGINE		15
#define TTC_BLOCK_SIZE			70
#define TTC_OBJECT_SIZE			(TTC_BLOCK_SIZE - TTC_MARGIN_SIZE)
#define TTC_GRID_IMAGE			"/AppData/Games/TTC/gridPart.bmp"
#define TTC_CROSS_IMAGE			"/AppData/Games/TTC/cross.bmp"
#define TTC_CIRCLE_IMAGE		"/AppData/Games/TTC/circle.bmp"


typedef enum
{
	enGameObjectNone = 0,
	enGameObjectError,
	enGameObjectNotice,
	enGameObjectWin,
	enGameObjectSelect,
	enGameObjectCross,
	enGameObjectCircle
}PFEnGameObject;

static PFword blockOrigines[10][2] = 
{
	{5, 5},				// block 1, 1
	{85, 5},			// block 1, 2
	{165, 5},			// block 1, 3
	{5, 85},			// block 2, 1
	{85, 85},			// block 2, 2
	{165, 85},			// block 2, 3
	{5, 165},			// block 3, 1
	{85, 165},			// block 3, 2
	{165, 165},			// block 3, 3
	{135, 245}			// Notification block
};



static PFbyte gameWinState = 0;
static PFbyte gameBlock = 0, blockCount = 0;
static PFbyte ttcLoop;
static PFbyte crossSuccessCount[8] = {0}, circleSuccessCount[8] = {0};

PFword crossImage[TTC_OBJECT_SIZE * TTC_OBJECT_SIZE] = {0};
PFword circleImage[TTC_OBJECT_SIZE * TTC_OBJECT_SIZE] = {0};
PFword gridPartImage[TTC_GRID_SIZE * TTC_GRID_SIZE] = {0};
static PFword* ttcImagePtr[2] = {crossImage, circleImage};
static PFEnGameObject objectInBlock[TTC_BLOCK_COUNT] = {enGameObjectNone};



static PFEnGameObject gamePlayTurn = enGameObjectCross;
static PFword blockBgClr = 0, blockSelectClr = 0, blockErrClr = 0;


void ttcCleanup(void)
{
	pfILI9320SetAreaMax();
	pfILI9320FillRGB(WHITE);
	pfGfxDrawString(32, 130, "Tic-Tac-Toe", enFont_24X16, BLACK, WHITE);
	pfGfxDrawString(32, 156, "Loading...", enFont_8X16, BLUE, WHITE);
	
	// Initialize all variables with default values
	gameWinState = 0;
	blockCount = 0;
	gamePlayTurn = enGameObjectCross;
	gameBlock = 0;
	for(ttcLoop = 0; ttcLoop < 8; ttcLoop++)
	{
		crossSuccessCount[ttcLoop] = 0;
		circleSuccessCount[ttcLoop] = 0;
	}
	for(ttcLoop = 0; ttcLoop < TTC_BLOCK_COUNT; ttcLoop++)
	{
		objectInBlock[ttcLoop] = enGameObjectNone;
	}
	
	// Initialize colour variables
	blockBgClr = bmpPickColor(TTC_GRID_IMAGE, (TTC_MARGIN_SIZE/2) + 1, (TTC_MARGIN_SIZE/2) + 1);
	blockSelectClr = 0xFFFF - blockBgClr;
	blockErrClr =  (blockBgClr + blockSelectClr) / 2;
	
	// Draw graphics
	
	bmpLoadImage(TTC_GRID_IMAGE, gridPartImage);
	bmpLoadImage(TTC_CROSS_IMAGE, crossImage);
	bmpLoadImage(TTC_CIRCLE_IMAGE, circleImage);

	ttcDrawGrid();
	//bmpDrawImage(TTC_GRID_IMAGE, 1, 1);
	//gameDrawObject(gameBlock, enGameObjectSelect);
	pfGfxSetColor(BLUE);
	pfGfxDrawString(15, 270, "Player Turn", FONT_8X16, BLACK, WHITE);
	ttcFillArea(blockOrigines[9][0]+10, blockOrigines[9][1]+10, TTC_BLOCK_SIZE-20, blockBgClr);
	gameDrawObject(9, gamePlayTurn);
}

void gameDrawObject(PFbyte block, PFEnGameObject object)
{
	PFword objectX, objectY;
	objectX = blockOrigines[block][0];
	objectY = blockOrigines[block][1];
	lcdSetAreaMax();
	switch(object)
	{
		case enGameObjectNone:
			ttcFillArea(objectX, objectY, TTC_BLOCK_SIZE, blockBgClr);
			if(objectInBlock[block] != enGameObjectNone)
			{
				//bmpDrawImage(ttcImagePtr[objectInBlock[block]-enGameObjectCross], objectX + (TTC_MARGIN_SIZE/2), objectY + (TTC_MARGIN_SIZE/2));
				TTC_DRAW_PREV_OBJECT(TTC_OBJECT_COORDINATE(objectX), TTC_OBJECT_COORDINATE(objectY));
			}
			
			break;
		
		case enGameObjectError:
			ttcFillArea(objectX, objectY, TTC_BLOCK_SIZE, RED);
			//bmpDrawImage(ttcImagePtr[objectInBlock[block]-enGameObjectCross], objectX + (TTC_MARGIN_SIZE/2), objectY + (TTC_MARGIN_SIZE/2));
			TTC_DRAW_PREV_OBJECT(TTC_OBJECT_COORDINATE(objectX), TTC_OBJECT_COORDINATE(objectY));
			pfTickDelayMs(200);
			ttcFillArea(objectX, objectY, TTC_BLOCK_SIZE, blockBgClr);
			//bmpDrawImage(ttcImagePtr[objectInBlock[block]-enGameObjectCross], objectX + (TTC_MARGIN_SIZE/2), objectY + (TTC_MARGIN_SIZE/2));
			TTC_DRAW_PREV_OBJECT(TTC_OBJECT_COORDINATE(objectX), TTC_OBJECT_COORDINATE(objectY));
			pfTickDelayMs(200);
			ttcFillArea(objectX, objectY, TTC_BLOCK_SIZE, RED);
			//bmpDrawImage(ttcImagePtr[objectInBlock[block]-enGameObjectCross], objectX + (TTC_MARGIN_SIZE/2), objectY + (TTC_MARGIN_SIZE/2));
			TTC_DRAW_PREV_OBJECT(TTC_OBJECT_COORDINATE(objectX), TTC_OBJECT_COORDINATE(objectY));
			pfTickDelayMs(200);
			ttcFillArea(objectX, objectY, TTC_BLOCK_SIZE, blockBgClr);
			//bmpDrawImage(ttcImagePtr[objectInBlock[block]-enGameObjectCross], objectX + (TTC_MARGIN_SIZE/2), objectY + (TTC_MARGIN_SIZE/2));
			TTC_DRAW_PREV_OBJECT(TTC_OBJECT_COORDINATE(objectX), TTC_OBJECT_COORDINATE(objectY));
			//pfTickDelayMs(200);
			//ttcFillArea(objectX, objectY, TTC_BLOCK_SIZE, blockSelectClr);
			//bmpDrawImage(ttcImagePtr[objectInBlock[block]-enGameObjectCross], objectX + (TTC_MARGIN_SIZE/2), objectY + (TTC_MARGIN_SIZE/2));
			break;
		
		case enGameObjectNotice:
			break;
		
		case enGameObjectWin:
			ttcFillArea(objectX, objectY, TTC_BLOCK_SIZE, GREEN);
			break;
		
		case enGameObjectSelect:
			ttcFillArea(objectX, objectY, TTC_BLOCK_SIZE, blockSelectClr);
			if(objectInBlock[block] != enGameObjectNone)
			{
				//bmpDrawImage(ttcImagePtr[objectInBlock[block]-enGameObjectCross], objectX + (TTC_MARGIN_SIZE/2), objectY + (TTC_MARGIN_SIZE/2));
				TTC_DRAW_PREV_OBJECT(TTC_OBJECT_COORDINATE(objectX), TTC_OBJECT_COORDINATE(objectY));
			}
			else
			{
				ttcFillArea(TTC_OBJECT_COORDINATE(objectX), TTC_OBJECT_COORDINATE(objectY), (TTC_BLOCK_SIZE - TTC_MARGIN_SIZE), blockBgClr);
			}
			break;
		
		case enGameObjectCross:
			//bmpDrawImage(TTC_CROSS_IMAGE, objectX + (TTC_MARGIN_SIZE/2), objectY + (TTC_MARGIN_SIZE/2));
			TTC_DRAW_CROSS(TTC_OBJECT_COORDINATE(objectX), TTC_OBJECT_COORDINATE(objectY));
			break;
		
		case enGameObjectCircle:
			//bmpDrawImage(TTC_CIRCLE_IMAGE, objectX + (TTC_MARGIN_SIZE/2), objectY + (TTC_MARGIN_SIZE/2));
			TTC_DRAW_CIRCLE(TTC_OBJECT_COORDINATE(objectX), TTC_OBJECT_COORDINATE(objectY));
			break;
	}
}