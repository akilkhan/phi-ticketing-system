#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_gpio.h"
#include "prime_tick.h"
#include "prime_rit.h"
#include "prime_lcd.h"
#include "prime_spi0.h"
#include "prime_touch.h"
#include "boardConfig.h"

#define PORTRAIT							0
#define LANDSCAPE							1

#define MarginX								2128
#define MarginY								2240
#define TouchMaxX							4048
#define TouchMaxY							3984

#define DispMaxX							240
#define DispMaxY							320

#define PREC_LOW							5
#define PREC_MEDIUM							15
#define PREC_HI								30
#define PREC_EXTREME						50

#define TOUCH_READ_ENABLE					0x80
#define TOUCH_READ_X						0x50			
#define TOUCH_READ_Y						0x10
#define TOUCH_READ_Z1						0x30
#define TOUCH_READ_Z2						0x40
#define TOUCH_SINGLE_ENDED					0x04
#define TOUCH_DIFFERENTIAL					0x00
#define TOUCH_PREC_8_BIT					0x00
#define TOUCH_PREC_12_BIT					0x08
#define TOUCH_PWR_DOWN_ENABLE				0x00
#define TOUCH_PWR_DOWN_DISABLE				0x03

#define TOUCH_APP_MODE_DRAWING				0x00
#define TOUCH_APP_MODE_GUI					0x01

#define TOUCH_PRESSURE_THRESHOLD			2750

#define touchGetX()			pfTouchSendCommand(TOUCH_READ_X)
#define touchGetY()			pfTouchSendCommand(TOUCH_READ_Y)
#define touchGetZ1()		pfTouchSendCommand(TOUCH_READ_Z1)
#define touchGetZ2()		pfTouchSendCommand(TOUCH_READ_Z2)

#define median(pArray, sizeOfArray) pfkth_element(pArray ,sizeOfArray, (((sizeOfArray)&1)?((sizeOfArray)/2):(((sizeOfArray)/2)-1)))

#define touchGetCmdPara()	(tParameter.power_down | tParameter.conversion_width|tParameter.power_down )

#define ELEM_SWAP(a,b) { swapTemp =(a); (a) = (b); (b) = swapTemp; }
	
/** Touch Panel settings structure */
typedef struct{
	PFdword orientation;
	PFdword power_down;
	PFdword conversion_mode;
	PFdword conversion_width;
	PFdword pressure_threshold;
	PFdword precision;
	PFdword max_x;
	PFdword max_y;
	PFdword margin_x;
	PFdword margin_y;
	PFdword appllication_mode;
}PFTouchPanelSettings, *pTouchSettings;

static PFdword TP_X = 0, TP_Y = 0, TP_Z = 0;
static PFdword xArray[100]={0};
static PFdword yArray[100]={0};
static PFdword zArray[100]={0};
static PFbyte touchSpiId;

volatile PFTouchPanelSettings tParameter;
static PFword touchInitFlag=0;
static PFGpioPortPin touchSpiCs;

PFdword pfTouchSendCommand(PFbyte coordinate);
PFdword pfTouchFilter(PFdword *array, PFdword numberOfData);
PFdword pfTouchMapX(PFdword touch_val);
PFdword pfTouchMapY(PFdword touch_val);
PFdword pfkth_element(PFdword* inputArray, PFdword size, PFdword k);

static PFpCfgTouch tpconfig= {0};

PFEnStatus pfTouchOpen(PFpCfgTouch touchConfig)
{
	#if(PF_TOUCH_DEBUG==1)
	if(touchConfig == NULL)
		return enStatusNotConfigured;
    #endif
	pfGpioPinsSet(touchConfig->gpioTouchCsGpio.port, touchConfig->gpioTouchCsGpio.pin);
	
	if((touchConfig->spiRegisterDevice(&touchSpiId ,&touchConfig->gpioTouchCsGpio)) != enStatusSuccess)
	{
		return enStatusError;
	}
     
	touchSpiCs =touchConfig-> gpioTouchCsGpio;
	
	if((touchConfig->spiChipSelect(&touchSpiId, 1)) != enStatusSuccess)
		return enStatusError;
	// Set touch panel parameters
	tParameter.orientation = PORTRAIT;
	tParameter.power_down = TOUCH_PWR_DOWN_ENABLE;
	if(touchConfig->refsel == enTouchReferenceSelect_Single)
	{
		tParameter.conversion_mode = 0x00;
	}
	else
	{
		tParameter.conversion_mode = 0x04;
    }	
	if(tParameter.conversion_width == enTouchPrecision_12bit)
	{
	tParameter.conversion_width = enTouchPrecision_12bit;
	}
	else
	{		
	tParameter.conversion_width =enTouchPrecision_8bit; 
	}
	tParameter.pressure_threshold = TOUCH_PRESSURE_THRESHOLD;
	tParameter.precision = PREC_LOW;
	tParameter.max_x = DispMaxX;
	tParameter.max_y = DispMaxY;
	tParameter.margin_x = MarginX;
	tParameter.margin_y = MarginY;
	tParameter.appllication_mode = TOUCH_APP_MODE_DRAWING;
	
	pfMemCopy(&tpconfig,&touchConfig,(sizeof(PFpCfgTouch)));
	touchInitFlag=1;
	return enStatusSuccess;
}

PFdword pfTouchSendCommand(PFbyte coordinate)
{
	PFbyte pos[2] = {0};
	PFbyte cmd = 0;
	PFdword pos_val = 0, readBytes = 0;
	
	cmd = coordinate | TOUCH_READ_ENABLE| touchGetCmdPara();
	//cmd &= 0xF0;
	if(tpconfig->spiChipSelect(&touchSpiId, 0) != enStatusSuccess)
		return enStatusError;

	tpconfig->spiWrite(&touchSpiId, &cmd, 1);
	pfTickDelayMs(10);
	tpconfig->spiRead(&touchSpiId, pos, 2, &readBytes);
	
	tpconfig->spiChipSelect(&touchSpiId, 1);

	pos_val = ( (pos[0]<<4) |(pos[1]>>8) );
//	pos_val = pos[0];
	return pos_val;
}

PFEnStatus pfTouchGetCoordinates(PFdword* x_pos, PFdword* y_pos)
{
	
	PFdword loop, nVal, dispX, dispY;
	PFdword p1 = 0, p2 = 0, tPressure = 0;
	
	nVal = tParameter.precision / 2;
	#if(PF_TOUCH_DEBUG==1)
	if(touchInitFlag==0)
		return enStatusNotConfigured;
    #endif

	for (loop = 0; loop <= tParameter.precision; loop++)
	{
		xArray[loop] = touchGetX();
		yArray[loop] = touchGetY();
		p2 = touchGetZ2();
		p1 = touchGetZ1();
		zArray[loop] = xArray[loop] * ((p2 / p1) - 1);
	}
	// Filter X, Y and Z coordinate data	
	TP_Z = pfTouchFilter(&(zArray[nVal]), tParameter.precision);
	if (TP_Z && (TP_Z > tParameter.pressure_threshold))												// No valid touch obtained
	{
		return 0;
	}
	
	TP_X = pfTouchFilter(&(xArray[nVal]), tParameter.precision);
	TP_Y = pfTouchFilter(&(yArray[nVal]), tParameter.precision);
	
	// Clip X and Y coordinates to its max and min limits
	if(TP_X > TouchMaxX)
		TP_X = TouchMaxX;
	if(TP_X < MarginX)	
		TP_X = MarginX;
		
	if(TP_Y > TouchMaxY)
		TP_Y = TouchMaxY;
	if(TP_Y < MarginY)	
		TP_Y = MarginY;

	// Map touch panel point according to display dimensions
	if (tParameter.orientation == PORTRAIT)
	{
		dispX = pfTouchMapX(TP_X);
		dispY = pfTouchMapY(TP_Y);
	}
	else
	{
		dispX = pfTouchMapY(TP_Y);
		dispY = pfTouchMapX(TP_X);
	}
	
	if(dispX > DispMaxX)
		dispX = DispMaxX;		
	if(dispY > DispMaxY)
		dispY = DispMaxY;
	
	*x_pos = dispX;
	*y_pos = dispY;
	
	return 1;
}

PFdword pfTouchFilter(PFdword *array, PFdword numberOfData)
{
	return median(array, numberOfData);
}


PFEnStatus pfTouchDataAvailable(PFEnBoolean *data)
{
	#if(PF_TOUCH_DEBUG==1)
	if(touchInitFlag==0)
		return enStatusNotConfigured;
    #endif
	if((pfGpioPortRead(tpconfig->gpioTouchIntPin.port) &tpconfig->gpioTouchIntPin.pin) == 0 )
	{
		*data = enBooleanTrue; 
	}	
	else
	{
		*data = enBooleanFalse;
	}
	return enStatusSuccess;
}

PFdword pfTouchMapX(PFdword touch_val)
{
	PFdword disp_val;
  	
	disp_val = ( (touch_val - MarginX) * DispMaxX ) / (TouchMaxX - MarginX);
	
	return disp_val;
}

PFdword pfTouchMapY(PFdword touch_val)
{
	PFdword disp_val;
  	
	disp_val =  ( (touch_val - MarginY) * DispMaxY ) / (TouchMaxY - MarginY);
	disp_val = 320 - disp_val;
	
	return disp_val;  
}
	
PFEnStatus pfTouchClose(void)
{
#if(PF_TOUCH_DEBUG==1)
	if(touchInitFlag==0)
		return enStatusNotConfigured;
 #endif
	pfGpioPinsClear(tpconfig->gpioTouchCsGpio.port, tpconfig->gpioTouchCsGpio.pin);
	return enStatusSuccess;
	
}
 
PFdword pfkth_element(PFdword* inputArray, PFdword size, PFdword k)
{
	PFdword i,j,l,m ;
    PFdword x;
	PFdword swapTemp = 0;

    l=0; 
	m = size-1;
	
    while(l < m) 
	{
        x =*(inputArray + k) ;
        i = l ;
        j = m ;
        do 
		{
            while(*(inputArray + i) < x) i++ ;
            while(x < *(inputArray + j)) j-- ;
            if(i <= j) 
			{
                ELEM_SWAP(*(inputArray + i), *(inputArray + j)) ;
                i++; 
				j--;
            }
        } while(i <= j);
		
        if(j < k)
			l = i;
        if(k < i)
			m = j;
    }
    return *(inputArray + k) ;
}
