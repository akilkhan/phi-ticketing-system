/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework SGTL5000 driver .
 * 
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup  SGTL5000_Register_Addresses SGTL5000 Register Addresses 
 * @{
 */


#define SGTL5000_CHIP_ID				0x0000
#define SGTL5000_CHIP_DIG_POWER			0x0002
#define SGTL5000_CHIP_CLK_CTRL			0x0004
#define SGTL5000_CHIP_I2S_CTRL			0x0006
#define SGTL5000_CHIP_SSS_CTRL			0x000a
#define SGTL5000_CHIP_ADCDAC_CTRL		0x000e
#define SGTL5000_CHIP_DAC_VOL			0x0010
#define SGTL5000_CHIP_PAD_STRENGTH		0x0014
#define SGTL5000_CHIP_ANA_ADC_CTRL		0x0020
#define SGTL5000_CHIP_ANA_HP_CTRL		0x0022
#define SGTL5000_CHIP_ANA_CTRL			0x0024
#define SGTL5000_CHIP_LINREG_CTRL		0x0026
#define SGTL5000_CHIP_REF_CTRL			0x0028
#define SGTL5000_CHIP_MIC_CTRL			0x002a
#define SGTL5000_CHIP_LINE_OUT_CTRL		0x002c
#define SGTL5000_CHIP_LINE_OUT_VOL		0x002e
#define SGTL5000_CHIP_ANA_POWER			0x0030
#define SGTL5000_CHIP_PLL_CTRL			0x0032
#define SGTL5000_CHIP_CLK_TOP_CTRL		0x0034
#define SGTL5000_CHIP_ANA_STATUS		0x0036
#define SGTL5000_CHIP_SHORT_CTRL		0x003c
#define SGTL5000_CHIP_ANA_TEST2			0x003a
#define SGTL5000_DAP_CTRL				0x0100
#define SGTL5000_DAP_PEQ				0x0102
#define SGTL5000_DAP_BASS_ENHANCE		0x0104
#define SGTL5000_DAP_BASS_ENHANCE_CTRL	0x0106
#define SGTL5000_DAP_AUDIO_EQ			0x0108
#define SGTL5000_DAP_SURROUND			0x010a
#define SGTL5000_DAP_FLT_COEF_ACCESS	0x010c
#define SGTL5000_DAP_COEF_WR_B0_MSB		0x010e
#define SGTL5000_DAP_COEF_WR_B0_LSB		0x0110
#define SGTL5000_DAP_EQ_BASS_BAND0		0x0116
#define SGTL5000_DAP_EQ_BASS_BAND1		0x0118
#define SGTL5000_DAP_EQ_BASS_BAND2		0x011a
#define SGTL5000_DAP_EQ_BASS_BAND3		0x011c
#define SGTL5000_DAP_EQ_BASS_BAND4		0x011e
#define SGTL5000_DAP_MAIN_CHAN			0x0120
#define SGTL5000_DAP_MIX_CHAN			0x0122
#define SGTL5000_DAP_AVC_CTRL			0x0124
#define SGTL5000_DAP_AVC_THRESHOLD		0x0126
#define SGTL5000_DAP_AVC_ATTACK			0x0128
#define SGTL5000_DAP_AVC_DECAY			0x012a
#define SGTL5000_DAP_COEF_WR_B1_MSB		0x012c
#define SGTL5000_DAP_COEF_WR_B1_LSB		0x012e
#define SGTL5000_DAP_COEF_WR_B2_MSB		0x0130
#define SGTL5000_DAP_COEF_WR_B2_LSB		0x0132
#define SGTL5000_DAP_COEF_WR_A1_MSB		0x0134
#define SGTL5000_DAP_COEF_WR_A1_LSB		0x0136
#define SGTL5000_DAP_COEF_WR_A2_MSB		0x0138
#define SGTL5000_DAP_COEF_WR_A2_LSB		0x013a



/** Enumeration for SGTL5000 Sample Rate Selection */
typedef enum
{ 
	 enSgtl5000SampleRate_32K=0,
	 enSgtl5000SampleRate_44_1K=1,
	 enSgtl5000SampleRate_48K=2,
	 enSgtl5000SampleRate_96K=3
}PFEnSgtl5000SampelRateSel;

/** Enumeration for SGTL5000 DataLength	selection */
typedef enum
{
	enSgtl5000Dl_32=0,
	enSgtl5000Dl_24=1,
	enSgtl5000Dl_20=2,
	enSgtl5000Dl_16=3,	
}PFEnSgtl5000DLSelection;

/** Enumeration for SGTL5000 sample rate division factor*/
typedef enum
{ 
	 enSgtl5000SampleRateModeDiv_0=0,
	 enSgtl5000SampleRateModeDiv_2=1,
	 enSgtl5000SampleRateModeDiv_4=2,
	 enSgtl5000SampleRateModeDiv_6=3
}PFEnSgtl5000SampelRateModeDiv;
/** Configuration structure for SGTL5000	*/

typedef struct
{
	  PFbyte hpVolume;									/** Headphone volume				*/
	  PFbyte dacVolume;        							/**  DAC volume  					*/
	  PFEnSgtl5000SampelRateSel sampleSel; 				/** Sample rate selection option	*/
	   PFEnSgtl5000SampelRateModeDiv divMode;			/** Sample rate division factor 	*/
	  PFEnSgtl5000DLSelection dataLength;				/** Data length selection option	*/
      PFEnStatus  (*sgtl5000I2CWrite)(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFdword size); 					/** Function pointer to I2C write function  		*/
      PFEnStatus  (*sgtl5000I2CRead)(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFdword size, PFdword* readBytes); /** Function pointer to I2C read function			*/
}PFCfgSGTL5000;

/** Pointer to PFCfgSgtl500 structure		*/
typedef PFCfgSGTL5000* PFpCfgSGTL5000;

/**
*  Initializes the SGTL5000 module with provided settings
*
*  \param configuration structure which contains the settings for the SGTL5000 module
*
*  \return SGTL5000 initialization status. 
*/

PFEnStatus pfSgtl5000open(PFpCfgSGTL5000 config);
/**
 * The function sends multiple bytes to sgtl5000 module.  
 *
 * \param Register address to which data has to be writter.
 *
 * \param Data to be written to the register
 *
 * \return SGTL5000 write status.
 */
PFEnStatus pfSgtl5000Write(PFword RegAdd,PFword data);
/**
 * The function read multiple bytes from sgtl5000 module.  
 *
 * \param Register address  of which data has to be read.
 *
 * \param Data  pointer in which read data will be stored
 *
 * \param size Total number of bytes to read.
 *
 * \param readBytes Pointer to double word, in which function will fill number bytes actually read
 *
 * \return SGTL5000 read status
 */
PFEnStatus pfSgtl5000Read(PFbyte RegAdd,PFbyte *data,PFbyte size,PFdword *readBytes);
/**
 * The function to set headphone volume level  
 *
 * \param Volume level that has to be set.
 *
 * \return SGTL5000 HP volume status
 */
PFEnStatus pfSgtl5000SetHeadPhoneVolume(PFword volume);
/**
 * The function to set DAC volume level  
 *
 * \param Volume level that has to be set.
 *
 * \return SGTL5000 DAC volume status
 */
PFEnStatus pfSgtl5000SetDacVolume(PFword volume);
/**
 * The function to close the SGTL5000 module  
 *
 * \param Volume level that has to be set.
 *
 * \return SGTL5000 close status
 */
PFEnStatus pfSgtl5000Close(void);
