#include "prime_framework.h"
#include "prime_utils.h"

#ifdef PRIME_DEBUG
	#warning Prime Framework Debug Version
#else
	#warning Prime Framework Release Version
#endif

#ifdef PF_LITTLE_ENDIAN
	#warning Little Endian Platform
#elif PF_BIG_ENDIAN	
	#warning Big Endian Platform
#else
	#error Please define system endianess
#endif	

#ifdef PRIME_DEBUG
	#if defined ( __CC_ARM )
		#warning "USING ARMCC COMPILER"
	#elif defined ( __ICCARM__ )
		#warning "USING IAR ICCARM"
	#elif defined ( __TMS470__ )
		#warning "USING TI CCS COMPILER"
	#elif defined ( __GNUC__ )
		#warning "USING GNU COMPILER"
	#elif defined ( __TASKING__ )
		#warning "USING TASKING COMPILER"
	#else
		#error "NO COMPILER SELECTED!!!"
	#endif	
#endif

#ifdef MCU_FAMILY_lpc17xx
	#warning LPC17XX MCU FAMILY
#endif

#if (PF_USE_ADC == 1)				
	#warning PF_USE_ADC 
#endif

// Native hardware enable macros
#if (PF_USE_EINT0 == 1)			
#warning PF_USE_EINT0 
#endif
#if (PF_USE_EINT1 == 1)			
#warning PF_USE_EINT1 
#endif
#if (PF_USE_EINT2 == 1)			
#warning PF_USE_EINT2 
#endif
#if (PF_USE_EINT3 == 1)			
#warning PF_USE_EINT3 
#endif

#if (PF_USE_UART0 == 1)				
#warning  PF_USE_UART0
#endif
#if (PF_USE_UART1 == 1)				
#warning  PF_USE_UART1
#endif
#if (PF_USE_UART2 == 1)				
#warning  PF_USE_UART2
#endif
#if (PF_USE_UART3 == 1)				
#warning  PF_USE_UART3
#endif

#if (PF_USE_SPI0 == 1)				
#warning  PF_USE_SPI0
#endif
#if (PF_USE_SPI1 == 1)				
#warning  PF_USE_SPI1
#endif

#if (PF_USE_I2C0 == 1)				
#warning  PF_USE_I2C0
#endif
#if (PF_USE_I2C1 == 1)				
#warning  PF_USE_I2C1
#endif
#if (PF_USE_I2C2 == 1)				
#warning  PF_USE_I2C2
#endif

#if (PF_USE_CAN1 == 1)				
#warning  PF_USE_CAN1
#endif
#if (PF_USE_CAN2 == 1)				
#warning  PF_USE_CAN2
#endif

#if (PF_USE_TIMER0 == 1)				
#warning  PF_USE_TIMER0
#endif
#if (PF_USE_TIMER1 == 1)				
#warning  PF_USE_TIMER1
#endif
#if (PF_USE_TIMER3 == 1)				
#warning  PF_USE_TIMER3
#endif
#if (PF_USE_TIMER4 == 1)				
#warning  PF_USE_TIMER4
#endif

#if (PF_USE_PWM == 1)				
#warning  PF_USE_PWM
#endif

#if (PF_USE_RIT == 1)				
#warning  PF_USE_RIT
#endif

#if (PF_USE_RTC == 1)				
#warning  PF_USE_RIT
#endif

#if (PF_USE_SYSTICK == 1)			
#warning  PF_USE_SYSTICK
#endif


// Native hardware debug macros
#if (PF_ADC_DEBUG == 1)				
	#warning PF_ADC_DEBUG 
#endif

#if (PF_EINT0_DEBUG == 1)			
#warning PF_EINT0_DEBUG 
#endif
#if (PF_EINT1_DEBUG == 1)			
#warning PF_EINT1_DEBUG 
#endif
#if (PF_EINT2_DEBUG == 1)			
#warning PF_EINT2_DEBUG 
#endif
#if (PF_EINT3_DEBUG == 1)			
#warning PF_EINT3_DEBUG 
#endif

#if (PF_UART0_DEBUG == 1)				
#warning  PF_UART0_DEBUG
#endif
#if (PF_UART1_DEBUG == 1)				
#warning  PF_UART1_DEBUG
#endif
#if (PF_UART2_DEBUG == 1)				
#warning  PF_UART2_DEBUG
#endif
#if (PF_UART3_DEBUG == 1)				
#warning  PF_UART3_DEBUG
#endif


#if (PF_SPI0_DEBUG == 1)				
#warning  PF_SPI0_DEBUG
#endif
#if (PF_SPI1_DEBUG == 1)				
#warning  PF_SPI1_DEBUG
#endif

#if (PF_I2C0_DEBUG == 1)				
#warning  PF_I2C0_DEBUG
#endif
#if (PF_I2C1_DEBUG == 1)				
#warning  PF_I2C1_DEBUG
#endif
#if (PF_I2C2_DEBUG == 1)				
#warning  PF_I2C2_DEBUG
#endif

#if (PF_CAN_DEBUG == 1)				
#warning  PF_CAN_DEBUG
#endif
#if (PF_CAN1_DEBUG == 1)				
#warning  PF_CAN1_DEBUG
#endif
#if (PF_CAN2_DEBUG == 1)				
#warning  PF_CAN2_DEBUG
#endif

#if (PF_TIMER0_DEBUG == 1)				
#warning  PF_TIMER0_DEBUG
#endif
#if (PF_TIMER1_DEBUG == 1)				
#warning  PF_TIMER1_DEBUG
#endif
#if (PF_TIMER3_DEBUG == 1)				
#warning  PF_TIMER3_DEBUG
#endif
#if (PF_TIMER4_DEBUG == 1)				
#warning  PF_TIMER4_DEBUG
#endif


#if (PF_PWM_DEBUG == 1)				
#warning  PF_PWM_DEBUG
#endif

#if (PF_RIT_DEBUG == 1)				
#warning  PF_RIT_DEBUG
#endif

#if (PF_RTC_DEBUG == 1)				
#warning  PF_RIT_DEBUG
#endif

#if (PF_SYSCLK_DEBUG == 1)				
#warning  PF_SYSCLK_DEBUG
#endif

#if (PF_SYSTICK_DEBUG == 1)			
#warning  PF_SYSTICK_DEBUG
#endif

// Device driver debug macros
#if (PF_A3977_DEBUG == 1)				
#warning  PF_A3977_DEBUG
#endif
#if (PF_L6470_DEBUG == 1)				
#warning  PF_L6470_DEBUG
#endif
#if (PF_CHEETAH_DEBUG == 1)			
#warning  PF_CHEETAH_DEBUG
#endif
#if (PF_LS7366_DEBUG == 1)				
#warning  PF_LS7366_DEBUG
#endif
#if (PF_GP2Y0A02_DEBUG == 1)			
#warning  PF_GP2Y0A02_DEBUG
#endif
#if (PF_GP2Y0A21_DEBUG == 1)			
#warning  PF_GP2Y0A21_DEBUG
#endif
#if (PF_GP2Y0A710_DEBUG == 1)			
#warning  PF_GP2Y0A710_DEBUG
#endif
#if (PF_GPS_DEBUG == 1)				
#warning  PF_GPS_DEBUG
#endif
#if (PF_ULTRASONIC_DEBUG == 1)			
#warning  PF_ULTRASONIC_DEBUG
#endif
#if (PF_VCNL4000_DEBUG == 1)			
#warning  PF_VCNL4000_DEBUG
#endif
#if (PF_MPU6050_DEBUG == 1)			
#warning  PF_MPU6050_DEBUG
#endif
#if (PF_LSM303_DEBUG == 1)				
#warning  PF_LSM303_DEBUG
#endif
#if (PF_BLUETOOTH_DEBUG == 1)			
#warning  PF_BLUETOOTH_DEBUG
#endif
#if (PF_TCS230_DEBUG == 1)				
#warning  PF_TCS230_DEBUG
#endif
#if (PF_ILI9320_DEBUG == 1)
#warning PF_ILI9320_DEBUG
#endif


// Other debug macros
#if (PF_UTILS_DEBUG == 1)				
#warning  PF_UTILS_DEBUG
#endif

#define PRIME_VERSION		"Prime Framework Version:" PF_STRING(PF_VERSION)
#define PRIME_DATE			"Prime Framework Build Date:" PF_STRING(__DATE__)
#define PRIME_TIME			"Prime Framework Build Time:" PF_STRING(__TIME__)
#define PRIME_CPU			"Prime Framework Build Time:" PF_STRING(MCU_CHIP)
#ifdef PRIME_DEBUG
#define PRIME_BUILD_MODE	"Prime Framework Build Mode: Debug"
#else
#define PRIME_BUILD_MODE	"Prime Framework Build Mode: Release"
#endif

const PFbyte prime_version[] = PRIME_VERSION;
const PFbyte prime_date[] = PRIME_DATE;
const PFbyte prime_time[] = PRIME_TIME;
const PFbyte prime_build[] = PRIME_BUILD_MODE;
const PFbyte prime_cpu[] = PRIME_CPU;

