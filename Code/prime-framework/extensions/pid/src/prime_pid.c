//#include "system.h"
#include "prime_framework.h"
#include "prime_pidHwconfig.h"
#include "prime_pid.h"

#if(PF_USE_FIXEDMATH_CAL == 1)
#warning Using fixed math calculation for PID
#include "fixedMath.h" 
#else
#define PF_USE_INTMATH_CAL
#warning Using interger calculation for PID
#endif

PFEnBoolean pidInit =enBooleanFalse;
PFEnBoolean idPid[MAX_PID_NO]={enBooleanFalse};
PFPidContext contextPid[MAX_PID_NO]={0};
PFCfgPid pidConfig[MAX_PID_NO]={0};



#if(PF_USE_FIXEDMATH_CAL == 1)
static int64_t gerror[MAX_PID_NO] = {0};
static int64_t kp[MAX_PID_NO] = {0};
static int64_t ti[MAX_PID_NO] = {0};
static int64_t td[MAX_PID_NO] = {0};
static int64_t maxOutput[MAX_PID_NO] = {0};
static int64_t minOutput[MAX_PID_NO] = {0};
static int64_t maxInput[MAX_PID_NO] = {0};
static int64_t minInput[MAX_PID_NO] = {0};
static int64_t outputSpan[MAX_PID_NO] = {0};
static int64_t inputSpan[MAX_PID_NO] = {0};
static int64_t accErr[MAX_PID_NO] = {0};
static int64_t lastErr[MAX_PID_NO] = {0};
static int64_t lastCO[MAX_PID_NO] = {0};
int64_t setPoint[MAX_PID_NO] = {0};
static pfPidData_t kCoVal = {K_CO_INTPART_VAL, K_CO_FRACTPART_VAL, K_CO_FRACTION_PRECISION};
static int64_t kco = 0;


PFEnStatus pfPidOpen(PFbyte* idArray,const PFpCfgPid  config,PFbyte count)
{
	PFbyte loop=0,i;
	PFsdword temp;
	
#ifdef _DEBUG_	
	if(config == 0)
	return enStatusInvArgs;
	if(count > MAX_PID_NO)
	return enStatusNotSupported;
#endif
	
	for(i =0;i < count ;i++)
	{
		for(loop=0 ; loop < MAX_PID_NO ;loop++ )
		{
			if(idPid[loop] == enBooleanFalse)
			{
				contextPid[loop].configuration = &pidConfig[loop];
				
				// Pid Config			
				
				contextPid[loop].configuration->kp.intPart = config[i].kp.intPart;
				contextPid[loop].configuration->kp.fractPart = config[i].kp.fractPart;
				contextPid[loop].configuration->kp.divFact = config[i].kp.divFact;				
			
				contextPid[loop].configuration->ti.intPart = config[i].ti.intPart;
				contextPid[loop].configuration->ti.fractPart = config[i].ti.fractPart;
				contextPid[loop].configuration->ti.divFact = config[i].ti.divFact;				
				
				contextPid[loop].configuration->td.intPart = config[i].td.intPart;
				contextPid[loop].configuration->td.fractPart = config[i].td.fractPart;
				contextPid[loop].configuration->td.divFact = config[i].td.divFact;				
				
				contextPid[loop].configuration->maxPidOutput.intPart = config[i].maxPidOutput.intPart;
				contextPid[loop].configuration->maxPidOutput.fractPart = config[i].maxPidOutput.fractPart;
				contextPid[loop].configuration->maxPidOutput.divFact = config[i].maxPidOutput.divFact;				
			
				contextPid[loop].configuration->minPidOutput.intPart = config[i].minPidOutput.intPart;
				contextPid[loop].configuration->minPidOutput.fractPart = config[i].minPidOutput.fractPart;
				contextPid[loop].configuration->minPidOutput.divFact = config[i].minPidOutput.divFact;
				
				contextPid[loop].configuration->outputSpan.intPart = config[i].outputSpan.intPart;
				contextPid[loop].configuration->outputSpan.fractPart = config[i].outputSpan.fractPart;
				contextPid[loop].configuration->outputSpan.divFact = config[i].outputSpan.divFact;				
				
				contextPid[loop].configuration->maxPidInput.intPart = config[i].maxPidInput.intPart;
				contextPid[loop].configuration->maxPidInput.fractPart = config[i].maxPidInput.fractPart;
				contextPid[loop].configuration->maxPidInput.divFact = config[i].maxPidInput.divFact;				
					
				contextPid[loop].configuration->minPidInput.intPart = config[i].minPidInput.intPart;
				contextPid[loop].configuration->minPidInput.fractPart = config[i].minPidInput.fractPart;
				contextPid[loop].configuration->minPidInput.divFact = config[i].minPidOutput.divFact;
							
				contextPid[loop].configuration->inputSpan.intPart = config[i].inputSpan.intPart;
				contextPid[loop].configuration->inputSpan.fractPart = config[i].inputSpan.fractPart;
				contextPid[loop].configuration->inputSpan.divFact = config[i].inputSpan.divFact;				
				
				// PID Context 
				
				contextPid[loop].accumulatedError.intPart = 0;
				contextPid[loop].accumulatedError.fractPart = 0;
				contextPid[loop].accumulatedError.divFact = 1;				
				
				contextPid[loop].lastErrorInput.intPart = 0;
				contextPid[loop].lastErrorInput.fractPart = 0;
				contextPid[loop].lastErrorInput.divFact = 1;
				
				contextPid[loop].lastControlOut.intPart = 0;
				contextPid[loop].lastControlOut.fractPart = 0;
				contextPid[loop].lastControlOut.divFact = 1;
				
				contextPid[loop].setPoint.intPart = 0;
				contextPid[loop].setPoint.fractPart = 0;
				contextPid[loop].setPoint.divFact = 1;
				
				pfDecimal2FixedNum(&kp[loop], &contextPid[loop].configuration->kp);
				pfDecimal2FixedNum(&ti[loop], &contextPid[loop].configuration->ti);
				pfDecimal2FixedNum(&td[loop], &contextPid[loop].configuration->td);
				pfDecimal2FixedNum(&maxOutput[loop], &contextPid[loop].configuration->maxPidOutput);
				pfDecimal2FixedNum(&minOutput[loop], &contextPid[loop].configuration->minPidOutput);
				pfDecimal2FixedNum(&outputSpan[loop], &contextPid[loop].configuration->outputSpan);
				pfDecimal2FixedNum(&maxInput[loop], &contextPid[loop].configuration->maxPidInput);
				pfDecimal2FixedNum(&minInput[loop], &contextPid[loop].configuration->minPidInput);
				pfDecimal2FixedNum(&inputSpan[loop], &contextPid[loop].configuration->inputSpan);
				pfDecimal2FixedNum(&accErr[loop], &contextPid[loop].accumulatedError);
				pfDecimal2FixedNum(&lastErr[loop], &contextPid[loop].lastErrorInput);
				pfDecimal2FixedNum(&lastCO[loop], &contextPid[loop].lastControlOut);
				pfDecimal2FixedNum(&setPoint[loop], &contextPid[loop].setPoint);
				
				pfDecimal2FixedNum(&kco, &kCoVal);
				
				idPid[loop] = enBooleanTrue;
				idArray[i] = loop;
				pfMemCopy(contextPid[loop].configuration, &config[i], sizeof(PFCfgPid));
				break;
			}
		}		
	}
	pidInit = enBooleanTrue ;
	return enStatusSuccess;
	
}

int64_t pfPidUpdate(PFbyte id, int64_t controlInput,pfPidErrors *errPrint)
{
	volatile int64_t error =0, deltaErr = 0;		
	int64_t pidOutputNum =0, pidOutputDenom =0, unFilteredCO = 0, filteredCo = 0, filteredCoNum = 0, filteredCoDenom = 1;
	int64_t pTermNum = 0, pTermDenom = 1, pTerm = 0;
	int64_t iTermNum = 0, iTermDenom = 1, iTerm = 0;
	int64_t dTermNum = 0, dTermDenom = 1, dTerm = 0;
	int64_t unFilteredCo = 0, unFilteredCoNum = 0, unFilteredCoDenom = 1, errordmsnl = 0, deltaErrDmsnl = 0, pidOutput = 0, pidInput = 0;
	
		
#ifdef _DEBUG_
	if(pidInit == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > MAX_PID_NO )
	return enStatusInvArgs;
#endif
	
		
	error = setPoint[id] - controlInput ;

	pfFixedSmul32(&errordmsnl, error, INT_FIXED_CONVERSION(100));
	pfFixedSdiv32(&errordmsnl, errordmsnl, maxInput[id]);
	
	/* proportional Errror */
	
	pfFixedSmul32(&pTerm, kp[id], errordmsnl);
	
	/* Integral Error Accumulation windup according to MAX limit of PWM */
	/* Accumulation error is added only when the value of error is -ve and the last MAX_PID value is reached or vice versa */
		
	if((!((errordmsnl > 0) && ( (lastCO[id])  == maxOutput[id] )))\
		&& (!((errordmsnl < 0) && ( lastCO[id]  == -maxOutput[id]))))
	{
		accErr[id] += errordmsnl;
	}
	/* ki = kp/Ti (accError) */
	
	pfFixedSmul32(&iTerm, kp[id], accErr[id] );
	pfFixedSdiv32(&iTerm, iTerm, INT_FIXED_CONVERSION(FPS));
	pfFixedSdiv32(&iTerm, iTerm, ti[id]);
	
	deltaErr = (error - lastErr[id]);
	
	pfFixedSmul32(&deltaErrDmsnl, deltaErr, INT_FIXED_CONVERSION(100));
	pfFixedSdiv32(&deltaErrDmsnl, deltaErrDmsnl, maxInput[id]);
	

	pfFixedSmul32(&deltaErrDmsnl, deltaErrDmsnl, INT_FIXED_CONVERSION(FPS));
		
	/* kd = kp * td * de(t)/dt */
	
	pfFixedSmul32(&dTerm, kp[id], td[id]);
	pfFixedSmul32(&dTerm, dTerm, deltaErrDmsnl);
	
	/* PWM Count calculation*/
	#ifdef USE_PID
	pidOutput = pTerm + iTerm + dTerm ;
	#else
	pidOutput = pTerm + iTerm ;
	#endif
	
	pfFixedSmul32(&unFilteredCO, pidOutput, maxOutput[id]);
	pfFixedSdiv32(&unFilteredCO , unFilteredCO ,INT_FIXED_CONVERSION(100));
	
	if((unFilteredCO < 0) && (unFilteredCO > (-minOutput[id])))
		unFilteredCO = -minOutput[id];
	
	else if((unFilteredCO >0) && ((unFilteredCO) < minOutput[id]))
		unFilteredCO = minOutput[id];
	
	else if(unFilteredCO < (-maxOutput[id]))
		unFilteredCO = -maxOutput[id];
	
	else if(unFilteredCO > maxOutput[id])
		unFilteredCO = maxOutput[id];

	
	pfFixedSmul32(&filteredCoNum ,((1 << BIT_PRECISION ) - kco ), lastCO[id]);
	pfFixedSmul32(&filteredCoDenom, kco, unFilteredCO);
	
	filteredCo = filteredCoNum + filteredCoDenom ;
	
	lastCO[id] = filteredCo;
	lastErr[id] = error;
		
	errPrint->errEachLoop = lastErr[id];
	errPrint->errAccumalated = accErr[id] ;
	
	return lastCO[id] ;

}

#else if(PF_USE_FIXEDMATH_CAL == 0)

PFEnStatus pfPidOpen(PFbyte* idArray,const PFpCfgPid  config,PFbyte count)
{
	PFbyte loop=0,i;
#ifdef _DEBUG_
	
	if(config == 0)
	return enStatusInvArgs;
	if(count > MAX_PID_NO)
	return enStatusNotSupported;
#endif
	for(i =0;i < count ;i++)
	{
		for(loop=0 ; loop < MAX_PID_NO ;loop++ )
		{
			if(idPid[loop] == enBooleanFalse)
			{
				contextPid[loop].configuration = &pidConfig[loop];
				
				/* PID Configuration */
				contextPid[loop].configuration->kpNum = config[i].kpNum;
				contextPid[loop].configuration->kpDenom = config[i].kpDenom;
				
				contextPid[loop].configuration->tiNum = config[i].tiNum;
				contextPid[loop].configuration->tiDenom = config[i].tiDenom;
				
				contextPid[loop].configuration->tdNum = config[i].tdNum;
				contextPid[loop].configuration->tdDenom = config[i].tdDenom;
				
				contextPid[loop].configuration->maxPidOutputNum = config[i].maxPidOutputNum;;
				contextPid[loop].configuration->maxPidOutputDenom = config[i].maxPidOutputDenom;;
				
				contextPid[loop].configuration->minPidOutputNum = config[i].minPidOutputNum;
				contextPid[loop].configuration->minPidOutputDenom = config[i].minPidOutputDenom;
				
				contextPid[loop].configuration->maxPidInputNum = config[i].maxPidInputNum ;
				contextPid[loop].configuration->maxPidInputDenom = config[i].maxPidInputDenom;
				
				contextPid[loop].configuration->minPidInputNum = config[i].minPidInputNum;
				contextPid[loop].configuration->minPidInputDenom = config[i].minPidInputDenom;
				
				contextPid[loop].configuration->outputSpan = config[i].outputSpan;
				contextPid[loop].configuration->inputSpan = config[i].inputSpan;
							
				/* PID Context */
				contextPid[loop].accumulatedErrorNum = 0;
				contextPid[loop].accumulatedErrorDenom = 1;
				contextPid[loop].lastErrorInputNum = 0; 
				contextPid[loop].lastErrorInputDenom = 1; 
				contextPid[loop].lastFilteredCO = 0; 
				contextPid[loop].setPoint = 0; 
				
				idPid[loop] = enBooleanTrue;
				idArray[i] = loop;
				pfMemCopy(contextPid[loop].configuration, &config[i], sizeof(PFCfgPid));
				break;
			}
		}
		
	}
	pidInit = enBooleanTrue ;
	return enStatusSuccess;
	
}

pfPidData_t pfPidUpdate(PFbyte id, pfPidData_t controlInput,pfPidErrors *errPrint)
{
	volatile PFsdword error =0, errorNum = 0, errorDenom = 1, deltaErrNum = 0, deltaErrDenom = 1;		
	int64_t pidOutputNum =0, pidOutputDenom =0, unFilteredCO = 0, filteredCoNum = 0, filteredCoDenom = 1;
	int64_t pTermNum = 0, pTermDenom = 1;
	int64_t iTermNum = 0, iTermDenom = 1;
	int64_t dTermNum = 0, dTermDenom = 1;
	int64_t unFilteredCoNum = 0, unFilteredCoDenom = 1;
		
#ifdef _DEBUG_
	if(pidInit == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > MAX_PID_NO )
	return enStatusInvArgs;
#endif
	
	errorNum = (contextPid[id].setPoint - controlInput)*100;
	errorDenom = contextPid[id].configuration->maxPidInputNum;
	
	/* proportional Errror */
	
	pTermNum = (errorNum * (contextPid[id].configuration->kpNum));
	pTermDenom = (errorDenom * (contextPid[id].configuration->kpDenom));
	
	/* Integral Error Accumulation windup according to MAX limit of PWM */
	/* Accumulation error is added only when the value of error is -ve and the last MAX_PID value is reached or vice versa */
		
	if((!((errorNum > 0) && \
		( (contextPid[id].lastFilteredCO * contextPid[id].configuration->maxPidOutputDenom)  == contextPid[id].configuration->maxPidOutputNum )))\
		&& (!((errorNum < 0) \
		&& ( (contextPid[id].lastFilteredCO * contextPid[id].configuration->maxPidOutputDenom)  == -contextPid[id].configuration->maxPidOutputNum ))))
	{
		contextPid[id].accumulatedErrorNum = ((contextPid[id].accumulatedErrorNum) + errorNum); 
		contextPid[id].accumulatedErrorDenom = (errorDenom * FPS) ;
		//contextPid[id].accumulatedErrorDenom = (errorDenom) ;
	}
	/* ki = kp/Ti (accError) */
	iTermNum = ((((int64_t)contextPid[id].accumulatedErrorNum) * (contextPid[id].configuration->kpNum))* ((int64_t)contextPid[id].configuration->tiDenom));
	iTermDenom = (((int64_t)contextPid[id].configuration->tiNum) * (contextPid[id].configuration->kpDenom)*((int64_t)contextPid[id].accumulatedErrorDenom));

	/* Differential Error Calculations */
	/* d/dt(e(t) - e(t-1));dt = 1/fps */
	
	deltaErrNum	= FPS * (errorNum - contextPid[id].lastErrorInputNum);	
	deltaErrDenom = errorDenom;
	
	/* kd = kp * td * de(t)/dt */
	dTermNum = ((((int64_t)contextPid[id].configuration->kpNum) * (contextPid[id].configuration->tdNum)) * (int64_t)deltaErrNum );
	dTermDenom = ((((int64_t)contextPid[id].configuration->kpDenom) * (contextPid[id].configuration->tdDenom)) *(int64_t)deltaErrDenom);
	
	/* PWM Count calculation*/
	#ifdef USE_PID
	pidOutputDenom = (int64_t)pTermDenom * iTermDenom;
	pidOutputNum = ((int64_t)pTermNum * iTermDenom) + ((int64_t)pTermDenom * iTermNum) + ((int64_t)dTermNum * pidOutputDenom)/dTermDenom;
	#else
	pidOutputNum = ((int64_t)pTermNum * iTermDenom) + ((int64_t)pTermDenom * iTermNum);
	pidOutputDenom = (int64_t)pTermDenom * iTermDenom;
	#endif
	
	unFilteredCoNum = ((int64_t)pidOutputNum * (contextPid[id].configuration->maxPidOutputNum)) ;
	unFilteredCoDenom = ((int64_t)100 *((int64_t)pidOutputDenom * contextPid[id].configuration->maxPidOutputDenom ));
	
	unFilteredCO  = (int64_t)unFilteredCoNum / unFilteredCoDenom;
	
	
	if((unFilteredCO < 0) && ((unFilteredCoNum * contextPid[id].configuration->minPidOutputDenom) > -(unFilteredCoDenom * contextPid[id].configuration->minPidOutputNum)))
		unFilteredCO = -contextPid[id].configuration->minPidOutputNum /contextPid[id].configuration->minPidOutputDenom;
	
	else if((unFilteredCO >0) && ((unFilteredCoNum * contextPid[id].configuration->minPidOutputDenom) < unFilteredCoDenom * contextPid[id].configuration->minPidOutputNum))
		unFilteredCO = contextPid[id].configuration->minPidOutputNum /contextPid[id].configuration->minPidOutputDenom;
	
	else if(unFilteredCoNum * contextPid[id].configuration->maxPidOutputDenom < - (unFilteredCoDenom * contextPid[id].configuration->maxPidOutputNum))
		unFilteredCO = -contextPid[id].configuration->maxPidOutputNum / contextPid[id].configuration->maxPidOutputDenom;
	
	else if(unFilteredCoNum *contextPid[id].configuration->maxPidOutputDenom > unFilteredCoDenom * contextPid[id].configuration->maxPidOutputNum)
		unFilteredCO = contextPid[id].configuration->maxPidOutputNum / contextPid[id].configuration->maxPidOutputDenom;;

	
	filteredCoNum = (K_CO_DENOM - K_CO_NUM) * contextPid[id].lastFilteredCO + K_CO_NUM * unFilteredCO;
	filteredCoDenom = K_CO_DENOM;
	
		
	contextPid[id].lastFilteredCO = filteredCoNum / filteredCoDenom;
	
	contextPid[id].lastErrorInputNum = errorNum; 
	contextPid[id].lastErrorInputDenom = errorDenom;
	
	errPrint->errEachLoop = errorNum/100;
	errPrint->errAccumalated = contextPid[id].accumulatedErrorNum/100 ;
	
	
	return contextPid[id].lastFilteredCO;

}
#endif


PFEnStatus pfPidChangeSetPoint(PFbyte id, pfPidData_t* setPointNew)
{
	int64_t fixedNum;
	
#if(PF_USE_FIXEDMATH_CAL == 0)
	contextPid[id].setPoint = *setPointNew;
#else	
	pfDecimal2FixedNum(&fixedNum, setPointNew);	
	setPoint[id] = fixedNum ;
#endif
	return enStatusSuccess;	
}


PFEnStatus pfPidControlSetParams(PFbyte id, pfPidConstant_t kp, pfPidConstant_t ki, pfPidConstant_t kd, pidLoopTime_t sampleWindow)
{
#ifdef _DEBUG_
	if(pidInit == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > MAX_PID_NO )
	return enStatusInvArgs;
#endif
	
	return enStatusSuccess;
}


PFEnStatus pfPidControlReset(PFbyte id)
{
#ifdef _DEBUG_
	if(id > MAX_PID_NO)
	return enStatusNotSupported;
	
#endif
	pfMemSet(&contextPid[id],0,sizeof(PFPidContext));
	return enStatusSuccess;
}



