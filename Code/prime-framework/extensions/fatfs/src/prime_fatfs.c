#include "prime_framework.h"
#include "prime_fatfs.h"			/* FatFs configurations and declarations 		*/
#include "prime_fatconfig.h"		/* FatFs configurations  						*/
#include "prime_diskio.h"			/* Declarations of low level disk I/O functions */

/* File function return code (PFEnStatus) */


#define FR_OK 				enStatusSuccess
#define FR_DISK_ERR			enStatusDiskError
#define FR_INT_ERR			enStatusInternalError
#define FR_NOT_READY		enStatusDiskNotReady
#define FR_NO_FILE			enStatusNoFile
#define FR_NO_PATH			enStatusNoPath
#define FR_INVALID_NAME		enStatusInvalidName
#define FR_DENIED			enStatusAccessDenied
#define FR_EXIST			enStatusFileAlreadyExist
#define FR_INVALID_OBJECT	enStatusInvalidObject
#define FR_WRITE_PROTECTED	enStatusWriteProtected
#define FR_INVALID_DRIVE	enStatusInvalidDrive
#define FR_NOT_ENABLED		enStatusNotEnabled
#define FR_NO_FILESYSTEM	enStatusNoFilesystem
#define FR_MKFS_ABORTED		enStatusMkfsAborted
#define FR_TIMEOUT			enStatusTimeout

/*--------------------------------------------------------------------------

   Module Private Definitions

---------------------------------------------------------------------------*/

#if PF_FATFS != 0x007E
#error Wrong include file (prime_fatfs.h).
#endif

#if PF_FS_REENTRANT
#if PF_FS_USE_LFN == 1
#error Static LFN work area must not be used in re-entrant configuration.
#endif
#define	ENTER_FF(fs)		{ if (!pfFsLock(fs)) return FR_TIMEOUT; }
#define	LEAVE_FF(fs, res)	{ pfFsUnlock(fs, res); return res; }

#else
#define	ENTER_FF(fs)
#define LEAVE_FF(fs, res)	return res

#endif

#define	ABORT(fs, res)		{ fp->flag |= FA__ERROR; LEAVE_FF(fs, res); }

#ifndef PF_NULL
#define	PF_NULL	0
#endif

/* Name status flags */
#define NS			11		/* Offset of name status byte */
#define NS_LOSS		0x01	/* Out of 8.3 format */
#define NS_LFN		0x02	/* Force to create LFN entry */
#define NS_LAST		0x04	/* Last segment */
#define NS_BODY		0x08	/* Lower case flag (body) */
#define NS_EXT		0x10	/* Lower case flag (ext) */
#define NS_DOT		0x20	/* Dot entry */




/*--------------------------------------------------------------------------

   Private Work Area

---------------------------------------------------------------------------*/

#if PF_FS_MAX_DRIVES < 1 || PF_FS_MAX_DRIVES > 9
#error Number of drives must be 1-9.
#endif
static PFFatFs *FatFs[PF_FS_MAX_DRIVES];	/* Pointer to the file system objects (logical drives) */

static PFword Fsid;				/* File system mount ID */

#if PF_FS_RPATH
static PFbyte Drive;				/* Current drive */
#endif


#if PF_FS_USE_LFN == 1	/* LFN with static LFN working buffer */
static
PFword LfnBuf[PF_FS_MAX_LFN + 1];
#define	NAMEBUF(sp,lp)	PFbyte sp[12]; PFword *lp = LfnBuf
#define INITBUF(dj,sp,lp)	dj.fn = sp; dj.lfn = lp

#elif PF_FS_USE_LFN > 1	/* LFN with dynamic LFN working buffer */
#define	NAMEBUF(sp,lp)	PFbyte sp[12]; PFword lbuf[PF_FS_MAX_LFN + 1], *lp = lbuf
#define INITBUF(dj,sp,lp)	dj.fn = sp; dj.lfn = lp

#else				/* No LFN */
#define	NAMEBUF(sp,lp)	PFbyte sp[12]
#define INITBUF(dj,sp,lp)	dj.fn = sp

#endif




/*--------------------------------------------------------------------------

   Module Private Functions

---------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------*/
/* String functions                                                      */
/*-----------------------------------------------------------------------*/

/* Check if chr is contained in the string */
static PFsdword pfCheckChar(const PFchar* str, PFsdword chr) 
{
	while (*str && *str != chr)
	{
		str++;
	}
	return *str;
}



/*-----------------------------------------------------------------------*/
/* Request/Release grant to access the volume                            */
/*-----------------------------------------------------------------------*/
#if PF_FS_REENTRANT

static PFEnBoolean pfFsLock(PFFatFs *fs)
{
	return ff_req_grant(fs->sobj);
}

static void pfFsUnlock(PFFatFs *fs,	PFEnStatus res)
{
	if (res != FR_NOT_ENABLED &&
		res != FR_INVALID_DRIVE &&
		res != FR_INVALID_OBJECT &&
		res != FR_TIMEOUT) {
		ff_rel_grant(fs->sobj);
	}
}
#endif


/*-----------------------------------------------------------------------*/
/* Change window offset                                                  */
/*-----------------------------------------------------------------------*/

static PFEnStatus pfFsMoveWindow(PFFatFs *fs, PFdword sector)
{
	PFdword wsect;
	wsect = fs->winsect;
	if (wsect != sector) 	/* Changed current window */
	{	
#if !PF_FS_READONLY
		if (fs->wflag) 	/* Write back dirty window if needed */
		{
			if (pfDiskWrite(fs->drive, fs->win, wsect, 1) != enStatusSuccess)
			{	
				return FR_DISK_ERR;
			}
			fs->wflag = 0;
			if (wsect < (fs->fatbase + fs->sects_fat)) 		/* In FAT area */
			{
				PFbyte nf;
				for (nf = fs->n_fats; nf > 1; nf--) 	/* Refrect the change to all FAT copies */
				{
					wsect += fs->sects_fat;
					pfDiskWrite(fs->drive, fs->win, wsect, 1);
				}
			}
		}
#endif
		if (sector) 
		{
			if (pfDiskRead(fs->drive, fs->win, sector, 1) != enStatusSuccess)
			{
				return FR_DISK_ERR;
			}
			fs->winsect = sector;
		}
	}

	return enStatusSuccess;
}




/*-----------------------------------------------------------------------*/
/* Clean-up cached data                                                  */
/*-----------------------------------------------------------------------*/
#if !PF_FS_READONLY
static PFEnStatus pfFsSync(PFFatFs *fs)
{
	PFEnStatus res;

	res = pfFsMoveWindow(fs, 0);
	if (res == enStatusSuccess) 
	{
		/* Update FSInfo sector if needed */
		if (fs->fs_type == FS_FAT32 && fs->fsi_flag) 
		{
			fs->winsect = 0;
			pfMemSet(fs->win, 0, 512);
			ST_WORD(fs->win+BS_55AA, 0xAA55);
			ST_DWORD(fs->win+FSI_LeadSig, 0x41615252);
			ST_DWORD(fs->win+FSI_StrucSig, 0x61417272);
			ST_DWORD(fs->win+FSI_Free_Count, fs->free_clust);
			ST_DWORD(fs->win+FSI_Nxt_Free, fs->last_clust);
			pfDiskWrite(fs->drive, fs->win, fs->fsi_sector, 1);
			fs->fsi_flag = 0;
		}
		/* Make sure that no pending write process in the physical drive */
		if (pfDiskIoCtrl(fs->drive, CTRL_SYNC, (void*)PF_NULL) != enStatusSuccess)
		{
			res = FR_DISK_ERR;
		}
	}

	return res;
}
#endif

/*-----------------------------------------------------------------------*/
/* FAT access - Read value of a FAT entry                                */
/*-----------------------------------------------------------------------*/


static PFdword pfFsGetFat(PFFatFs *fs, PFdword clst)
{
	PFdword wc, bc;
	PFdword fsect;

	if (clst < 2 || clst >= fs->max_clust)	/* Range check */
	{
		return 1;
	}
	fsect = fs->fatbase;
	switch (fs->fs_type) 
	{
		case FS_FAT12:
			bc = clst; 
			bc += bc / 2;
			if (pfFsMoveWindow(fs, fsect + (bc / SS(fs)))) 
			{
				break;
			}
			wc = fs->win[bc & (SS(fs) - 1)]; 
			bc++;
			if (pfFsMoveWindow(fs, fsect + (bc / SS(fs)))) 
			{
				break;
			}
			wc |= (PFword)fs->win[bc & (SS(fs) - 1)] << 8;
			return (clst & 1) ? (wc >> 4) : (wc & 0xFFF);

		case FS_FAT16 :
			if (pfFsMoveWindow(fs, fsect + (clst / (SS(fs) / 2)))) 
			{
				break;
			}
			return LD_WORD(&fs->win[((PFword)clst * 2) & (SS(fs) - 1)]);

		case FS_FAT32 :
			if (pfFsMoveWindow(fs, fsect + (clst / (SS(fs) / 4)))) 
			{
				break;
			}
			return LD_DWORD(&fs->win[((PFword)clst * 4) & (SS(fs) - 1)]) & 0x0FFFFFFF;
	}

	return 0xFFFFFFFF;	/* An error occured at the disk I/O layer */
}




/*-----------------------------------------------------------------------*/
/* FAT access - Change value of a FAT entry                              */
/*-----------------------------------------------------------------------*/
#if !PF_FS_READONLY

static PFEnStatus pfFsPutFat(PFFatFs *fs, PFdword clst,	PFdword val)
{
	PFdword bc;
	PFbyte *p;
	PFdword fsect;
	PFEnStatus res;

	if (clst < 2 || clst >= fs->max_clust)	/* Range check */
	{
		res = FR_INT_ERR;

	} 
	else 
	{
		fsect = fs->fatbase;
		switch (fs->fs_type) 
		{
			case FS_FAT12 :
				bc = clst; 
				bc += bc / 2;
				res = pfFsMoveWindow(fs, fsect + (bc / SS(fs)));
				if (res != enStatusSuccess)
				{
					break;
				}
				p = &fs->win[bc & (SS(fs) - 1)];
				*p = (clst & 1) ? ((*p & 0x0F) | ((PFbyte)val << 4)) : (PFbyte)val;
				bc++;
				fs->wflag = 1;
				res = pfFsMoveWindow(fs, fsect + (bc / SS(fs)));
				if (res != enStatusSuccess) 
				{
					break;
				}
				p = &fs->win[bc & (SS(fs) - 1)];
				*p = (clst & 1) ? (PFbyte)(val >> 4) : ((*p & 0xF0) | ((PFbyte)(val >> 8) & 0x0F));
				break;

			case FS_FAT16 :
				res = pfFsMoveWindow(fs, fsect + (clst / (SS(fs) / 2)));
				if (res != enStatusSuccess) 
				{
					break;
				}
				ST_WORD(&fs->win[((PFword)clst * 2) & (SS(fs) - 1)], (PFword)val);
				break;

			case FS_FAT32 :
				res = pfFsMoveWindow(fs, fsect + (clst / (SS(fs) / 4)));
				if (res != enStatusSuccess)
				{
					break;
				}
				ST_DWORD(&fs->win[((PFword)clst * 4) & (SS(fs) - 1)], val);
				break;

			default :
				res = FR_INT_ERR;
		}
		fs->wflag = 1;
	}

	return res;
}
#endif /* !PF_FS_READONLY */




/*-----------------------------------------------------------------------*/
/* FAT handling - Remove a cluster chain                                 */
/*-----------------------------------------------------------------------*/
#if !PF_FS_READONLY
static PFEnStatus pfFsRemoveChain(PFFatFs *fs, PFdword clst)
{
	PFEnStatus res;
	PFdword nxt;

	if (clst < 2 || clst >= fs->max_clust) 	/* Check the range of cluster# */
	{
		res = FR_INT_ERR;
	} 
	else 
	{
		res = enStatusSuccess;
		while (clst < fs->max_clust) 			/* Not a last link? */
		{	
			nxt = pfFsGetFat(fs, clst);			/* Get cluster status */
			if (nxt == 0) 						/* Empty cluster? */
			{
				break;
			}
			if (nxt == 1) 						/* Internal error? */
			{ 
				res = FR_INT_ERR; 
				break; 
			}	
			if (nxt == 0xFFFFFFFF) 				/* Disk error? */
			{ 
				res = FR_DISK_ERR; 
				break; 
			}	
			res = pfFsPutFat(fs, clst, 0);		/* Mark the cluster "empty" */
			if (res != enStatusSuccess)
			{
				break;
			}
			if (fs->free_clust != 0xFFFFFFFF)	/* Update FSInfo */	
			{	
				fs->free_clust++;
				fs->fsi_flag = 1;
			}
			clst = nxt;	/* Next cluster */
		}
	}

	return res;
}
#endif


/*-----------------------------------------------------------------------*/
/* FAT handling - Stretch or Create a cluster chain                      */
/*-----------------------------------------------------------------------*/
#if !PF_FS_READONLY
static PFdword pfFsCreateChain(PFFatFs *fs,	PFdword clst)
{
	PFdword cs, ncl, scl, mcl;

	mcl = fs->max_clust;
	if (clst == 0) 		/* Create new chain */
	{
		scl = fs->last_clust;			/* Get suggested start point */
		if (scl == 0 || scl >= mcl) scl = 1;
	}
	else 					/* Stretch existing chain */
	{
		cs = pfFsGetFat(fs, clst);			/* Check the cluster status */
		if (cs < 2) 			/* It is an invalid cluster */
		{
			return 1;
		}
		if (cs < mcl) 		/* It is already followed by next cluster */
		{
			return cs;
		}
		scl = clst;
	}

	ncl = scl;				/* Start cluster */
	for (;;) 
	{
		ncl++;							/* Next cluster */
		if (ncl >= mcl) 				/* Wrap around */
		{
			ncl = 2;
			if (ncl > scl) 	/* No free custer */
			{
				return 0;
			}
		}
		cs = pfFsGetFat(fs, ncl);			/* Get the cluster status */
		if (cs == 0) 				/* Found a free cluster */
		{
			break;
		}
		if (cs == 0xFFFFFFFF || cs == 1)/* An error occured */
		{
			return cs;
		}
		if (ncl == scl) 		/* No free custer */
		{
			return 0;
		}
	}

	if (pfFsPutFat(fs, ncl, 0x0FFFFFFF))	/* Mark the new cluster "in use" */
	{
		return 0xFFFFFFFF;
	}
	if (clst != 0) 					/* Link it to the previous one if needed */
	{
		if (pfFsPutFat(fs, clst, ncl))
		{
			return 0xFFFFFFFF;
		}
	}

	fs->last_clust = ncl;				/* Update FSINFO */
	if (fs->free_clust != 0xFFFFFFFF) 
	{
		fs->free_clust--;
		fs->fsi_flag = 1;
	}

	return ncl;		/* Return new cluster number */
}
#endif /* !PF_FS_READONLY */

/*-----------------------------------------------------------------------*/
/* Get sector# from cluster#                                             */
/*-----------------------------------------------------------------------*/

static PFdword pfFsClustToSect(PFFatFs *fs,	PFdword clst)
{
	clst -= 2;
	if (clst >= (fs->max_clust - 2)) 		/* Invalid cluster# */
	{
		return 0;
	}
	return clst * fs->csize + fs->database;
}


/*-----------------------------------------------------------------------*/
/* Directory handling - Seek directory index                             */
/*-----------------------------------------------------------------------*/

static PFEnStatus pfFsDirSeek(PFFsDir *dj, PFword idx)
{
	PFdword clst;
	PFword ic;

	dj->index = idx;
	clst = dj->sclust;
	if (clst == 1 || clst >= dj->fs->max_clust)	/* Check start cluster range */
	{
		return FR_INT_ERR;
	}
	if (!clst && dj->fs->fs_type == FS_FAT32)	/* Replace cluster# 0 with root cluster# if in FAT32 */
	{
		clst = dj->fs->dirbase;
	}
	if (clst == 0) 	/* Static table */
	{	
		dj->clust = clst;
		if (idx >= dj->fs->n_rootdir)		/* Index is out of range */
		{
			return FR_INT_ERR;
		}
		dj->sect = dj->fs->dirbase + idx / (SS(dj->fs) / 32);	/* Sector# */
	}
	else 				/* Dynamic table */
	{
		ic = SS(dj->fs) / 32 * dj->fs->csize;	/* Entries per cluster */
		while (idx >= ic) 	/* Follow cluster chain */
		{	
			clst = pfFsGetFat(dj->fs, clst);				/* Get next cluster */
			if (clst == 0xFFFFFFFF) 	/* Disk error */
			{
				return FR_DISK_ERR;
			}
			if (clst < 2 || clst >= dj->fs->max_clust)	/* Reached to end of table or int error */
			{
				return FR_INT_ERR;
			}
			idx -= ic;
		}
		dj->clust = clst;
		dj->sect = pfFsClustToSect(dj->fs, clst) + idx / (SS(dj->fs) / 32);	/* Sector# */
	}

	dj->dir = dj->fs->win + (idx % (SS(dj->fs) / 32)) * 32;	/* Ptr to the entry in the sector */

	return enStatusSuccess;	/* Seek succeeded */
}

/*-----------------------------------------------------------------------*/
/* Directory handling - Move directory index next                        */
/*-----------------------------------------------------------------------*/

static PFEnStatus pfFsDirNext(PFFsDir *dj, PFEnBoolean streach)
{
	PFdword clst;
	PFword i;

	i = dj->index + 1;
	if (!i || !dj->sect)	/* Report EOT when index has reached 65535 */
	{
		return FR_NO_FILE;
	}

	if (!(i % (SS(dj->fs) / 32)))	/* Sector changed? */
	{
		dj->sect++;					/* Next sector */

		if (dj->clust == 0) 	/* Static table */
		{	
			if (i >= dj->fs->n_rootdir)	/* Report EOT when end of table */
			{
				return FR_NO_FILE;
			}
		}
		else 					/* Dynamic table */
		{
			if (((i / (SS(dj->fs) / 32)) & (dj->fs->csize - 1)) == 0) 	/* Cluster changed? */
			{
				clst = pfFsGetFat(dj->fs, dj->clust);				/* Get next cluster */
				if (clst <= 1) 
				{
					return FR_INT_ERR;
				}
				if (clst == 0xFFFFFFFF) 
				{
					return FR_DISK_ERR;
				}
				if (clst >= dj->fs->max_clust) 				/* When it reached end of dynamic table */
				{	
#if !PF_FS_READONLY
					PFbyte c;
					if (!streach) 			/* When do not streach, report EOT */
					{
						return FR_NO_FILE;
					}
					clst = pfFsCreateChain(dj->fs, dj->clust);		/* Streach cluster chain */
					if (clst == 0) 			/* No free cluster */
					{
						return FR_DENIED;
					}
					if (clst == 1) 
					{
						return FR_INT_ERR;
					}
					if (clst == 0xFFFFFFFF) 
					{
						return FR_DISK_ERR;
					}
					/* Clean-up streached table */
					if (pfFsMoveWindow(dj->fs, 0)) 	/* Flush active window */
					{
						return FR_DISK_ERR;
					}
					pfMemSet(dj->fs->win, 0, SS(dj->fs));			/* Clear window buffer */
					dj->fs->winsect = pfFsClustToSect(dj->fs, clst);	/* Cluster start sector */
					for (c = 0; c < dj->fs->csize; c++) 		/* Fill the new cluster with 0 */
					{
						dj->fs->wflag = 1;
						if (pfFsMoveWindow(dj->fs, 0)) 
						{
							return FR_DISK_ERR;
						}
						dj->fs->winsect++;
					}
					dj->fs->winsect -= c;						/* Rewind window address */
#else	// #if !PF_FS_READONLY
					return FR_NO_FILE;			/* Report EOT */
#endif	// #if !PF_FS_READONLY
				}
				dj->clust = clst;				/* Initialize data for new cluster */
				dj->sect = pfFsClustToSect(dj->fs, clst);
			}
		}
	}

	dj->index = i;
	dj->dir = dj->fs->win + (i % (SS(dj->fs) / 32)) * 32;

	return enStatusSuccess;
}

/*-----------------------------------------------------------------------*/
/* LFN handling - Test/Pick/Fit an LFN segment from/to directory entry   */
/*-----------------------------------------------------------------------*/
#if PF_FS_USE_LFN
static
const PFbyte LfnOfs[] = {1,3,5,7,9,14,16,18,20,22,24,28,30};	/* Offset of LFN chars in the directory entry */

static PFEnBoolean pfFsCmpLFN(PFword *lfnbuf, PFbyte *dir)
{
	PFsdword i, s;
	PFword wc, uc;

	i = ((dir[LDIR_Ord] & 0xBF) - 1) * 13;	/* Get offset in the LFN buffer */
	s = 0; wc = 1;
	do 
	{
		uc = LD_WORD(dir+LfnOfs[s]);	/* Pick an LFN character from the entry */
		if (wc) 	/* Last PFchar has not been processed */
		{
			wc = ff_wtoupper(uc);		/* Convert it to upper case */
			if (i >= PF_FS_MAX_LFN || wc != ff_wtoupper(lfnbuf[i++]))	/* Compare it */
			{
				return enBooleanFalse;			/* Not matched */
			}
		} 
		else 
		{
			if (uc != 0xFFFF) 	/* Check filler */
			{
				return enBooleanFalse;
			}
		}
	} while (++s < 13);				/* Repeat until all chars in the entry are checked */

	if ((dir[LDIR_Ord] & 0x40) && wc && lfnbuf[i])	/* Last segment matched but different length */
	{
		return enBooleanFalse;
	}

	return enBooleanTrue;					/* The part of LFN matched */
}

static PFEnBoolean pfFsPickLFN(PFword *lfnbuf, PFbyte *dir)
{
	PFsdword i, s;
	PFword wc, uc;

	i = ((dir[LDIR_Ord] & 0x3F) - 1) * 13;	/* Offset in the LFN buffer */

	s = 0; wc = 1;
	do 
	{
		uc = LD_WORD(dir+LfnOfs[s]);			/* Pick an LFN character from the entry */
		if (wc) 	/* Last char has not been processed */
		{
			if (i >= PF_FS_MAX_LFN) 	/* Buffer overflow? */
			{
				return enBooleanFalse;
			}
			lfnbuf[i++] = wc = uc;				/* Store it */
		} 
		else 
		{
			if (uc != 0xFFFF)					/* Check filler */
			{
				return enBooleanFalse;		
			}
		}
	} while (++s < 13);						/* Read all character in the entry */

	if (dir[LDIR_Ord] & 0x40) 				/* Put terminator if it is the last LFN part */
	{
		if (i >= PF_FS_MAX_LFN)				/* Buffer overflow? */
		{			
			return enBooleanFalse;	
		}
		lfnbuf[i] = 0;
	}

	return enBooleanTrue;
}


#if !PF_FS_READONLY
static void pfFsFitLFN(const PFword *lfnbuf, PFbyte *dir, PFbyte ord, PFbyte sum)
{
	PFsdword i, s;
	PFword wc;

	dir[LDIR_Chksum] = sum;			/* Set check sum */
	dir[LDIR_Attr] = AM_LFN;		/* Set attribute. LFN entry */
	dir[LDIR_Type] = 0;
	ST_WORD(dir+LDIR_FstClusLO, 0);

	i = (ord - 1) * 13;				/* Get offset in the LFN buffer */
	s = wc = 0;
	do 
	{
		if (wc != 0xFFFF)			/* Get an effective char */
		{	
			wc = lfnbuf[i++];	
		}
		ST_WORD(dir+LfnOfs[s], wc);	/* Put it */
		if (!wc) 							/* Padding chars following last char */
		{
			wc = 0xFFFF;
		}
	} while (++s < 13);
	if (wc == 0xFFFF || !lfnbuf[i]) 	/* Bottom LFN part is the start of LFN sequence */
	{
		ord |= 0x40;
	}
	dir[LDIR_Ord] = ord;			/* Set the LFN order */
}

#endif	// #if !PF_FS_READONLY
#endif	// #if PF_FS_USE_LFN



/*-----------------------------------------------------------------------*/
/* Create numbered name                                                  */
/*-----------------------------------------------------------------------*/
#if PF_FS_USE_LFN
void pfFsGetNumName(PFbyte *dst, const PFbyte *src,	const PFword *lfn, PFword num)
{
	PFchar ns[8];
	PFsdword i, j;

	pfMemCopy(dst, src, 11);

	if (num > 5)	/* On many collisions, generate a hash number instead of sequencial number */
	{
		do 
		{
			num = (num >> 1) + (num << 15) + (PFword)*lfn++; 
		}while (*lfn);
	}

	/* itoa */
	i = 7;
	do 
	{
		ns[i--] = (num % 10) + '0';
		num /= 10;
	} while (num);
	ns[i] = '~';

	/* Append the number */
	for (j = 0; j < i && dst[j] != ' '; j++) 
	{
		if (IsDBCS1(dst[j])) 
		{
			if (j == i - 1) 
			{
				break;
			}
			j++;
		}
	}
	do 
	{
		dst[j++] = (i < 8) ? ns[i++] : ' ';
	} while (j < 8);
}
#endif	// #if PF_FS_USE_LFN




/*-----------------------------------------------------------------------*/
/* Calculate sum of an SFN                                               */
/*-----------------------------------------------------------------------*/
#if PF_FS_USE_LFN
static PFbyte pfFsSumSFN(const PFbyte *dir)
{
	PFbyte sum = 0;
	PFsdword n = 11;

	do 
	{
		sum = (sum >> 1) + (sum << 7) + *dir++; 
	}while (--n);
	return sum;
}
#endif	// #if PF_FS_USE_LFN

/*-----------------------------------------------------------------------*/
/* Directory handling - Find an object in the directory                  */
/*-----------------------------------------------------------------------*/
static PFEnStatus pfFsDirFind(PFFsDir *dj)
{
	PFEnStatus res;
	PFbyte c, *dir;
#if PF_FS_USE_LFN
	PFbyte a, ord, sum;
#endif	// #if PF_FS_USE_LFN

	res = pfFsDirSeek(dj, 0);			/* Rewind directory object */
	if (res != enStatusSuccess) 
	{
		return res;
	}

#if PF_FS_USE_LFN
	ord = sum = 0xFF;
#endif	// #if PF_FS_USE_LFN
	do 
	{
		res = pfFsMoveWindow(dj->fs, dj->sect);
		if (res != enStatusSuccess) 
		{
			break;
		}
		dir = dj->dir;					/* Ptr to the directory entry of current index */
		c = dir[DIR_Name];
		if (c == 0) 					/* Reached to end of table */		
		{ 
			res = FR_NO_FILE; 
			break; 
		}	
#if PF_FS_USE_LFN	/* LFN configuration */
		a = dir[DIR_Attr] & AM_MASK;
		if (c == 0xE5 || ((a & AM_VOL) && a != AM_LFN)) 	/* An entry without valid data */
		{
			ord = 0xFF;
		} 
		else 
		{
			if (a == AM_LFN) 			/* An LFN entry is found */
			{
				if (dj->lfn) 
				{
					if (c & 0x40) 		/* Is it start of LFN sequence? */
					{
						sum = dir[LDIR_Chksum];
						c &= 0xBF; 		/* LFN start order */
						ord = c;	
						dj->lfn_idx = dj->index;
					}
					/* Check validity of the LFN entry and compare it with given name */
					ord = (c == ord && sum == dir[LDIR_Chksum] && pfFsCmpLFN(dj->lfn, dir)) ? ord - 1 : 0xFF;
				}
			} 
			else 					/* An SFN entry is found */
			{
				if (!ord && sum == pfFsSumSFN(dir)) 	/* LFN matched? */
				{
					break;
				}
				ord = 0xFF; 				/* Reset LFN sequence */
				dj->lfn_idx = 0xFFFF;	
				if ( !(dj->fn[NS] & NS_LOSS) && (pfMemCompare(dir, dj->fn, 11) == enBooleanTrue) ) 	/* SFN matched? */
				{
					break;
				}
			}
		}
#else		/* Non LFN configuration */
		if ( !(dir[DIR_Attr] & AM_VOL) && (pfMemCompare(dir, dj->fn, 11) == enBooleanTrue) ) /* Is it a valid entry? */
		{
			break;
		}
#endif	// #if PF_FS_USE_LFN
		res = pfFsDirNext(dj, enBooleanFalse);		/* Next entry */
	} while (res == enStatusSuccess);

	return res;
}




/*-----------------------------------------------------------------------*/
/* Read an object from the directory                                     */
/*-----------------------------------------------------------------------*/
#if PF_FS_MINIMIZE <= 1
static PFEnStatus pfFsDirRead(PFFsDir *dj)
{
	PFEnStatus res;
	PFbyte c, *dir;
#if PF_FS_USE_LFN
	PFbyte a, ord = 0xFF, sum = 0xFF;
#endif	// #if PF_FS_USE_LFN

	res = FR_NO_FILE;
	while (dj->sect) 
	{
		res = pfFsMoveWindow(dj->fs, dj->sect);
		if (res != enStatusSuccess) 
		{
			break;
		}
		dir = dj->dir;					/* Ptr to the directory entry of current index */
		c = dir[DIR_Name];
		if (c == 0) 					/* Reached to end of table */
		{ 
			res = FR_NO_FILE; 
			break; 
		}	
#if PF_FS_USE_LFN	/* LFN configuration */
		a = dir[DIR_Attr] & AM_MASK;
		if (c == 0xE5 || (!PF_FS_RPATH && c == '.') || ((a & AM_VOL) && a != AM_LFN)) 	/* An entry without valid data */
		{	
			ord = 0xFF;
		} 
		else 
		{
			if (a == AM_LFN) 			/* An LFN entry is found */
			{	
				if (c & 0x40) 			/* Is it start of LFN sequence? */
				{
					sum = dir[LDIR_Chksum];
					c &= 0xBF; ord = c;
					dj->lfn_idx = dj->index;
				}
				/* Check LFN validity and capture it */
				ord = (c == ord && sum == dir[LDIR_Chksum] && pfFsPickLFN(dj->lfn, dir)) ? ord - 1 : 0xFF;
			} 
			else 						/* An SFN entry is found */
			{
				if (ord || sum != pfFsSumSFN(dir))	/* Is there a valid LFN? */
				{
					dj->lfn_idx = 0xFFFF;		/* It has no LFN. */
				}
				break;
			}
		}
#else		/* Non LFN configuration */
		if (c != 0xE5 && (PF_FS_RPATH || c != '.') && !(dir[DIR_Attr] & AM_VOL))	/* Is it a valid entry? */
		{
			break;
		}
#endif	// #if PF_FS_USE_LFN
		res = pfFsDirNext(dj, enBooleanFalse);				/* Next entry */
		if (res != enStatusSuccess) 
		{
			break;
		}
	}

	if (res != enStatusSuccess) 
	{
		dj->sect = 0;
	}

	return res;
}
#endif	// #if PF_FS_MINIMIZE <= 1



/*-----------------------------------------------------------------------*/
/* Register an object to the directory                                   */
/*-----------------------------------------------------------------------*/
#if !PF_FS_READONLY
static PFEnStatus pfFsDirRegister(PFFsDir *dj)
{
	PFEnStatus res;
	PFbyte c, *dir;
#if PF_FS_USE_LFN	/* LFN configuration */
	PFword n, ne, is;
	PFbyte sn[12], *fn, sum;
	PFword *lfn;

	fn = dj->fn; lfn = dj->lfn;
	pfMemCopy(sn, fn, 12);

	if (PF_FS_RPATH && (sn[NS] & NS_DOT)) 	/* Cannot create dot entry */
	{
		return FR_INVALID_NAME;	
	}

	if (sn[NS] & NS_LOSS) 			/* When LFN is out of 8.3 format, generate a numbered name */
	{
		fn[NS] = 0; dj->lfn = PF_NULL;			/* Find only SFN */
		for (n = 1; n < 100; n++) 
		{
			pfFsGetNumName(fn, sn, lfn, n);	/* Generate a numbered name */
			res = pfFsDirFind(dj);				/* Check if the name collides with existing SFN */
			if (res != enStatusSuccess) 
			{
				break;
			}
		}
		if (n == 100) 		/* Abort if too many collisions */
		{
			return FR_DENIED;
		}
		if (res != FR_NO_FILE) 	/* Abort if the result is other than 'not collided' */
		{
			return res;
		}
		fn[NS] = sn[NS]; dj->lfn = lfn;
	}

	if (sn[NS] & NS_LFN) 			/* When LFN is to be created, reserve reserve an SFN + LFN entries. */
	{
		for (ne = 0; lfn[ne]; ne++) ;
		ne = (ne + 25) / 13;
	} 
	else 							/* Otherwise reserve only an SFN entry. */
	{
		ne = 1;
	}

	/* Reserve contiguous entries */
	res = pfFsDirSeek(dj, 0);
	if (res != enStatusSuccess) 
	{
		return res;
	}
	n = is = 0;
	do 
	{
		res = pfFsMoveWindow(dj->fs, dj->sect);
		if (res != enStatusSuccess) 
		{
			break;
		}
		c = *dj->dir;				/* Check the entry status */
		if (c == 0xE5 || c == 0) 	/* Is it a blank entry? */
		{
			if (n == 0) 	/* First index of the contigulus entry */
			{
				is = dj->index;
			}
			if (++n == ne) 	/* A contiguous entry that requiered count is found */
			{
				break;
			}
		} 
		else 
		{
			n = 0;					/* Not a blank entry. Restart to search */
		}
		res = pfFsDirNext(dj, enBooleanTrue);	/* Next entry with table streach */
	} while (res == enStatusSuccess);

	if (res == enStatusSuccess && ne > 1) 	/* Initialize LFN entry if needed */
	{
		res = pfFsDirSeek(dj, is);
		if (res == enStatusSuccess) 
		{
			sum = pfFsSumSFN(dj->fn);	/* Sum of the SFN tied to the LFN */
			ne--;
			do 					/* Store LFN entries in bottom first */
			{
				res = pfFsMoveWindow(dj->fs, dj->sect);
				if (res != enStatusSuccess) 
				{
					break;
				}
				pfFsFitLFN(dj->lfn, dj->dir, (PFbyte)ne, sum);
				dj->fs->wflag = 1;
				res = pfFsDirNext(dj, enBooleanFalse);	/* Next entry */
			} while (res == enStatusSuccess && --ne);
		}
	}

#else	/* Non LFN configuration */
	res = pfFsDirSeek(dj, 0);
	if (res == enStatusSuccess) 
	{
		do	/* Find a blank entry for the SFN */
		{
			res = pfFsMoveWindow(dj->fs, dj->sect);
			if (res != enStatusSuccess) 
			{
				break;
			}
			c = *dj->dir;
			if (c == 0xE5 || c == 0) 	/* Is it a blank entry? */
			{
				break;
			}
			res = pfFsDirNext(dj, enBooleanTrue);		/* Next entry with table streach */
		} while (res == enStatusSuccess);
	}
#endif	// #if PF_FS_USE_LFN	

	if (res == enStatusSuccess) 		/* Initialize the SFN entry */
	{
		res = pfFsMoveWindow(dj->fs, dj->sect);
		if (res == enStatusSuccess) 
		{
			dir = dj->dir;
			pfMemSet(dir, 0, 32);		/* Clean the entry */
			pfMemCopy(dir, dj->fn, 11);	/* Put SFN */
			dir[DIR_NTres] = *(dj->fn+NS) & (NS_BODY | NS_EXT);	/* Put NT flag */
			dj->fs->wflag = 1;
		}
	}

	return res;
}
#endif /* !PF_FS_READONLY */

/*-----------------------------------------------------------------------*/
/* Remove an object from the directory                                   */
/*-----------------------------------------------------------------------*/
#if !PF_FS_READONLY && !PF_FS_MINIMIZE
static PFEnStatus pfFsDirRemove(PFFsDir *dj)
{
	PFEnStatus res;
#if PF_FS_USE_LFN	/* LFN configuration */
	PFword i;

	i = dj->index;	/* SFN index */
	res = pfFsDirSeek(dj, (PFword)((dj->lfn_idx == 0xFFFF) ? i : dj->lfn_idx));	/* Goto the SFN or top of the LFN entries */
	if (res == enStatusSuccess) 
	{
		do 
		{
			res = pfFsMoveWindow(dj->fs, dj->sect);
			if (res != enStatusSuccess) 
			{
				break;
			}
			*dj->dir = 0xE5;			/* Mark the entry "deleted" */
			dj->fs->wflag = 1;
			if (dj->index >= i) 			/* When reached SFN, all entries of the object has been deleted. */
			{
				break;
			}
			res = pfFsDirNext(dj, enBooleanFalse);	/* Next entry */
		} while (res == enStatusSuccess);
		if (res == FR_NO_FILE) 
		{
			res = FR_INT_ERR;
		}
	}
#else			/* Non LFN configuration */
	res = pfFsDirSeek(dj, dj->index);
	if (res == enStatusSuccess) 
	{
		res = pfFsMoveWindow(dj->fs, dj->sect);
		if (res == enStatusSuccess) 
		{
			*dj->dir = 0xE5;			/* Mark the entry "deleted" */
			dj->fs->wflag = 1;
		}
	}
#endif	// Non LFN configuration

	return res;
}
#endif /* !PF_FS_READONLY */

/*-----------------------------------------------------------------------*/
/* Pick a segment and create the object name in directory form           */
/*-----------------------------------------------------------------------*/

static PFEnStatus pfFsCreateName(PFFsDir *dj, const XCHAR **path)
{
#ifdef _EXCVT
	static const PFbyte cvt[] = _EXCVT;
#endif	// #ifdef _EXCVT

#if PF_FS_USE_LFN	/* LFN configuration */
	PFbyte b, cf;
	PFword w, *lfn;
	PFsdword i, ni, si, di;
	const XCHAR *p;

	/* Create LFN in Unicode */
	si = di = 0;
	p = *path;
	lfn = dj->lfn;
	for (;;) 
	{
		w = p[si++];					/* Get a character */
		if (w < ' ' || w == '/' || w == '\\') 	/* Break on end of segment */
		{
			break;
		}
		if (di >= PF_FS_MAX_LFN)				/* Reject too long name */
		{
			return FR_INVALID_NAME;
		}
#if !PF_FS_LFN_UNICODE
		w &= 0xFF;
		if (IsDBCS1(w)) 				/* If it is a DBC 1st byte */
		{
			b = p[si++];				/* Get 2nd byte */
			if (!IsDBCS2(b))			/* Reject invalid code for DBC */
			{
				return FR_INVALID_NAME;
			}
			w = (w << 8) + b;
		}
		w = ff_convert(w, 1);			/* Convert OEM to Unicode */
		if (!w) 						/* Reject invalid code */
		{
			return FR_INVALID_NAME;	
		}
#endif	// #if !PF_FS_LFN_UNICODE
		if (w < 0x80 && pfCheckChar("\"*:<>\?|\x7F", w)) /* Reject illegal chars for LFN */
		{
			return FR_INVALID_NAME;
		}
		lfn[di++] = w;					/* Store the Unicode char */
	}
	*path = &p[si];						/* Rerurn pointer to the next segment */
	cf = (w < ' ') ? NS_LAST : 0;		/* Set last segment flag if end of path */
#if PF_FS_RPATH
	if ((di == 1 && lfn[di - 1] == '.') || /* Is this a dot entry? */
		(di == 2 && lfn[di - 1] == '.' && lfn[di - 2] == '.')) 
	{
		lfn[di] = 0;
		for (i = 0; i < 11; i++)
		{
			dj->fn[i] = (i < di) ? '.' : ' ';
		}
		dj->fn[i] = cf | NS_DOT;		/* This is a dot entry */
		return enStatusSuccess;
	}
#endif	// #if PF_FS_RPATH
	while (di) 						/* Strip trailing spaces and dots */
	{
		w = lfn[di - 1];
		if (w != ' ' && w != '.') 
		{
			break;
		}
		di--;
	}
	if (!di) 			/* Reject null string */
	{
		return FR_INVALID_NAME;	
	}

	lfn[di] = 0;						/* LFN is created */

	/* Create SFN in directory form */
	pfMemSet(dj->fn, ' ', 11);
	for (si = 0; lfn[si] == ' ' || lfn[si] == '.'; si++) ;	/* Strip leading spaces and dots */
	if (si) 
	{
		cf |= NS_LOSS | NS_LFN;
	}
	while (di && lfn[di - 1] != '.') 	/* Find extension (di<=si: no extension) */
	{
		di--;
	}

	b = i = 0; ni = 8;
	for (;;) 
	{
		w = lfn[si++];					/* Get an LFN char */
		if (!w) 					/* Break on enf of the LFN */
		{
			break;
		}
		if (w == ' ' || (w == '.' && si != di)) 	/* Remove spaces and dots */
		{	
			cf |= NS_LOSS | NS_LFN; 
			continue;
		}

		if (i >= ni || si == di) 		/* Extension or end of SFN */
		{	
			if (ni == 11) 				/* Long extension */
			{	
				cf |= NS_LOSS | NS_LFN; 
				{
					break;
				}
			}
			if (si != di) 					/* Out of 8.3 format */
			{
				cf |= NS_LOSS | NS_LFN;
			}
			if (si > di) 			/* No extension */
			{
				break;
			}
			si = di; 
			i = 8; 
			ni = 11;	/* Enter extension section */
			b <<= 2; 
			continue;
		}

		if (w >= 0x80) 				/* Non ASCII char */
		{
#ifdef _EXCVT
			w = ff_convert(w, 0);		/* Unicode -> OEM code */
			if (w) 						/* Convert extended char to upper (SBCS) */
			{
				w = cvt[w - 0x80];	
			}
#else	// #ifdef _EXCVT
			w = ff_convert(ff_wtoupper(w), 0);	/* Upper converted Unicode -> OEM code */
#endif	// #ifdef _EXCVT
			cf |= NS_LFN;				/* Force create LFN entry */
		}

		if (_DF1S && w >= 0x100) 		/* Double byte char */
		{	
			if (i >= ni - 1) 
			{
				cf |= NS_LOSS | NS_LFN; i = ni; 
				continue;
			}
			dj->fn[i++] = (PFbyte)(w >> 8);
		} 
		else 							/* Single byte char */
		{
			if (!w || pfCheckChar("+,;[=]", w)) 		/* Replace illegal chars for SFN */
			{	
				w = '_'; 
				cf |= NS_LOSS | NS_LFN;	/* Lossy conversion */
			}
			else 
			{
				if (IsUpper(w)) 		/* ASCII large capital */
				{	
					b |= 2;
				}
				else 
				{
					if (IsLower(w)) 	/* ASCII small capital */
					{	
						b |= 1; 
						w -= 0x20;
					}
				}
			}
		}
		dj->fn[i++] = (PFbyte)w;
	}

	if (dj->fn[0] == 0xE5) 	/* If the first char collides with deleted mark, replace it with 0x05 */
	{
		dj->fn[0] = 0x05;
	}

	if (ni == 8) 
	{
		b <<= 2;
	}
	if ((b & 0x0C) == 0x0C || (b & 0x03) == 0x03)	/* Create LFN entry when there are composite capitals */
	{
		cf |= NS_LFN;
	}
	if (!(cf & NS_LFN)) 						/* When LFN is in 8.3 format without extended char, NT flags are created */
	{
		if ((b & 0x03) == 0x01) 	/* NT flag (Extension has only small capital) */
		{
			cf |= NS_EXT;
		}
		if ((b & 0x0C) == 0x04) 	/* NT flag (Filename has only small capital) */
		{
			cf |= NS_BODY;
		}
	}

	dj->fn[NS] = cf;	/* SFN is created */

	return enStatusSuccess;


#else	/* Non-LFN configuration */
	PFbyte b, c, d, *sfn;
	PFsdword ni, si, i;
	const PFchar *p;

	/* Create file name in directory form */
	sfn = dj->fn;
	pfMemSet(sfn, ' ', 11);
	si = i = b = 0; ni = 8;
	p = *path;
#if PF_FS_RPATH
	if (p[si] == '.')  /* Is this a dot entry? */
	{
		for (;;) 
		{
			c = p[si++];
			if (c != '.' || si >= 3) 
			{
				break;
			}
			sfn[i++] = c;
		}
		if (c != '/' && c != '\\' && c > ' ') 
		{	
			return FR_INVALID_NAME;
		}
		*path = &p[si];									/* Rerurn pointer to the next segment */
		sfn[NS] = (c <= ' ') ? NS_LAST | NS_DOT : NS_DOT;	/* Set last segment flag if end of path */
		return enStatusSuccess;
	}
#endif	// #if PF_FS_RPATH
	for (;;) 
	{
		c = p[si++];
		if (c <= ' ' || c == '/' || c == '\\') 	/* Break on end of segment */
		{
			break;
		}
		if (c == '.' || i >= ni) 
		{
			if (ni != 8 || c != '.') 
			{
				return FR_INVALID_NAME;
			}
			i = 8; 
			ni = 11;
			b <<= 2; 
			continue;
		}
		if (c >= 0x80) 				/* Extended char */
		{
#ifdef _EXCVT
			c = cvt[c - 0x80];			/* Convert extend char (SBCS) */
#else	// #ifdef _EXCVT
			b |= 3;						/* Eliminate NT flag if ext char is exist */
#if !_DF1S	/* ASCII only cfg */
			return FR_INVALID_NAME;
#endif	// #if !_DF1S
#endif	// #ifdef _EXCVT
		}
		if (IsDBCS1(c)) 				/* DBC 1st byte? */
		{	
			d = p[si++];				/* Get 2nd byte */
			if (!IsDBCS2(d) || i >= ni - 1)	/* Reject invalid DBC */
				return FR_INVALID_NAME;
			sfn[i++] = c;
			sfn[i++] = d;
		} 
		else 							/* Single byte code */
		{
			if (pfCheckChar(" \"*+,[=]|\x7F", c))	/* Reject illegal chrs for SFN */
			{
				return FR_INVALID_NAME;
			}
			if (IsUpper(c)) 			/* ASCII large capital? */
			{	
				b |= 2;
			}
			else 
			{
				if (IsLower(c)) 		/* ASCII small capital? */
				{
					b |= 1; 
					c -= 0x20;
				}
			}
			sfn[i++] = c;
		}
	}
	*path = &p[si];						/* Rerurn pointer to the next segment */
	c = (c <= ' ') ? NS_LAST : 0;		/* Set last segment flag if end of path */

	if (!i) 							/* Reject null string */
	{
		return FR_INVALID_NAME;		
	}
	if (sfn[0] == 0xE5) 	/* When first char collides with 0xE5, replace it with 0x05 */
	{
		sfn[0] = 0x05;
	}

	if (ni == 8) 
	{
		b <<= 2;
	}
	if ((b & 0x03) == 0x01) 	/* NT flag (Extension has only small capital) */
	{
		c |= NS_EXT;
	}
	if ((b & 0x0C) == 0x04) 	/* NT flag (Filename has only small capital) */
	{
		c |= NS_BODY;
	}

	sfn[NS] = c;		/* Store NT flag, File name is created */

	return enStatusSuccess;
#endif
}




/*-----------------------------------------------------------------------*/
/* Get file information from directory entry                             */
/*-----------------------------------------------------------------------*/
#if PF_FS_MINIMIZE <= 1
static void pfFsGetFileInfo(PFFsDir *dj, PFFsFileInfo *fno)
{
	PFsdword i;
	PFbyte c, nt, *dir;
	PFchar *p;

	p = fno->fname;
	if (dj->sect) {
		dir = dj->dir;
		nt = dir[DIR_NTres];		/* NT flag */
		for (i = 0; i < 8; i++) {	/* Copy name body */
			c = dir[i];
			if (c == ' ') break;
			if (c == 0x05) c = 0xE5;
			if (PF_FS_USE_LFN && (nt & NS_BODY) && IsUpper(c)) c += 0x20;
			*p++ = c;
		}
		if (dir[8] != ' ') {		/* Copy name extension */
			*p++ = '.';
			for (i = 8; i < 11; i++) {
				c = dir[i];
				if (c == ' ') break;
				if (PF_FS_USE_LFN && (nt & NS_EXT) && IsUpper(c)) c += 0x20;
				*p++ = c;
			}
		}
		fno->fattrib = dir[DIR_Attr];				/* Attribute */
		fno->fsize = LD_DWORD(dir+DIR_FileSize);	/* Size */
		fno->fdate = LD_WORD(dir+DIR_WrtDate);		/* Date */
		fno->ftime = LD_WORD(dir+DIR_WrtTime);		/* Time */
	}
	*p = 0;

#if PF_FS_USE_LFN
	if (fno->lfname) {
		XCHAR *tp = fno->lfname;
		PFword w, *lfn;

		i = 0;
		if (dj->sect && dj->lfn_idx != 0xFFFF) {/* Get LFN if available */
			lfn = dj->lfn;
			while ((w = *lfn++) != 0) {			/* Get an LFN char */
#if !PF_FS_LFN_UNICODE
				w = ff_convert(w, 0);			/* Unicode -> OEM conversion */
				if (!w) { i = 0; break; }		/* Could not convert, no LFN */
				if (_DF1S && w >= 0x100)		/* Put 1st byte if it is a DBC */
					tp[i++] = (XCHAR)(w >> 8);
#endif
				if (i >= fno->lfsize - 1) { i = 0; break; }	/* Buffer overrun, no LFN */
				tp[i++] = (XCHAR)w;
			}
		}
		tp[i] = 0;	/* Terminator */
	}
#endif
}
#endif /* PF_FS_MINIMIZE <= 1 */




/*-----------------------------------------------------------------------*/
/* Follow a file path                                                    */
/*-----------------------------------------------------------------------*/

static PFEnStatus pfFsFollowPath(PFFsDir *dj, const XCHAR *path)
{
	PFEnStatus res;
	PFbyte *dir, last;

	while (!PF_FS_USE_LFN && *path == ' ') path++;	/* Skip leading spaces */
#if PF_FS_RPATH
	if (*path == '/' || *path == '\\') { /* There is a heading separator */
		path++;	dj->sclust = 0;		/* Strip it and start from the root dir */
	} else {							/* No heading saparator */
		dj->sclust = dj->fs->cdir;	/* Start from the current dir */
	}
#else
	if (*path == '/' || *path == '\\')	/* Strip heading separator if exist */
		path++;
	dj->sclust = 0;						/* Start from the root dir */
#endif

	if ((PFdword)*path < ' ') {			/* Null path means the start directory itself */
		res = pfFsDirSeek(dj, 0);
		dj->dir = PF_NULL;

	} else {							/* Follow path */
		for (;;) {
			res = pfFsCreateName(dj, &path);	/* Get a segment */
			if (res != enStatusSuccess) break;
			res = pfFsDirFind(dj);				/* Find it */
			last = *(dj->fn+NS) & NS_LAST;
			if (res != enStatusSuccess) {				/* Could not find the object */
				if (res == FR_NO_FILE && !last)
					res = FR_NO_PATH;
				break;
			}
			if (last) break;				/* Last segment match. Function completed. */
			dir = dj->dir;					/* There is next segment. Follow the sub directory */
			if (!(dir[DIR_Attr] & AM_DIR)) { /* Cannot follow because it is a file */
				res = FR_NO_PATH; break;
			}
			dj->sclust = ((PFdword)LD_WORD(dir+DIR_FstClusHI) << 16) | LD_WORD(dir+DIR_FstClusLO);
		}
	}

	return res;
}




/*-----------------------------------------------------------------------*/
/* Load boot record and check if it is an FAT boot record                */
/*-----------------------------------------------------------------------*/

static PFbyte pfFsCheckFs(PFFatFs *fs, PFdword sect)
{
	if (pfDiskRead(fs->drive, fs->win, sect, 1) != enStatusSuccess)	/* Load boot record */
		return 3;
	if (LD_WORD(&fs->win[BS_55AA]) != 0xAA55)		/* Check record signature (always placed at offset 510 even if the sector size is >512) */
		return 2;

	if ((LD_DWORD(&fs->win[BS_FilSysType]) & 0xFFFFFF) == 0x544146)	/* Check "FAT" string */
		return 0;
	if ((LD_DWORD(&fs->win[BS_FilSysType32]) & 0xFFFFFF) == 0x544146)
		return 0;

	return 1;
}




/*-----------------------------------------------------------------------*/
/* Make sure that the file system is valid                               */
/*-----------------------------------------------------------------------*/

static PFEnStatus pfFsCheckMounted(const XCHAR **path, PFFatFs **rfs, PFbyte chk_wp)
{
	PFbyte fmt, *tbl;
	PFdword vol;
	PFEnStatus stat;
	PFdword bsect, fsize, tsect, mclst;
	const XCHAR *p = *path;
	PFFatFs *fs;

	/* Get logical drive number from the path name */
	vol = p[0] - 'A';				/* Is there a drive number? */
	if (vol <= 9 && p[1] == ':') {	/* Found a drive number, get and strip it */
		p += 2; *path = p;			/* Return pointer to the path name */
	} else {						/* No drive number is given */
#if PF_FS_RPATH
		vol = Drive;				/* Use current drive */
#else
		vol = 0;					/* Use drive 0 */
#endif
	}

	/* Check if the logical drive is valid or not */
	if (vol >= PF_FS_MAX_DRIVES) 			/* Is the drive number valid? */
		return FR_INVALID_DRIVE;
	*rfs = fs = FatFs[vol];			/* Returen pointer to the corresponding file system object */
	if (!fs) return FR_NOT_ENABLED;	/* Is the file system object available? */

	ENTER_FF(fs);					/* Lock file system */

	if (fs->fs_type) {				/* If the logical drive has been mounted */
		stat = pfDiskGetStatus(fs->drive);
		if (stat != enStatusNotConfigured) 			/* and the physical drive is kept initialized (has not been changed), */
		{
#if !PF_FS_READONLY
			if (chk_wp && (stat == enStatusWriteProtected))	/* Check write protection if needed */
			{	
				return enStatusWriteProtected;
			}
#endif
			return enStatusSuccess;			/* The file system object is valid */
		}
	}

	/* The logical drive must be mounted. Following code attempts to mount the volume */

	fs->fs_type = 0;					/* Clear the file system object */
//	fs->drive = (PFbyte)LD2PD(vol);		/* Bind the logical drive and a physical drive */
	stat = pfDiskInit(fs->drive);	/* Initialize low level disk I/O layer */
	if (stat != enStatusSuccess)				/* Check if the drive is ready */
	{
		return FR_NOT_READY;
	}
#if PF_FS_MAX_SS != 512						/* Get disk sector size if needed */
	if ( (pfDiskIoCtrl(fs->drive, GET_SECTOR_SIZE, &SS(fs)) != enStatusSuccess) || (SS(fs) > PF_FS_MAX_SS) )
	{
		return FR_NO_FILESYSTEM;
	}
#endif
#if !PF_FS_READONLY
	if (chk_wp && (stat == enStatusWriteProtected))	/* Check disk write protection if needed */
		return enStatusWriteProtected;
#endif
	/* Search FAT partition on the drive */
	fmt = pfFsCheckFs(fs, bsect = 0);		/* Check sector 0 as an SFD format */
	if (fmt == 1) 							/* Not an FAT boot record, it may be patitioned */
	{
		/* Check a partition listed in top of the partition table */
		tbl = &fs->win[MBR_Table + /*LD2PT(vol)*/0 * 16];	/* Partition table */
		if (tbl[4]) 									/* Is the partition existing? */
		{
			bsect = LD_DWORD(&tbl[8]);					/* Partition offset in LBA */
			fmt = pfFsCheckFs(fs, bsect);					/* Check the partition */
		}
	}
	if (fmt == 3) 
	{
		return FR_DISK_ERR;
	}
	if (fmt || LD_WORD(fs->win+BPB_BytsPerSec) != SS(fs))	/* No valid FAT patition is found */
	{
		return FR_NO_FILESYSTEM;
	}

	/* Initialize the file system object */
	fsize = LD_WORD(fs->win+BPB_FATSz16);				/* Number of sectors per FAT */
	if (!fsize) 
	{
		fsize = LD_DWORD(fs->win+BPB_FATSz32);
	}
	fs->sects_fat = fsize;
	fs->n_fats = fs->win[BPB_NumFATs];					/* Number of FAT copies */
	fsize *= fs->n_fats;								/* (Number of sectors in FAT area) */
	fs->fatbase = bsect + LD_WORD(fs->win+BPB_RsvdSecCnt); /* FAT start sector (lba) */
	fs->csize = fs->win[BPB_SecPerClus];				/* Number of sectors per cluster */
	fs->n_rootdir = LD_WORD(fs->win+BPB_RootEntCnt);	/* Nmuber of root directory entries */
	tsect = LD_WORD(fs->win+BPB_TotSec16);				/* Number of sectors on the volume */
	if (!tsect) 
	{
		tsect = LD_DWORD(fs->win+BPB_TotSec32);
	}
	fs->max_clust = mclst = (tsect						/* Last cluster# + 1 (Number of clusters + 2) */
		- LD_WORD(fs->win+BPB_RsvdSecCnt) - fsize - fs->n_rootdir / (SS(fs)/32)
		) / fs->csize + 2;

	fmt = FS_FAT12;						/* Determine the FAT sub type */
	if (mclst >= 0xFF7) 				/* Number of clusters >= 0xFF5 */
	{
		fmt = FS_FAT16;					
	}
	if (mclst >= 0xFFF7) 				/* Number of clusters >= 0xFFF5 */
	{
		fmt = FS_FAT32;				
	}

	if (fmt == FS_FAT32)
	{
		fs->dirbase = LD_DWORD(fs->win+BPB_RootClus);	/* Root directory start cluster */
	}
	else
	{
		fs->dirbase = fs->fatbase + fsize;				/* Root directory start sector (lba) */
	}
	fs->database = fs->fatbase + fsize + fs->n_rootdir / (SS(fs)/32);	/* Data start sector (lba) */

#if !PF_FS_READONLY
	/* Initialize allocation information */
	fs->free_clust = 0xFFFFFFFF;
	fs->wflag = 0;
	/* Get fsinfo if needed */
	if (fmt == FS_FAT32) 
	{
	 	fs->fsi_flag = 0;
		fs->fsi_sector = bsect + LD_WORD(fs->win+BPB_FSInfo);
		if (pfDiskRead(fs->drive, fs->win, fs->fsi_sector, 1) == enStatusSuccess &&
			LD_WORD(fs->win+BS_55AA) == 0xAA55 &&
			LD_DWORD(fs->win+FSI_LeadSig) == 0x41615252 &&
			LD_DWORD(fs->win+FSI_StrucSig) == 0x61417272) {
			fs->last_clust = LD_DWORD(fs->win+FSI_Nxt_Free);
			fs->free_clust = LD_DWORD(fs->win+FSI_Free_Count);
		}
	}
#endif
	fs->fs_type = fmt;		/* FAT sub-type */
	fs->winsect = 0;		/* Invalidate sector cache */
#if PF_FS_RPATH
	fs->cdir = 0;			/* Current directory (root dir) */
#endif
	fs->id = ++Fsid;		/* File system mount ID */

	return enStatusSuccess;
}




/*-----------------------------------------------------------------------*/
/* Check if the file/dir object is valid or not                          */
/*-----------------------------------------------------------------------*/

static PFEnStatus pfFsValidate(PFFatFs *fs, PFword id)
{
	if (!fs || !fs->fs_type || fs->id != id)
	{
		return FR_INVALID_OBJECT;
	}
	ENTER_FF(fs);		/* Lock file system */

	return pfDiskGetStatus(fs->drive);
}


/*--------------------------------------------------------------------------

   Public Functions

--------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------*/
/* Mount/Unmount a Locical Drive                                         */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsMount(PFbyte drive, PFFatFs *fs)
{
	PFFatFs *rfs;

	if (drive >= PF_FS_MAX_DRIVES)				/* Check if the drive number is valid */
		return FR_INVALID_DRIVE;
	rfs = FatFs[drive];				/* Get current fs object */

	if (rfs) {
#if PF_FS_REENTRANT					/* Discard sync object of the current volume */
		if (!ff_del_syncobj(rfs->sobj)) return FR_INT_ERR;
#endif
		rfs->fs_type = 0;			/* Clear old fs object */
	}

	if (fs) {
		fs->fs_type = 0;			/* Clear new fs object */
#if PF_FS_REENTRANT					/* Create sync object for the new volume */
		if (!ff_cre_syncobj(vol, &fs->sobj)) return FR_INT_ERR;
#endif
		fs->drive = drive;
	}
	FatFs[drive] = fs;				/* Register new fs object */

	return enStatusSuccess;
}


/*-----------------------------------------------------------------------*/
/* Open or Create a File                                                 */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsFileOpen(PFFsFile *fp, const XCHAR *path, PFbyte mode)
{
	PFEnStatus res;
	PFFsDir dj;
	NAMEBUF(sfn, lfn);
	PFbyte *dir;
	
	fp->fs = PF_NULL;		/* Clear file object */
#if !PF_FS_READONLY
	mode &= (FA_READ | FA_WRITE | FA_CREATE_ALWAYS | FA_OPEN_ALWAYS | FA_CREATE_NEW);
	res = pfFsCheckMounted(&path, &dj.fs, (PFbyte)(mode & (FA_WRITE | FA_CREATE_ALWAYS | FA_OPEN_ALWAYS | FA_CREATE_NEW)));
#else
	mode &= FA_READ;
	res = pfFsCheckMounted(&path, &dj.fs, 0);
#endif
	if (res != enStatusSuccess) LEAVE_FF(dj.fs, res);
	INITBUF(dj, sfn, lfn);
	res = pfFsFollowPath(&dj, path);	/* Follow the file path */

#if !PF_FS_READONLY
	/* Create or Open a file */
	if (mode & (FA_CREATE_ALWAYS | FA_OPEN_ALWAYS | FA_CREATE_NEW)) {
		PFdword ps, cl;

		if (res != enStatusSuccess) {			/* No file, create new */
			if (res == FR_NO_FILE)	/* There is no file to open, create a new entry */
				res = pfFsDirRegister(&dj);
			if (res != enStatusSuccess) LEAVE_FF(dj.fs, res);
			mode |= FA_CREATE_ALWAYS;
			dir = dj.dir;			/* Created entry (SFN entry) */
		}
		else {						/* Any object is already existing */
			if (mode & FA_CREATE_NEW)			/* Cannot create new */
				LEAVE_FF(dj.fs, FR_EXIST);
			dir = dj.dir;
			if (!dir || (dir[DIR_Attr] & (AM_RDO | AM_DIR)))	/* Cannot overwrite it (R/O or PFFsDir) */
				LEAVE_FF(dj.fs, FR_DENIED);
			if (mode & FA_CREATE_ALWAYS) {		/* Resize it to zero on over write mode */
				cl = ((PFdword)LD_WORD(dir+DIR_FstClusHI) << 16) | LD_WORD(dir+DIR_FstClusLO);	/* Get start cluster */
				ST_WORD(dir+DIR_FstClusHI, 0);	/* cluster = 0 */
				ST_WORD(dir+DIR_FstClusLO, 0);
				ST_DWORD(dir+DIR_FileSize, 0);	/* size = 0 */
				dj.fs->wflag = 1;
				ps = dj.fs->winsect;			/* Remove the cluster chain */
				if (cl) {
					res = pfFsRemoveChain(dj.fs, cl);
					if (res) LEAVE_FF(dj.fs, res);
					dj.fs->last_clust = cl - 1;	/* Reuse the cluster hole */
				}
				res = pfFsMoveWindow(dj.fs, ps);
				if (res != enStatusSuccess) LEAVE_FF(dj.fs, res);
			}
		}
		if (mode & FA_CREATE_ALWAYS) {
			dir[DIR_Attr] = 0;					/* Reset attribute */
			ps = get_fattime();
			ST_DWORD(dir+DIR_CrtTime, ps);		/* Created time */
			dj.fs->wflag = 1;
			mode |= FA__WRITTEN;				/* Set file changed flag */
		}
	}
	/* Open an existing file */
	else {
#endif /* !PF_FS_READONLY */
		if (res != enStatusSuccess) LEAVE_FF(dj.fs, res);	/* Follow failed */
		dir = dj.dir;
		if (!dir || (dir[DIR_Attr] & AM_DIR))	/* It is a directory */
			LEAVE_FF(dj.fs, FR_NO_FILE);
#if !PF_FS_READONLY
		if ((mode & FA_WRITE) && (dir[DIR_Attr] & AM_RDO)) /* R/O violation */
			LEAVE_FF(dj.fs, FR_DENIED);
	}
	fp->dir_sect = dj.fs->winsect;		/* Pointer to the directory entry */
	fp->dir_ptr = dj.dir;
#endif
	fp->flag = mode;					/* File access mode */
	fp->org_clust =						/* File start cluster */
		((PFdword)LD_WORD(dir+DIR_FstClusHI) << 16) | LD_WORD(dir+DIR_FstClusLO);
	fp->fsize = LD_DWORD(dir+DIR_FileSize);	/* File size */
	fp->fptr = 0; fp->csect = 255;		/* File pointer */
	fp->dsect = 0;
	fp->fs = dj.fs; fp->id = dj.fs->id;	/* Owner file system object of the file */

	LEAVE_FF(dj.fs, enStatusSuccess);
}




/*-----------------------------------------------------------------------*/
/* Read File                                                             */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsFileRead(PFFsFile *fp, void *pBuf, PFdword btr, PFdword *br)
{
	PFEnStatus res;
	PFdword clst, sect, remain;
	PFdword rcnt, cc;
	PFbyte *rbuff = pBuf;

	*br = 0;	/* Initialize bytes read */

	res = pfFsValidate(fp->fs, fp->id);					/* Check validity of the object */
	if (res != enStatusSuccess) LEAVE_FF(fp->fs, res);
	if (fp->flag & FA__ERROR)						/* Check abort flag */
		LEAVE_FF(fp->fs, FR_INT_ERR);
	if (!(fp->flag & FA_READ)) 						/* Check access mode */
		LEAVE_FF(fp->fs, FR_DENIED);
	remain = fp->fsize - fp->fptr;
	if (btr > remain) btr = (PFdword)remain;			/* Truncate btr by remaining bytes */

	for ( ;  btr;									/* Repeat until all data transferred */
		rbuff += rcnt, fp->fptr += rcnt, *br += rcnt, btr -= rcnt) {
		if ((fp->fptr % SS(fp->fs)) == 0) {			/* On the sector boundary? */
			if (fp->csect >= fp->fs->csize) {		/* On the cluster boundary? */
				clst = (fp->fptr == 0) ?			/* On the top of the file? */
					fp->org_clust : pfFsGetFat(fp->fs, fp->curr_clust);
				if (clst <= 1) ABORT(fp->fs, FR_INT_ERR);
				if (clst == 0xFFFFFFFF) ABORT(fp->fs, FR_DISK_ERR);
				fp->curr_clust = clst;				/* Update current cluster */
				fp->csect = 0;						/* Reset sector offset in the cluster */
			}
			sect = pfFsClustToSect(fp->fs, fp->curr_clust);	/* Get current sector */
			if (!sect) ABORT(fp->fs, FR_INT_ERR);
			sect += fp->csect;
			cc = btr / SS(fp->fs);					/* When remaining bytes >= sector size, */
			if (cc) {								/* Read maximum contiguous sectors directly */
				if (fp->csect + cc > fp->fs->csize)	/* Clip at cluster boundary */
					cc = fp->fs->csize - fp->csect;
				if (pfDiskRead(fp->fs->drive, rbuff, sect, (PFbyte)cc) != enStatusSuccess)
					ABORT(fp->fs, FR_DISK_ERR);
#if !PF_FS_READONLY && PF_FS_MINIMIZE <= 2
#if PF_FS_TINY
				if (fp->fs->wflag && fp->fs->winsect - sect < cc)		/* Replace one of the read sectors with cached data if it contains a dirty sector */
					pfMemCopy(rbuff + ((fp->fs->winsect - sect) * SS(fp->fs)), fp->fs->win, SS(fp->fs));
#else
				if ((fp->flag & FA__DIRTY) && fp->dsect - sect < cc)	/* Replace one of the read sectors with cached data if it contains a dirty sector */
					pfMemCopy(rbuff + ((fp->dsect - sect) * SS(fp->fs)), fp->buf, SS(fp->fs));
#endif
#endif
				fp->csect += (PFbyte)cc;				/* Next sector address in the cluster */
				rcnt = SS(fp->fs) * cc;				/* Number of bytes transferred */
				continue;
			}
#if !PF_FS_TINY
#if !PF_FS_READONLY
			if (fp->flag & FA__DIRTY) {			/* Write sector I/O buffer if needed */
				if (pfDiskWrite(fp->fs->drive, fp->buf, fp->dsect, 1) != enStatusSuccess)
					ABORT(fp->fs, FR_DISK_ERR);
				fp->flag &= ~FA__DIRTY;
			}
#endif
			if (fp->dsect != sect) {			/* Fill sector buffer with file data */
				if (pfDiskRead(fp->fs->drive, fp->buf, sect, 1) != enStatusSuccess)
					ABORT(fp->fs, FR_DISK_ERR);
			}
#endif
			fp->dsect = sect;
			fp->csect++;							/* Next sector address in the cluster */
		}
		rcnt = SS(fp->fs) - (fp->fptr % SS(fp->fs));	/* Get partial sector data from sector buffer */
		if (rcnt > btr) rcnt = btr;
#if PF_FS_TINY
		if (pfFsMoveWindow(fp->fs, fp->dsect))			/* Move sector window */
			ABORT(fp->fs, FR_DISK_ERR);
		pfMemCopy(rbuff, &fp->fs->win[fp->fptr % SS(fp->fs)], rcnt);	/* Pick partial sector */
#else
		pfMemCopy(rbuff, &fp->buf[fp->fptr % SS(fp->fs)], rcnt);	/* Pick partial sector */
#endif
	}

	LEAVE_FF(fp->fs, enStatusSuccess);
}




#if !PF_FS_READONLY
/*-----------------------------------------------------------------------*/
/* Write File                                                            */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsFileWrite(PFFsFile *fp, const void *pBuf, PFdword btw, PFdword *bw)
{
	PFEnStatus res;
	PFdword clst, sect;
	PFdword wcnt, cc;
	const PFbyte *wbuff = pBuf;

	*bw = 0;	/* Initialize bytes written */

	res = pfFsValidate(fp->fs, fp->id);					/* Check validity of the object */
	if (res != enStatusSuccess) LEAVE_FF(fp->fs, res);
	if (fp->flag & FA__ERROR)						/* Check abort flag */
		LEAVE_FF(fp->fs, FR_INT_ERR);
	if (!(fp->flag & FA_WRITE))						/* Check access mode */
		LEAVE_FF(fp->fs, FR_DENIED);
	if (fp->fsize + btw < fp->fsize) btw = 0;		/* File size cannot reach 4GB */

	for ( ;  btw;									/* Repeat until all data transferred */
		wbuff += wcnt, fp->fptr += wcnt, *bw += wcnt, btw -= wcnt) {
		if ((fp->fptr % SS(fp->fs)) == 0) {			/* On the sector boundary? */
			if (fp->csect >= fp->fs->csize) {		/* On the cluster boundary? */
				if (fp->fptr == 0) {				/* On the top of the file? */
					clst = fp->org_clust;			/* Follow from the origin */
					if (clst == 0)					/* When there is no cluster chain, */
						fp->org_clust = clst = pfFsCreateChain(fp->fs, 0);	/* Create a new cluster chain */
				} else {							/* Middle or end of the file */
					clst = pfFsCreateChain(fp->fs, fp->curr_clust);			/* Follow or streach cluster chain */
				}
				if (clst == 0) break;				/* Could not allocate a new cluster (disk full) */
				if (clst == 1) ABORT(fp->fs, FR_INT_ERR);
				if (clst == 0xFFFFFFFF) ABORT(fp->fs, FR_DISK_ERR);
				fp->curr_clust = clst;				/* Update current cluster */
				fp->csect = 0;						/* Reset sector address in the cluster */
			}
#if PF_FS_TINY
			if (fp->fs->winsect == fp->dsect && pfFsMoveWindow(fp->fs, 0))	/* Write back data buffer prior to following direct transfer */
				ABORT(fp->fs, FR_DISK_ERR);
#else
			if (fp->flag & FA__DIRTY) {		/* Write back data buffer prior to following direct transfer */
				if (pfDiskWrite(fp->fs->drive, fp->buf, fp->dsect, 1) != enStatusSuccess)
					ABORT(fp->fs, FR_DISK_ERR);
				fp->flag &= ~FA__DIRTY;
			}
#endif
			sect = pfFsClustToSect(fp->fs, fp->curr_clust);	/* Get current sector */
			if (!sect) ABORT(fp->fs, FR_INT_ERR);
			sect += fp->csect;
			cc = btw / SS(fp->fs);					/* When remaining bytes >= sector size, */
			if (cc) {								/* Write maximum contiguous sectors directly */
				if (fp->csect + cc > fp->fs->csize)	/* Clip at cluster boundary */
					cc = fp->fs->csize - fp->csect;
				if (pfDiskWrite(fp->fs->drive, wbuff, sect, (PFbyte)cc) != enStatusSuccess)
					ABORT(fp->fs, FR_DISK_ERR);
#if PF_FS_TINY
				if (fp->fs->winsect - sect < cc) {	/* Refill sector cache if it gets dirty by the direct write */
					pfMemCopy(fp->fs->win, wbuff + ((fp->fs->winsect - sect) * SS(fp->fs)), SS(fp->fs));
					fp->fs->wflag = 0;
				}
#else
				if (fp->dsect - sect < cc) {		/* Refill sector cache if it gets dirty by the direct write */
					pfMemCopy(fp->buf, wbuff + ((fp->dsect - sect) * SS(fp->fs)), SS(fp->fs));
					fp->flag &= ~FA__DIRTY;
				}
#endif
				fp->csect += (PFbyte)cc;				/* Next sector address in the cluster */
				wcnt = SS(fp->fs) * cc;				/* Number of bytes transferred */
				continue;
			}
#if PF_FS_TINY
			if (fp->fptr >= fp->fsize) {			/* Avoid silly buffer filling at growing edge */
				if (pfFsMoveWindow(fp->fs, 0)) ABORT(fp->fs, FR_DISK_ERR);
				fp->fs->winsect = sect;
			}
#else
			if (fp->dsect != sect) {				/* Fill sector buffer with file data */
				if (fp->fptr < fp->fsize &&
					pfDiskRead(fp->fs->drive, fp->buf, sect, 1) != enStatusSuccess)
						ABORT(fp->fs, FR_DISK_ERR);
			}
#endif
			fp->dsect = sect;
			fp->csect++;							/* Next sector address in the cluster */
		}
		wcnt = SS(fp->fs) - (fp->fptr % SS(fp->fs));	/* Put partial sector into file I/O buffer */
		if (wcnt > btw) wcnt = btw;
#if PF_FS_TINY
		if (pfFsMoveWindow(fp->fs, fp->dsect))			/* Move sector window */
			ABORT(fp->fs, FR_DISK_ERR);
		pfMemCopy(&fp->fs->win[fp->fptr % SS(fp->fs)], wbuff, wcnt);	/* Fit partial sector */
		fp->fs->wflag = 1;
#else
		pfMemCopy(&fp->buf[fp->fptr % SS(fp->fs)], wbuff, wcnt);	/* Fit partial sector */
		fp->flag |= FA__DIRTY;
#endif
	}

	if (fp->fptr > fp->fsize) fp->fsize = fp->fptr;	/* Update file size if needed */
	fp->flag |= FA__WRITTEN;						/* Set file changed flag */

	LEAVE_FF(fp->fs, enStatusSuccess);
}




/*-----------------------------------------------------------------------*/
/* Synchronize the File Object                                           */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsFileSync(PFFsFile *fp)
{
	PFEnStatus res;
	PFdword tim;
	PFbyte *dir;

	res = pfFsValidate(fp->fs, fp->id);		/* Check validity of the object */
	if (res == enStatusSuccess) {
		if (fp->flag & FA__WRITTEN) {	/* Has the file been written? */
#if !PF_FS_TINY	/* Write-back dirty buffer */
			if (fp->flag & FA__DIRTY) {
				if (pfDiskWrite(fp->fs->drive, fp->buf, fp->dsect, 1) != enStatusSuccess)
					LEAVE_FF(fp->fs, FR_DISK_ERR);
				fp->flag &= ~FA__DIRTY;
			}
#endif
			/* Update the directory entry */
			res = pfFsMoveWindow(fp->fs, fp->dir_sect);
			if (res == enStatusSuccess) {
				dir = fp->dir_ptr;
				dir[DIR_Attr] |= AM_ARC;					/* Set archive bit */
				ST_DWORD(dir+DIR_FileSize, fp->fsize);		/* Update file size */
				ST_WORD(dir+DIR_FstClusLO, fp->org_clust);	/* Update start cluster */
				ST_WORD(dir+DIR_FstClusHI, fp->org_clust >> 16);
				tim = get_fattime();			/* Updated time */
				ST_DWORD(dir+DIR_WrtTime, tim);
				fp->flag &= ~FA__WRITTEN;
				fp->fs->wflag = 1;
				res = pfFsSync(fp->fs);
			}
		}
	}

	LEAVE_FF(fp->fs, res);
}

#endif /* !PF_FS_READONLY */




/*-----------------------------------------------------------------------*/
/* Close File                                                            */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsFileClose(PFFsFile *fp)
{
	PFEnStatus res;

#if PF_FS_READONLY
	res = pfFsValidate(fp->fs, fp->id);
	if (res == enStatusSuccess) fp->fs = PF_NULL;
	LEAVE_FF(fp->fs, res);
#else
	res = pfFsFileSync(fp);
	if (res == enStatusSuccess) fp->fs = PF_NULL;
	return res;
#endif
}

/*-----------------------------------------------------------------------*/
/* Change Current Drive/Directory                                        */
/*-----------------------------------------------------------------------*/

#if PF_FS_RPATH

PFEnStatus pfFsChangeDrive(PFbyte drv)
{
	if (drv >= PF_FS_MAX_DRIVES) return FR_INVALID_DRIVE;

	Drive = drv;

	return enStatusSuccess;
}

PFEnStatus pfFsChangeDirectory(const XCHAR *path)
{
	PFEnStatus res;
	PFFsDir dj;
	NAMEBUF(sfn, lfn);
	PFbyte *dir;

	res = pfFsCheckMounted(&path, &dj.fs, 0);
	if (res == enStatusSuccess) {
		INITBUF(dj, sfn, lfn);
		res = pfFsFollowPath(&dj, path);		/* Follow the file path */
		if (res == enStatusSuccess) {					/* Follow completed */
			dir = dj.dir;					/* Pointer to the entry */
			if (!dir) {
				dj.fs->cdir = 0;			/* No entry (root dir) */
			} else {
				if (dir[DIR_Attr] & AM_DIR)	/* Reached to the dir */
					dj.fs->cdir = ((PFdword)LD_WORD(dir+DIR_FstClusHI) << 16) | LD_WORD(dir+DIR_FstClusLO);
				else
					res = FR_NO_PATH;		/* Could not reach the dir (it is a file) */
			}
		}
		if (res == FR_NO_FILE) res = FR_NO_PATH;
	}

	LEAVE_FF(dj.fs, res);
}

#endif


#if PF_FS_MINIMIZE <= 2
/*-----------------------------------------------------------------------*/
/* Seek File R/W Pointer                                                 */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsFileSeek(PFFsFile *fp, PFdword ofs)
{
	PFEnStatus res;
	PFdword clst, bcs, nsect, ifptr;

	res = pfFsValidate(fp->fs, fp->id);		/* Check validity of the object */
	if (res != enStatusSuccess) LEAVE_FF(fp->fs, res);
	if (fp->flag & FA__ERROR)			/* Check abort flag */
		LEAVE_FF(fp->fs, FR_INT_ERR);
	if (ofs > fp->fsize					/* In read-only mode, clip offset with the file size */
#if !PF_FS_READONLY
		 && !(fp->flag & FA_WRITE)
#endif
		) ofs = fp->fsize;

	ifptr = fp->fptr;
	fp->fptr = nsect = 0; fp->csect = 255;
	if (ofs > 0) {
		bcs = (PFdword)fp->fs->csize * SS(fp->fs);	/* Cluster size (byte) */
		if (ifptr > 0 &&
			(ofs - 1) / bcs >= (ifptr - 1) / bcs) {	/* When seek to same or following cluster, */
			fp->fptr = (ifptr - 1) & ~(bcs - 1);	/* start from the current cluster */
			ofs -= fp->fptr;
			clst = fp->curr_clust;
		} else {									/* When seek to back cluster, */
			clst = fp->org_clust;					/* start from the first cluster */
#if !PF_FS_READONLY
			if (clst == 0) {						/* If no cluster chain, create a new chain */
				clst = pfFsCreateChain(fp->fs, 0);
				if (clst == 1) ABORT(fp->fs, FR_INT_ERR);
				if (clst == 0xFFFFFFFF) ABORT(fp->fs, FR_DISK_ERR);
				fp->org_clust = clst;
			}
#endif
			fp->curr_clust = clst;
		}
		if (clst != 0) {
			while (ofs > bcs) {						/* Cluster following loop */
#if !PF_FS_READONLY
				if (fp->flag & FA_WRITE) {			/* Check if in write mode or not */
					clst = pfFsCreateChain(fp->fs, clst);	/* Force streached if in write mode */
					if (clst == 0) {				/* When disk gets full, clip file size */
						ofs = bcs; break;
					}
				} else
#endif
					clst = pfFsGetFat(fp->fs, clst);	/* Follow cluster chain if not in write mode */
				if (clst == 0xFFFFFFFF) ABORT(fp->fs, FR_DISK_ERR);
				if (clst <= 1 || clst >= fp->fs->max_clust) ABORT(fp->fs, FR_INT_ERR);
				fp->curr_clust = clst;
				fp->fptr += bcs;
				ofs -= bcs;
			}
			fp->fptr += ofs;
			fp->csect = (PFbyte)(ofs / SS(fp->fs));	/* Sector offset in the cluster */
			if (ofs % SS(fp->fs)) {
				nsect = pfFsClustToSect(fp->fs, clst);	/* Current sector */
				if (!nsect) ABORT(fp->fs, FR_INT_ERR);
				nsect += fp->csect;
				fp->csect++;
			}
		}
	}
	if (fp->fptr % SS(fp->fs) && nsect != fp->dsect) {
#if !PF_FS_TINY
#if !PF_FS_READONLY
		if (fp->flag & FA__DIRTY) {			/* Write-back dirty buffer if needed */
			if (pfDiskWrite(fp->fs->drive, fp->buf, fp->dsect, 1) != enStatusSuccess)
				ABORT(fp->fs, FR_DISK_ERR);
			fp->flag &= ~FA__DIRTY;
		}
#endif
		if (pfDiskRead(fp->fs->drive, fp->buf, nsect, 1) != enStatusSuccess)
			ABORT(fp->fs, FR_DISK_ERR);
#endif
		fp->dsect = nsect;
	}
#if !PF_FS_READONLY
	if (fp->fptr > fp->fsize) {			/* Set changed flag if the file size is extended */
		fp->fsize = fp->fptr;
		fp->flag |= FA__WRITTEN;
	}
#endif

	LEAVE_FF(fp->fs, res);
}




#if PF_FS_MINIMIZE <= 1
/*-----------------------------------------------------------------------*/
/* Create a Directroy Object                                             */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsDirOpen(PFFsDir *dj, const XCHAR *path)
{
	PFEnStatus res;
	NAMEBUF(sfn, lfn);
	PFbyte *dir;

	res = pfFsCheckMounted(&path, &dj->fs, 0);
	if (res == enStatusSuccess) {
		INITBUF((*dj), sfn, lfn);
		res = pfFsFollowPath(dj, path);			/* Follow the path to the directory */
		if (res == enStatusSuccess) {						/* Follow completed */
			dir = dj->dir;
			if (dir) {							/* It is not the root dir */
				if (dir[DIR_Attr] & AM_DIR) {	/* The object is a directory */
					dj->sclust = ((PFdword)LD_WORD(dir+DIR_FstClusHI) << 16) | LD_WORD(dir+DIR_FstClusLO);
				} else {						/* The object is not a directory */
					res = FR_NO_PATH;
				}
			}
			if (res == enStatusSuccess) {
				dj->id = dj->fs->id;
				res = pfFsDirSeek(dj, 0);			/* Rewind dir */
			}
		}
		if (res == FR_NO_FILE) res = FR_NO_PATH;
	}

	LEAVE_FF(dj->fs, res);
}


/*-----------------------------------------------------------------------*/
/* Read Directory Entry in Sequense                                      */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsDirReadEntry(PFFsDir *dj, PFFsFileInfo *fno)
{
	PFEnStatus res;
	NAMEBUF(sfn, lfn);

	res = pfFsValidate(dj->fs, dj->id);			/* Check validity of the object */
	if (res == enStatusSuccess) 
	{
		INITBUF((*dj), sfn, lfn);
		if (!fno) 
		{
			res = pfFsDirSeek(dj, 0);
		} 
		else 
		{
			res = pfFsDirRead(dj);
			if (res == FR_NO_FILE) 
			{
				dj->sect = 0;
				res = enStatusSuccess;
			}
			if (res == enStatusSuccess) {				/* A valid entry is found */
				pfFsGetFileInfo(dj, fno);		/* Get the object information */
				res = pfFsDirNext(dj, enBooleanFalse);	/* Increment index for next */
				if (res == FR_NO_FILE) 
				{
					dj->sect = 0;
					res = enStatusSuccess;
				}
			}
		}
	}

	LEAVE_FF(dj->fs, res);
}

/*-----------------------------------------------------------------------*/
/* Get File Status                                                       */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsGetFileStatus(const XCHAR *path,	PFFsFileInfo *fno)
{
	PFEnStatus res;
	PFFsDir dj;
	NAMEBUF(sfn, lfn);


	res = pfFsCheckMounted(&path, &dj.fs, 0);
	if (res == enStatusSuccess) {
		INITBUF(dj, sfn, lfn);
		res = pfFsFollowPath(&dj, path);	/* Follow the file path */
		if (res == enStatusSuccess) {				/* Follwo completed */
			if (dj.dir)	/* Found an object */
				pfFsGetFileInfo(&dj, fno);
			else		/* It is root dir */
				res = FR_INVALID_NAME;
		}
	}

	LEAVE_FF(dj.fs, res);
}

#if PF_FS_MINIMIZE == 0
#if !PF_FS_READONLY
/*-----------------------------------------------------------------------*/
/* Get Number of Free Clusters                                           */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsGetFreeClusters(const XCHAR *path, PFdword *nclst, PFFatFs **fatfs)
{
	PFEnStatus res;
	PFdword n, clst, sect, stat;
	PFdword i;
	PFbyte fat, *p;

	/* Get drive number */
	res = pfFsCheckMounted(&path, fatfs, 0);
	if (res != enStatusSuccess) LEAVE_FF(*fatfs, res);

	/* If number of free cluster is valid, return it without cluster scan. */
	if ((*fatfs)->free_clust <= (*fatfs)->max_clust - 2) {
		*nclst = (*fatfs)->free_clust;
		LEAVE_FF(*fatfs, enStatusSuccess);
	}

	/* Get number of free clusters */
	fat = (*fatfs)->fs_type;
	n = 0;
	if (fat == FS_FAT12) {
		clst = 2;
		do {
			stat = pfFsGetFat(*fatfs, clst);
			if (stat == 0xFFFFFFFF) LEAVE_FF(*fatfs, FR_DISK_ERR);
			if (stat == 1) LEAVE_FF(*fatfs, FR_INT_ERR);
			if (stat == 0) n++;
		} while (++clst < (*fatfs)->max_clust);
	} else {
		clst = (*fatfs)->max_clust;
		sect = (*fatfs)->fatbase;
		i = 0; p = 0;
		do {
			if (!i) {
				res = pfFsMoveWindow(*fatfs, sect++);
				if (res != enStatusSuccess)
					LEAVE_FF(*fatfs, res);
				p = (*fatfs)->win;
				i = SS(*fatfs);
			}
			if (fat == FS_FAT16) {
				if (LD_WORD(p) == 0) n++;
				p += 2; i -= 2;
			} else {
				if ((LD_DWORD(p) & 0x0FFFFFFF) == 0) n++;
				p += 4; i -= 4;
			}
		} while (--clst);
	}
	(*fatfs)->free_clust = n;
	if (fat == FS_FAT32) (*fatfs)->fsi_flag = 1;
	*nclst = n;

	LEAVE_FF(*fatfs, enStatusSuccess);
}

/*-----------------------------------------------------------------------*/
/* Truncate File                                                         */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsFileTruncate(PFFsFile *fp)
{
	PFEnStatus res;
	PFdword ncl;


	res = pfFsValidate(fp->fs, fp->id);		/* Check validity of the object */
	if (res != enStatusSuccess) LEAVE_FF(fp->fs, res);
	if (fp->flag & FA__ERROR)			/* Check abort flag */
		LEAVE_FF(fp->fs, FR_INT_ERR);
	if (!(fp->flag & FA_WRITE))			/* Check access mode */
		LEAVE_FF(fp->fs, FR_DENIED);

	if (fp->fsize > fp->fptr) {
		fp->fsize = fp->fptr;	/* Set file size to current R/W point */
		fp->flag |= FA__WRITTEN;
		if (fp->fptr == 0) {	/* When set file size to zero, remove entire cluster chain */
			res = pfFsRemoveChain(fp->fs, fp->org_clust);
			fp->org_clust = 0;
		} else {				/* When truncate a part of the file, remove remaining clusters */
			ncl = pfFsGetFat(fp->fs, fp->curr_clust);
			res = enStatusSuccess;
			if (ncl == 0xFFFFFFFF) res = FR_DISK_ERR;
			if (ncl == 1) res = FR_INT_ERR;
			if (res == enStatusSuccess && ncl < fp->fs->max_clust) {
				res = pfFsPutFat(fp->fs, fp->curr_clust, 0x0FFFFFFF);
				if (res == enStatusSuccess) res = pfFsRemoveChain(fp->fs, ncl);
			}
		}
	}
	if (res != enStatusSuccess) fp->flag |= FA__ERROR;

	LEAVE_FF(fp->fs, res);
}

/*-----------------------------------------------------------------------*/
/* Delete a File or Directory                                            */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsFileDelete(const XCHAR *path)
{
	PFEnStatus res;
	PFFsDir dj, sdj;
	NAMEBUF(sfn, lfn);
	PFbyte *dir;
	PFdword dclst;

	res = pfFsCheckMounted(&path, &dj.fs, 1);
	if (res != enStatusSuccess) LEAVE_FF(dj.fs, res);

	INITBUF(dj, sfn, lfn);
	res = pfFsFollowPath(&dj, path);			/* Follow the file path */
	if (PF_FS_RPATH && res == enStatusSuccess && (dj.fn[NS] & NS_DOT))
		res = FR_INVALID_NAME;
	if (res != enStatusSuccess) LEAVE_FF(dj.fs, res); /* Follow failed */

	dir = dj.dir;
	if (!dir)								/* Is it the root directory? */
		LEAVE_FF(dj.fs, FR_INVALID_NAME);
	if (dir[DIR_Attr] & AM_RDO)				/* Is it a R/O object? */
		LEAVE_FF(dj.fs, FR_DENIED);
	dclst = ((PFdword)LD_WORD(dir+DIR_FstClusHI) << 16) | LD_WORD(dir+DIR_FstClusLO);

	if (dir[DIR_Attr] & AM_DIR) {			/* It is a sub-directory */
		if (dclst < 2) LEAVE_FF(dj.fs, FR_INT_ERR);
		pfMemCopy(&sdj, &dj, sizeof(PFFsDir));	/* Check if the sub-dir is empty or not */
		sdj.sclust = dclst;
		res = pfFsDirSeek(&sdj, 2);
		if (res != enStatusSuccess) LEAVE_FF(dj.fs, res);
		res = pfFsDirRead(&sdj);
		if (res == enStatusSuccess) res = FR_DENIED;	/* Not empty sub-dir */
		if (res != FR_NO_FILE) LEAVE_FF(dj.fs, res);
	}

	res = pfFsDirRemove(&dj);					/* Remove directory entry */
	if (res == enStatusSuccess) 
	{
		if (dclst)
		{
			res = pfFsRemoveChain(dj.fs, dclst);	/* Remove the cluster chain */
		}
		if (res == enStatusSuccess) 
		{
			res = pfFsSync(dj.fs);
		}
	}

	LEAVE_FF(dj.fs, res);
}

/*-----------------------------------------------------------------------*/
/* Create a Directory                                                    */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsDirCreate(const XCHAR *path)
{
	PFEnStatus res;
	PFFsDir dj;
	NAMEBUF(sfn, lfn);
	PFbyte *dir, n;
	PFdword dsect, dclst, pclst, tim;

	res = pfFsCheckMounted(&path, &dj.fs, 1);
	if (res != enStatusSuccess) LEAVE_FF(dj.fs, res);

	INITBUF(dj, sfn, lfn);
	res = pfFsFollowPath(&dj, path);			/* Follow the file path */
	if (res == enStatusSuccess) res = FR_EXIST;		/* Any file or directory is already existing */
	if (PF_FS_RPATH && res == FR_NO_FILE && (dj.fn[NS] & NS_DOT))
		res = FR_INVALID_NAME;
	if (res != FR_NO_FILE)					/* Any error occured */
		LEAVE_FF(dj.fs, res);

	dclst = pfFsCreateChain(dj.fs, 0);			/* Allocate a new cluster for new directory table */
	res = enStatusSuccess;
	if (dclst == 0) 
	{
		res = FR_DENIED;
	}
	if (dclst == 1) 
	{
		res = FR_INT_ERR;
	}
	if (dclst == 0xFFFFFFFF) 
	{
		res = FR_DISK_ERR;
	}
	if (res == enStatusSuccess)
	{
		res = pfFsMoveWindow(dj.fs, 0);
	}
	if (res != enStatusSuccess) LEAVE_FF(dj.fs, res);
	{
		dsect = pfFsClustToSect(dj.fs, dclst);
	}

	dir = dj.fs->win;						/* Initialize the new directory table */
	pfMemSet(dir, 0, SS(dj.fs));
	pfMemSet(dir+DIR_Name, ' ', 8+3);		/* Create "." entry */
	dir[DIR_Name] = '.';
	dir[DIR_Attr] = AM_DIR;
	tim = get_fattime();
	ST_DWORD(dir+DIR_WrtTime, tim);
	ST_WORD(dir+DIR_FstClusLO, dclst);
	ST_WORD(dir+DIR_FstClusHI, dclst >> 16);
	pfMemCopy(dir+32, dir, 32); 			/* Create ".." entry */
	dir[33] = '.';
	pclst = dj.sclust;
	if (dj.fs->fs_type == FS_FAT32 && pclst == dj.fs->dirbase)
	{
		pclst = 0;
	}
	ST_WORD(dir+32+DIR_FstClusLO, pclst);
	ST_WORD(dir+32+DIR_FstClusHI, pclst >> 16);
	for (n = 0; n < dj.fs->csize; n++) 	/* Write dot entries and clear left sectors */
	{
		dj.fs->winsect = dsect++;
		dj.fs->wflag = 1;
		res = pfFsMoveWindow(dj.fs, 0);
		if (res) 
		{
			LEAVE_FF(dj.fs, res);
		}
		pfMemSet(dir, 0, SS(dj.fs));
	}

	res = pfFsDirRegister(&dj);
	if (res != enStatusSuccess) 
	{		
		pfFsRemoveChain(dj.fs, dclst);
	} 
	else 
	{
		dir = dj.dir;
		dir[DIR_Attr] = AM_DIR;					/* Attribute */
		ST_DWORD(dir+DIR_WrtTime, tim);			/* Crated time */
		ST_WORD(dir+DIR_FstClusLO, dclst);		/* Table start cluster */
		ST_WORD(dir+DIR_FstClusHI, dclst >> 16);
		dj.fs->wflag = 1;
		res = pfFsSync(dj.fs);
	}

	LEAVE_FF(dj.fs, res);
}

/*-----------------------------------------------------------------------*/
/* Change File Attribute                                                 */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsFileChangeAttrib (const XCHAR *path,	PFbyte value, PFbyte mask)
{
	PFEnStatus res;
	PFFsDir dj;
	NAMEBUF(sfn, lfn);
	PFbyte *dir;


	res = pfFsCheckMounted(&path, &dj.fs, 1);
	if (res == enStatusSuccess) {
		INITBUF(dj, sfn, lfn);
		res = pfFsFollowPath(&dj, path);		/* Follow the file path */
		if (PF_FS_RPATH && res == enStatusSuccess && (dj.fn[NS] & NS_DOT))
			res = FR_INVALID_NAME;
		if (res == enStatusSuccess) {
			dir = dj.dir;
			if (!dir) {						/* Is it a root directory? */
				res = FR_INVALID_NAME;
			} else {						/* File or sub directory */
				mask &= AM_RDO|AM_HID|AM_SYS|AM_ARC;	/* Valid attribute mask */
				dir[DIR_Attr] = (value & mask) | (dir[DIR_Attr] & (PFbyte)~mask);	/* Apply attribute change */
				dj.fs->wflag = 1;
				res = pfFsSync(dj.fs);
			}
		}
	}

	LEAVE_FF(dj.fs, res);
}

/*-----------------------------------------------------------------------*/
/* Change Timestamp                                                      */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsFileChangeTimestamp(const XCHAR *path, const PFFsFileInfo *fno)
{
	PFEnStatus res;
	PFFsDir dj;
	NAMEBUF(sfn, lfn);
	PFbyte *dir;


	res = pfFsCheckMounted(&path, &dj.fs, 1);
	if (res == enStatusSuccess) {
		INITBUF(dj, sfn, lfn);
		res = pfFsFollowPath(&dj, path);	/* Follow the file path */
		if (PF_FS_RPATH && res == enStatusSuccess && (dj.fn[NS] & NS_DOT))
			res = FR_INVALID_NAME;
		if (res == enStatusSuccess) {
			dir = dj.dir;
			if (!dir) {				/* Root directory */
				res = FR_INVALID_NAME;
			} else {				/* File or sub-directory */
				ST_WORD(dir+DIR_WrtTime, fno->ftime);
				ST_WORD(dir+DIR_WrtDate, fno->fdate);
				dj.fs->wflag = 1;
				res = pfFsSync(dj.fs);
			}
		}
	}

	LEAVE_FF(dj.fs, res);
}


/*-----------------------------------------------------------------------*/
/* Rename File/Directory                                                 */
/*-----------------------------------------------------------------------*/

PFEnStatus pfFsFileRename(const XCHAR *path_old, const XCHAR *path_new)
{
	PFEnStatus res;
	PFFsDir dj_old, dj_new;
	NAMEBUF(sfn, lfn);
	PFbyte buf[21], *dir;
	PFdword dw;


	INITBUF(dj_old, sfn, lfn);
	res = pfFsCheckMounted(&path_old, &dj_old.fs, 1);
	if (res == enStatusSuccess) {
		dj_new.fs = dj_old.fs;
		res = pfFsFollowPath(&dj_old, path_old);	/* Check old object */
		if (PF_FS_RPATH && res == enStatusSuccess && (dj_old.fn[NS] & NS_DOT))
			res = FR_INVALID_NAME;
	}
	if (res != enStatusSuccess) LEAVE_FF(dj_old.fs, res);	/* The old object is not found */

	if (!dj_old.dir) LEAVE_FF(dj_old.fs, FR_NO_FILE);	/* Is root dir? */
	pfMemCopy(buf, dj_old.dir+DIR_Attr, 21);		/* Save the object information */

	pfMemCopy(&dj_new, &dj_old, sizeof(PFFsDir));
	res = pfFsFollowPath(&dj_new, path_new);		/* Check new object */
	if (res == enStatusSuccess) res = FR_EXIST;			/* The new object name is already existing */
	if (res == FR_NO_FILE) { 					/* Is it a valid path and no name collision? */
		res = pfFsDirRegister(&dj_new);			/* Register the new object */
		if (res == enStatusSuccess) {
			dir = dj_new.dir;					/* Copy object information into new entry */
			pfMemCopy(dir+13, buf+2, 19);
			dir[DIR_Attr] = buf[0] | AM_ARC;
			dj_old.fs->wflag = 1;
			if (dir[DIR_Attr] & AM_DIR) {		/* Update .. entry in the directory if needed */
				dw = pfFsClustToSect(dj_new.fs, (PFdword)LD_WORD(dir+DIR_FstClusHI) | LD_WORD(dir+DIR_FstClusLO));
				if (!dw) {
					res = FR_INT_ERR;
				} else {
					res = pfFsMoveWindow(dj_new.fs, dw);
					dir = dj_new.fs->win+32;
					if (res == enStatusSuccess && dir[1] == '.') {
						dw = (dj_new.fs->fs_type == FS_FAT32 && dj_new.sclust == dj_new.fs->dirbase) ? 0 : dj_new.sclust;
						ST_WORD(dir+DIR_FstClusLO, dw);
						ST_WORD(dir+DIR_FstClusHI, dw >> 16);
						dj_new.fs->wflag = 1;
					}
				}
			}
			if (res == enStatusSuccess) {
				res = pfFsDirRemove(&dj_old);			/* Remove old entry */
				if (res == enStatusSuccess)
					res = pfFsSync(dj_old.fs);
			}
		}
	}

	LEAVE_FF(dj_old.fs, res);
}

#endif // !PF_FS_READONLY
#endif // PF_FS_MINIMIZE == 0
#endif // PF_FS_MINIMIZE <= 1
#endif // PF_FS_MINIMIZE <= 2


/*-----------------------------------------------------------------------*/
/* Forward data to the stream directly (Available on only PF_FS_TINY cfg)  */
/*-----------------------------------------------------------------------*/
#if PF_FS_USE_FORWARD && PF_FS_TINY

PFEnStatus pfFsFileForward(PFFsFile *fp, PFdword (*func)(const PFbyte*,PFdword), PFdword btr, PFdword *bf)
{
	PFEnStatus res;
	PFdword remain, clst, sect;
	PFdword rcnt;

	*bf = 0;

	res = pfFsValidate(fp->fs, fp->id);					/* Check validity of the object */
	if (res != enStatusSuccess) LEAVE_FF(fp->fs, res);
	if (fp->flag & FA__ERROR)						/* Check error flag */
		LEAVE_FF(fp->fs, FR_INT_ERR);
	if (!(fp->flag & FA_READ))						/* Check access mode */
		LEAVE_FF(fp->fs, FR_DENIED);

	remain = fp->fsize - fp->fptr;
	if (btr > remain) btr = (PFdword)remain;			/* Truncate btr by remaining bytes */

	for ( ;  btr && (*func)(PF_NULL, 0);				/* Repeat until all data transferred or stream becomes busy */
		fp->fptr += rcnt, *bf += rcnt, btr -= rcnt) {
		if ((fp->fptr % SS(fp->fs)) == 0) {			/* On the sector boundary? */
			if (fp->csect >= fp->fs->csize) {		/* On the cluster boundary? */
				clst = (fp->fptr == 0) ?			/* On the top of the file? */
					fp->org_clust : pfFsGetFat(fp->fs, fp->curr_clust);
				if (clst <= 1) ABORT(fp->fs, FR_INT_ERR);
				if (clst == 0xFFFFFFFF) ABORT(fp->fs, FR_DISK_ERR);
				fp->curr_clust = clst;				/* Update current cluster */
				fp->csect = 0;						/* Reset sector address in the cluster */
			}
			fp->csect++;							/* Next sector address in the cluster */
		}
		sect = pfFsClustToSect(fp->fs, fp->curr_clust);	/* Get current data sector */
		if (!sect) ABORT(fp->fs, FR_INT_ERR);
		sect += fp->csect - 1;
		if (pfFsMoveWindow(fp->fs, sect))				/* Move sector window */
			ABORT(fp->fs, FR_DISK_ERR);
		fp->dsect = sect;
		rcnt = SS(fp->fs) - (PFword)(fp->fptr % SS(fp->fs));	/* Forward data from sector window */
		if (rcnt > btr) rcnt = btr;
		rcnt = (*func)(&fp->fs->win[(PFword)fp->fptr % SS(fp->fs)], rcnt);
		if (!rcnt) ABORT(fp->fs, FR_INT_ERR);
	}

	LEAVE_FF(fp->fs, enStatusSuccess);
}
#endif /* PF_FS_USE_FORWARD */



#if PF_FS_USE_MKFS && !PF_FS_READONLY
/*-----------------------------------------------------------------------*/
/* Create File System on the Drive                                       */
/*-----------------------------------------------------------------------*/
#define N_ROOTDIR	512			/* Multiple of 32 and <= 2048 */
#define N_FATS		1			/* 1 or 2 */
#define MAX_SECTOR	131072000UL	/* Maximum partition size */
#define MIN_SECTOR	2000UL		/* Minimum partition size */


PFEnStatus pfFsFormat(PFbyte drv, PFbyte partition,	PFword allocsize)
{
	static const PFdword sstbl[] = { 2048000, 1024000, 512000, 256000, 128000, 64000, 32000, 16000, 8000, 4000,   0 };
	static const PFword cstbl[] =  {   32768,   16384,   8192,   4096,   2048, 16384,  8192,  4096, 2048, 1024, 512 };
	PFbyte fmt, m, *tbl;
	PFdword b_part, b_fat, b_dir, b_data;		/* Area offset (LBA) */
	PFdword n_part, n_rsv, n_fat, n_dir;		/* Area size */
	PFdword n_clst, d, n;
	PFword as;
	PFFatFs *fs;
	PFEnStatus stat;


	/* Check validity of the parameters */
	if (drv >= PF_FS_MAX_DRIVES) return FR_INVALID_DRIVE;
	if (partition >= 2) return FR_MKFS_ABORTED;

	/* Check mounted drive and clear work area */
	fs = FatFs[drv];
	if (!fs) return FR_NOT_ENABLED;
	fs->fs_type = 0;
	drv = LD2PD(drv);

	/* Get disk statics */
	stat = proxy_disk_initialize(pDrv, drv);
	if (stat & STA_NOINIT) return FR_NOT_READY;
	if (stat & STA_PROTECT) return FR_WRITE_PROTECTED;
#if PF_FS_MAX_SS != 512						/* Get disk sector size */
	if (pfDiskIoCtrl(pDev, drv, GET_SECTOR_SIZE, &SS(fs)) != enStatusSuccess
		|| SS(fs) > PF_FS_MAX_SS)
		return FR_MKFS_ABORTED;
#endif
	if (pfDiskIoCtrl(pDev, drv, GET_SECTOR_COUNT, &n_part) != enStatusSuccess || n_part < MIN_SECTOR)
		return FR_MKFS_ABORTED;
	if (n_part > MAX_SECTOR) n_part = MAX_SECTOR;
	b_part = (!partition) ? 63 : 0;		/* Boot sector */
	n_part -= b_part;
	for (d = 512; d <= 32768U && d != allocsize; d <<= 1) ;	/* Check validity of the allocation unit size */
	if (d != allocsize) allocsize = 0;
	if (!allocsize) {					/* Auto selection of cluster size */
		d = n_part;
		for (as = SS(fs); as > 512U; as >>= 1) d >>= 1;
		for (n = 0; d < sstbl[n]; n++) ;
		allocsize = cstbl[n];
	}
	if (allocsize < SS(fs)) allocsize = SS(fs);

	allocsize /= SS(fs);		/* Number of sectors per cluster */

	/* Pre-compute number of clusters and FAT type */
	n_clst = n_part / allocsize;
	fmt = FS_FAT12;
	if (n_clst >= 0xFF5) fmt = FS_FAT16;
	if (n_clst >= 0xFFF5) fmt = FS_FAT32;

	/* Determine offset and size of FAT structure */
	switch (fmt) {
	case FS_FAT12:
		n_fat = ((n_clst * 3 + 1) / 2 + 3 + SS(fs) - 1) / SS(fs);
		n_rsv = 1 + partition;
		n_dir = N_ROOTDIR * 32 / SS(fs);
		break;
	case FS_FAT16:
		n_fat = ((n_clst * 2) + 4 + SS(fs) - 1) / SS(fs);
		n_rsv = 1 + partition;
		n_dir = N_ROOTDIR * 32 / SS(fs);
		break;
	default:
		n_fat = ((n_clst * 4) + 8 + SS(fs) - 1) / SS(fs);
		n_rsv = 33 - partition;
		n_dir = 0;
	}
	b_fat = b_part + n_rsv;			/* FATs start sector */
	b_dir = b_fat + n_fat * N_FATS;	/* Directory start sector */
	b_data = b_dir + n_dir;			/* Data start sector */

	/* Align data start sector to erase block boundary (for flash memory media) */
	if (pfDiskIoCtrl(pDev, drv, GET_BLOCK_SIZE, &n) != enStatusSuccess) return FR_MKFS_ABORTED;
	n = (b_data + n - 1) & ~(n - 1);
	n_fat += (n - b_data) / N_FATS;
	/* b_dir and b_data are no longer used below */

	/* Determine number of cluster and final check of validity of the FAT type */
	n_clst = (n_part - n_rsv - n_fat * N_FATS - n_dir) / allocsize;
	if (   (fmt == FS_FAT16 && n_clst < 0xFF5)
		|| (fmt == FS_FAT32 && n_clst < 0xFFF5))
		return FR_MKFS_ABORTED;

	/* Create partition table if needed */
	if (!partition) {
		PFdword n_disk = b_part + n_part;

		pfMemSet(fs->win, 0, SS(fs));
		tbl = fs->win+MBR_Table;
		ST_DWORD(tbl, 0x00010180);		/* Partition start in CHS */
		if (n_disk < 63UL * 255 * 1024) {	/* Partition end in CHS */
			n_disk = n_disk / 63 / 255;
			tbl[7] = (PFbyte)n_disk;
			tbl[6] = (PFbyte)((n_disk >> 2) | 63);
		} else {
			ST_WORD(&tbl[6], 0xFFFF);
		}
		tbl[5] = 254;
		if (fmt != FS_FAT32)			/* System ID */
			tbl[4] = (n_part < 0x10000) ? 0x04 : 0x06;
		else
			tbl[4] = 0x0c;
		ST_DWORD(tbl+8, 63);			/* Partition start in LBA */
		ST_DWORD(tbl+12, n_part);		/* Partition size in LBA */
		ST_WORD(tbl+64, 0xAA55);		/* Signature */
		if (pfDiskWrite(pDev, drv, fs->win, 0, 1) != enStatusSuccess)
			return FR_DISK_ERR;
		partition = 0xF8;
	} else {
		partition = 0xF0;
	}

	/* Create boot record */
	tbl = fs->win;								/* Clear buffer */
	pfMemSet(tbl, 0, SS(fs));
	ST_DWORD(tbl+BS_jmpBoot, 0x90FEEB);			/* Boot code (jmp $, nop) */
	ST_WORD(tbl+BPB_BytsPerSec, SS(fs));		/* Sector size */
	tbl[BPB_SecPerClus] = (PFbyte)allocsize;		/* Sectors per cluster */
	ST_WORD(tbl+BPB_RsvdSecCnt, n_rsv);			/* Reserved sectors */
	tbl[BPB_NumFATs] = N_FATS;					/* Number of FATs */
	ST_WORD(tbl+BPB_RootEntCnt, SS(fs) / 32 * n_dir); /* Number of rootdir entries */
	if (n_part < 0x10000) {						/* Number of total sectors */
		ST_WORD(tbl+BPB_TotSec16, n_part);
	} else {
		ST_DWORD(tbl+BPB_TotSec32, n_part);
	}
	tbl[BPB_Media] = partition;					/* Media descripter */
	ST_WORD(tbl+BPB_SecPerTrk, 63);				/* Number of sectors per track */
	ST_WORD(tbl+BPB_NumHeads, 255);				/* Number of heads */
	ST_DWORD(tbl+BPB_HiddSec, b_part);			/* Hidden sectors */
	n = get_fattime();							/* Use current time as a VSN */
	if (fmt != FS_FAT32) {
		ST_DWORD(tbl+BS_VolID, n);				/* Volume serial number */
		ST_WORD(tbl+BPB_FATSz16, n_fat);		/* Number of secters per FAT */
		tbl[BS_DrvNum] = 0x80;					/* Drive number */
		tbl[BS_BootSig] = 0x29;					/* Extended boot signature */
		pfMemCopy(tbl+BS_VolLab, "NO NAME    FAT     ", 19);	/* Volume lavel, FAT signature */
	} else {
		ST_DWORD(tbl+BS_VolID32, n);			/* Volume serial number */
		ST_DWORD(tbl+BPB_FATSz32, n_fat);		/* Number of secters per FAT */
		ST_DWORD(tbl+BPB_RootClus, 2);			/* Root directory cluster (2) */
		ST_WORD(tbl+BPB_FSInfo, 1);				/* FSInfo record offset (bs+1) */
		ST_WORD(tbl+BPB_BkBootSec, 6);			/* Backup boot record offset (bs+6) */
		tbl[BS_DrvNum32] = 0x80;				/* Drive number */
		tbl[BS_BootSig32] = 0x29;				/* Extended boot signature */
		pfMemCopy(tbl+BS_VolLab32, "NO NAME    FAT32   ", 19);	/* Volume lavel, FAT signature */
	}
	ST_WORD(tbl+BS_55AA, 0xAA55);				/* Signature */
	if (SS(fs) > 512U) {
		ST_WORD(tbl+SS(fs)-2, 0xAA55);
	}
	if (pfDiskWrite(pDev, drv, tbl, b_part+0, 1) != enStatusSuccess)
		return FR_DISK_ERR;
	if (fmt == FS_FAT32)
		pfDiskWrite(pDev, drv, tbl, b_part+6, 1);

	/* Initialize FAT area */
	for (m = 0; m < N_FATS; m++) {
		pfMemSet(tbl, 0, SS(fs));		/* 1st sector of the FAT  */
		if (fmt != FS_FAT32) {
			n = (fmt == FS_FAT12) ? 0x00FFFF00 : 0xFFFFFF00;
			n |= partition;
			ST_DWORD(tbl, n);				/* Reserve cluster #0-1 (FAT12/16) */
		} else {
			ST_DWORD(tbl+0, 0xFFFFFFF8);	/* Reserve cluster #0-1 (FAT32) */
			ST_DWORD(tbl+4, 0xFFFFFFFF);
			ST_DWORD(tbl+8, 0x0FFFFFFF);	/* Reserve cluster #2 for root dir */
		}
		if (pfDiskWrite(pDev, drv, tbl, b_fat++, 1) != enStatusSuccess)
			return FR_DISK_ERR;
		pfMemSet(tbl, 0, SS(fs));		/* Following FAT entries are filled by zero */
		for (n = 1; n < n_fat; n++) {
			if (pfDiskWrite(pDev, drv, tbl, b_fat++, 1) != enStatusSuccess)
				return FR_DISK_ERR;
		}
	}

	/* Initialize Root directory */
	m = (PFbyte)((fmt == FS_FAT32) ? allocsize : n_dir);
	do {
		if (pfDiskWrite(pDev, drv, tbl, b_fat++, 1) != enStatusSuccess)
			return FR_DISK_ERR;
	} while (--m);

	/* Create FSInfo record if needed */
	if (fmt == FS_FAT32) {
		ST_WORD(tbl+BS_55AA, 0xAA55);
		ST_DWORD(tbl+FSI_LeadSig, 0x41615252);
		ST_DWORD(tbl+FSI_StrucSig, 0x61417272);
		ST_DWORD(tbl+FSI_Free_Count, n_clst - 1);
		ST_DWORD(tbl+FSI_Nxt_Free, 0xFFFFFFFF);
		pfDiskWrite(pDev, drv, tbl, b_part+1, 1);
		pfDiskWrite(pDev, drv, tbl, b_part+7, 1);
	}

	return (pfDiskIoCtrl(pDev, drv, CTRL_SYNC, (void*)PF_NULL) == enStatusSuccess) ? enStatusSuccess : FR_DISK_ERR;
}

#endif /* PF_FS_USE_MKFS && !PF_FS_READONLY */




#if PF_FS_USE_STRFUNC
/*-----------------------------------------------------------------------*/
/* Get a string from the file                                            */
/*-----------------------------------------------------------------------*/
PFchar* pfFsFileGetString(PFchar* pBuf,	PFsdword len, PFFsFile* fil)
{
	PFsdword i = 0;
	PFchar *p = pBuf;
	PFdword rc;


	while (i < len - 1) {			/* Read bytes until buffer gets filled */
		pfFsFileRead(fil, p, 1, &rc);
		if (rc != 1) break;			/* Break when no data to read */
#if PF_FS_USE_STRFUNC >= 2
		if (*p == '\r') continue;	/* Strip '\r' */
#endif
		i++;
		if (*p++ == '\n') break;	/* Break when reached end of line */
	}
	*p = 0;
	return i ? pBuf : PF_NULL;			/* When no data read (eof or error), return with error. */
}



#if !PF_FS_READONLY
#include <stdarg.h>
/*-----------------------------------------------------------------------*/
/* Put a character to the file                                           */
/*-----------------------------------------------------------------------*/
PFsdword pfFsFilePutChar(PFsdword chr, PFFsFile* fil)
{
	PFdword bw;
	PFchar c;


#if PF_FS_USE_STRFUNC >= 2
	if (chr == '\n') pfFsFilePutChar ('\r', fil);	/* LF -> CRLF conversion */
#endif
	if (!fil) {	/* Special value may be used to switch the destination to any other device */
	/*	put_console(chr);	*/
		return chr;
	}
	c = (PFchar)chr;
	pfFsFileWrite(fil, &c, 1, &bw);	/* Write a byte to the file */
	return bw ? chr : PF_EOF;		/* Return the result */
}




/*-----------------------------------------------------------------------*/
/* Put a string to the file                                              */
/*-----------------------------------------------------------------------*/
PFsdword pfFsFilePutString(const PFchar* str, PFFsFile* fil)
{
	PFsdword n;

	for (n = 0; *str; str++, n++) 
	{
		if (pfFsFilePutChar(*str, fil) == PF_EOF) 
		{
			return PF_EOF;
		}
	}
	return n;
}



#ifdef PF_USE_PRINTF
/*-----------------------------------------------------------------------*/
/* Put a formatted string to the file                                    */
/*-----------------------------------------------------------------------*/
PFsdword pfFsFilePrint(PFFsFile* fil, const PFchar* str,...)
{
	va_list arp;
	PFbyte c, f, r;
	PFdword val;
	PFchar s[16];
	PFsdword i, w, res, cc;


	va_start(arp, str);

	for (cc = res = 0; cc != PF_EOF; res += cc) 
	{
		c = *str++;
		if (c == 0) 		/* End of string */
		{
			break;	
		}
		if (c != '%') 				/* Non escape cahracter */
		{	
			cc = pfFsFilePutChar(c, fil);
			if (cc != PF_EOF) 
			{
				cc = 1;
			}
			continue;
		}
		w = f = 0;
		c = *str++;
		if (c == '0')				/* Flag: '0' padding */
		{
			f = 1; 
			c = *str++;
		}
		while (c >= '0' && c <= '9') 	/* Precision */
		{
			w = w * 10 + (c - '0');
			c = *str++;
		}
		if (c == 'l') 				/* Prefix: Size is long int */
		{
			f |= 2; 
			c = *str++;
		}
		if (c == 's') 				/* Type is string */
		{
			cc = pfFsFilePutString(va_arg(arp, PFchar*), fil);
			continue;
		}
		if (c == 'c') 				/* Type is character */
		{
			cc = pfFsFilePutChar(va_arg(arp, PFsdword), fil);
			if (cc != PF_EOF) 
			{
				cc = 1;
			}
			continue;
		}
		r = 0;
		if (c == 'd') /* Type is signed decimal */
		{
			r = 10;		
		}
		if (c == 'u') 		/* Type is unsigned decimal */
		{
			r = 10;
		}
		if (c == 'X') 		/* Type is unsigned hexdecimal */
		{
			r = 16;
		}
		if (r == 0) 			/* Unknown type */
		{
			break;
		}
		if (f & 2) 				/* Get the value */
		{
			val = (PFdword)va_arg(arp, long);
		} 
		else 
		{
			val = (c == 'd') ? (PFdword)(long)va_arg(arp, PFsdword) : (PFdword)va_arg(arp, unsigned PFsdword);
		}
		/* Put numeral string */
		if (c == 'd') 
		{
			if (val & 0x80000000) 
			{
				val = 0 - val;
				f |= 4;
			}
		}
		i = sizeof(s) - 1; s[i] = 0;
		do 
		{
			c = (PFbyte)(val % r + '0');
			if (c > '9') c += 7;
			s[--i] = c;
			val /= r;
		} while (i && val);
		if (i && (f & 4)) 
		{
			s[--i] = '-';
		}
		w = sizeof(s) - 1 - w;
		while (i && i > w) 
		{
			s[--i] = (f & 1) ? '0' : ' ';
		}
		cc = pfFsFilePutString(&s[i], fil);
	}

	va_end(arp);
	return (cc == PF_EOF) ? cc : res;
}
#endif	// #ifdef PF_USE_PRINTF


#endif /* !PF_FS_READONLY */
#endif /* PF_FS_USE_STRFUNC */

