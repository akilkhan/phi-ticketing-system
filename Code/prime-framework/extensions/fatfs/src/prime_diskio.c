/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2012        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various existing      */
/* storage control module to the FatFs module with a defined API.        */
/*-----------------------------------------------------------------------*/

#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_diskio.h"		/* FatFs lower layer API */

static PFCfgDisk diskConfig[PF_MAX_DISK_SUPPORTED];
static PFEnBoolean diskStatus[PF_MAX_DISK_SUPPORTED] = {enBooleanFalse};
static PFbyte diskId[PF_MAX_DISK_SUPPORTED] = {0};

/*-----------------------------------------------------------------------*/
/* Initialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

PFEnStatus pfDiskOpen(PFbyte* drive, PFpCfgDisk config)
{
	PFbyte drvNum;
#if (PF_FS_DEBUG == 1)	
	if(config == 0)
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_FS_DEBUG == 1)
	for(drvNum = 0; drvNum < PF_MAX_DISK_SUPPORTED; drvNum++)
	{
		if(diskStatus[drvNum] == enBooleanFalse)
		{
			*drive = drvNum;
			pfMemCopy(&diskConfig[drvNum], config, sizeof(PFCfgDisk));
			break;
		}
	}
	if(drvNum >= PF_MAX_DISK_SUPPORTED)
	{
		return enStatusNoMem;
	}
	
	return enStatusSuccess;
}

PFEnStatus pfDiskInit(PFbyte drive)
{
	PFEnStatus status;
#if (PF_FS_DEBUG == 1)	
	if(diskStatus[drive] == enBooleanFalse)
	{
		return enStatusInvalidDrive;
	}
#endif	// #if (PF_FS_DEBUG == 1)
	
	status = diskConfig[drive].devOpen(&diskId[drive], diskConfig[drive].devConfig);

	return status;
}



//-----------------------------------------------------------------------
// Get Disk Status                                                       
//-----------------------------------------------------------------------

PFEnStatus pfDiskGetStatus(PFbyte drive)
{
	PFEnStatus status;
#if (PF_FS_DEBUG == 1)	
	if(diskStatus[drive] == enBooleanFalse)
	{
		return enStatusInvalidDrive;
	}
#endif	// #if (PF_FS_DEBUG == 1)
	
	status = diskConfig[drive].devGetStatus(&diskId[drive]);

	return status;
}



//-----------------------------------------------------------------------
// Read Sector(s)                                                        
//-----------------------------------------------------------------------

PFEnStatus pfDiskRead(PFbyte drive, PFbyte* buff, PFdword sector, PFbyte count)
{
	PFEnStatus status;
#if (PF_FS_DEBUG == 1)	
	if(diskStatus[drive] == enBooleanFalse)
	{
		return enStatusInvalidDrive;
	}
#endif	// #if (PF_FS_DEBUG == 1)
	
	status = diskConfig[drive].devRead(&diskId[drive], buff, sector, count);

	return status;

}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

PFEnStatus pfDiskWrite(PFbyte drive, const PFbyte *buff, PFdword sector, PFbyte count)
{
	PFEnStatus status;
#if (PF_FS_DEBUG == 1)	
	if(diskStatus[drive] == enBooleanFalse)
	{
		return enStatusInvalidDrive;
	}
#endif	// #if (PF_FS_DEBUG == 1)
	if(diskConfig[drive].allowWrite == enBooleanTrue)	
	{
		status = diskConfig[drive].devWrite(&diskId[drive], (PFbyte*)buff, sector, count);
	}
	else
	{
		status = enStatusWriteProtected;
	}

	return status;
}


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/
PFEnStatus pfDiskIoCtrl(PFbyte drive, PFbyte cmd, void* buff)
{
	PFEnStatus status;
#if (PF_FS_DEBUG == 1)	
	if(diskStatus[drive] == enBooleanFalse)
	{
		return enStatusInvalidDrive;
	}
#endif	// #if (PF_FS_DEBUG == 1)
	if(diskConfig[drive].allowIoCtrl == enBooleanTrue)	
	{
		status = diskConfig[drive].devIoCtrl(&diskId[drive], cmd, buff);
	}
	else
	{
		status = enStatusNotSupported;
	}

	return status;
}

