#ifndef PRIMEDCP_PACKET_H_
#define PRIMEDCP_PACKET_H_

#include "prime_dcpconfig.h"
#include "prime_list.h"
#include "prime_dcptypes.h"

/*
#if PF_ARCH_LOW_BYTE_FIRST == 1
 0..VER..3 4..FLG..7 8............PS..............23 24......SB.....31
#else
 0....SB....7  8..VER..11 12..FLG..15 16............PS..............31
#endif
 32..............CRC............47 48.......SEQ.......59 60...ID1...63
 64...ID2...71 72..........FN...........83 84...........SID.........96
 96.............................DST_ADDR............................127
 128............................SRC_ADDR............................159
*/
#define PF_DCP_HDR_SB_MASK      0xFF
#define PF_DCP_HDR_VER_MASK     0xF
#define PF_DCP_HDR_FLG_MASK     0xF
#define PF_DCP_HDR_PS_MASK      0xFFFF
#if PF_ARCH_LOW_BYTE_FIRST == 1
#define PF_DCP_HDR_SB(_v_)      (((_v_) & PF_DCP_HDR_SB_MASK) << 24)
#define PF_DCP_HDR_SB_GET(_q_)  (((_q_) >> 24) & PF_DCP_HDR_SB_MASK)
#define PF_DCP_HDR_VER(_v_)     (((_v_) & PF_DCP_HDR_VER_MASK) << 0)
#define PF_DCP_HDR_VER_GET(_q_) (((_q_) >> 0) & PF_DCP_HDR_VER_MASK)
#define PF_DCP_HDR_FLG(_v_)     (((_v_) & PF_DCP_HDR_FLG_MASK) << 4)
#define PF_DCP_HDR_FLG_GET(_q_) (((_q_) >> 4) & PF_DCP_HDR_FLG_MASK)
#define PF_DCP_HDR_PS(_v_)      (((_v_) & PF_DCP_HDR_PS_MASK) << 8)
#define PF_DCP_HDR_PS_GET(_q_)  (((_q_) >> 8) & PF_DCP_HDR_PS_MASK)
#else
#define PF_DCP_HDR_SB(_v_)      (((_v_) & PF_DCP_HDR_SB_MASK) << 0)
#define PF_DCP_HDR_SB_GET(_q_)  (((_q_) >> 0) & PF_DCP_HDR_SB_MASK)
#define PF_DCP_HDR_VER(_v_)     (((_v_) & PF_DCP_HDR_VER_MASK) << 8)
#define PF_DCP_HDR_VER_GET(_q_) (((_q_) >> 8) & PF_DCP_HDR_VER_MASK)
#define PF_DCP_HDR_FLG(_v_)     (((_v_) & PF_DCP_HDR_FLG_MASK) << 12)
#define PF_DCP_HDR_FLG_GET(_q_) (((_q_) >> 12) & PF_DCP_HDR_FLG_MASK)
#define PF_DCP_HDR_PS(_v_)      (((_v_) & PF_DCP_HDR_PS_MASK) << 16)
#define PF_DCP_HDR_PS_GET(_q_)  (((_q_) >> 16) & PF_DCP_HDR_PS_MASK)
#endif
#define PF_DCP_HDR_CRC_MASK     0xFFFF
#define PF_DCP_HDR_CRC(_v_)     (((_v_) & PF_DCP_HDR_CRC_MASK) << 0)
#define PF_DCP_HDR_CRC_GET(_q_) (((_q_) >> 0) & PF_DCP_HDR_CRC_MASK)
#define PF_DCP_HDR_SEQ_MASK     0xFFF
#define PF_DCP_HDR_SEQ(_v_)     (((_v_) & PF_DCP_HDR_SEQ_MASK) << 16)
#define PF_DCP_HDR_SEQ_GET(_q_) (((_q_) >> 16) & PF_DCP_HDR_SEQ_MASK)
#define PF_DCP_HDR_ID1_MASK     0xF
#define PF_DCP_HDR_ID1(_v_)     (((_v_) & PF_DCP_HDR_ID1_MASK) << 28)
#define PF_DCP_HDR_ID1_GET(_q_) (((_q_) >> 28) & PF_DCP_HDR_ID1_MASK)
#define PF_DCP_HDR_ID2_MASK     0xFF
#define PF_DCP_HDR_ID2(_v_)     ((((_v_) >> 4) & PF_DCP_HDR_ID2_MASK) << 0)
#define PF_DCP_HDR_ID2_GET(_q_) ((((_q_) >> 0) & PF_DCP_HDR_ID2_MASK) << 4)
#define PF_DCP_HDR_ID_GET(_q1_, _q2_) (PF_DCP_HDR_ID1_GET(_q1_) | PF_DCP_HDR_ID2_GET(_q2_))
#define PF_DCP_HDR_FN_MASK      0xFFF
#define PF_DCP_HDR_FN(_v_)      (((_v_) & PF_DCP_HDR_FN_MASK) << 8)
#define PF_DCP_HDR_FN_GET(_q_)  (((_q_) >> 8) & PF_DCP_HDR_FN_MASK)
#define PF_DCP_HDR_SID_MASK     0xFFF
#define PF_DCP_HDR_SID(_v_)     (((_v_) & PF_DCP_HDR_SID_MASK) << 20)
#define PF_DCP_HDR_SID_GET(_q_) (((_q_) >> 20) & PF_DCP_HDR_SID_MASK)

/*
#if PF_ARCH_LOW_BYTE_FIRST == 1
 0..VER..3 4..FLG..7 8............WS..............23 24......SB.....31
#else
 0....SB....7  8..VER..11 12..FLG..15 16............WS..............31
#endif
 32..............CRC............47 48.......SEQ.......59 60...ID1...63
 64...ID2...71 72..........FN...........83 84...........SID.........96
 96.............................DST_ADDR............................127
 128............................SRC_ADDR............................159
*/
#define PF_DCP_ACK_WS_MASK      PF_DCP_HDR_PS_MASK
#define PF_DCP_ACK_WS(_v_)      PF_DCP_HDR_PS(_v_)
#define PF_DCP_ACK_WS_GET(_q_)  PF_DCP_HDR_PS_GET(_q_)

#define PF_DCP_HDR_SIZE             20
#define PF_DCP_PS_MAX_SIZE          (1<<16)
#define PF_DCP_HDR_SB_MARK          0x7E
#define PF_DCP_VER                  0x1
#define PF_DCP_HDR_FLG_NONE         0x0
#define PF_DCP_HDR_FLG_LAST_FRAG    0x1
#define PF_DCP_HDR_FLG_SYN_FRAG     0x2
#define PF_DCP_HDR_FLG_SPEC_PACK    0x4
#define PF_DCP_SPEC_PACKID_ACK      0
#define PF_DCP_SPEC_PACKID_NACK     1
#define PF_DCP_SPEC_PACKID_DGNACK   2
#define PF_DCP_SPEC_PACKID_RESET    3
#define PF_DCP_SPEC_PACKID_REJECT   4

#if PF_DCP_MAX_PAYLOAD_SIZE > PF_DCP_PS_MAX_SIZE
#error too large PF_DCP_MAX_PAYLOAD_SIZE!
#endif

#define PF_DCP_MAX_PACKET_SIZE  (PF_DCP_HDR_SIZE + PF_DCP_MAX_PAYLOAD_SIZE)

typedef struct
{
    PFNode                   node;
    PFdcpmemsize            uSize;
    PFtime                      uRetryTime;
    PFbyte                     uRetryNum;
} PFDcpPacketBuf;
typedef PFDcpPacketBuf * PFpDcpPacketBuf;

#define PF_DCP_PACKET_DATA(_pb_)    ((PFpdcpmemptr)((_pb_)+1))

typedef struct
{
    PFNode               node;
    union
    {
        PFbyte     data8[PF_DCP_HDR_SIZE];
        PFdword    data32[PF_DCP_HDR_SIZE/sizeof(PFdword)];
    } headerBuf;
    PFdcpHwAddr         hwAddr;
    PFdcpmemsize        uSize;
    PFtime                  uExpTime;
    PFbyte                 uRetryNum;
    PFDcpL2DevDesc      l2Dev;
} PFDcpIncomPacketBuf;
typedef PFDcpIncomPacketBuf * PFpDcpIncomPacketBuf;

#endif /*PRIMEDCP_PACKET_H_*/
