#ifndef PRIMEDCP_RECV_H_
#define PRIMEDCP_RECV_H_

#include "prime_dcpcore.h"

PF_EXTERN_C_BEGIN

void pfDcpInEpHandle(PFpDcpServiceContext pServ, PFpDcpInEndpoint pEp);
PFpDcpInEndpoint pfDcpInEpFind(PFpDcpServiceContext pServ, PFdcpDevAddr uAddr);
PFEnStatus pfDcpDataPacketHandle(PFpDcpServiceContext pServ, PFpDcpInEndpoint pEp, PFpDcpL2DevDesc pL2Dev, 
    PFpDcpIncomPacketBuf pIncPackBuf, PFpuint8 pBuf, PFdcpfxmemsize uBufSz);

PF_EXTERN_C_END

#endif /*PRIMEDCP_RECV_H_*/
