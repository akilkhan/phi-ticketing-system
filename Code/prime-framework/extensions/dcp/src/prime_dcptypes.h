#ifndef PRIMEDCP_TYPES_H_
#define PRIMEDCP_TYPES_H_

typedef PFbyte    PFdcpVersion;
typedef PFbyte    PFdcpFlags;
typedef PFuint16   PFdcpPacketSize;
typedef PFuint16   PFdcpCrc;
typedef PFuint16   PFdcpSeqNum;
typedef PFuint16 * PFpdcpSeqNum;
typedef PFuint16   PFdcpFragNum;
typedef PFuint16 * PFpdcpServiceId;
typedef PFdword * PFpdcpDevAddr;
typedef PFbyte    PFdcpHwAddr[PF_DCP_MAX_HWADRRESS_SIZE];

typedef PFdword    PFdcpl2IfaceNum;
typedef PFdword    PFdcpl2DevsNum;
typedef PFdword    PFdcpServNum;
typedef PFdword    PFdcpEpNum;
typedef PFdword    PFdcpIncomPacketsNum;
typedef PFdword    PFdcpMtuSize;
typedef PFbyte     PFdcpHwAddrSize;


#endif /*PRIMEDCP_TYPES_H_*/
