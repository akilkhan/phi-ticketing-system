/**
 *
 *                              Copyright (c) 2013
 *                      PhiRobotics Technologies Pvt Ltd
 *               Vedant Commercial Complex, Vartak Nagar, Thane(w),
 *                           Maharashtra-400606, India
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework External Interrupt driver for LPC17xx.
 *
 * 
 * Review status: NO
 *
 */
#pragma once

/**
 * \defgroup PF_EXT_INT_API EXT INT API
 * @{
 */ 

/**		Enumeration for External Interrupt mode	*/

#define EINT5_CH				5
#define EINT5_CHANNEL			PF_CONCAT(EINT, EINT5_CH)

typedef enum{
	enIntModeLowLevel = 0,		/**< The low level of EINT4 generates an interrupt request						*/	
	enIntModToggle,				/**< Any edge of EINT4 generates asynchronously an interrupt request			*/	
	enIntModeFallingEdge,		/**< The falling edge of EINT4 generates asynchronously an interrupt request	*/	
	enIntModeRisingEdge			/**< The rising edge of EINT4 generates asynchronously an interrupt request		*/	
}PFEnEint5Mode;


/**		External interrupt	configure Structure	*/
typedef struct
{
	PFEnEint5Mode	mode;								/**< External interrupt mode									*/
	PFdword			maxCallbacks;						/**< Maximum number of callbacks allowed for tailchaining		*/
	PFcallback* callbackList;			/**< Pointer to array of callbacks to attach to interrupt		*/
}PFCfgEint5;

typedef PFCfgEint5* PFpCfgEint5;

/**
 * The function configures and enables External Interrupt with given settings.
 
 * \param config configuration structure which contains the settings for the external interrupt to be used.

 * \return External Interrupt status.
 */
PFEnStatus pfEint5Open(PFpCfgEint5 config);

/**
 * The function enables External Interrupt 
 *
 * \return External Interrupt status.
 */
PFEnStatus pfEint5Enable(void);

/**
 * The function disables External Interrupt 
 *
 * \return External Interrupt status.
 */
PFEnStatus pfEint5Disable(void);

/**
 * The function adds a callback to callback list if tailchaining is enabled.
 *
 * \param callback callback function to add in the callback list.
 *
 * \return add callback status
 */
PFEnStatus pfEint5AddCallback(PFcallback callback);

/**
 * The function removes the specified callback from callback list.
 *
 * \param callback callback function to add in the callback list.
 *
 * \return remove callback status
 */
PFEnStatus pfEint5RemoveCallback(PFcallback callback);

/**
 * The function disables External Interrupt .
 *
 * \return External Interrupt status.
 */
PFEnStatus pfEint5Close(void);


/** @} */
