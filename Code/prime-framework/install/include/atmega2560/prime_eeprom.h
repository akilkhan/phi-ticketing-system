/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Onchip EEPROM driver for ATmega2560.
 * 
 *
 * Review status: NO
 *
 */ 


#pragma once

#define EEPROM_CH				EEPROM
#define EEPROM_CHANNEL			PERIPH(EEPROM_CH)


typedef enum
{
	enEepromEraseNwrite,
	enEepromErase,
	enEepromWrite,	
}PFEnEEpromMode;

typedef struct
{
	PFEnEEpromMode mode;	
		
}PFCfgEeprom;

typedef PFCfgEeprom* PFpCfgEeprom;

PFEnStatus pfEEpromOpen(PFpCfgEeprom config);


PFEnStatus pfEEpromRead(PFword uiAddress ,PFbyte *data,PFbyte size);


PFEnStatus pfEEpromWriteByte(PFword uiAddress, PFbyte ucData);


PFEnStatus pfEEpromClose(void);

