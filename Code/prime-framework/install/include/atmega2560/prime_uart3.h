/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework UART3 driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */
#pragma once

#define  UART3_USE_FIFO 1

#define UART3_CH				UART3
#define UART3_CHANNEL		PERIPH(UART3_CH)

#if(UART3_USE_FIFO != 0)
	#define  UART3_BUFFER_SIZE 64
#endif	//#if(UART3_USE_FIFO != 0)

typedef enum
{
	enUart3Baudrate_2400 = 0,		/**< UART3 baudrate 2400				*/
	enUart3Baudrate_4800,			/**< UART3 baudrate 4800				*/
	enUart3Baudrate_9600,			/**< UART3 baudrate 9600				*/
	enUart3Baudrate_19200,			/**< UART3 baudrate 19200			*/
	enUart3Baudrate_38400,			/**< UART3 baudrate 38400			*/
	enUart3Baudrate_57600,			/**< UART3 baudrate 57600			*/
	enUart3Baudrate_115200			/**< UART3 baudrate 115200			*/
}PFEnUart3Baudrate;

/**		Enumeration for parity setting for the Uart3 channel		*/
typedef enum
{
	enUart3ParityNone = 0,			/**< Selecting No parity for communication for Uart3	*/
	enUart3ParityEven,				/**< Selecting Even parity for communication  for Uart3*/
	enUart3parityOdd				/**< Selecting Odd parity for communication  for Uart3	*/
}PFEnUart3Parity;

/**		Enumeration for number of stop bits to use for Uart3 channel		*/
typedef enum
{
	enUart3StopBits_1 = 0,			/**< Selecting Number of Stop Bits = 1  for Uart3	*/
	enUart3StopBits_2,				/**< Selecting Number of Stop Bits = 2  for Uart3	*/
}PFEnUart3StopBits;

typedef enum
{
	enUart3DataBits_5=0x00,			/**< Selecting Number of Date Bits = 5  for Uart3	*/
	enUart3DataBits_6=0x01,			/**< Selecting Number of Date Bits = 6  for Uart3*/
	enUart3DataBits_7=0x02,			/**< Selecting Number of Date Bits = 7  for Uart3	*/
	enUart3DataBits_8=0x03,			/**< Selecting Number of Date Bits = 8 	for Uart3*/
	enUart3DataBits_9=0x07			/**< Selecting Number of Date Bits = 9  for Uart3	*/
}PFEnUart3DataBits;



/**		Enumeration for interrupts to enable for the Uart3 channel		*/
typedef enum
{
	enUart3IntNone,					/**< Interrupts dissabled  for Uart3*/
	enUart3IntTx=0x04,				/**< Transmission Interrupts are enabled  for Uart3*/
	enUart3IntRx=0x08,				/**< Reception Interrupts are enabled  for Uart3*/
	enUart3IntTxRx=0x0c				/**< Transmission as well as reception Interrupts are enabled for Uart3*/
}PFEnUart3Interrupt;


typedef enum
{
	enUart3ModeTx=0x01,				/**<  Uart3 in transmission mode */
 	enUart3ModeRx,					/**<  Uart3 in reception mode */
	enUart3ModeTxRx					/**<  Uart3 in transmission as well as reception mode */
}PFEnUart3Mode;

/**		Uart3  configuration structure			*/
typedef struct
{
	PFEnUart3Mode		mode;			/**<   receiver or transmitter mode */
	PFEnUart3Baudrate 	baudrate;		/**<   Set baudrate for channel */
	PFEnUart3Parity 		parity;		/**<   Parity to be used for communication */
	PFEnUart3StopBits 	stopBits;		/**<   Number of stop bits to be used */
	PFEnUart3DataBits 	dataBits;		/**<   Number of stop bits to be used */
	PFEnUart3Interrupt interrupts;		/**<   Interrupts to enable for the channel */
#if(Uart3_USE_FIFO == 0)
	PFcallback		transmitCallback;
	PFcallback		receiveCallback;
#endif
}PFCfgUart3;
typedef PFCfgUart3* PFpCfgUart3;


/**
Description: The function initializes the port with given settings.

Parameters:
config - configuration structure which contains the settings for the communication channel to be used.

Return:	ENStatus. Returns UART3 initialization status.
*/
PFEnStatus pfUart3Open(PFpCfgUart3 config);


/**
Description: Turn offs the UART3 channel

Return:	ENStatus. Returns UART3 initialization status.
 */
PFEnStatus pfUart3Close(void);


/**
Description: The function writes one byte to UART3 data register

Parameters:
channel - UART3 channel to be used for communication.
data - 8 bit data to write to the UART3 channel.

Return:	ENStatus. Returns UART3 write status.
*/
PFEnStatus pfUart3WriteByte(PFbyte data);

/**
Description: 	The function sends multiple bytes on UART3 channel. 
				If transmit interrupt is enabled, the function will enqueue the data in transmit FIFO.
				Otherwise it will wait in the function and send each byte by polling the line status.

Parameters:
channel - UART3 channel to be used for communication.
data - Unsigned char pointer to the data to be sent.
size - Total number of bytes to send.

Return:	ENStatus. Returns UART3 write status.
*/
PFEnStatus pfUart3Write(PFbyte* data, PFdword size);

/**
 * The function reads one byte from UART3 channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \return read byte from UART3 channel.
 */
PFEnStatus pfUart3ReadByte(PFbyte* data);

/**
 * The function reads one byte from UART3 channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \param data Unsigned char pointer to the buffer where the read data should be loaded.
 * \param size Total number of bytes to read.
 * \return UART3 read status.
 */
PFEnStatus pfUart3Read(PFbyte* data, PFdword size, PFdword* readBytes);

#if(UART3_USE_FIFO != 0)
/**
 * Returns the number of bytes received in UART3 buffer.
 *
 * \return number of bytes received in UART3 buffer.
 */
PFEnStatus pfUart3GetRxBufferCount(PFdword* count);

/**
 * This function empties the transmit buffer.
 */
PFEnStatus pfUart3TxBufferFlush(void);

/**
 * This function empties the receive buffer.
 */
PFEnStatus pfUart3RxBufferFlush(void);

#endif

