#pragma once
#ifdef MCU_CHIP_lpc1768
/** Convention: Jumper_Pin number_Periph_Name of the pin_port/pin */

/**							 LED configuration macros 					*/

#define		EDUARM_LED_UL1_PORT	                GPIO_PORT_1
#define 	EDUARM_LED_UL1_PIN		            GPIO_PIN_8

#define		EDUARM_LED_UL2_PORT	                GPIO_PORT_1
#define 	EDUARM_LED_UL2_PIN		            GPIO_PIN_9

#define 	EDUARM_LED_UL3_PORT	                GPIO_PORT_1
#define		EDUARM_LED_UL3_PIN		            GPIO_PIN_10


/** 					Pushbutton configuration macros 			*/

#define		EDUARM_PUSHBUTTON_PB1_PORT 	        GPIO_PORT_1
#define		EDUARM_PUSHBUTTON_PB1_PIN		    GPIO_PIN_14

#define 	EDUARM_PUSHBUTTON_PB2_PORT 	        GPIO_PORT_1
#define		EDUARM_PUSHBUTTON_PB2_PIN		    GPIO_PIN_15

#define 	EDUARM_PUSHBUTTON_PB3_PORT 	        GPIO_PORT_1
#define		EDUARM_PUSHBUTTON_PB3_PIN		    GPIO_PIN_4

#define 	EDUARM_PUSHBUTTON_PB4_PORT 	        GPIO_PORT_1
#define		EDUARM_PUSHBUTTON_PB4_PIN		    GPIO_PIN_0

#define 	EDUARM_PUSHBUTTON_PB5_PORT 	        GPIO_PORT_1
#define		EDUARM_PUSHBUTTON_PB5_PIN		    GPIO_PIN_1


/** 						UART configuration macros 						*/

/** UART 0 */
#define 	EDUARM_UART_0_TX_PORT		        GPIO_PORT_0
#define 	EDUARM_UART_0_TX_PIN			    GPIO_PIN_2

#define 	EDUARM_UART_0_RX_PORT		        GPIO_PORT_0
#define		EDUARM_UART_0_RX_PIN			    GPIO_PIN_3

/** UART2 */
#define    	EDUARM_UART_2_TX_PORT				GPIO_PORT_0
#define 	EDUARM_UART_2_TX_PIN				GPIO_PIN_10

#define    	EDUARM_UART_2_RX_PORT				GPIO_PORT_0
#define 	EDUARM_UART_2_RX_PIN				GPIO_PIN_11
		
/** UART3 */
#define    	EDUARM_UART_3_TX_PORT				GPIO_PORT_0
#define 	EDUARM_UART_3_TX_PIN				GPIO_PIN_0

#define    	EDUARM_UART_3_RX_PORT				GPIO_PORT_0
#define 	EDUARM_UART_3_RX_PIN				GPIO_PIN_1

/** 					External Interrupt configuration macros 					*

/** EINT 3 */
#define		EDUARM_J2_13_EINT_3_PORT		    GPIO_PORT_2
#define		EDUARM_J6_13_EINT_3_PIN			    GPIO_PIN_13

