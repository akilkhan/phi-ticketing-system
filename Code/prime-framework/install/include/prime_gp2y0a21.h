/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Sharp GP2Y0A21 IR sensor Driver.
 * 
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup GP2Y0A21_SHARP_IR_control_Function GP2Y0A21 SHARP IR control Function
 * @{
 */

#define MAX_GP2Y0A21_SENSORS_SUPPORTED   5

 /**
 * to initialize and open GP2Y0A21 sensor
 *
 * \param id specify id allocated to driver
 *
 * \param adc channel corresponding to the sensor
 *
 * \param Number of devices/sensors to initialize
 *
 * \return status of initialization
 *
 * \note Function needs GPIO & Timer to be initialized before calling this function
 */

PFEnStatus pfGP2Y0A21Open(PFbyte* devId,PFbyte* adcChannel,PFbyte deviceCount);

/**
 * to get distance between object and GP2Y0A21 scaled in mm (milli-meter)
 *
 * \param id specify id allocated to driver
 * 
 * \param distance is pointer to copy distance
 *
 * \return status of get distance
 *
 * \note Function needs GPIO and ADC to be initialized before calling this function
 */
 
PFEnStatus pfGP2Y0A21GetDistance(PFbyte id, PFdword* distance);

/**
 * to get distance Raw Digital value according to the processed analog input signal
 *
 * \param id specify id allocated to driver
 * 
 * \param value is pointer to raw output value
 *
 * \return status of get distance
 *
 * \note Function needs GPIO and ADC to be initialized before calling this function
 */
 
PFEnStatus pfGP2Y0A21GetRawValue(PFbyte id, PFword* value);

/**
 * to close the GP2Y0A21 sensor with id number devId.
 *
 * \param devId specify id assigned to driver
 *
 * \return status of initialization
 */
 
PFEnStatus pfGP2Y0A21Close(PFbyte devId);

/** @} */
