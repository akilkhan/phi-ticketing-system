/*
 * atmega2560.h
 *
 * Created: 02-01-2014 14:01:35
 *  Author: Admin
 */ 


#ifndef AT90can128_H_
#define AT90can128_H_

//#pragma anon_unions

#define _RW volatile
#define _R volatile const

typedef struct
{
	_RW PFbyte PIN;
	_RW PFbyte DDR;
	_RW PFbyte PORT;
}GPIO_typedef;

typedef struct
{
	_RW PFbyte UCSRA;
	_RW PFbyte UCSRB;
	_RW PFbyte UCSRC;
	_R PFbyte reserved;
	union
	{
		_RW PFword UBRR;
		struct
		{
			_RW PFbyte UBRRL;
			_RW PFbyte UBRRH;	
		};
	};
	_RW PFbyte UDR;
}UART_typedef;

typedef struct
{
	_RW PFbyte SPCR;
	_RW PFbyte SPSR;
	_RW PFbyte SPDR;
}SPI_typedef;


typedef struct
{
	_RW PFbyte TCCRA;
	_R 	PFbyte reserved;
	_RW PFbyte TCNT;
	_RW PFbyte OCRA;
	_R 	PFbyte reserved2;
}TIMER_8bit_typedef;


typedef struct
{
	_RW PFbyte TIMER0;
	_RW PFbyte TIMER1;
	_RW PFbyte TIMER2;
	_RW PFbyte TIMER3;
}TIMER_interrupt_typedef;

typedef struct
{
	_RW PFbyte TCCRA;
	_RW PFbyte TCCRB;
	_RW PFbyte TCCRC;
	_R  PFbyte reserved;
	union
	{
		_RW PFword TCNT;
		struct
		{
			_RW PFbyte TCNTL;
			_RW PFbyte TCNTH;
		};
	};
	union
	{
		_RW PFword ICR;
		struct
		{
			_RW PFbyte ICRL;
			_RW PFbyte ICRH;
		};
	};
	union
	{
		_RW PFword OCRA;
		struct
		{
			_RW PFbyte OCRAL;
			_RW PFbyte OCRAH;
		};
	};
	union
	{
		_RW PFword OCRB;
		struct
		{
			_RW PFbyte OCRBL;
			_RW PFbyte OCRBH;
		};
	};
	union
	{
		_RW PFword OCRC;
		struct
		{
			_RW PFbyte OCRCL;
			_RW PFbyte OCRCH;
		};
	};						
	
}TIMER_16bit_typedef;


typedef struct
{
	_RW PFbyte TWBR;
	_RW PFbyte TWSR;
	_RW PFbyte TWAR;
	_RW PFbyte TWDR;
	_RW PFbyte TWCR;
	_RW PFbyte TWAMR;
}TWI_typedef;



typedef struct
{
	_RW PFbyte ADCL;
	_RW PFbyte ADCH;
	_RW PFbyte ADCSRA;
	_RW PFbyte ADCSRB;
	_RW PFbyte ADMUX;
}ADC_typedef;

typedef struct
{
	_RW PFbyte EECR;
	_RW PFbyte EEDR;
	_RW PFbyte EEARL;
	_RW PFbyte EEARH;	
}EEPROM_typedef;

typedef struct
{
	_RW PFbyte EICRB;
	_RW PFbyte EICRA;	
}EXTINT_typedef;

typedef struct
{
	_RW PFbyte EIMSK;
}EIMSK_typedef;


typedef struct
{
	_RW PFbyte TIMER0;
	_RW PFbyte TIMER1;
	_RW PFbyte TIMER2;
	_RW PFbyte TIMER3;
	_R 	PFbyte reserved[3];
	_RW PFbyte EXINT;
}IntrFlag_typedef;

typedef struct
{
	_RW PFbyte CANGCON;
	_RW PFbyte CANGSTA;
	_RW PFbyte CANGIT;
	_RW PFbyte CANGIE;
	_RW PFbyte CANEN2;
	_RW PFbyte CANEN1;
	_RW PFbyte CANIE2;
	_RW PFbyte CANIE1;
	_RW PFbyte CANSIT2;
	_RW PFbyte CANSIT1;
	_RW PFbyte CANBT1;
	_RW PFbyte CANBT2;
	_RW PFbyte CANBT3;
	_RW PFbyte CANTCON;
	_RW PFbyte CANTIML;
	_RW PFbyte CANTIMH;
	_RW PFbyte CANTTCL;
	_RW PFbyte CANTTCH;
	_RW PFbyte CANTEC;
	_RW PFbyte CANREC;
	_RW PFbyte CANHPMOB;
	_RW PFbyte CANPAGE;
	_RW PFbyte CANSTMOB;
	_RW PFbyte CANCDMOB;
	_RW PFbyte CANIDT4;
	_RW PFbyte CANIDT3;
	_RW PFbyte CANIDT2;
	_RW PFbyte CANIDT1;
	_RW PFbyte CANIDM4;
	_RW PFbyte CANIDM3;
	_RW PFbyte CANIDM2;
	_RW PFbyte CANIDM1;
	_RW PFbyte CANSTML;
	_RW PFbyte CANSTMH;
	_RW PFbyte CANMSG;
}CAN_typedef;

typedef struct
{
	_RW PFbyte WDTCR;
}WDT_typedef;

#define BIT_0		0x01
#define BIT_1		0x02
#define BIT_2		0x04
#define BIT_3		0x08
#define BIT_4		0x10
#define BIT_5		0x20
#define BIT_6		0x40
#define BIT_7		0x80

/** All vectors */

#define		RESET			0
#define		EINT0			1
#define		EINT1			2
#define		EINT2			3
#define		EINT3			4
#define		EINT4			5
#define		EINT5			6
#define		EINT6			7
#define		EINT7			8
#define		TIMER2_COMP		9
#define		TIMER2_OVF		10
#define		TIMER1_CAPT		11
#define		TIMER1_COMPA	12
#define		TIMER1_COMPB	13
#define		TIMER1_COMPC	14
#define		TIMER1_OVF		15
#define		TIMER0_COMP		16
#define		TIMER0_OVF		17
#define		CANIT_VECT		18
#define		OVRIT_VECT		19
#define		SPI_VECT		20
#define		USART0_RX		21
#define		USART0_UDRE		22
#define		USART0_TX		23
#define		ANALOG_COMP		24
#define		ADC_VECT		25
#define		EEPROM_VECT		26
#define		TIMER3_CAPT		27
#define		TIMER3_COMPA	28
#define		TIMER3_COMPB	29
#define		TIMER3_COMPC	30
#define		TIMER3_OVF		31
#define		USART1_RX		32
#define		USART1_UDRE		33
#define		USART1_TX		34
#define		I2C_VECT		35
#define		SPROGMEM		36


/** ALL gpio port*/

#define PORTA_BASE  		0x20
#define PORTB_BASE  		0x23
#define PORTC_BASE  		0x26
#define PORTD_BASE  		0x29
#define PORTE_BASE  		0x2c
#define PORTF_BASE  		0x2f
#define PORTG_BASE  		0x32

/** UART */

#define UART0_BASE  		0xC0
#define UART1_BASE  		0xC8

/** SPI*/
#define SPI_BASE			0x4C

/** TIMER 8bit*/
#define TIMER0_BASE			0x44
#define TIMER2_BASE			0xB0

/** TIMER 16bit */
#define TIMER1_BASE			0x80
#define TIMER3_BASE			0x90

#define TIMER_INT_BASE		0x6E

/** INTERRUPT FLAG */
#define INT_FLAG_BASE		0x35

/** TWI (I2c)*/
#define TWI_BASE			0xB8

/** EXTINT */
#define EXTINT_BASE			0x69

/** EXTINT */
#define EIMSK_BASE			0x3D

/** ADC */
#define ADC_BASE			0x78

/** EEPROM */
#define EEPROM_BASE			0x3F

/** CAN */
#define CAN_BASE			0xD8

/** WDT */
#define WDT_BASE			0x60

#define GPIO_PORTA			((GPIO_typedef *) PORTA_BASE )
#define GPIO_PORTB			((GPIO_typedef *) PORTB_BASE )
#define GPIO_PORTC			((GPIO_typedef *) PORTC_BASE )
#define GPIO_PORTD			((GPIO_typedef *) PORTD_BASE )
#define GPIO_PORTE			((GPIO_typedef *) PORTE_BASE )
#define GPIO_PORTF			((GPIO_typedef *) PORTF_BASE )
#define GPIO_PORTG			((GPIO_typedef *) PORTG_BASE )

#define PERIPH_UART0		((UART_typedef *) UART0_BASE )
#define PERIPH_UART1		((UART_typedef *) UART1_BASE )

#define PERIPH_SPI			((SPI_typedef *) SPI_BASE )

#define PERIPH_TIMER0		((TIMER_8bit_typedef *) TIMER0_BASE )
#define PERIPH_TIMER1		((TIMER_16bit_typedef *) TIMER1_BASE )
#define PERIPH_TIMER2		((TIMER_8bit_typedef *) TIMER2_BASE )
#define PERIPH_TIMER3		((TIMER_16bit_typedef *) TIMER3_BASE )

#define TIMSK_INT			((TIMER_interrupt_typedef *) TIMER_INT_BASE )

#define PERIPH_TWI			((TWI_typedef *) TWI_BASE )
#define PERIPH_ADC			((ADC_typedef *) ADC_BASE )
#define PERIPH_EEPROM		((EEPROM_typedef *) EEPROM_BASE	)
#define PERIPH_CAN			((CAN_typedef *) CAN_BASE )
#define PERIPH_WDT			((WDT_typedef *) WDT_BASE)

#define EXTINT				((EXTINT_typedef *) EXTINT_BASE	)
#define PERIPH_EIMSK		((EIMSK_typedef *) EIMSK_BASE )

#define INT_FLAG			((IntrFlag_typedef *) INT_FLAG_BASE	)


void __vector_0(void)  __attribute__ ((signal,used));
void __vector_1(void)  __attribute__ ((signal,used));
void __vector_2(void)  __attribute__ ((signal,used));
void __vector_3(void)  __attribute__ ((signal,used));
void __vector_4(void)  __attribute__ ((signal,used));
void __vector_5(void)  __attribute__ ((signal,used));
void __vector_6(void)  __attribute__ ((signal,used));
void __vector_7(void)  __attribute__ ((signal,used));
void __vector_8(void)  __attribute__ ((signal,used));
void __vector_9(void)  __attribute__ ((signal,used));
void __vector_10(void)  __attribute__ ((signal,used));
void __vector_11(void)  __attribute__ ((signal,used));
void __vector_12(void)  __attribute__ ((signal,used));
void __vector_13(void)  __attribute__ ((signal,used));
void __vector_14(void)  __attribute__ ((signal,used));
void __vector_15(void)  __attribute__ ((signal,used));
void __vector_16(void)  __attribute__ ((signal,used));
void __vector_17(void)  __attribute__ ((signal,used));
void __vector_18(void)  __attribute__ ((signal,used));
void __vector_19(void)  __attribute__ ((signal,used));
void __vector_20(void)  __attribute__ ((signal,used));
void __vector_21(void)  __attribute__ ((signal,used));
void __vector_22(void)  __attribute__ ((signal,used));
void __vector_23(void)  __attribute__ ((signal,used));
void __vector_24(void)  __attribute__ ((signal,used));
void __vector_25(void)  __attribute__ ((signal,used));
void __vector_26(void)  __attribute__ ((signal,used));
void __vector_27(void)  __attribute__ ((signal,used));
void __vector_28(void)  __attribute__ ((signal,used));
void __vector_29(void)  __attribute__ ((signal,used));
void __vector_30(void)  __attribute__ ((signal,used));
void __vector_31(void)  __attribute__ ((signal,used));
void __vector_32(void)  __attribute__ ((signal,used));
void __vector_33(void)  __attribute__ ((signal,used));
void __vector_34(void)  __attribute__ ((signal,used));
void __vector_35(void)  __attribute__ ((signal,used));
void __vector_36(void)  __attribute__ ((signal,used));


#endif /* AT90can128_H_ */