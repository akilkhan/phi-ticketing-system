/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework 16-bit Timer driver for AT90CAN128.
 *
 * 
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_TIMER_API Timer API
 * @{
 */ 
 #define Timer1InputCaptureNoiseCanceler__  	0
 #define TIMER1_CH								TIMER1
 #define TIMER1_CHANNEL							PERIPH(TIMER1_CH)
 
/** Enumeration for timer modes			*/
typedef enum
{
	enTimer1Noclock = 0,			/**< No clock source is selected and the timer is disabled				*/
	enTimer1NoprescaledClk,			/**< clock source is selected and the timer is not pre-scaled			*/
	enTimer1ClkDivideby8,			/**< Clock source frequency is divided by 8								*/
	enTimer1ClkDivideby64,			/**< Clock source frequency is divided by 64							*/
	enTimer1ClkDivideby256,			/**< Clock source frequency is divided by 256							*/
	enTimer1ClkDivideby1024,		/**< Clock source frequency is divided by 1024							*/
	enTimer1ExtClkFallingEdge,		/**<  timer with external source on falling edge 						*/
	enTimer1ExtClkRisingEdge		/**<  timer with external source on falling edge 						*/
}PFEnTimer1Clocksource;


/** Enumeration for external match control		*/
typedef enum
{
	enTimer1ExtMatchCtrlNone = 0,				/**< Do nothing on count match								*/
	enTimer1ExtMatchCtrlTogglePin,			/**< Toggle match pin											*/
	enTimer1ExtMatchCtrlClearPin,				/**< Clear match pin										*/
	enTimer1ExtMatchCtrlSetPin				/**< Set match pin												*/
}PFEnTimer1ExtMatchCtrl;

typedef enum
{
	enTimer1NormalMode=0x00,	    /**<  timer in Normal Mode 												*/
	enTimer1CtcOcrMode=0x08,		/**<  timer in clear timer on compare  Mode,  compare with OCR			*/
	enTimer1CtcIcrMode=0x18		/**<  timer in clear timer on compare  Mode, compare with ICR			*/
}PFEnTimer1Mode;

/**	No interrupt	*/
#define TIMER1_INT_NONE			0x00
/**	interrupt on overflow	*/
#define TIMER1_INT_OVERFLOW		0x01
	/**	interrupt on matching with the value in OCR1A 	*/
#define TIMER1_INT_MATCHREG_A	0x02
/**	interrupt on matching with the value in OCR1B 	*/
#define TIMER1_INT_MATCHREG_B	0x04
/**	interrupt on matching with the value in OCR1C 		*/
#define TIMER1_INT_MATCHREG_C	0x08
/**	interrupt on matching with the value in ICR1 		*/
#define TIMER1_INT_INPUTCAPTURE	0x20
/**	interrupt on matching with the value in OCR1A ,OCR1B,ICR1 and overflow 	*/
#define TIMER1_INT_ALL			0x2F


/**		Timer configure structure		*/
typedef struct
{
	PFEnTimer1Clocksource		clockSource;			/**< Select clock source							*/
	PFEnTimer1Mode		        timer1Mode;				/** timer mode										*/
	PFword				        matchValueA;			/**< Match register A compare value					*/
	PFword				        matchValueB;			/**< Match register B compare value					*/
	PFword				        matchValueC;			/**< Match register C compare value					*/
	PFEnTimer1ExtMatchCtrl 		exMatchActionA;			/**< match pin control on count match 				*/
	PFEnTimer1ExtMatchCtrl 		exMatchActionB;			/**< match pin control on count match 				*/ 
	PFEnTimer1ExtMatchCtrl 		exMatchActionC;			/**< match pin control on count match 				*/
	PFbyte	      			 	interrupt;				/**< To enable or disable timer interrupt			*/
	PFEnBoolean                 inputCaptureRisingEdge; /**< to set input capture signal edge selection 	*/
	PFcallback			        cmpMatchACallback;		/**< Callback function for timer ISR				*/
	PFcallback			        cmpMatchBCallback;
	PFcallback			        cmpMatchCCallback;
	PFcallback			        inputCaptureCallback;
	PFcallback			        overFlowCallback;
}PFCfgTimer1;

/** Pinter to PFCfgTiemr structure		*/
typedef PFCfgTimer1* PFpCfgTimer1;

/**
 * Initialized timer with given parameters
 * 
 * \param config timer configuration structure
 * 
 * \return timer initialization status
 */
PFEnStatus pfTimer1Open(PFpCfgTimer1 config);

/**
 * Stops timer operation and turn offs the timer module
 * 
 * \return timer turn off operation status
 */
PFEnStatus pfTimer1Close(void);

/**
 * Starts timer operation
 * 
 * \return timer start status
 */
PFEnStatus pfTimer1Start(void);

/**
 * Stops timer operation
 * 
 * \return timer stop status
 */
PFEnStatus pfTimer1Stop(void);

/**
 * Resets the timer operation. Timer will start counting from zero again.
 * 
 * \return timer reset status
 */
PFEnStatus pfTimer1Reset(void);

/**
 * Enables timer interrupt.
 * 
 * \param interrupt value to enable specific timer interrupt.
 * 
 * \return timer interrupt enable status.
 */
PFEnStatus pfTimer1IntEnable(PFbyte interrupt);

/**
 * Disables timer interrupt.
 * 
 * \param interrupt value to disable specific timer interrupt.
 * 
 * \return timer interrupt disable status.
 */
PFEnStatus pfTimer1IntDisable(PFbyte interrupt);

/**
 * Returns the timer count
 * 
 * \return timer count
 */
PFEnStatus pfTimer1ReadCount(PFdword* count);

/**
 * Returns Time stamp at which event occurred at ICP pin
 * 
 * \return Input capture count
 */
PFEnStatus pfTimer1ReadCaptureCount(PFdword* count);

/**
* Writes new value to the match register and enables latch for the match register
*
* \param regNum index of match register to be updated
* \param regValue new value for match register
*
* \return match register update status
*/
PFEnStatus pfTimer1UpdateMatchRegister(PFbyte regNum, PFword regVal); 

/** @} */
