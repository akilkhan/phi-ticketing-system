#ifndef PRIMEDCP_CONFIG_H_
#define PRIMEDCP_CONFIG_H_

#include "prime_types.h"

extern int pfdcp_debug_printf(char * format, ...);

/** defines whether to print debug information of DCP (boolean). */
#define PF_DCP_DEBUG     0

/** defines function to print debug information */
#define PF_DCP_DEBUG_PRINT(...)         pfdcp_debug_printf(__VA_ARGS__)

/** defines function to get time in mili seconds */
#define pfDcpTimGet()   			 	pfDelayGetTick()

/** defines function to wait for ownership of critical section. */
#define pfDcpLock(...)				    0
/** defines function to release ownership of critical section. */
#define pdDcpUnlock(...)               do{}while(0)
    
//-----------------------------------------------------------------------
/** maximum number of DCP Instance supported */
#define	PF_DCP_MAX_INSTANCE_SUPPORTED		1

/** defines whether to use dynamic routing table (boolean). */
#define PF_DCP_CONFIG_DYNAMIC_ROUTES           1

/** maximum number of dynamic peer route supported */
#define	PF_DCP_MAX_DYNAMICROUTES_SUPPORTED  	1

/** Own-mask: Address from received packet will be masked with this value */
#define PF_DCP_OWNMASK                     	0xFFFFFFFF

/** DCP protocol memory pool size. */
#define PF_DCP_PROTOMEMPOOL_SIZE				11240

/** maximum number of service supported */
#define PF_DCP_MAX_SERVICES_NUM         		2

/** maximum number outgoing endpoint supported */
#define PF_DCP_MAX_OUTEP_NUM               	2

/** maximum number incoming packets supported at same time */
#define PF_DCP_MAX_INCOMPACK_NUM            	10

/** defines maximum receive buffer size */
#define PF_DCP_MAX_RXBUF_SIZE            	256
/** defines maximum receive buffer number */
#define PF_DCP_MAX_RXBUF_NUM                8

//-----------------------------------------------------------------------
/** defines transmit buffer size of L2 device */
#define PF_DCP_L2TX_BUFFER_SIZE             (PF_DCP_HDR_SIZE+50)		//Must not greater then MIN(UART_BUF_SZ,SPI_BUF_SZ, (CAN_BUFFER_SIZE/CAN_MSG_SIZE))
/** defines receive buffer size of L2 device*/
#define PF_DCP_L2RX_BUFFER_SIZE             128

/** maximum number of CAN interface supported */
#define PF_DCP_L2CAN_MAX_INTERFACE          2
/** maximum number of UART interface supported */
#define PF_DCP_L2UART_MAX_INTERFACE         2
/** maximum number of SPI interface supported */
#define PF_DCP_L2SPI_MAX_INTERFACE          2

/** defines whether to use SPI interface as master(boolean). */
#define PF_DCP_L2SPI_ENABLE_MASTER          1

/** defines synchronization daly in micro second for SPI interface */
#define PF_DCP_L2SPI_SYNC_DELAY             340 /*us*/   /* min:330us*/
//-----------------------------------------------------------------------.

/** defines maximum payload supported by the device */
#define PF_DCP_MAX_PAYLOAD_SIZE             256

/** maximum hardware address length (shall be maximum from the address length used by hardware interfaces of the platform). */
#define PF_DCP_MAX_HWADRRESS_SIZE           sizeof(PFdword)

/** defines transmit retry time out value in mili second*/
#define PF_DCP_TX_RETRY_TMO                 1000 /*ms*/
/** defines maximum transmit retry number*/
#define PF_DCP_TX_RETRIES_NUM               5

/** defines incomplete received packet time out value in mili second*/
#define PF_DCP_RX_PACKET_TMO                1000 /*ms*/

/** defines incomplete received datagram time out value in mili second*/
#define PF_DCP_RX_DGRAM_TMO                 (5*PF_DCP_TX_RETRY_TMO) /*ms*/

/** defines acknowledge transmit retry time out value in mili second*/
#define PF_DCP_SPECPACK_TX_RETRY_TMO        150

/** defines maximum acknowledge transmit retry number*/
#define PF_DCP_SPECPACK_RETRIES_NUM         2

#endif /*PRIMEDCP_CONFIG_H_*/


