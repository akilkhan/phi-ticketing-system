/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework MB1200 driver.
 *
 * 
 * Review status: NO
 *
 */


#pragma once
#include "prime_gpio.h"

#define		MB1200_USE_ADC				1
#define		MB1200_USE_TIMER			2

#define		MB1200_INTERFACE			MB1200_USE_TIMER

/**
 * \defgroup Ultrasonic_Range_Finder_control_Function Ultrasonic Range Finder control Function
 * @{
 */
#define MAX_NO_OF_ULTRASONICMB1200_SUPPORTED 5     /** number of ultrasonic devices connected */

/** enumeration for setting reference voltage of ADC */
typedef enum
{
	enUltrasonicMb1200RefVtg5V=0x00,
	enUltrasonicMb1200RefVtg3V3
}PFEnUltrasonicMb1200RefVtg;

/** Ultrasonic Range Finder configuration structure */
typedef struct
{
	#if(MB1200_INTERFACE == MB1200_USE_TIMER)
  PFGpioPortPin trigger;                    							/**< specify Trigger port and pin at which Trigger pulse to apply */
  PFGpioPortPin echo;                    									/**< specify Echo port and pin at which Trigger pulse to apply */
	#endif
	#if(MB1200_INTERFACE == MB1200_USE_ADC)
	PFEnUltrasonicMb1200RefVtg refVtg;       								/**< specify reference voltage for ADC */
	PFbyte channel;																					/**< ADC Channel No. to which Sensor is connected */
	PFEnStatus (*pfAdcGetVoltageSingleConversion)(PFbyte channel, PFdword* milliVolt);		/**	Function pointer of pfAdcGetVoltageSingleConversion for ADC volatage reading */
	#endif
}PFCfgUltrasonicMb1200;

/** pointer to PFCfgUltrasonicMb1200 structure */
typedef PFCfgUltrasonicMb1200* PPfCfgUltrasonicMb1200;

/**
 * to initialize Ultrasonic Range Finder
 *
 * \param devId pointer to copy id/id's assigned by driver
 *
 * \param sensorConfig structure to initialize Ultrasonic Range Finder
 *
 * \param deviceCount  number of Ultrasonic Range Finder to be initialized
 * 
 * \return status of initialization
 *
 * \note Function needs GPIO & Timer to be initialized before calling this function
 */
PFEnStatus pfUltrasonicMb1200Open(PFbyte* devId, PPfCfgUltrasonicMb1200 sensorConfig, PFbyte deviceCount);

/**
 * to get distance of obstacle
 *
 * \param devId specify id allocated to driver
 *
 * \param distance is pointer to copy distance in mm
 *
 * \return status of get distance
 */
 
PFEnStatus pfUltrasonicMb1200GetDistance(PFbyte* devId, PFdword* distance);

/** 
 * Function closes Ultrasonic Range Finder module with devId
 *
 * \param devId specify id allocated to driver
 *
 * \return Ultrasonic Range Finder disable status
 */
PFEnStatus pfUltrasonicMb1200Close(PFbyte* devId);

/** @} */
