/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework I2C driver for AT90CAN128.
 *
 * 
 * Review status: NO
 *
 */
#pragma once

/**
 * \defgroup PF_CAN_API CAN API
 * @{
 */ 
 
 /**		CAN Config Macros			*/ 

#define CAN_CH						CAN
#define CAN_CHANNEL					PERIPH(CAN_CH) 

#define CAN_MAX_MOBS_NUMBER			15
#define CAN_RX_MOB					13
#define CAN_TX_MOB					(CAN_MAX_MOBS_NUMBER - RX_MOB)

/** 
 * Mark CAN_USE_FIFO as 1 if internal software buffer is to be used in interrupt based communication.
 * If it is marked as 0, user should provide callbacks to handle transmit and receive interrupts.
 */
#define CAN_USE_FIFO				1
	
#if(CAN_USE_FIFO != 0)
/** 
 * Define size in bytes for internal software buffer.
 * The buffer size should be a non-zero and power of 2 number.
 */
	#define CAN_BUFFER_SIZE		256
#endif	// #if(CAN_USE_FIFO != 0)

/**		Enumeration for interrupts to enable for the CAN channel		*/
typedef enum
{
	enCanIntOvrt=0x01,		/**< Can Timer overrun interrupt */
	enCanIntEnerg=0x02,	/**< general errors interrupt  */
	enCanIntEnbx=0x04,		/**< frame buffer interrupt  */
	enCanIntErr=0x08,	/**< MOb errors  interrupt  */
	enCanIntTx=0x10,		/**< transmit interrupt  */
	enCanIntRx=0x20,		/**<  receive interrupt  */
	enCanIntBoff=0x40,	/**< bus off interrupt  */
	//enCanIntEnit=0x80,		/**< CANIT interrupt   */
}PFEnCanInterrupt;
/**\note Enabling the CANGIE's ENIT is must to access the CAN vector */

/** Enumeration for CAN frame format		*/
typedef enum
{
	enCanFrameStandard = 0,				/**< CAN standard frame	with 11 bit ID	*/
	enCanFrameExtended					/**< CAN extended frame	with 29 bit ID	*/
}PFEnCanFrameFormat;

/** 		CAN message structure		*/
typedef struct
{
	PFdword id;							/**< CAN message ID													*/
	PFword length;						/**< CAN message length in number of bytes. Length should be less than or equal to 8	*/
	PFEnCanFrameFormat frameFormat;		/**< Choose between extended frame or standard frame				*/
	PFEnBoolean remoteFrame;			/**< Choose between normal transmission or remote frame request		*/
	PFbyte data[8];						/**< CAN packet data. Data can be of maximum 8 bytes.				*/
}PFCanMessage;

/** 		Pointer to PFCanMessage structure		*/
typedef PFCanMessage* PFpCanMessage;

/**			CAN Configuration Structure				*/
typedef struct
{
	PFdword baudrate;				/**< Baudrate for CAN module						*/
	PFEnCanInterrupt interrupt;		/**< Interrupts to enable for CAN channel			*/
	PFcallback errCallback;			/**< Callback for error counter overflow			*/
#if(CAN_USE_FIFO == 0)			
	PFcallback rxCallback;			/**< Callback for message receive interrupt			*/
	PFcallback txCallback;			/**< Callback for message transmit interrupt		*/
#endif // #if(CAN_USE_FIFO == 0)	
}PFCfgCan;

/** Pointer to PFCfgCan structure		*/
typedef PFCfgCan* PFpCfgCan;

/**
 * Initializes CAN channel with proveded settings
 * 
 * \param config Configuration structure for CAN channel.
 * \return Status
 */
PFEnStatus pfCanOpen(PFpCfgCan config);

/**
 * Deinitializes and turns off CAN channel.
 * 
 * \return Status
 */
PFEnStatus pfCanClose(void);

/**
 * Sends a message over CAN bus. 
 * According to the macro and initialization settings chosen, 
 * this function will decide to write the message directly or push it onto FIFO buffer.
 * 
 * \param message Pointer to CAN message structure to be sent.
 * \return Status
 */
PFEnStatus pfCanWrite(PFpCanMessage message);

/**
 * Reads a message received from CAN bus. 
 * According to the macro and initialization settings chosen, 
 * this function will decide to read the message directly or from FIFO buffer.
 * 
 * \param message Pointer to CAN message structure at which the received message should be stored.
 * \return Status
 */
PFEnStatus pfCanRead(PFpCanMessage message);

/**
 * The function will check the CAN status flag for mentioned parameter.
 *
 * \param param Parameter to check the status for.
 * \return Status of the mentioned parameter, if the flag is raised is or not.
 */
//PFEnBoolean pfCanCheckStatus(PFEnCanStatus param);

/**
 * The function provides interface to read transmission error counter.
 *
 * \param errCount Pointer to byte to which the function should write error counter value.
 * \return status
 */
PFEnStatus pfCanGetTxErrCounter(PFbyte* errCount);

/**
 * The function provides interface to read reception error counter.
 *
 * \param errCount Pointer to byte to which the function should write error counter value.
 * \return status
 */
PFEnStatus pfCanGetRxErrCounter(PFbyte* errCount);

#if(CAN_USE_FIFO != 0)
/**
 * Returns the number of bytes received in CAN buffer.
 *
 * \return number of bytes received in CAN buffer.
 */
PFEnStatus pfCanGetRxBufferCount(PFdword* buffer_count);

/**
 * This function empties the transmit buffer.
 */
void pfCanTxBufferFlush(void);

/**
 * This function empties the receive buffer.
 */
void pfCanRxBufferFlush(void);
#endif	// #if(CAN_USE_FIFO != 0)


 
/** @} */
 

