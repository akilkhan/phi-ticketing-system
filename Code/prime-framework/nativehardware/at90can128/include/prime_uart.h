/**		Enumeration for baudrate to set for UART channel		*/
//#include "prime_types.h"
#pragma once

#define  UART_USE_FIFO 1



#define UART_CH				UART0
#define UART_CHANNEL		PERIPH(UART_CH)

#if(UART_USE_FIFO != 0)
	#define  UART_BUFFER_SIZE 64
#endif	//#if(UART_USE_FIFO != 0)

typedef enum
{
	enUartBaudrate_2400 = 0,
	enUartBaudrate_4800,
	enUartBaudrate_9600,
	enUartBaudrate_19200,
	enUartBaudrate_38400,
	enUartBaudrate_57600,
	enUartBaudrate_115200
}PFEnUartBaudrate;


/**		Enumeration for parity setting for the UART channel		*/
typedef enum
{
	enUartParityNone = 0,
	enUartParityEven,
	enUartparityOdd
}PFEnUartParity;




/**		Enumeration for number of stop bits to use for UART channel		*/
typedef enum
{
	enUartStopBits_1 = 0,
	enUartStopBits_2,
}PFEnUartStopBits;

typedef enum
{
	enUartDataBits_5=0x00,
	enUartDataBits_6=0x01,
	enUartDataBits_7=0x02,
	enUartDataBits_8=0x03,
	enUartDataBits_9=0x07
}PFEnUartDataBits;


/**		Enumeration for interrupts to enable for the UART channel		*/
typedef enum
{
	enUartIntNone,
	enUartIntTx=0x04,
	enUartIntRx=0x08,
	enUartIntTxRx=0x0c
}PFEnUartInterrupt;


typedef enum
{
	enUartModeTx=0x01,
	enUartModeRx,
	enUartModeTxRx
}PFEnUartMode;
/**		UART configuration structure			*/
typedef struct
{
	PFEnUartMode		mode;			// receiver or transmitter mode
	PFEnUartBaudrate 	baudrate;		// Set baudrate for channel
	PFEnUartParity 	parity;			// Parity to be used for communication
	PFEnUartStopBits 	stopBits;		// Number of stop bits to be used
	PFEnUartDataBits 	dataBits;		// Number of stop bits to be used
	PFEnUartInterrupt interrupts;		// Interrupts to enable for the channel
#if(UART_USE_FIFO == 0)
	PFcallback		transmitCallback;
	PFcallback		receiveCallback;
#endif
}PFCfgUart;

typedef PFCfgUart* PPFCfgUart;


/**
Description: The function initializes the port with given settings.

Parameters:
config - configuration structure which contains the settings for the communication channel to be used.

Return:	ENStatus. Returns UART initialization status.
*/
PFEnStatus pfUartOpen(PPFCfgUart config);


/**
Description: Turn offs the UART channel

Return:	ENStatus. Returns UART initialization status.
 */
PFEnStatus pfUartClose(void);


/**
Description: The function writes one byte to UART data register

Parameters:
channel - UART channel to be used for communication.
data - 8 bit data to write to the UART channel.

Return:	ENStatus. Returns UART write status.
*/
PFEnStatus pfUartWriteByte(PFbyte data);

/**
Description: 	The function sends multiple bytes on UART channel. 
				If transmit interrupt is enabled, the function will enqueue the data in transmit FIFO.
				Otherwise it will wait in the function and send each byte by polling the line status.

Parameters:
channel - UART channel to be used for communication.
data - Unsigned char pointer to the data to be sent.
size - Total number of bytes to send.

Return:	ENStatus. Returns UART write status.
*/
//EnStatus UartWrite(PFbyte channel, PPFbyte data, uint32_t size);



PFEnStatus pfUartWrite(PFbyte* data, PFdword size);

/**
 * The function reads one byte from UART channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \return read byte from UART channel.
 */
PFEnStatus pfUartReadByte(PFbyte* read_byte);

/**
 * The function reads one byte from UART channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \param data Unsigned char pointer to the buffer where the read data should be loaded.
 * \param size Total number of bytes to read.
 * \return UART read status.
 */
PFEnStatus pfUartRead(PFbyte* data, PFdword size, PFdword* readBytes);

#if(UART_USE_FIFO != 0)
/**
 * Returns the number of bytes received in UART buffer.
 *
 * \return number of bytes received in UART buffer.
 */
PFEnStatus pfUartGetRxBufferCount(PFdword* buffer_count);

/**
 * This function empties the transmit buffer.
 */
void pfUartTxBufferFlush(void);

/**
 * This function empties the receive buffer.
 */
void pfUartRxBufferFlush(void);

#endif