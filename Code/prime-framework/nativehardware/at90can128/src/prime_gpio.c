#include "prime_framework.h"
#include "prime_gpio.h"

PFEnStatus pfGpioPinConfig(PFpCfgGpio config)
{
#ifdef PF_GPIO_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if(config->direction > 1 )
	{
		return enStatusInvArgs;	
	}
	
	if(config->mode > 1 )
	{
		return enStatusInvArgs;
	}	
#endif //PF_GPIO_DEBUG

	if(config->direction != enGpioDirInput )
	{
		config->port->DDR |= (config->pins);
		
	}
	else
	{
		config->port->DDR &= ~(config->pins);
	}
	if(config->mode != enGpioPinModePullDown )
	{
		config->port->PORT =(config->pins);
	}	
	else
	{
		config->port->PORT =(config->pins);
	}	
	return enStatusSuccess;
	
}

PFEnStatus pfGpioInit(PFpCfgGpio config, PFbyte count)
{
	PFbyte i;
	PFEnStatus status;
#ifdef PF_GPIO_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}	
#endif //PF_GPIO_DEBUG
	if(config !=0)
	{
		for (i=0;i<count;i++)
		{
			status=pfGpioPinConfig(&config[i]);
			if(status!=enStatusSuccess)
			{
				return enStatusNotConfigured;
				break;
			}			
		}		
	}
	
	return enStatusSuccess;	
}
