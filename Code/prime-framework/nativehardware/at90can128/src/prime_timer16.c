#include "prime_framework.h"
#include "prime_timer16.h"
// #include <avr/io.h>
// #include <avr/interrupt.h>

static PFEnStatus timer16InitFlag=0;

static PFcallback timer16cmpMatchACallback;
static PFcallback timer16cmpMatchBCallback;
static PFcallback timer16cmpMatchCCallback;
static PFcallback timer16inputCaptureCallback;
static PFcallback timer16overFlowCallback;

PFEnStatus pfTimer16Open(PPFCfgTimer16 config)
{
#ifdef PFTIMER16_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}		
    if( (config->timerMode<0) || (config->timerMode != 0x08) && (config->timerMode != 0x18) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionB < 0) || (config->exMatchActionB > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionC < 0) || (config->exMatchActionC > 3) )
	{
		return enStatusInvArgs;
	}		
#endif
// 	TIMER16_CHANNEL->TCCRA = 0;
// 	TIMER16_CHANNEL->TCCRB = 0;
// 	
	if(config->timerMode)
	{
		TIMER16_CHANNEL->TCCRB = config->timerMode;
	}	
	TIMER16_CHANNEL->OCRA = config->matchValueA;
	TIMER16_CHANNEL->OCRB = config->matchValueB;
	TIMER16_CHANNEL->OCRC = config->matchValueC;
	
	if(config->inputCaptureRisingEdge)
	{
		TIMER16_CHANNEL->TCCRB |= 1<<6;
	}	
	if(Timer16InputCaptureNoiseCanceler__)
	{
		TIMER16_CHANNEL->TCCRB |= 1<<7;
	}	
	if(config->exMatchActionA != enTimer16ExtMatchCtrlNone)
	{
		TIMER16_CHANNEL->TCCRA = config->exMatchActionA << 6;
	}	
	if(config->exMatchActionB!=enTimer16ExtMatchCtrlNone)
	{
		TIMER16_CHANNEL->TCCRA |=config->exMatchActionB << 4;
	}	
	if(config->exMatchActionC!=enTimer16ExtMatchCtrlNone)
	{
		TIMER16_CHANNEL->TCCRA |=config->exMatchActionC << 2;
	}	
	if(config->cmpMatchACallback !=0)
	{
		timer16cmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->cmpMatchBCallback !=0)
	{
		timer16cmpMatchBCallback =config->cmpMatchBCallback;
	}	
	if(config->cmpMatchCCallback !=0)
	{
		timer16cmpMatchCCallback =config->cmpMatchCCallback;
	}	
	if(config->inputCaptureCallback !=0)
	{
		timer16inputCaptureCallback =config->inputCaptureCallback;
	}	
	if(config->overFlowCallback !=0)
	{
		timer16overFlowCallback =config->overFlowCallback;
	}
	if( config->interrupt != enTimer16IntNone )
	{
		//TIMER16_CHANNEL->TIMSK = config.interrupt;
		TIMSK_INT->TIMER1 = config->interrupt;
	}		
	if(config->clockSource <= 7)
	{
		TIMER16_CHANNEL->TCCRB |= config->clockSource;
	}		

    timer16InitFlag=1;

	return enStatusSuccess;
	
}

PFEnStatus pfTimer16Close(void)
{
#ifdef PFTIMER16_DEBUG
    if(timer16InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	TIMER16_CHANNEL->TCCRB = 0x00;
	TIMER16_CHANNEL->TCNT = 0;
	
	return enStatusSuccess;
}

PFEnStatus pfTimer16Start(void)
{
#ifdef PFTIMER16_DEBUG
	if(timer16InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	PFbyte val;
	val=TIMER16_CHANNEL->TCCRB;
	TIMER16_CHANNEL->TCCRB = 0x00;
	TIMER16_CHANNEL->TCCRB |= val;
	TIMER16_CHANNEL->TCNT = 0;
	
	return enStatusSuccess;
}
PFEnStatus pfTimer16Reset(void)
{
#ifdef PFTIMER16_DEBUG
	if(timer16InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	TIMER16_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}
PFEnStatus pfTimer16ReadCount(PFword* count)
{
#ifdef PFTIMER16_DEBUG
	if(timer16InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	//PFword data;
	count =TIMER16_CHANNEL->TCNT;
	return enStatusSuccess;
}

PFEnStatus pfTimer16ReadCaptureCount(PFword* CaptureCount)
{
#ifdef PFTIMER16_DEBUG
	if(timer16InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
//	PFword data;
	CaptureCount =TIMER16_CHANNEL->ICR;
	return enStatusSuccess;
}

PFEnStatus pfTimer16UpdateMatchRegister(PFbyte regNum, PFword regVal)
{
#ifdef PFTIMER16_DEBUG
	if(timer16InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if( (regNum < 'A') || (regNum > 'C') )
	{
		return enStatusNotConfigured;
	}	
#endif
	switch(regNum)
	{
		case 'A':	
				  TIMER16_CHANNEL->OCRA =regVal;
				  break;
		
		case 'B':
				  TIMER16_CHANNEL->OCRB =regVal;
				  break;
				
		case 'C':
				  TIMER16_CHANNEL->OCRC =regVal;
				  break;
	}
	return enStatusSuccess;
}

//ISR(TIMER1_COMPA_vect)
void INT_HANDLER(TIMER1_COMPA)(void)
//INT_HANDLER(TIMER1_COMPA)
{
	if(timer16cmpMatchACallback !=0)
	timer16cmpMatchACallback();
}

//ISR(TIMER1_COMPB_vect)
void INT_HANDLER(TIMER1_COMPB)(void)
//INT_HANDLER(TIMER1_COMPB)
{
	if(timer16cmpMatchBCallback !=0)
	timer16cmpMatchBCallback();
}

//ISR(TIMER1_COMPC_vect)
void INT_HANDLER(TIMER1_COMPC)(void)
//INT_HANDLER(TIMER1_COMPC)
{
	if(timer16cmpMatchCCallback !=0)
	timer16cmpMatchCCallback();
}

//ISR(TIMER1_CAPT_vect)
void INT_HANDLER(TIMER1_CAPT)(void)
//INT_HANDLER(TIMER1_CAPT)
{
	if(timer16inputCaptureCallback !=0)
	timer16inputCaptureCallback();
}

//ISR(TIMER1_OVF_vect)
void INT_HANDLER(TIMER1_OVF)(void)
//INT_HANDLER(TIMER1_OVF)
{
	if(timer16overFlowCallback !=0)
	timer16overFlowCallback();
}