/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework 16-bit Timer driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_TIMER_API Timer API
 * @{
 */ 
 #define Timer4InputCaptureNoiseCanceler__  	0
 #define TIMER4_CH								TIMER4
 #define TIMER4_CHANNEL						PERIPH(TIMER4_CH)
 
/** Enumeration for timer modes			*/
typedef enum
{
	enTimer4Noclock = 0,			/**< No clock source is selected and the timer is disabled				*/
	enTimer4NoprescaledClk,		/**< clock source is selected and the timer is not pre-scaled			*/
	enTimer4ClkDivideby8,			/**< Clock source frequency is divided by 8								*/
	enTimer4ClkDivideby64,			/**< Clock source frequency is divided by 64							*/
	enTimer4ClkDivideby256,		/**< Clock source frequency is divided by 256							*/
	enTimer4ClkDivideby1024,		/**< Clock source frequency is divided by 1024							*/
	enTimer4ExtClkFallingEdge,		/**<  timer with external source on falling edge 						*/
	enTimer4ExtClkRisingEdge		/**<  timer with external source on falling edge 						*/
}PFEnTimer4Clocksource;


/** Enumeration for external match control		*/
typedef enum
{
	enTimer4ExtMatchCtrlNone = 0,				/**< Do nothing on count match								*/
	enTimer4ExtMatchCtrlTogglePin,			/**< Toggle match pin											*/
	enTimer4ExtMatchCtrlClearPin,				/**< Clear match pin										*/
	enTimer4ExtMatchCtrlSetPin				/**< Set match pin												*/
}PFEnTimer4ExtMatchCtrl;

typedef enum
{
	enTimer4NormalMode=0x00,	    /**<  timer in Normal Mode 												*/
	enTimer4CtcOcrMode=0x08,		/**<  timer in clear timer on compare  Mode,  compare with OCR			*/
	enTimer4CtcIcrMode=0x18		/**<  timer in clear timer on compare  Mode, compare with ICR			*/
}PFEnTimer4Mode;

/**	No interrupt	*/
#define TIMER4_INT_NONE			0x00
/**	interrupt on overflow	*/
#define TIMER4_INT_OVERFLOW		0x01
/**	interrupt on matching with the value in OCR4A 	*/
#define TIMER4_INT_MATCHREG_A	0x02
/**	interrupt on matching with the value in OCR4B 	*/
#define TIMER4_INT_MATCHREG_B	0x04
/**	interrupt on matching with the value in OCR4C 		*/
#define TIMER4_INT_MATCHREG_C	0x08
/**	interrupt on matching with the value in ICR4 		*/
#define TIMER4_INT_INPUTCAPTURE	0x20
/**	interrupt on matching with the value in OCR4A ,OCR4B,ICR4 and overflow 	*/
#define TIMER4_INT_ALL			0x2F

/**		Timer configure structure		*/
typedef struct
{
	PFEnTimer4Clocksource		clockSource;			/**< Select clock source							*/
	PFEnTimer4Mode		        timer4Mode;				/** timer mode										*/
	PFword				        matchValueA;			/**< Match register A compare value					*/
	PFword				        matchValueB;			/**< Match register B compare value					*/
	PFword				        matchValueC;			/**< Match register C compare value					*/
	PFEnTimer4ExtMatchCtrl 		exMatchActionA;			/**< match pin control on count match 				*/
	PFEnTimer4ExtMatchCtrl 		exMatchActionB;			/**< match pin control on count match 				*/ 
	PFEnTimer4ExtMatchCtrl 		exMatchActionC;			/**< match pin control on count match 				*/
	PFbyte			      		interrupt;				/**< To enable or disable timer interrupt			*/
	PFEnBoolean                 inputCaptureRisingEdge; /**< to set input capture signal edge selection 	*/
	PFcallback			        cmpMatchACallback;		/**< Callback function for timer ISR				*/
	PFcallback			        cmpMatchBCallback;
	PFcallback			        cmpMatchCCallback;
	PFcallback			        inputCaptureCallback;
	PFcallback			        overFlowCallback;
}PFCfgTimer4;

/** Pinter to PFCfgTiemr structure		*/
typedef PFCfgTimer4* PFpCfgTimer4;

/**
 * Initialized timer with given parameters
 * 
 * \param config timer configuration structure
 * 
 * \return timer initialization status
 */
PFEnStatus pfTimer4Open(PFpCfgTimer4 config);

/**
 * Stops timer operation and turn offs the timer module
 * 
 * \return timer turn off operation status
 */
PFEnStatus pfTimer4Close(void);

/**
 * Starts timer operation
 * 
 * \return timer start status
 */
PFEnStatus pfTimer4Start(void);

/**
 * Stops timer operation
 * 
 * \return timer stop status
 */
PFEnStatus pfTimer4Stop(void);

/**
 * Resets the timer operation. Timer will start counting from zero again.
 * 
 * \return timer reset status
 */
PFEnStatus pfTimer4Reset(void);

/**
 * Enables timer interrupt.
 * 
 * \param interrupt value to enable specific timer interrupt.
 * 
 * \return timer interrupt enable status.
 */
PFEnStatus pfTimer4IntEnable(PFbyte interrupt);

/**
 * Disables timer interrupt.
 * 
 * \param interrupt value to disable specific timer interrupt.
 * 
 * \return timer interrupt disable status.
 */
PFEnStatus pfTimer4IntDisable(PFbyte interrupt);

/**
 * Returns the timer count
 * 
 * \return timer count
 */
PFEnStatus pfTimer4ReadCount(PFdword* count);

/**
 * Returns Time stamp at which event occurred at ICP pin
 * 
 * \return Input capture count
 */
PFEnStatus pfTimer4ReadCaptureCount(PFdword* count);

/**
* Writes new value to the match register and enables latch for the match register
*
* \param regNum index of match register to be updated
* \param regValue new value for match register
*
* \return match register update status
*/
PFEnStatus pfTimer4UpdateMatchRegister(PFbyte regNum, PFword regVal); 

/** @} */
