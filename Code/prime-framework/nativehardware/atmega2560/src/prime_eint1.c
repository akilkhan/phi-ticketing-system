#include "prime_framework.h"
#if (PF_USE_EINT1 == 1)
#include "prime_eint1.h"


static PFcallback eint1CallBackList[PF_EXT_INT_MAX_CALLBACK]={0};
static PFbyte eint1MaxIntCount=0;
static PFbyte eint1InitFlag = 0;


PFEnStatus pfEint1Open(PFpCfgEint1 config)
{
	PFbyte index;
#ifdef PF_EINT1_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if((config->mode > 3)|| (config->mode < enIntModeLowLevel))
	{
		return enStatusInvArgs;
	}
	if(config->callbackList == 0)
	{
		return enStatusInvArgs;
	}
	if(config->maxCallbacks < 0)
	{
		return enStatusInvArgs;
	}
#endif

	if(config->mode <=3)
	{
		EXTINT->EICRA |=config->mode <<EINT1_CH ;
	}	
	
	eint1MaxIntCount=config->maxCallbacks ;
// 	if(eint1MaxIntCount >0)
// 	eint1CallBackList =(PFcallback*)malloc(sizeof(PFcallback)*eint1MaxIntCount);
// 	else
// 	return enStatusNotSupported;
		
	for(index=0;index<eint1MaxIntCount;index++)
	{
		if(config->callbackList!=0 )
		eint1CallBackList[index]=config->callbackList[index];
	}
	 eint1InitFlag = enBooleanTrue ;
	
	return enStatusSuccess;
	
}


PFEnStatus pfEint1Enable(void)
{
#ifdef PF_EINT1_DEBUG
	if(eint1InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK |= (1<<EINT1_CH);
	return enStatusSuccess;
}


PFEnStatus pfEint1Disable(void)
{
#ifdef PF_EINT1_DEBUG
	if(eint1InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT1_CH);
	return enStatusSuccess;
}



PFEnStatus pfEint1AddCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT1_DEBUG
	if(eint1InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(callback == 0)
	{
		return enStatusInvArgs;
	}
#endif	
	for(index=0;index<=eint1MaxIntCount;index++)
	{
		if (eint1CallBackList[index]==0)
		{
			eint1CallBackList[index]=callback;
			break;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfEint1RemoveCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT1_DEBUG
	if(eint1InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	
	for(index=0;index<=eint1MaxIntCount;index++)
	{
		if (eint1CallBackList[index]==callback)
		{
			eint1CallBackList[index]=0;
			break;
		}
	}
	return enStatusSuccess;
	
}


PFEnStatus pfEint1Close(void)
{
#ifdef PF_EINT1_DEBUG
	if(eint1InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT1_CH);
	//free(eint1CallBackList);
	return enStatusSuccess;
}

void INT_HANDLER(EINT1_CHANNEL)(void)
{
	PFbyte index=0;
	
	for(index=0;index<=eint1MaxIntCount;index++)
	{
		if(eint1CallBackList[index] !=0)
		eint1CallBackList[index]();
		
	}
}

#endif		//#if (PF_USE_EINT1 == 1)


