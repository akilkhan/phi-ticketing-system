#include "prime_framework.h"
#if (PF_USE_TIMER3 == 1)
#include "prime_timer3.h"

static PFEnStatus timer3InitFlag=0;
static PFcallback timer3cmpMatchACallback;
static PFcallback timer3cmpMatchBCallback;
static PFcallback timer3cmpMatchCCallback;
static PFcallback timer3inputCaptureCallback;
static PFcallback timer3overFlowCallback;

PFEnStatus pfTimer3Open(PFpCfgTimer3 config)
{
#ifdef PF_TIMER3_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}		
    if( (config->timer3Mode != 0x08) && (config->timer3Mode != 0x18) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionB < 0) || (config->exMatchActionB > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionC < 0) || (config->exMatchActionC> 3) )
	{
		return enStatusInvArgs;
	}		
	
	if( ((config->interrupt & TIMER3_INT_MATCHREG_A) != 0) && (config->cmpMatchACallback == 0) )
	{
		return enStatusInvArgs;
	}
	if( ((config->interrupt & TIMER3_INT_MATCHREG_B) != 0) && (config->cmpMatchBCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER3_INT_MATCHREG_C) != 0) && (config->cmpMatchCCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER3_INT_OVERFLOW) != 0) && (config->overFlowCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER3_INT_INPUTCAPTURE) != 0) && (config->inputCaptureCallback == 0) )
	{
		return enStatusInvArgs;
	}
#endif
	if(config->timer3Mode)
	{
		TIMER3_CHANNEL->TCCRB = config->timer3Mode;
	}	
	TIMER3_CHANNEL->OCRA = config->matchValueA;
	TIMER3_CHANNEL->OCRB = config->matchValueB;
	TIMER3_CHANNEL->OCRC = config->matchValueC;
	
	if(config->inputCaptureRisingEdge)
	{
		TIMER3_CHANNEL->TCCRB |= 1<<6;
	}	
	if(Timer3InputCaptureNoiseCanceler__)
	{
		TIMER3_CHANNEL->TCCRB |= 1<<7;
	}	
	if(config->exMatchActionA != enTimer3ExtMatchCtrlNone)
	{
		TIMER3_CHANNEL->TCCRA = config->exMatchActionA << 6;
	}	
	if(config->exMatchActionB!=enTimer3ExtMatchCtrlNone)
	{
		TIMER3_CHANNEL->TCCRA |=config->exMatchActionB << 4;
	}	
	if(config->exMatchActionC!=enTimer3ExtMatchCtrlNone)
	{
		TIMER3_CHANNEL->TCCRA |=config->exMatchActionC << 2;
	}	
	if(config->cmpMatchACallback !=0)
	{
		timer3cmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->cmpMatchBCallback !=0)
	{
		timer3cmpMatchBCallback =config->cmpMatchBCallback;
	}	
	if(config->cmpMatchCCallback !=0)
	{
		timer3cmpMatchCCallback =config->cmpMatchCCallback;
	}	
	if(config->inputCaptureCallback !=0)
	{
		timer3inputCaptureCallback =config->inputCaptureCallback;
	}	
	if(config->overFlowCallback !=0)
	{
		timer3overFlowCallback =config->overFlowCallback;
	}
	if( config->interrupt != TIMER3_INT_NONE )
	{
		//TIMER3_CHANNEL->TIMSK = config.interrupt;
		TIMSK_INT->TIMER3 = config->interrupt;
	}		
	if(config->clockSource <= 7)
	{
		TIMER3_CHANNEL->TCCRB |= config->clockSource;
	}		

    timer3InitFlag=1;

	return enStatusSuccess;
	
}

PFEnStatus pfTimer3Close(void)
{
#ifdef PF_TIMER3_DEBUG
    if(timer3InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	TIMER3_CHANNEL->TCCRB = 0x00;
	TIMER3_CHANNEL->TCNT = 0;
	
	return enStatusSuccess;
}

PFEnStatus pfTimer3Start(void)
{
#ifdef PF_TIMER3_DEBUG
	if(timer3InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	PFbyte val;
	val=TIMER3_CHANNEL->TCCRB;
	TIMER3_CHANNEL->TCCRB = 0x00;
	TIMER3_CHANNEL->TCCRB |= val;
	TIMER3_CHANNEL->TCNT = 0;
	
	return enStatusSuccess;
}
PFEnStatus pfTimer3Reset(void)
{
#ifdef PF_TIMER3_DEBUG
	if(timer3InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	TIMER3_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}

PFEnStatus pfTimer3ReadCount(PFdword* count)
{
#ifdef PF_TIMER3_DEBUG
	if(timer3InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(count == 0)
	{
		return enStatusInvArgs;
	}
#endif
	*count = TIMER3_CHANNEL->TCNT;
	return enStatusSuccess;
}

PFEnStatus pfTimer3ReadCaptureCount(PFdword* count)
{
#ifdef PF_TIMER3_DEBUG
	if(timer3InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
	if(count == 0)
	{
		return enStatusInvArgs;
	}
#endif
	*count = TIMER3_CHANNEL->ICR;
	return enStatusSuccess;
}

PFEnStatus pfTimer3UpdateMatchRegister(PFbyte regNum, PFword regVal)
{
#ifdef PF_TIMER3_DEBUG
	if(timer3InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if( (regNum < 'A') || (regNum > 'C') )
	{
		return enStatusNotConfigured;
	}	
#endif
	switch(regNum)
	{
		case 'A':	
				  TIMER3_CHANNEL->OCRA =regVal;
				  break;
		
		case 'B':
				  TIMER3_CHANNEL->OCRB =regVal;
				  break;
				
		case 'C':
				  TIMER3_CHANNEL->OCRC =regVal;
				  break;
	}
	return enStatusSuccess;
}

PFEnStatus pfTimer3IntEnable(PFbyte interrupt)
{
#ifdef PF_TIMER3_DEBUG
    if(interrupt && (~TIMER3_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER3 |= interrupt;
	return enStatusSuccess;	
}

PFEnStatus pfTimer3IntDisable(PFbyte interrupt)
{
#ifdef PF_TIMER3_DEBUG
    if(interrupt && (~TIMER3_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER3 &= (~interrupt);
	return enStatusSuccess;	
}

void INT_HANDLER(TIMER3_COMPA)(void)
{
	if(timer3cmpMatchACallback !=0)
	timer3cmpMatchACallback();
}

void INT_HANDLER(TIMER3_COMPB)(void)
{
	if(timer3cmpMatchBCallback !=0)
	timer3cmpMatchBCallback();
}

void INT_HANDLER(TIMER3_COMPC)(void)
{
	if(timer3cmpMatchCCallback !=0)
	timer3cmpMatchCCallback();
}

void INT_HANDLER(TIMER3_CAPT)(void)
{
	if(timer3inputCaptureCallback !=0)
	timer3inputCaptureCallback();
}

void INT_HANDLER(TIMER3_OVF)(void)
{
	if(timer3overFlowCallback !=0)
	timer3overFlowCallback();
}

#endif		//#if (PF_USE_TIMER3 == 1)
