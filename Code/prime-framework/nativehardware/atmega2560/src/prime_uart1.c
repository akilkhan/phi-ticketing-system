#include "prime_framework.h"
#if (PF_USE_UART1 == 1)
#include "prime_fifo.h"
#include "prime_uart1.h"

#if(UART1_USE_FIFO != 0)
	#warning UART1 FIFO is enabled for interrupt based communication
	#if( (UART1_BUFFER_SIZE == 0) || ((UART1_BUFFER_SIZE & (UART1_BUFFER_SIZE - 1)) != 0) )
		#error UART1_BUFFER_SIZE cannot be zero. UART1_BUFFER_SIZE should be power of 2
	#endif
#endif

static PFword uart1BaudArray[]={383,191,95,63,47,31,23,15,11,7,3};

static PFEnBoolean uart1ChInit;						// initialize flag for the channel
static PFEnBoolean uart1ChBusy;						// busy flag for the channel
static PFbyte uart1ChInt;
	
#if(UART1_USE_FIFO != 0)
static PFbyte uart1RxBuffer[UART1_BUFFER_SIZE];	// UART1 transmit buffer
static PFbyte uart1TxBuffer[UART1_BUFFER_SIZE];	// UART1 receive buffer
static PFFifo uart1TxFifo;							// UART1 transmit fifo structure
static PFFifo uart1RxFifo;							// UART1 receive fifo structure
#else	
static PFcallback uart1TxCallback;					// transmit callback for the channel
static PFcallback uart1RxCallback;					// receive callback for the channel
#endif	// #if(UART1_USE_FIFO != 0)


PFEnStatus pfUart1Open(PFpCfgUart1 config)
{	
#ifdef PF_UART1_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if ((config->mode) >0x03)
	{
		return enStatusNotSupported;
	}
	if ((config->parity) > 0x02)
	{
		return enStatusNotSupported;
	}
	if ((config->stopBits) > enUart1StopBits_2)
	{
		return enStatusNotSupported;
	}	
#if(UART1_USE_FIFO == 0)
	if((config->transmitCallback) ==0)
	{
		return enStatusInvArgs;
	}
	if((config->receiveCallback) ==0)
	{
		return enStatusInvArgs;
	}	
#endif
#endif

	
	UART1_CHANNEL->UBRRH = (uart1BaudArray[(config->baudrate)] >> 8);
	UART1_CHANNEL->UBRRL = uart1BaudArray[(config->baudrate)];
	if ((config->mode) <=0x03)
	{
		UART1_CHANNEL->UCSRB |= ((config->mode)<<3);
	} 
				
	if ((config->parity) !=enUart1ParityNone)
	{
		if ((config->parity) !=enUart1ParityEven)
		{
			UART1_CHANNEL->UCSRC = ((1<<5)|(1<<4));
		} 
		else
		{
			UART1_CHANNEL->UCSRC = ((1<<5)|(0<<4));
		}
	} 
	
				
	if ((config->stopBits) !=enUart1StopBits_1)
	{
		UART1_CHANNEL->UCSRC |=(1<<3);
	}
 
	if ((config->interrupts) != enUart1IntNone)
	{
		UART1_CHANNEL->UCSRB |=(config->interrupts) <<4;
		uart1ChInt =(config->interrupts);
	} 
				
	if(((config->dataBits) & 0x07) !=0x07)
	{	
		UART1_CHANNEL->UCSRC |= config->dataBits <<1;
	}
	else
	{	
		UART1_CHANNEL->UCSRC |= (config->dataBits)<<1;
		UART1_CHANNEL->UCSRB |= (1<<2);
	}
		
#if(UART1_USE_FIFO != 0)
	// initialize fifo 
	pfFifoInit(&uart1TxFifo, uart1TxBuffer, UART1_BUFFER_SIZE);
	pfFifoInit(&uart1RxFifo, uart1RxBuffer, UART1_BUFFER_SIZE);
#else	
	// set transmit and receive user callbacks
	if((config->transmitCallback) !=0)
	{
		uart1TxCallback = (config->transmitCallback);
	}
	if((config->receiveCallback) !=0)
	{
		uart1RxCallback = (config->receiveCallback);
	}	
#endif	// #if(UART1_USE_FIFO != 0)
	uart1ChInit =  enBooleanTrue;
	return enStatusSuccess;

}
			
/** ---------------------------------------------------*/
PFEnStatus pfUart1WriteByte(PFbyte data)
{

#ifdef PF_UART1_DEBUG
	// check if the channel is initialized
	if(uart1ChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
#endif	// _DEBUG_

	UART1_CHANNEL->UDR =data;
	while(!(UART1_CHANNEL->UCSRA &(1<<5)));
	return enStatusSuccess;
}

PFEnStatus pfUart1ReadByte(PFbyte* data)
{
#ifdef PF_UART1_DEBUG
	// check if the channel is initialized
	if(uart1ChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
	if(data == 0)
	{
		return enStatusInvArgs;
	}
#endif	// PF_UART1_DEBUG

	while(!(UART1_CHANNEL->UCSRA &(1<<7)));
	*data = UART1_CHANNEL->UDR;
	return enStatusSuccess;
}

PFEnStatus pfUart1Write(PFbyte* data, PFdword size)
{
	PFdword index;
#ifdef PF_UART1_DEBUG
	// check if the channel is initialized
	if(uart1ChInit !=  enBooleanTrue)
	{
		return enStatusNotConfigured;
	}		
#endif	// PF_UART1_DEBUG
	
	// check if the channel is busy
	if(uart1ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}		
	
	// set channel busy	
	uart1ChBusy = enBooleanTrue;
	
	// if fifo is to be used push data into the fifo
	if((uart1ChInt & enUart1IntTx) != 0)
	{
	#if(UART1_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&uart1TxFifo, *(data + index));
		}
		UART1_CHANNEL->UDR = pfFifoPop(&uart1TxFifo);
	#else
		UART1_CHANNEL->UDR = *data;
	#endif	// #if(UART1_USE_FIFO != 0)	
		//LPC_UART10->IER = uart1ChInt & (0x7F);
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			UART1_CHANNEL->UDR = *(data + index);
			while(!(UART1_CHANNEL->UCSRA &(1<<5)));

		}
	}
	
	uart1ChBusy = enBooleanFalse;
	return enStatusSuccess;
}




PFEnStatus pfUart1Read(PFbyte* data, PFdword size,PFdword* readBytes)
{
	PFdword index;
#ifdef PF_UART1_DEBUG
	if(uart1ChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
#endif	// PF_UART1_DEBUG
	
	if(uart1ChBusy != enBooleanFalse)
	{	
		return enStatusBusy;
	}		
	uart1ChBusy = enBooleanTrue;
	
	if((uart1ChInt & enUart1IntRx) != 0)
	{
	#if(UART1_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			if(pfFifoIsEmpty(&uart1RxFifo) == enBooleanTrue)
				break;
			
			*(data + index) = pfFifoPop(&uart1RxFifo);
				*readBytes += 1;
		}
	#endif	// #if(UART1_USE_FIFO != 0)	
	}
	else
	{
		for(index = 0; index < size; index++)
		{
			while(!(UART1_CHANNEL->UCSRA &(1<<7)));
			*(data + index) = UART1_CHANNEL->UDR;
				*readBytes++;
		}
	}
	uart1ChBusy = enBooleanFalse;
	return enStatusSuccess;
}


PFEnStatus pfUart1Close(void)
{
#ifdef PF_UART1_DEBUG
	// check if the channel is initialized
	if(uart1ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_UART1_DEBUG
	UART1_CHANNEL->UCSRB |=(enUart1ModeTxRx)<<3;
	uart1ChInit = enBooleanFalse;
	return enStatusSuccess;
}

#if(UART1_USE_FIFO != 0)
PFEnStatus pfUart1GetRxBufferCount(PFdword* count)
{
#ifdef PF_UART1_DEBUG
	// check if the channel is initialized
	if(uart1ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
	if(count == 0)
		return enStatusInvArgs;
#endif	// PF_UART1_DEBUG

	*count = uart1RxFifo.count;
	return enStatusSuccess;
}

PFEnStatus pfUart1TxBufferFlush(void)
{
#ifdef PF_UART1_DEBUG
	// check if the channel is initialized
	if(uart1ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_UART1_DEBUG

	pfFifoFlush(&uart1TxFifo);
	return enStatusSuccess;
}

PFEnStatus pfUart1RxBufferFlush(void)
{
#ifdef PF_UART1_DEBUG
	// check if the channel is initialized
	if(uart1ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_UART1_DEBUG

	pfFifoFlush(&uart1RxFifo);
	return enStatusSuccess;
}
#endif	// #if(UART1_USE_FIFO != 0)

void INT_HANDLER(USART1_RX)(void)
{
	PFbyte rxByte;
	#if(UART1_USE_FIFO != 0)
		rxByte = UART1_CHANNEL->UDR;
		pfFifoPush(&uart1RxFifo, rxByte);
	#else
		if(uart1RxCallback != 0)
		{
			uart1RxCallback();
		}
		return;
	#endif	
}

void INT_HANDLER(USART1_TX)(void)
{
	#if(UART1_USE_FIFO != 0)
		if(pfFifoIsEmpty(&uart1TxFifo) != enBooleanTrue)
		{
			UART1_CHANNEL->UDR = pfFifoPop(&uart1TxFifo);
		}
		
	#else	
		if(uart1TxCallback != 0)
		{
			uart1TxCallback();
		}
		return;
	#endif		//
}

#endif 		//#if (PF_USE_UART1 == 1)