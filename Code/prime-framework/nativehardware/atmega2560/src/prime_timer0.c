#include "prime_framework.h"
#if (PF_USE_TIMER0 == 1)
#include "prime_timer0.h"

static PFEnStatus timer0InitFlag=0;
static PFcallback timer0CmpMatchACallback;
static PFcallback timer0CmpMatchBCallback;
static PFcallback timer0OverFlowCallback;

PFEnStatus pfTimer0Open(PFpCfgTimer0 config)
{
#ifdef PF_TIMER0_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
    if(config->timer0Mode > enTimer0CtcMode)
	{
		return enStatusInvArgs;
	}
	if(  (config->exMatchActionB < 0) || (config->exMatchActionB > 3) )
	{
		return enStatusInvArgs;
	}

	
	if( ((config->interrupt & TIMER0_INT_MATCHREG_A) != 0) && (config->cmpMatchACallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER0_INT_MATCHREG_B) != 0) && (config->cmpMatchBCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER0_INT_OVERFLOW) != 0) && (config->overFlowCallback == 0) )
	{
		return enStatusInvArgs;
	}		
#endif
	
	if(config->timer0Mode !=0)
	{
		TIMER0_CHANNEL->TCCRA |=config->timer0Mode;
	}	
	TIMER0_CHANNEL->OCRA =config->matchValueA;
	TIMER0_CHANNEL->OCRB =config->matchValueB;
	
	if(config->exMatchActionA !=enTimer0ExtMatchCtrlNone)
	{
		TIMER0_CHANNEL->TCCRA |=config->exMatchActionA << 6;
	}
		
	if(config->exMatchActionB!=enTimer0ExtMatchCtrlNone)
	{
		TIMER0_CHANNEL->TCCRA |=config->exMatchActionB << 4;
	}
		
	if(config->cmpMatchACallback !=0)
	{
		timer0CmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->cmpMatchBCallback !=0)
	{
		timer0CmpMatchBCallback =config->cmpMatchBCallback;
	}	
	if(config->overFlowCallback !=0)
	{
		timer0OverFlowCallback =config->overFlowCallback;
	}
	if(config->interrupt != TIMER0_INT_NONE)
	{
		TIMSK_INT->TIMER0 = config->interrupt;
	}
	if(config->clockSource <=7)
	{
		TIMER0_CHANNEL->TCCRB |= config->clockSource;
	}
	
	
	timer0InitFlag = enBooleanTrue;

	return enStatusSuccess;
	
}

PFEnStatus pfTimer0Close(void)
{
#ifdef PF_TIMER0_DEBUG
    if(timer0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif

	TIMER0_CHANNEL->TCCRB = 0x00;
	TIMER0_CHANNEL->TCNT = 0;
	return enStatusSuccess;
	
}

PFEnStatus pfTimer0Start(void)
{
	PFbyte val;
#ifdef PF_TIMER0_DEBUG
    if(timer0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif

	val=TIMER0_CHANNEL->TCCRB;
	TIMER0_CHANNEL->TCCRB = 0x00;
	TIMER0_CHANNEL->TCCRB |= val;
	TIMER0_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}
PFEnStatus pfTimer0Reset(void)
{
#ifdef PF_TIMER0_DEBUG
    if(timer0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}		
#endif

	TIMER0_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}
PFEnStatus pfTimer0ReadCount(PFbyte* data)
{
#ifdef PF_TIMER0_DEBUG
    if(timer0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	*data =TIMER0_CHANNEL->TCNT;
	return enStatusSuccess;
}

PFEnStatus pfTimer0UpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
#ifdef PF_TIMER0_DEBUG
    if(timer0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}		
#endif
	switch(regNum)
	{
		case 0:	
				TIMER0_CHANNEL->OCRA =regVal;
				break;
		
		case 1:
				TIMER0_CHANNEL->OCRB =regVal;
				break;
	}
	return enStatusSuccess;
}

PFEnStatus pfTimer0IntEnable(PFbyte interrupt)
{
#ifdef PF_TIMER0_DEBUG
    if(interrupt && (~TIMER0_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER0 |= interrupt;
	return enStatusSuccess;	
}

PFEnStatus pfTimer0IntDisable(PFbyte interrupt)
{
#ifdef PF_TIMER0_DEBUG
    if(interrupt && (~TIMER0_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER0 &= (~interrupt);
	return enStatusSuccess;	
}

void INT_HANDLER(TIMER0_COMPA)(void)
{
	if(timer0CmpMatchACallback !=0)
	timer0CmpMatchACallback();
}


void INT_HANDLER(TIMER0_COMPB)(void)
{
	if(timer0CmpMatchBCallback !=0)
	timer0CmpMatchBCallback();
}

void INT_HANDLER(TIMER0_OVF)(void)
{
	if(timer0OverFlowCallback !=0)
	timer0OverFlowCallback();
}

#endif		// #if (PF_USE_TIMER0 == 1)
