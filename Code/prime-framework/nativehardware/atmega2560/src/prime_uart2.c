#include "prime_framework.h"
#if (PF_USE_UART2 == 1)
#include "prime_fifo.h"
#include "prime_uart2.h"

#if(UART2_USE_FIFO != 0)
	#warning UART2 FIFO is enabled for interrupt based communication
	#if( (UART2_BUFFER_SIZE == 0) || ((UART2_BUFFER_SIZE & (UART2_BUFFER_SIZE - 1)) != 0) )
		#error UART2_BUFFER_SIZE cannot be zero. UART2_BUFFER_SIZE should be power of 2
	#endif
#endif

static PFword uart2BaudArray[]={383,191,95,63,47,31,23,15,11,7,3};

static PFEnBoolean uart2ChInit;						// initialize flag for the channel
static PFEnBoolean uart2ChBusy;						// busy flag for the channel
static PFbyte uart2ChInt;
	
#if(UART2_USE_FIFO != 0)
static PFbyte uart2RxBuffer[UART2_BUFFER_SIZE];	// UART2 transmit buffer
static PFbyte uart2TxBuffer[UART2_BUFFER_SIZE];	// UART2 receive buffer
static PFFifo uart2TxFifo;							// UART2 transmit fifo structure
static PFFifo uart2RxFifo;							// UART2 receive fifo structure
#else	
static PFcallback uart2TxCallback;					// transmit callback for the channel
static PFcallback uart2RxCallback;					// receive callback for the channel
#endif	// #if(UART2_USE_FIFO != 0)


PFEnStatus pfUart2Open(PFpCfgUart2 config)
{	
#ifdef PF_UART2_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if ((config->mode) >0x03)
	{
		return enStatusNotSupported;
	}
	if ((config->parity) > 0x02)
	{
		return enStatusNotSupported;
	}
	if ((config->stopBits) > enUart2StopBits_2)
	{
		return enStatusNotSupported;
	}	
#if(UART2_USE_FIFO == 0)
	if((config->transmitCallback) ==0)
	{
		return enStatusInvArgs;
	}
	if((config->receiveCallback) ==0)
	{
		return enStatusInvArgs;
	}	
#endif
#endif

	
	UART2_CHANNEL->UBRRH = (uart2BaudArray[(config->baudrate)] >> 8);
	UART2_CHANNEL->UBRRL = uart2BaudArray[(config->baudrate)];
	if ((config->mode) <=0x03)
	{
		UART2_CHANNEL->UCSRB |= ((config->mode)<<3);
	} 
				
	if ((config->parity) !=enUart2ParityNone)
	{
		if ((config->parity) !=enUart2ParityEven)
		{
			UART2_CHANNEL->UCSRC = ((1<<5)|(1<<4));
		} 
		else
		{
			UART2_CHANNEL->UCSRC = ((1<<5)|(0<<4));
		}
	} 
	
				
	if ((config->stopBits) !=enUart2StopBits_1)
	{
		UART2_CHANNEL->UCSRC |=(1<<3);
	}
 
	if ((config->interrupts) != enUart2IntNone)
	{
		UART2_CHANNEL->UCSRB |=(config->interrupts) <<4;
		uart2ChInt =(config->interrupts);
	} 
				
	if(((config->dataBits) & 0x07) !=0x07)
	{	
		UART2_CHANNEL->UCSRC |= config->dataBits <<1;
	}
	else
	{	
		UART2_CHANNEL->UCSRC |= (config->dataBits)<<1;
		UART2_CHANNEL->UCSRB |= (1<<2);
	}
		
#if(UART2_USE_FIFO != 0)
	// initialize fifo 
	pfFifoInit(&uart2TxFifo, uart2TxBuffer, UART2_BUFFER_SIZE);
	pfFifoInit(&uart2RxFifo, uart2RxBuffer, UART2_BUFFER_SIZE);
#else	
	// set transmit and receive user callbacks
	if((config->transmitCallback) !=0)
	{
		uart2TxCallback = (config->transmitCallback);
	}
	if((config->receiveCallback) !=0)
	{
		uart2RxCallback = (config->receiveCallback);
	}	
#endif	// #if(UART2_USE_FIFO != 0)
	uart2ChInit =  enBooleanTrue;
	return enStatusSuccess;

}
			
/** ---------------------------------------------------*/
PFEnStatus pfUart2WriteByte(PFbyte data)
{

#ifdef PF_UART2_DEBUG
	// check if the channel is initialized
	if(uart2ChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
#endif	// _DEBUG_

	UART2_CHANNEL->UDR =data;
	while(!(UART2_CHANNEL->UCSRA &(1<<5)));
	return enStatusSuccess;
}

PFEnStatus pfUart2ReadByte(PFbyte* data)
{
#ifdef PF_UART2_DEBUG
	// check if the channel is initialized
	if(uart2ChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
	if(data == 0)
	{
		return enStatusInvArgs;
	}
#endif	// PF_UART2_DEBUG

	while(!(UART2_CHANNEL->UCSRA &(1<<7)));
	*data = UART2_CHANNEL->UDR;
	return enStatusSuccess;
}

PFEnStatus pfUart2Write(PFbyte* data, PFdword size)
{
	PFdword index;
#ifdef PF_UART2_DEBUG
	// check if the channel is initialized
	if(uart2ChInit !=  enBooleanTrue)
	{
		return enStatusNotConfigured;
	}		
#endif	// PF_UART2_DEBUG
	
	// check if the channel is busy
	if(uart2ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}		
	
	// set channel busy	
	uart2ChBusy = enBooleanTrue;
	
	// if fifo is to be used push data into the fifo
	if((uart2ChInt & enUart2IntTx) != 0)
	{
	#if(UART2_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&uart2TxFifo, *(data + index));
		}
		UART2_CHANNEL->UDR = pfFifoPop(&uart2TxFifo);
	#else
		UART2_CHANNEL->UDR = *data;
	#endif	// #if(UART2_USE_FIFO != 0)	
		//LPC_UART20->IER = uart2ChInt & (0x7F);
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			UART2_CHANNEL->UDR = *(data + index);
			while(!(UART2_CHANNEL->UCSRA &(1<<5)));

		}
	}
	
	uart2ChBusy = enBooleanFalse;
	return enStatusSuccess;
}




PFEnStatus pfUart2Read(PFbyte* data, PFdword size,PFdword* readBytes)
{
	PFdword index;
#ifdef PF_UART2_DEBUG
	if(uart2ChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
#endif	// PF_UART2_DEBUG
	
	if(uart2ChBusy != enBooleanFalse)
	{	
		return enStatusBusy;
	}		
	uart2ChBusy = enBooleanTrue;
	
	if((uart2ChInt & enUart2IntRx) != 0)
	{
	#if(UART2_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			if(pfFifoIsEmpty(&uart2RxFifo) == enBooleanTrue)
				break;
			
			*(data + index) = pfFifoPop(&uart2RxFifo);
				*readBytes += 1;
		}
	#endif	// #if(UART2_USE_FIFO != 0)	
	}
	else
	{
		for(index = 0; index < size; index++)
		{
			while(!(UART2_CHANNEL->UCSRA &(1<<7)));
			*(data + index) = UART2_CHANNEL->UDR;
				*readBytes++;
		}
	}
	uart2ChBusy = enBooleanFalse;
	return enStatusSuccess;
}


PFEnStatus pfUart2Close(void)
{
#ifdef PF_UART2_DEBUG
	// check if the channel is initialized
	if(uart2ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_UART2_DEBUG
	UART2_CHANNEL->UCSRB |=(enUart2ModeTxRx)<<3;
	uart2ChInit = enBooleanFalse;
	return enStatusSuccess;
}

#if(UART2_USE_FIFO != 0)
PFEnStatus pfUart2GetRxBufferCount(PFdword* count)
{
#ifdef PF_UART2_DEBUG
	// check if the channel is initialized
	if(uart2ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
	if(count == 0)
		return enStatusInvArgs;
#endif	// PF_UART2_DEBUG

	*count = uart2RxFifo.count;
	return enStatusSuccess;
}

PFEnStatus pfUart2TxBufferFlush(void)
{
#ifdef PF_UART2_DEBUG
	// check if the channel is initialized
	if(uart2ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_UART2_DEBUG

	pfFifoFlush(&uart2TxFifo);
	return enStatusSuccess;
}

PFEnStatus pfUart2RxBufferFlush(void)
{
#ifdef PF_UART2_DEBUG
	// check if the channel is initialized
	if(uart2ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_UART2_DEBUG

	pfFifoFlush(&uart2RxFifo);
	return enStatusSuccess;
}
#endif	// #if(UART2_USE_FIFO != 0)

void INT_HANDLER(USART2_RX)(void)
{
	PFbyte rxByte;
	#if(UART2_USE_FIFO != 0)
		rxByte = UART2_CHANNEL->UDR;
		pfFifoPush(&uart2RxFifo, rxByte);
	#else
		if(uart2RxCallback != 0)
		{
			uart2RxCallback();
		}
		return;
	#endif	
}

void INT_HANDLER(USART2_TX)(void)
{
	#if(UART2_USE_FIFO != 0)
		if(pfFifoIsEmpty(&uart2TxFifo) != enBooleanTrue)
		{
			UART2_CHANNEL->UDR = pfFifoPop(&uart2TxFifo);
		}
		
	#else	
		if(uart2TxCallback != 0)
		{
			uart2TxCallback();
		}
		return;
	#endif		//
}
#endif 		//#if (PF_USE_UART2 == 1)
