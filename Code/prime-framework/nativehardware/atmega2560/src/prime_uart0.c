#include "prime_framework.h"
#if (PF_USE_UART0 == 1)
#include "prime_fifo.h"
#include "prime_uart0.h"

#if(UART0_USE_FIFO != 0)
	#warning UART0 FIFO is enabled for interrupt based communication
	#if( (UART0_BUFFER_SIZE == 0) || ((UART0_BUFFER_SIZE & (UART0_BUFFER_SIZE - 1)) != 0) )
		#error UART0_BUFFER_SIZE cannot be zero. UART0_BUFFER_SIZE should be power of 2
	#endif
#endif

static PFword uart0BaudArray[]={383,191,95,63,47,31,23,15,11,7,3};

static PFEnBoolean uart0ChInit;						// initialize flag for the channel
static PFEnBoolean uart0ChBusy;						// busy flag for the channel
static PFbyte uart0ChInt;
	
#if(UART0_USE_FIFO != 0)
static PFbyte uart0RxBuffer[UART0_BUFFER_SIZE];	// UART0 transmit buffer
static PFbyte uart0TxBuffer[UART0_BUFFER_SIZE];	// UART0 receive buffer
static PFFifo uart0TxFifo;							// UART0 transmit fifo structure
static PFFifo uart0RxFifo;							// UART0 receive fifo structure
#else	
static PFcallback uart0TxCallback;					// transmit callback for the channel
static PFcallback uart0RxCallback;					// receive callback for the channel
#endif	// #if(UART0_USE_FIFO != 0)


PFEnStatus pfUart0Open(PFpCfgUart0 config)
{	
#ifdef PF_UART0_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if ((config->mode) >0x03)
	{
		return enStatusNotSupported;
	}
	if ((config->parity) > 0x02)
	{
		return enStatusNotSupported;
	}
	if ((config->stopBits) > enUart0StopBits_2)
	{
		return enStatusNotSupported;
	}	
#if(UART0_USE_FIFO == 0)
	if((config->transmitCallback) ==0)
	{
		return enStatusInvArgs;
	}
	if((config->receiveCallback) ==0)
	{
		return enStatusInvArgs;
	}	
#endif
#endif

	
	UART0_CHANNEL->UBRRH = (uart0BaudArray[(config->baudrate)] >> 8);
	UART0_CHANNEL->UBRRL = uart0BaudArray[(config->baudrate)];
	if ((config->mode) <=0x03)
	{
		UART0_CHANNEL->UCSRB |= ((config->mode)<<3);
	} 
				
	if ((config->parity) !=enUart0ParityNone)
	{
		if ((config->parity) !=enUart0ParityEven)
		{
			UART0_CHANNEL->UCSRC = ((1<<5)|(1<<4));
		} 
		else
		{
			UART0_CHANNEL->UCSRC = ((1<<5)|(0<<4));
		}
	} 
	
				
	if ((config->stopBits) !=enUart0StopBits_1)
	{
		UART0_CHANNEL->UCSRC |=(1<<3);
	}
 
	if ((config->interrupts) != enUart0IntNone)
	{
		UART0_CHANNEL->UCSRB |=(config->interrupts) <<4;
		uart0ChInt =(config->interrupts);
	} 
				
	if(((config->dataBits) & 0x07) !=0x07)
	{	
		UART0_CHANNEL->UCSRC |= config->dataBits <<1;
	}
	else
	{	
		UART0_CHANNEL->UCSRC |= (config->dataBits)<<1;
		UART0_CHANNEL->UCSRB |= (1<<2);
	}
		
#if(UART0_USE_FIFO != 0)
	// initialize fifo 
	pfFifoInit(&uart0TxFifo, uart0TxBuffer, UART0_BUFFER_SIZE);
	pfFifoInit(&uart0RxFifo, uart0RxBuffer, UART0_BUFFER_SIZE);
#else	
	// set transmit and receive user callbacks
	if((config->transmitCallback) !=0)
	{
		uart0TxCallback = (config->transmitCallback);
	}
	if((config->receiveCallback) !=0)
	{
		uart0RxCallback = (config->receiveCallback);
	}	
#endif	// #if(UART0_USE_FIFO != 0)
	uart0ChInit =  enBooleanTrue;
	return enStatusSuccess;

}
			
/** ---------------------------------------------------*/
PFEnStatus pfUart0WriteByte(PFbyte data)
{

#ifdef PF_UART0_DEBUG
	// check if the channel is initialized
	if(uart0ChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
#endif	// _DEBUG_

	UART0_CHANNEL->UDR =data;
	while(!(UART0_CHANNEL->UCSRA &(1<<5)));
	return enStatusSuccess;
}

PFEnStatus pfUart0ReadByte(PFbyte* data)
{
#ifdef PF_UART0_DEBUG
	// check if the channel is initialized
	if(uart0ChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
	if(data == 0)
	{
		return enStatusInvArgs;
	}
#endif	// PF_UART0_DEBUG

	while(!(UART0_CHANNEL->UCSRA &(1<<7)));
	*data = UART0_CHANNEL->UDR;
	return enStatusSuccess;
}

PFEnStatus pfUart0Write(PFbyte* data, PFdword size)
{
	PFdword index;
#ifdef PF_UART0_DEBUG
	// check if the channel is initialized
	if(uart0ChInit !=  enBooleanTrue)
	{
		return enStatusNotConfigured;
	}		
#endif	// PF_UART0_DEBUG
	
	// check if the channel is busy
	if(uart0ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}		
	
	// set channel busy	
	uart0ChBusy = enBooleanTrue;
	
	// if fifo is to be used push data into the fifo
	if((uart0ChInt & enUart0IntTx) != 0)
	{
	#if(UART0_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&uart0TxFifo, *(data + index));
		}
		UART0_CHANNEL->UDR = pfFifoPop(&uart0TxFifo);
	#else
		UART0_CHANNEL->UDR = *data;
	#endif	// #if(UART0_USE_FIFO != 0)	
		//LPC_UART00->IER = uart0ChInt & (0x7F);
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			UART0_CHANNEL->UDR = *(data + index);
			while(!(UART0_CHANNEL->UCSRA &(1<<5)));

		}
	}
	
	uart0ChBusy = enBooleanFalse;
	return enStatusSuccess;
}




PFEnStatus pfUart0Read(PFbyte* data, PFdword size,PFdword* readBytes)
{
	PFdword index;
#ifdef PF_UART0_DEBUG
	if(uart0ChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
#endif	// PF_UART0_DEBUG
	
	if(uart0ChBusy != enBooleanFalse)
	{	
		return enStatusBusy;
	}		
	uart0ChBusy = enBooleanTrue;
	
	if((uart0ChInt & enUart0IntRx) != 0)
	{
	#if(UART0_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			if(pfFifoIsEmpty(&uart0RxFifo) == enBooleanTrue)
				break;
			
			*(data + index) = pfFifoPop(&uart0RxFifo);
				*readBytes += 1;
		}
	#endif	// #if(UART0_USE_FIFO != 0)	
	}
	else
	{
		for(index = 0; index < size; index++)
		{
			while(!(UART0_CHANNEL->UCSRA &(1<<7)));
			*(data + index) = UART0_CHANNEL->UDR;
				*readBytes++;
		}
	}
	uart0ChBusy = enBooleanFalse;
	return enStatusSuccess;
}


PFEnStatus pfUart0Close(void)
{
#ifdef PF_UART0_DEBUG
	// check if the channel is initialized
	if(uart0ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_UART0_DEBUG
	UART0_CHANNEL->UCSRB |=(enUart0ModeTxRx)<<3;
	uart0ChInit = enBooleanFalse;
	return enStatusSuccess;
}

#if(UART0_USE_FIFO != 0)
PFEnStatus pfUart0GetRxBufferCount(PFdword* count)
{
#ifdef PF_UART0_DEBUG
	// check if the channel is initialized
	if(uart0ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
	if(count == 0)
		return enStatusInvArgs;
#endif	// PF_UART0_DEBUG

	*count = uart0RxFifo.count;
	return enStatusSuccess;
}

PFEnStatus pfUart0TxBufferFlush(void)
{
#ifdef PF_UART0_DEBUG
	// check if the channel is initialized
	if(uart0ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_UART0_DEBUG

	pfFifoFlush(&uart0TxFifo);
	return enStatusSuccess;
}

PFEnStatus pfUart0RxBufferFlush(void)
{
#ifdef PF_UART0_DEBUG
	// check if the channel is initialized
	if(uart0ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_UART0_DEBUG

	pfFifoFlush(&uart0RxFifo);
	return enStatusSuccess;
}
#endif	// #if(UART0_USE_FIFO != 0)

void INT_HANDLER(USART0_RX)(void)
{
	PFbyte rxByte;
	#if(UART0_USE_FIFO != 0)
		rxByte = UART0_CHANNEL->UDR;
		pfFifoPush(&uart0RxFifo, rxByte);
	#else
		if(uart0RxCallback != 0)
		{
			uart0RxCallback();
		}
		return;
	#endif	
}

void INT_HANDLER(USART0_TX)(void)
{
	#if(UART0_USE_FIFO != 0)
		if(pfFifoIsEmpty(&uart0TxFifo) != enBooleanTrue)
		{
			UART0_CHANNEL->UDR = pfFifoPop(&uart0TxFifo);
		}
		
	#else	
		if(uart0TxCallback != 0)
		{
			uart0TxCallback();
		}
		return;
	#endif		//
}
#endif 		//#if (PF_USE_UART0 == 1)
