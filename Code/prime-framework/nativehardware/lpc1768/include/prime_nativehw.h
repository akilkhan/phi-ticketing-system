#pragma once

#include "prime_sysClk.h"

#include "prime_gpio.h"

#include "prime_timer0.h"
#include "prime_timer1.h"
#include "prime_timer2.h"
#include "prime_timer3.h"

#include "prime_systick.h"
#include "prime_rit.h"
#include "prime_rtc.h"

#include "prime_pwm.h"

#include "prime_adc.h"

#include "prime_eint0.h"
#include "prime_eint1.h"
#include "prime_eint2.h"
#include "prime_eint3.h"

#include "prime_uart0.h"	
#include "prime_uart1.h"	
#include "prime_uart2.h"	
#include "prime_uart3.h"	

#include "prime_spi0.h"
#include "prime_spi1.h"

#include "prime_i2c0.h"
#include "prime_i2c1.h"
#include "prime_i2c2.h"

#include "prime_canCommon.h"
#include "prime_can1.h"
#include "prime_can2.h"


