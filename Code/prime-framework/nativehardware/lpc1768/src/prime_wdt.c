#include "prime_framework.h"

#if (PF_USE_WDT == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_wdt.h"


#define PRIME_WDT_WDMOD(n)						((PFdword)(1<<1))
#define PRIME_WDT_CLKSEL_MASK					(PFbyte)(0x03)
#define PRIME_WDT_WDMOD_WDTOF					((PFdword)(1<<2))
#define PRIME_WDT_TIMEOUT_MIN					((PFdword)0x000000FF)
#define PRIME_WDT_TIMEOUT_MAX					((PFdword)0xFFFFFFFF)

static PFEnStatus  pfWatchdogSetTimeOut (PFbyte clk_source, PFdword timeout);
static PFbyte watchdogInitFlag=0;

PFEnStatus pfWatchdogOpen(PFpCfgWatchdog config)
{ 	
	PFdword ClockSrc = 0;
#if(PF_WDT_DEBUG == 1)
{ 
		CHECK_NULL_PTR(config);
		if(config->clkDiv > 3)
	{
		return enStatusInvArgs;
	}	
}
#endif
	pfSysSetPclkDiv(PCLK_DIV_WDT,config->clkDiv);
	//Set Clock Source
	WDT_CHANNEL->WDCLKSEL &= ~(BIT_MASK_0 | BIT_MASK_1); 		//Clearing the Bit 0 and Bit 1 of WDCLKSEL
	WDT_CHANNEL->WDCLKSEL =  config->clksrc;	//Setting Clksrc to CLKSEL reg
	//Set WatchDog Timer Mode

		WDT_CHANNEL->WDMOD |= (BIT_MASK_1);
	
	
	ClockSrc = WDT_CHANNEL->WDCLKSEL;
	ClockSrc &=PRIME_WDT_CLKSEL_MASK;
	pfWatchdogSetTimeOut(ClockSrc,config->timeout);
    watchdogInitFlag=1;
 return enStatusSuccess;	
}

PFEnStatus pfWatchdogStart(void)
{
#if(PF_WDT_DEBUG == 1)
{
	if(watchdogInitFlag == 0)
	{
		return enStatusInvArgs;
	}	
}
#endif
	WDT_CHANNEL->WDMOD |= (BIT_MASK_0);    //Setting WDEN bit in WDMOD reg
	pfWatchdogFeed();
	return enStatusSuccess;
}

PFEnStatus pfWatchdogStop(void)
{
#if(PF_WDT_DEBUG == 1)
{
	if(watchdogInitFlag == 0)
	{
		return enStatusInvArgs;
	}	
}
#endif
	WDT_CHANNEL->WDMOD &= ~(BIT_MASK_0);    //Setting WDEN bit in WDMOD reg
	pfWatchdogFeed();
	return enStatusSuccess;
}


PFEnStatus pfWatchdogFeed(void)
{
#if(PF_WDT_DEBUG == 1)
{
	if(watchdogInitFlag == 0)
	{
		return enStatusInvArgs;
	}	
}
#endif
 	WDT_CHANNEL->WDFEED = 0xAA;		//
	WDT_CHANNEL->WDFEED = 0x55;		//
	return enStatusSuccess;
}

PFEnStatus pfWatchdogGetCurrentCount(PFdword *count)
{ 
#if(PF_WDT_DEBUG == 1)
{
	if(watchdogInitFlag == 0)
	{
		return enStatusInvArgs;
	}	
}
#endif
	*count = WDT_CHANNEL->WDTV; 
	return enStatusSuccess;
}

PFEnStatus pfWatchdogClearTimoutFlag(void)
{ 
	WDT_CHANNEL->WDMOD &=~BIT_MASK_2; //CLEARING WDTOF in WDMOD reg
	return enStatusSuccess;
}

PFEnStatus pfWatchdogUpdateTimeout(PFdword *timeout)
{
#if(PF_WDT_DEBUG == 1)
{
	if(watchdogInitFlag == 0)
	{
		return enStatusInvArgs;
	}	
}
#endif
	PFdword ClockSrc = 0;
	ClockSrc = WDT_CHANNEL->WDCLKSEL;
	ClockSrc &=PRIME_WDT_CLKSEL_MASK;
	pfWatchdogSetTimeOut(ClockSrc,*timeout);
	pfWatchdogFeed();
	return enStatusSuccess;	
}
 
 PFEnStatus pfWatchTimerReadTimeoutFlag(PFEnBoolean *flagStatus)
 {
	if(((WDT_CHANNEL->WDMOD & PRIME_WDT_WDMOD_WDTOF) >>2) == 0x01)
	{
		*flagStatus = enBooleanTrue;
		
	}
	 else
	 {
		 *flagStatus = enBooleanFalse;
	 }
		 return enStatusSuccess;
 }
 
 PFEnStatus pfWatchdogClose(void)
 {
#if(PF_WDT_DEBUG == 1)
{
	if(watchdogInitFlag == 0)
	{
		return enStatusInvArgs;
	}	
}
#endif
	WDT_CHANNEL->WDMOD &= ~(BIT_MASK_0);
    watchdogInitFlag=0;
	return enStatusSuccess;
 }
static PFEnStatus  pfWatchdogSetTimeOut (PFbyte clk_source, PFdword timeout)
 {
  	PFdword pclk_wdt = 0;
	PFdword temp = 0;
	switch((PFEnWatchdogClkSrc)clk_source)
	{ 
		case enWatchdogClkIRC:
									pclk_wdt = 4000000;
									temp = (((pclk_wdt) / 1000000) * (timeout / 4));
									if(temp >= PRIME_WDT_TIMEOUT_MIN &&  temp <= PRIME_WDT_TIMEOUT_MAX)
									{
										 
										WDT_CHANNEL->WDTC = (PFdword)temp;
										return enStatusSuccess;
									}
									else
									{
										 if(temp > PRIME_WDT_TIMEOUT_MAX)
										 {
											WDT_CHANNEL->WDTC = (PFdword)PRIME_WDT_TIMEOUT_MAX;
											return enStatusSuccess;  
										 }
										 else
										 {
											WDT_CHANNEL->WDTC = (PFdword)PRIME_WDT_TIMEOUT_MIN;
											return enStatusSuccess;  
										 }
									}
									break ;
		case enWatchdogClkRTC:
									pclk_wdt = 32768;
									temp = (((pclk_wdt) / 1000000) * (timeout / 4));
									if(temp >= PRIME_WDT_TIMEOUT_MIN &&  temp <= PRIME_WDT_TIMEOUT_MAX)
									{
										WDT_CHANNEL->WDTC = (PFdword)temp;
										return enStatusSuccess;
									}
									break;
									
		case enWatchdogClkPCLK:
									pclk_wdt = pfSysGetPclk(PCLK_DIV_WDT);
									temp = (((pclk_wdt) / 1000000) * (timeout / 4));
									if(temp >= PRIME_WDT_TIMEOUT_MIN &&  temp <= PRIME_WDT_TIMEOUT_MAX)
									{
										WDT_CHANNEL->WDTC = temp;
										return enStatusSuccess;
									}
									break;
									
		default:
									break;
									
	}
	 return enStatusError;
 }
#endif