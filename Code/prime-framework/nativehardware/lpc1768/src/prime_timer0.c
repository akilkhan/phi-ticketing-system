#include "prime_framework.h"

#if (PF_USE_TIMER0 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_timer0.h"


static PFcallback timer0Callback = 0;
static PFEnBoolean timer0Init = enBooleanFalse;
static PFCfgTimer0 timer0Cfg;

PFEnStatus pfTimer0Open(PFpCfgTimer0 config)
{
	PFbyte loop;
#if (PF_TIMER0_DEBUG ==1)
	CHECK_NULL_PTR(config);
	if(config->clkDiv > 3)
	{
		return enStatusInvArgs;
	}
	if( (config->timer0Mode == 3) || (config->timer0Mode > 5) )
	{
		return enStatusInvArgs;
	}
	if( (config->interrupt == enBooleanTrue) && (config->callback == 0) )
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_TIMER0_DEBUG ==1)	
	// power on timer0 module
	POWER_ON(TIMER0_CH);
	
	// set PCLK divider
	pfSysSetPclkDiv(PCLK_DIV(TIMER0_CH), config->clkDiv);
	
	// set prescaler
	TIMER0_CHANNEL->PR = config->prescaler - 1;
	
	// set timer0 counter mode, with counter pin selection
	TIMER0_CHANNEL->CTCR = config->timer0Mode;
	
	// set match register count
	TIMER0_CHANNEL->MR0 = config->matchValue[0];
	TIMER0_CHANNEL->MR1 = config->matchValue[1];
	TIMER0_CHANNEL->MR2 = config->matchValue[2];
	TIMER0_CHANNEL->MR3 = config->matchValue[3];
	
	for(loop = 0; loop < 4; loop++)
	{
		// set match action	
		TIMER0_CHANNEL->MCR |= ( (config->matchAction[loop] & 0x07) << (loop * 3));
		
		// set pin action for count match
		TIMER0_CHANNEL->EMR |= ( (config->exMatchAction[loop] & 0x03) << ((loop * 2) + 4));
	}
	
	timer0Callback = config->callback;
	
	if(config->interrupt != enBooleanFalse)
	{
		NVIC_EnableIRQ(IRQ_NUM(TIMER0_CH));
	}
	
	pfMemCopy(&timer0Cfg, config, sizeof(PFCfgTimer0));
	timer0Init = enBooleanTrue;
	return enStatusSuccess;
}

void pfTimer0Close(void)
{
	timer0Init = enBooleanFalse;
	// power off timer0 module
	POWER_OFF(TIMER0_CH);
}

PFEnStatus pfTimer0Start(void)
{
#if (PF_TIMER0_DEBUG ==1)
        CHECK_DEV_INIT(timer0Init);	
#endif	// #if (PF_TIMER0_DEBUG ==1)	
	TIMER0_CHANNEL->TCR = 1;
	return enStatusSuccess;
}

PFEnStatus pfTimer0Stop(void)
{
#if (PF_TIMER0_DEBUG ==1)
	CHECK_DEV_INIT(timer0Init);	
#endif	// #if (PF_TIMER0_DEBUG ==1)	
	TIMER0_CHANNEL->TCR = 2;
	return enStatusSuccess;
}

PFEnStatus pfTimer0Reset(void)
{
#if (PF_TIMER0_DEBUG == 1)
	CHECK_DEV_INIT(timer0Init);	
#endif	// #if (PF_TIMER0_DEBUG ==1)	
	TIMER0_CHANNEL->TCR = 2;
	TIMER0_CHANNEL->TCR = 1;
	return enStatusSuccess;
}

PFEnStatus pfTimer0UpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
#if (PF_TIMER0_DEBUG == 1)
	CHECK_DEV_INIT(timer0Init);	
#endif	// #if (PF_TIMER0_DEBUG == 1)	
	switch(regNum)
	{
		case 0:
			TIMER0_CHANNEL->MR0 = regVal;
			break;

		case 1:
			TIMER0_CHANNEL->MR1 = regVal;
			break;

		case 2:
			TIMER0_CHANNEL->MR2 = regVal;
			break;

		case 3:
			TIMER0_CHANNEL->MR3 = regVal;
			break;
		
		default:
			return enStatusInvArgs;
	}
	return enStatusSuccess;
}

PFEnStatus pfTimer0ReadCount(PFdword* count)
{
#if (PF_TIMER0_DEBUG == 1)
	CHECK_DEV_INIT(timer0Init);	
#endif	// #if (PF_TIMER0_DEBUG == 1)	
        *count = TIMER0_CHANNEL->TC;
	return enStatusSuccess;
}

PFEnStatus pfTimer0GetIntStatus(PFdword* status)
{
#if (PF_TIMER0_DEBUG == 1)
        CHECK_DEV_INIT(timer0Init);	
#endif	// #if (PF_TIMER0_DEBUG == 1)	
	*status = TIMER0_CHANNEL->IR;
	return enStatusSuccess;
}

PFEnStatus pfTimer0ClearIntStatus(PFdword intStatus)
{
#if (PF_TIMER0_DEBUG == 1)
	CHECK_DEV_INIT(timer0Init);	
#endif	// #if (PF_TIMER0_DEBUG == 1)	
	TIMER0_CHANNEL->IR = intStatus;
	return enStatusSuccess;
}

void TIMER0_INT_HANDLER(void)
{
	if(timer0Callback != 0)
	{
		timer0Callback();
	}
	TIMER0_CHANNEL->IR = TIMER0_CHANNEL->IR;
}

PFEnStatus pfTimer0IntDisable(void)
{
#if (PF_TIMER0_DEBUG ==1)
        CHECK_DEV_INIT(timer0Init);	
#endif	// #if (PF_TIMER0_DEBUG ==1)
	NVIC_EnableIRQ(IRQ_NUM(TIMER0_CH));
	return enStatusSuccess;
}

PFEnStatus pfTimer0IntEnable(void)
{
#if (PF_TIMER0_DEBUG ==1)
        CHECK_DEV_INIT(timer0Init);	
#endif	// #if (PF_TIMER0_DEBUG ==1)
	NVIC_DisableIRQ(IRQ_NUM(TIMER0_CH));
	return enStatusSuccess;
}

PFEnStatus pfTimer0GetTickFreq(PFdword* tickFreq)
{
	PFdword timerPclk;
#if (PF_TIMER0_DEBUG == 1)	
	CHECK_DEV_INIT(timer0Init);	
#endif	// #if (PF_TIMER0_DEBUG == 1)
	timerPclk = pfSysGetPclk(PCLK_DIV(TIMER0_CH));
	*tickFreq = timerPclk / (TIMER0_CHANNEL->PR + 1);
	return enStatusSuccess;
}

PFEnStatus pfTimer0SetTickFreq(PFdword tickFreq)
{
	PFdword f_pclk, prescale;
	PFdword pclk[4] = {25000000, 100000000, 50000000, 12500000}	;
	PFbyte pclkInd, done = 0;
#if (PF_TIMER0_DEBUG == 1)	
	CHECK_DEV_INIT(timer0Init);	
#endif	// #if (PF_TIMER0_DEBUG == 1)	
	for(pclkInd = 0; pclkInd < 4; pclkInd++)
	{
		prescale = pclk[pclkInd] / tickFreq;
		if ((tickFreq * prescale) == pclk[pclkInd])
		{
			done = 1;
			pfSysSetPclkDiv(PCLK_DIV(TIMER0_CH), (PFEnPclkDivider)pclkInd);
			TIMER0_CHANNEL->PR = prescale-1;
			break;
		}
	}
	
	if(done == 1)
	{
		return enStatusSuccess;
	}
	else
	{
		return enStatusNotSupported;
	}
}

PFEnStatus pfTimer0UpdateMatchControlRegister(PFbyte regNum,PFEnTimer0MatchAction matchAction)
{
#if (PF_TIMER0_DEBUG == 1)	
	if((regNum >3) || (matchAction > 7)) 
        return enStatusInvArgs;
#endif	// #if (PF_TIMER0_DEBUG == 1)
    
    TIMER0_CHANNEL->MCR |= ( (matchAction & 0x07) << (regNum * 3));
	return enStatusSuccess;
}

#endif	// #if (PF_USE_TIMER0 == 1)
