#include "prime_framework.h"
#include "prime_gpio.h"

static void pfGpioSetDirection(PFdword port, PFdword pins, PFEnDirection direction);
static PFEnStatus pfGpioFunctionSelect(PFdword port, PFdword pins, PFEnAltFunction function);
static PFEnStatus pfGpioModeSelect(PFdword port, PFdword pins, PFEnGpioPinMode mode);
static PFEnStatus pfGpioOpenDrain(PFdword port, PFdword pin, PFEnOpenDrain odState);

PFEnStatus pfGpioInit(PFpCfgGpio config, PFdword count)
{
	PFdword loop;
	PFEnStatus status = enStatusSuccess;
	for(loop = 0; loop < count; loop++)
	{
		status = pfGpioPinConfig(&config[loop]);
		if(status != enStatusSuccess)
		{
			return status;
		}
	}
	return status;
}

PFEnStatus pfGpioPinConfig(PFpCfgGpio config)
{
	PFEnStatus status = enStatusSuccess;
	if((config->port / GPIO_PORT_OFFSET) >= GPIO_MAX_PORT) 
		return enStatusInvArgs;
	
	pfGpioSetDirection(config->port, config->pins, config->direction);
	pfGpioFunctionSelect(config->port, config->pins, config->altFuction);
	pfGpioModeSelect(config->port, config->pins, config->mode);
	pfGpioOpenDrain(config->port, config->pins, config->openDrain);
	
	return status;
}

void pfGpioSetDirection(PFdword port, PFdword pins, PFEnDirection direction)
{
	if(direction == enGpioDirOutput)
	{
		*((PFdword*)(GPIO_IODIR_BASE + port)) |= pins;
	}
	else
	{
		*((PFdword*)(GPIO_IODIR_BASE + port)) &= ~pins;
	}	
}

PFEnStatus pfGpioFunctionSelect(PFdword port, PFdword pins, PFEnAltFunction function)
{
	PFword pinIndex;

	function &= 0x03;

	switch(port)
	{
		case GPIO_PORT_0:
			for(pinIndex = 0; pinIndex < 16; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINSEL0 &= ~(3 << (pinIndex*2));
					PERIPH_PINCON->PINSEL0 |= (function << (pinIndex*2));
				}
			}
		
			for(pinIndex = 16; pinIndex < 32; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINSEL1 &= ~(3 << ((pinIndex*2)-32));
					PERIPH_PINCON->PINSEL1 |= (function << ((pinIndex*2)-32));
				}
			}
			break;

		case GPIO_PORT_1:
			for(pinIndex = 0; pinIndex < 16; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINSEL2 &= ~(3 << (pinIndex*2));
					PERIPH_PINCON->PINSEL2 |= (function << (pinIndex*2));
				}
			}
		
			for(pinIndex = 16; pinIndex < 32; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINSEL3 &= ~(3 << ((pinIndex*2)-32));
					PERIPH_PINCON->PINSEL3 |= (function << ((pinIndex*2)-32));
				}
			}
			break;

		case GPIO_PORT_2:
			for(pinIndex = 0; pinIndex < 16; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINSEL4 &= ~(3 << (pinIndex*2));
					PERIPH_PINCON->PINSEL4 |= (function << (pinIndex*2));
				}
			}
		
			for(pinIndex = 16; pinIndex < 32; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINSEL5 &= ~(3 << ((pinIndex*2)-32));
					PERIPH_PINCON->PINSEL5 |= (function << ((pinIndex*2)-32));
				}
			}
			break;

		case GPIO_PORT_3:
			for(pinIndex = 0; pinIndex < 16; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINSEL6 &= ~(3 << (pinIndex*2));
					PERIPH_PINCON->PINSEL6 |= (function << (pinIndex*2));
				}
			}
		
			for(pinIndex = 16; pinIndex < 32; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINSEL7 &= ~(3 << ((pinIndex*2)-32));
					PERIPH_PINCON->PINSEL7 |= (function << ((pinIndex*2)-32));
				}
			}
			break;

		case GPIO_PORT_4:
			for(pinIndex = 0; pinIndex < 16; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINSEL8 &= ~(3 << (pinIndex*2));
					PERIPH_PINCON->PINSEL8 |= (function << (pinIndex*2));
				}
			}
		
			for(pinIndex = 16; pinIndex < 32; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINSEL9 &= ~(3 << ((pinIndex*2)-32));
					PERIPH_PINCON->PINSEL9 |= (function << ((pinIndex*2)-32));
				}
			}
			break;
		
		default:
			return enStatusInvArgs;
	}
	return enStatusSuccess;
}

PFEnStatus pfGpioModeSelect(PFdword port, PFdword pins, PFEnGpioPinMode mode)
{
	PFword pinIndex;

	mode &= 0x03;

	switch(port)
	{
		case GPIO_PORT_0:
			for(pinIndex = 0; pinIndex < 16; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINMODE0 &= ~(3 << (pinIndex*2));
					PERIPH_PINCON->PINMODE0 |= (mode << (pinIndex*2));
				}
			}
		
			for(pinIndex = 16; pinIndex < 32; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINMODE1 &= ~(3 << ((pinIndex*2)-32));
					PERIPH_PINCON->PINMODE1 |= (mode << ((pinIndex*2)-32));
				}
			}
			break;

		case GPIO_PORT_1:
			for(pinIndex = 0; pinIndex < 16; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINMODE2 &= ~(3 << (pinIndex*2));
					PERIPH_PINCON->PINMODE2 |= (mode << (pinIndex*2));
				}
			}
		
			for(pinIndex = 16; pinIndex < 32; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINMODE3 &= ~(3 << ((pinIndex*2)-32));
					PERIPH_PINCON->PINMODE3 |= (mode << ((pinIndex*2)-32));
				}
			}
			break;

		case GPIO_PORT_2:
			for(pinIndex = 0; pinIndex < 16; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINMODE4 &= ~(3 << (pinIndex*2));
					PERIPH_PINCON->PINMODE4 |= (mode << (pinIndex*2));
				}
			}
		
			for(pinIndex = 16; pinIndex < 32; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINMODE5 &= ~(3 << ((pinIndex*2)-32));
					PERIPH_PINCON->PINMODE5 |= (mode << ((pinIndex*2)-32));
				}
			}
			break;

		case GPIO_PORT_3:
			for(pinIndex = 0; pinIndex < 16; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINMODE6 &= ~(3 << (pinIndex*2));
					PERIPH_PINCON->PINMODE6 |= (mode << (pinIndex*2));
				}
			}
		
			for(pinIndex = 16; pinIndex < 32; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINMODE7 &= ~(3 << ((pinIndex*2)-32));
					PERIPH_PINCON->PINMODE7 |= (mode << ((pinIndex*2)-32));
				}
			}
			break;

		case GPIO_PORT_4:
			for(pinIndex = 0; pinIndex < 16; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINMODE8 &= ~(3 << (pinIndex*2));
					PERIPH_PINCON->PINMODE8 |= (mode << (pinIndex*2));
				}
			}
		
			for(pinIndex = 16; pinIndex < 32; pinIndex++)
			{
				if( (pins & (1 << pinIndex)) == (1 << pinIndex) )
				{
					PERIPH_PINCON->PINMODE9 &= ~(3 << ((pinIndex*2)-32));
					PERIPH_PINCON->PINMODE9 |= (mode << ((pinIndex*2)-32));
				}
			}
			break;
			
		default:
			return enStatusInvArgs;
	}
	
	return enStatusSuccess;
}

PFEnStatus pfGpioOpenDrain(PFdword port, PFdword pin, PFEnOpenDrain odState)
{
	switch(port)
	{
		case GPIO_PORT_0:
			if(odState)
			{
				PERIPH_PINCON->PINMODE_OD0 |= pin;
			}	
			else
			{
				PERIPH_PINCON->PINMODE_OD0 &= ~pin;
			}	
			break;

		case GPIO_PORT_1:
			if(odState)
			{
				PERIPH_PINCON->PINMODE_OD1 |= pin;
			}
			else
			{
				PERIPH_PINCON->PINMODE_OD1 &= ~pin;
			}
			break;

		case GPIO_PORT_2:
			if(odState)
			{
				PERIPH_PINCON->PINMODE_OD2 |= pin;
			}
			else
			{
				PERIPH_PINCON->PINMODE_OD2 &= ~pin;
			}
			break;

		case GPIO_PORT_3:
			if(odState)
			{
				PERIPH_PINCON->PINMODE_OD3 |= pin;
			}
			else
			{
				PERIPH_PINCON->PINMODE_OD3 &= ~pin;
			}
			break;

		case GPIO_PORT_4:
			if(odState)
			{
				PERIPH_PINCON->PINMODE_OD4 |= pin;
			}
			else
			{
				PERIPH_PINCON->PINMODE_OD4 &= ~pin;
			}
			break;
			
		default:
			return enStatusInvArgs;
	}
	
	return enStatusSuccess;
}
