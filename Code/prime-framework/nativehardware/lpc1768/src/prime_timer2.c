#include "prime_framework.h"

#if (PF_USE_TIMER2 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_timer2.h"


static PFcallback timer2Callback = 0;
static PFEnBoolean timer2Init = enBooleanFalse;
static PFCfgTimer2 timer2Cfg;

PFEnStatus pfTimer2Open(PFpCfgTimer2 config)
{
	PFbyte loop;
#if (PF_TIMER2_DEBUG ==1)
	CHECK_NULL_PTR(config);
	if(config->clkDiv > 3)
	{
		return enStatusInvArgs;
	}
	if( (config->timer2Mode == 3) || (config->timer2Mode > 5) )
	{
		return enStatusInvArgs;
	}
	if( (config->interrupt == enBooleanTrue) && (config->callback == 0) )
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_TIMER2_DEBUG ==1)	
	// power on timer2 module
	POWER_ON(TIMER2_CH);
	
	// set PCLK divider
	pfSysSetPclkDiv(PCLK_DIV(TIMER2_CH), config->clkDiv);
	
	// set prescaler
	TIMER2_CHANNEL->PR = config->prescaler-1;
	
	// set timer2 counter mode, with counter pin selection
	TIMER2_CHANNEL->CTCR = config->timer2Mode;
	
	// set match register count
	TIMER2_CHANNEL->MR0 = config->matchValue[0];
	TIMER2_CHANNEL->MR1 = config->matchValue[1];
	TIMER2_CHANNEL->MR2 = config->matchValue[2];
	TIMER2_CHANNEL->MR3 = config->matchValue[3];
	
	for(loop = 0; loop < 4; loop++)
	{
		// set match action	
		TIMER2_CHANNEL->MCR |= ( (config->matchAction[loop] & 0x07) << (loop * 3));
		
		// set pin action for count match
		TIMER2_CHANNEL->EMR |= ( (config->exMatchAction[loop] & 0x03) << ((loop * 2) + 4));
	}
	
	timer2Callback = config->callback;
	
	if(config->interrupt != enBooleanFalse)
	{
		NVIC_EnableIRQ(IRQ_NUM(TIMER2_CH));
	}
	
	pfMemCopy(&timer2Cfg, config, sizeof(PFCfgTimer2));
	timer2Init = enBooleanTrue;
	return enStatusSuccess;
}

void pfTimer2Close(void)
{
	timer2Init = enBooleanFalse;
	// power off timer2 module
	POWER_OFF(TIMER2_CH);
}

PFEnStatus pfTimer2Start(void)
{
#if (PF_TIMER2_DEBUG ==1)
        CHECK_DEV_INIT(timer2Init);	
#endif	// #if (PF_TIMER2_DEBUG ==1)	
	TIMER2_CHANNEL->TCR = 1;
	return enStatusSuccess;
}

PFEnStatus pfTimer2Stop(void)
{
#if (PF_TIMER2_DEBUG ==1)
	CHECK_DEV_INIT(timer2Init);	
#endif	// #if (PF_TIMER2_DEBUG ==1)	
	TIMER2_CHANNEL->TCR = 2;
	return enStatusSuccess;
}

PFEnStatus pfTimer2Reset(void)
{
#if (PF_TIMER2_DEBUG == 1)
	CHECK_DEV_INIT(timer2Init);	
#endif	// #if (PF_TIMER2_DEBUG ==1)	
	TIMER2_CHANNEL->TCR = 2;
	TIMER2_CHANNEL->TCR = 1;
	return enStatusSuccess;
}

PFEnStatus pfTimer2UpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
#if (PF_TIMER2_DEBUG == 1)
	CHECK_DEV_INIT(timer2Init);	
#endif	// #if (PF_TIMER2_DEBUG == 1)	
	switch(regNum)
	{
		case 0:
			TIMER2_CHANNEL->MR0 = regVal;
			break;

		case 1:
			TIMER2_CHANNEL->MR1 = regVal;
			break;

		case 2:
			TIMER2_CHANNEL->MR2 = regVal;
			break;

		case 3:
			TIMER2_CHANNEL->MR3 = regVal;
			break;
		
		default:
			return enStatusInvArgs;
	}
	return enStatusSuccess;
}

PFEnStatus pfTimer2ReadCount(PFdword* count)
{
#if (PF_TIMER2_DEBUG == 1)
	CHECK_DEV_INIT(timer2Init);	
#endif	// #if (PF_TIMER2_DEBUG == 1)	
        *count = TIMER2_CHANNEL->TC;
	return enStatusSuccess;
}

PFEnStatus pfTimer2GetIntStatus(PFdword* status)
{
#if (PF_TIMER2_DEBUG == 1)
        CHECK_DEV_INIT(timer2Init);	
#endif	// #if (PF_TIMER2_DEBUG == 1)	
	*status = TIMER2_CHANNEL->IR;
	return enStatusSuccess;
}

PFEnStatus pfTimer2ClearIntStatus(PFdword intStatus)
{
#if (PF_TIMER2_DEBUG == 1)
	CHECK_DEV_INIT(timer2Init);	
#endif	// #if (PF_TIMER2_DEBUG == 1)	
	TIMER2_CHANNEL->IR = intStatus;
	return enStatusSuccess;
}

void TIMER2_INT_HANDLER(void)
{
	if(timer2Callback != 0)
	{
		timer2Callback();
	}
	TIMER2_CHANNEL->IR = TIMER2_CHANNEL->IR;
}

PFEnStatus pfTimer2IntDisable(void)
{
#if (PF_TIMER2_DEBUG ==1)
        CHECK_DEV_INIT(timer2Init);	
#endif	// #if (PF_TIMER2_DEBUG ==1)
	NVIC_EnableIRQ(IRQ_NUM(TIMER2_CH));
	return enStatusSuccess;
}

PFEnStatus pfTimer2IntEnable(void)
{
#if (PF_TIMER2_DEBUG ==1)
        CHECK_DEV_INIT(timer2Init);	
#endif	// #if (PF_TIMER2_DEBUG ==1)
	NVIC_DisableIRQ(IRQ_NUM(TIMER2_CH));
	return enStatusSuccess;
}

PFEnStatus pfTimer2GetTickFreq(PFdword* tickFreq)
{
	PFdword timerPclk;
#if (PF_TIMER2_DEBUG == 1)	
	CHECK_DEV_INIT(timer2Init);	
#endif	// #if (PF_TIMER2_DEBUG == 1)
	timerPclk = pfSysGetPclk(PCLK_DIV(TIMER2_CH));
	*tickFreq = timerPclk / (TIMER2_CHANNEL->PR + 1);
	return enStatusSuccess;
}

PFEnStatus pfTimer2SetTickFreq(PFdword tickFreq)
{
	PFdword f_pclk, prescale;
	PFdword pclk[4] = {25000000, 100000000, 50000000, 12500000}	;
	PFbyte pclkInd, done = 0;
#if (PF_TIMER2_DEBUG == 1)	
	CHECK_DEV_INIT(timer2Init);	
#endif	// #if (PF_TIMER2_DEBUG == 1)	
	for(pclkInd = 0; pclkInd < 4; pclkInd++)
	{
		prescale = pclk[pclkInd] / tickFreq;
		if ((tickFreq * prescale) == pclk[pclkInd])
		{
			done = 1;
			pfSysSetPclkDiv(PCLK_DIV(TIMER2_CH), (PFEnPclkDivider)pclkInd);
			TIMER2_CHANNEL->PR = prescale-1;
			break;
		}
	}
	
	if(done == 1)
	{
		return enStatusSuccess;
	}
	else
	{
		return enStatusNotSupported;
	}
}

PFEnStatus pfTimer2UpdateMatchControlRegister(PFbyte regNum,PFEnTimer2MatchAction matchAction)
{
#if (PF_TIMER2_DEBUG == 1)	
	if((regNum >3) || (matchAction > 7)) 
        return enStatusInvArgs;
#endif	// #if (PF_TIMER2_DEBUG == 1)
    
    TIMER2_CHANNEL->MCR |= ( (matchAction & 0x07) << (regNum * 3));
	return enStatusSuccess;
}

#endif	// #if (PF_USE_TIEMR2 == 1)
