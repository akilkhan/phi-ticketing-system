#include "prime_framework.h"
#if (PF_USE_CAN1 == 1)
#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_canCommon.h"
#include "prime_can1.h"

#if(CAN1_USE_FIFO != 0)
#include "prime_fifo.h"
	#warning CAN1 FIFO is enabled for interrupt based communication
	#if( (CAN1_BUFFER_SIZE == 0) || ((CAN1_BUFFER_SIZE & (CAN1_BUFFER_SIZE - 1)) != 0) )
		#error CAN1_BUFFER_SIZE can1not be zero. CAN1_BUFFER_SIZE should be power of 2
	#endif
#endif	// #if(CAN1_USE_FIFO != 0)
	
#define CAN1_MESSAGE_SIZE			(sizeof(PFCanMessage))
#define pfCan1ResetMode()			pfCan1SetCommand(CAN1_MOD_RM, enBooleanTrue)	
#define pfCan1OperatingMode()		pfCan1SetCommand(CAN1_MOD_RM, enBooleanFalse)

static PFEnStatus pfCan1SetBaudrate(PFdword baudrate);
static PFEnStatus pfCan1WriteMessage(PFpCanMessage message);
static PFEnStatus pfCan1ReadMessage(PFpCanMessage message);
static PFEnStatus pfCan1SetCommand(PFdword cmd, PFEnBoolean enable);

#if(CAN1_USE_FIFO != 0)
static PFEnStatus pfCan1PushRxMessage(PFpCanMessage msg);
static PFEnStatus pfCan1PopRxMessage(PFpCanMessage msg);
static PFEnStatus pfCan1PushTxMessage(PFpCanMessage msg);
static PFEnStatus pfCan1PopTxMessage(PFpCanMessage msg);
	
static void pfCan1Buffer1TxDefaultCallback(void);
static void pfCan1Buffer2TxDefaultCallback(void);
static void pfCan1Buffer3TxDefaultCallback(void);
static void pfCan1RxDefaultCallback(void);
	
#endif	// #if(CAN1_USE_FIFO != 0)	
	


/* Values of bit time register for different baudrates
   NT = Nominal bit time = TSEG1 + TSEG2 + 3
   SP = Sample point     = ((TSEG2 +1) / (TSEG1 + TSEG2 + 3)) * 100%
                                            SAM,  SJW, TSEG1, TSEG2, NT,  SP */
static const PFdword CAN1_BIT_TIME[] = {           0, /*             not used             */
                                           0, /*             not used             */
                                           0, /*             not used             */
                                           0, /*             not used             */
                                  0x0001C000, /* 0+1,  3+1,   1+1,   0+1,  4, 75% */
                                           0, /*             not used             */
                                  0x0012C000, /* 0+1,  3+1,   2+1,   1+1,  6, 67% */
                                           0, /*             not used             */
                                  0x0023C000, /* 0+1,  3+1,   3+1,   2+1,  8, 63% */
                                           0, /*             not used             */
                                  0x0025C000, /* 0+1,  3+1,   5+1,   2+1, 10, 70% */
                                           0, /*             not used             */
                                  0x0036C000, /* 0+1,  3+1,   6+1,   3+1, 12, 67% */
                                           0, /*             not used             */
                                           0, /*             not used             */
                                  0x0048C000, /* 0+1,  3+1,   8+1,   4+1, 15, 67% */
                                  0x0049C000, /* 0+1,  3+1,   9+1,   4+1, 16, 69% */
                                };

//static PFEnBoolean can1TxBuffFree[3] = {enBooleanFalse, enBooleanFalse, enBooleanFalse};
static PFEnBoolean can1ChInit = enBooleanFalse;
static PFEnCan1Interrupt can1Int;
#if(CAN1_USE_FIFO != 0)
static PFbyte can1RxBuffer[CAN1_BUFFER_SIZE];		// CAN1 transmit buffer
static PFbyte can1TxBuffer[CAN1_BUFFER_SIZE];		// CAN1 receive buffer
static PFFifo can1TxFifo;							// CAN1 transmit fifo structure
static PFFifo can1RxFifo;							// CAN1 receive fifo structure
#endif	// #if(CAN1_USE_FIFO  != 0)
static PFCfgCan1 can1Cfg;

PFEnStatus pfCan1Open(PFpCfgCan1 config)
{
#if (PF_CAN1_DEBUG == 1)
	// Validate config pointer
	CHECK_NULL_PTR(config);
	
	// Validate peripheral clock divider, baudrate value and error callback pointer
	if( (config->clkDiv > 3) || (config->baudrate > CAN1_MAX_BAUDRATE) || ((config->interrupt != enCan1IntNone) && IS_PTR_NULL(config->errCallback)) )
	{
		return enStatusInvArgs;
	}
#if(CAN1_USE_FIFO == 0)	
	// Validate TX and RX callback pointers
	if(((config->interrupt & enCan1IntRx) != 0) && IS_PTR_NULL(config->rxCallback))
	{
		return enStatusInvArgs;
	}
	if( ((config->interrupt & enCan1IntTx) != 0) && IS_PTR_NULL(config->txCallback))
	{
		return enStatusInvArgs;
	}
#endif // #if(CAN1_USE_FIFO == 0)		
#endif	// #if (PF_CAN1_DEBUG == 1)
	
	// power on CAN1 module
	POWER_ON(CAN1_CH);
	
	// set PCLK divider
	pfSysSetPclkDiv(PCLK_DIV(CAN1_CH), config->clkDiv);
	pfSysSetPclkDiv(PCLK_DIV_ACF, config->clkDiv);
	
	// Put CAN1 controller in reset mode
	CAN1_CHANNEL->MOD = CAN_MOD_RM;
	
	// Bypass acceptance filter
	PERIPH_CANAF->AFMR = 2;
	
	// Disable all interrupts 
	CAN1_CHANNEL->IER = 0;
	// Clear status register
	CAN1_CHANNEL->GSR = 0;

	// Set baudrate
	pfCan1SetBaudrate(config->baudrate);
	
#if(CAN1_USE_FIFO != 0)	
	// initialize fifo 
	pfFifoInit(&can1TxFifo, can1TxBuffer, CAN1_BUFFER_SIZE);
	pfFifoInit(&can1RxFifo, can1RxBuffer, CAN1_BUFFER_SIZE);
    pfCanSetCallback(CAN1_CONTROLLER, enCanBuff1TxCallback, pfCan1Buffer1TxDefaultCallback);
    pfCanSetCallback(CAN1_CONTROLLER, enCanBuff2TxCallback, pfCan1Buffer2TxDefaultCallback);
    pfCanSetCallback(CAN1_CONTROLLER, enCanBuff1TxCallback, pfCan1Buffer3TxDefaultCallback);
    pfCanSetCallback(CAN1_CONTROLLER, enCanRxCallback, pfCan1RxDefaultCallback);
#else
	// intialize callbacks
	pfCanSetCallback(1,enCanBuff1TxCallback,config->txCallback);
	pfCanSetCallback(1,enCanBuff2TxCallback,config->txCallback);
	pfCanSetCallback(1,enCanBuff3TxCallback,config->txCallback);
	pfCanSetCallback(1,enCanRxCallback,config->rxCallback);
#endif // #if(CAN1_USE_FIFO == 0)

	// Enable Tx and Rx interrupt
	if(config->interrupt != enCan1IntNone)
	{
		can1Int = config->interrupt;
		pfCanSetCallback(CAN1_CONTROLLER, enCanErrCallback, config->errCallback);
		if((config->interrupt & enCan1IntRx) != 0)
		{	
			CAN1_CHANNEL->IER |= BIT_MASK_0;
		}
		if((config->interrupt & enCan1IntTx) != 0)
		{	
			CAN1_CHANNEL->IER |= (BIT_MASK_1 | BIT_MASK_9 | BIT_MASK_10);
		}
		NVIC_EnableIRQ(CAN_IRQn);
	}
	
	// Put CAN1 controller in operating mode
	CAN1_CHANNEL->MOD = 0;
	
	// Transmitter ready for transmission
	while((CAN1_CHANNEL->GSR & CAN_GSR_TBS) == 0); 
	
//	can1TxBuffFree[0] = enBooleanTrue;
//	can1TxBuffFree[1] = enBooleanTrue;
//	can1TxBuffFree[2] = enBooleanTrue;
	
	pfMemCopy(&can1Cfg, config, sizeof(PFCfgCan1));
	can1ChInit = enBooleanTrue;
	pfCanSetChannelStatus(0,enBooleanTrue);
	return enStatusSuccess;
}

PFEnStatus pfCan1Close(void)
{
	POWER_OFF(CAN1_CH);
	can1ChInit = enBooleanFalse;
	pfCanSetChannelStatus(0,enBooleanFalse);
	return enStatusSuccess;
}

static PFEnStatus pfCan1SetBaudrate(PFdword baudrate)
{
	PFdword can1Pclk, result, nominal_time;
#if (PF_CAN1_DEBUG == 1)
	if(baudrate > CAN1_MAX_BAUDRATE)
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_CAN1_DEBUG == 1)	
	
	can1Pclk = pfSysGetPclk(PCLK_DIV(CAN1_CH));
	nominal_time = 10;
	/* Prepare value appropriate for bit time register */
	result  = (can1Pclk / (nominal_time * baudrate)) - 1;
	result &= 0x000003FF;
	result |= CAN1_BIT_TIME[nominal_time];

	CAN1_CHANNEL->BTR  = result;                           /* Set bit timing */
	
	return enStatusSuccess;
}

PFEnStatus pfCan1Write(PFpCanMsgHeader msgHeader,PFbyte* data, PFdword size)
{
	PFEnStatus status;
	PFdword index=0,remainingBytes=0,cpByte=0;
	PFCanMessage message={0};

#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
	CHECK_NULL_PTR(msgHeader);
	CHECK_NULL_PTR(data);
#endif	// #if (PF_CAN1_DEBUG == 1)	
	message.id = msgHeader->id;
	message.frameFormat = msgHeader->frameFormat;
	message.remoteFrame = msgHeader->remoteFrame;
	remainingBytes = size;
	
	if((can1Int & enCan1IntTx) != 0)
	{
	#if(CAN1_USE_FIFO !=0)
		if(size > ((CAN1_BUFFER_SIZE/CAN1_MESSAGE_SIZE) * 8))
			return enStatusNoMem;
		while(remainingBytes != 0)
		{
			if(remainingBytes >= 8)
				cpByte = 8;
			else
				cpByte =remainingBytes;
			
			pfMemCopy(message.data,&data[index],cpByte);
			message.length = cpByte;
			remainingBytes -= cpByte;
			index +=cpByte;
			pfCan1PushTxMessage(&message);
		}
		pfCan1PopTxMessage(&message);
		status = pfCan1WriteMessage(&message);
	#else		//user Callback
		if(size >= 8)
			cpByte = 8;
		else
			cpByte = size;
		pfMemCopy(message.data,&data[index],cpByte);
		message.length = cpByte;
		status = pfCan1WriteMessage(&message);
	#endif
	}
	//Polling Data Transmission
	else
	{
		while(remainingBytes != 0)
		{
			if(remainingBytes >= 8)
				cpByte = 8;
			else
				cpByte =remainingBytes;
			
			pfMemCopy(message.data,&data[index],cpByte);
			message.length = cpByte;
			remainingBytes -= cpByte;
			index +=cpByte;
			status = pfCan1WriteMessage(&message);
			if(status != enStatusSuccess)
				return status;
		}
		while((CAN1_CHANNEL->SR & CAN_SR_TCS1) == 0);
		while((CAN1_CHANNEL->SR & CAN_SR_TCS2) == 0);
		while((CAN1_CHANNEL->SR & CAN_SR_TCS3) == 0);
	}	
	return status;
}

PFEnStatus pfCan1Read(PFpCanMessage message)
{
	PFEnStatus status;
	PFdword buffCount;
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
	CHECK_NULL_PTR(message);
#endif	// #if (PF_CAN1_DEBUG == 1)	
	
#if(CAN1_USE_FIFO != 0)	
	if((can1Int & enCan1IntRx) != 0)
	{
		status = pfCan1GetRxBufferCount(&buffCount);
	#if (PF_CAN1_DEBUG == 1)
		if(status != enStatusSuccess)
		{
			return status;
		}
	#endif	// #if (PF_CAN1_DEBUG == 1)	
		if(buffCount != 0)
		{
			pfCan1PopRxMessage(message);
			return enStatusSuccess;
		}
		else
		{
			return enStatusError;
		}
	}	
#endif	// #if(CAN1_USE_FIFO !=0)
	status = pfCan1ReadMessage(message);
	return status;
}

static PFEnStatus pfCan1WriteMessage(PFpCanMessage message)
{
	PFdword can1Header = 0;
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
	if(IS_PTR_NULL(message))
	{
		return enStatusInvArgs;
	}	
#endif	// #if (PF_CAN1_DEBUG == 1)	
	
	can1Header = (message->length << 16);
	if(message->remoteFrame == enBooleanTrue)
	{
		can1Header |= BIT_MASK_30;
	}
	if(message->frameFormat == enCanFrameExtended)
	{
		can1Header |= BIT_MASK_31;
	}
	while(1)
	{
		// check status for transmit buffer 1
		if((CAN1_CHANNEL->SR & CAN_SR_TCS1) == CAN_SR_TCS1)
		{
//			can1TxBuffFree[0] = enBooleanFalse;
			CAN1_CHANNEL->TFI1 = can1Header;
			CAN1_CHANNEL->TID1 = message->id;
			CAN1_CHANNEL->TDA1 = message->data[0] | (message->data[1] << 8) | (message->data[2] << 16) | (message->data[3] << 24);
			CAN1_CHANNEL->TDB1 = message->data[4] | (message->data[5] << 8) | (message->data[6] << 16) | (message->data[7] << 24);
			pfCan1SetCommand(CAN_CMR_TR | CAN_CMR_STB1, enBooleanTrue);
			break;
		}
		else if((CAN1_CHANNEL->SR & CAN_SR_TCS2) == CAN_SR_TCS2)		// check status for transmit buffer 2
		{
//			can1TxBuffFree[1] = enBooleanFalse;
			CAN1_CHANNEL->TFI2 = can1Header;
			CAN1_CHANNEL->TID2 = message->id;
			CAN1_CHANNEL->TDA2 = message->data[0] | (message->data[1] << 8) | (message->data[2] << 16) | (message->data[3] << 24);
			CAN1_CHANNEL->TDB2 = message->data[4] | (message->data[5] << 8) | (message->data[6] << 16) | (message->data[7] << 24);
			pfCan1SetCommand(CAN_CMR_TR | CAN_CMR_STB2, enBooleanTrue);
			break;
		}
		else if((CAN1_CHANNEL->SR & CAN_SR_TCS3) == CAN_SR_TCS3)		// check status for transmit buffer 3
		{
//			can1TxBuffFree[2] = enBooleanFalse;
			CAN1_CHANNEL->TFI3 = can1Header;
			CAN1_CHANNEL->TID3 = message->id;
			CAN1_CHANNEL->TDA3 = message->data[0] | (message->data[1] << 8) | (message->data[2] << 16) | (message->data[3] << 24);
			CAN1_CHANNEL->TDB3 = message->data[4] | (message->data[5] << 8) | (message->data[6] << 16) | (message->data[7] << 24);
			pfCan1SetCommand(CAN_CMR_TR | CAN_CMR_STB3, enBooleanTrue);
			break;
		}
	}	//while(1)
	return enStatusSuccess;
}

static PFEnStatus pfCan1ReadMessage(PFpCanMessage message)
{
	PFbyte loop, byteCount = 0;
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
	CHECK_NULL_PTR(message);
#endif	// #if (PF_CAN1_DEBUG == 1)	
	// Check if there is any unread message in the receive buffer
	if((CAN1_CHANNEL->SR & CAN_SR_RBS) == 0)
	{ 
		return enStatusError;
	}
	// Check data length of received message
	message->length = (CAN1_CHANNEL->RFS >> 16) & 0x0F;
	if(message->length > 8)
	{
		message->length = 8;
	}
	
	// Check if the received message is remote frame request
	if((CAN1_CHANNEL->RFS & CAN_RFS_RTR) != 0)
	{
		message->remoteFrame = enBooleanTrue;
	}
	else
	{
		message->remoteFrame = enBooleanFalse;
	}
	
	// Check frame format for received message
	if((CAN1_CHANNEL->RFS & CAN_RFS_FF) != 0)
	{
		message->frameFormat = enCanFrameExtended;
	}
	else
	{
		message->frameFormat = enCanFrameStandard;
	}	
	
	message->id = CAN1_CHANNEL->RID;
	
	for(loop = 0; (loop < 4)&&(byteCount < message->length); loop++)
	{
		message->data[loop] = CAN1_CHANNEL->RDA >> (loop*8);
		byteCount++;
	}
	for(loop = 0; (loop < 4)&&(byteCount < message->length); loop++)
	{
		message->data[loop + 4] = CAN1_CHANNEL->RDB >> (loop*8);
		byteCount++;
	}
	while(byteCount < 8)
	{
		message->data[byteCount] = 0;
		byteCount++;
	}
		
	pfCan1SetCommand(CAN_CMR_RRB, enBooleanTrue);
	
	return enStatusSuccess;
}

static PFEnStatus pfCan1SetCommand(PFdword cmd, PFEnBoolean enable)
{
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
#endif	// #if (PF_CAN1_DEBUG == 1)	

	if(enable == enBooleanTrue)
	{
		CAN1_CHANNEL->CMR |= cmd;
	}
	else
	{
		CAN1_CHANNEL->CMR &= ~(cmd);
	}
	return enStatusSuccess;
}

PFEnBoolean pfCan1CheckStatus(PFEnCan1Status param)
{
	if((CAN1_CHANNEL->GSR & param) != 0)
	{
		return enBooleanTrue;
	}
	else
	{
		return enBooleanFalse;
	}
}

PFEnStatus pfCan1GetIntStatus(PFdword* status)
{
	#if (PF_CAN1_DEBUG == 1)
	    CHECK_NULL_PTR(status);
	#endif
	*status = CAN1_CHANNEL->ICR;
	return enStatusSuccess;
}

PFEnStatus pfCan1GetCtrlStatus(PFdword* status)
{
	#if (PF_CAN1_DEBUG == 1)
	    CHECK_NULL_PTR(status);
	#endif
	*status = CAN1_CHANNEL->SR;
	return enStatusSuccess;
}

PFEnStatus pfCan1GetTxErrCounter(PFbyte* errCount)
{
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
	CHECK_NULL_PTR(errCount);
#endif	// #if (PF_CAN1_DEBUG == 1)	

	*errCount = CAN1_CHANNEL->GSR >> 24;
	return enStatusSuccess;
}

PFEnStatus pfCan1GetRxErrCounter(PFbyte* errCount)
{
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
	CHECK_NULL_PTR(errCount);
#endif	// #if (PF_CAN1_DEBUG == 1)	

	*errCount = CAN1_CHANNEL->GSR >> 16;
	return enStatusSuccess;
}

#if(CAN1_USE_FIFO != 0)
static PFEnStatus pfCan1PushRxMessage(PFpCanMessage msg)
{
	PFbyte loop;
	PFbyte* msgPtr;
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
	CHECK_NULL_PTR(msg);
#endif	// #if (PF_CAN1_DEBUG == 1)		

	msgPtr = (PFbyte*)msg;
	for(loop = 0; loop < CAN1_MESSAGE_SIZE; loop++)
	{
		pfFifoPush(&can1RxFifo, *(msgPtr + loop));
	}
	return enStatusSuccess;
}

static PFEnStatus pfCan1PopRxMessage(PFpCanMessage msg)
{
	PFbyte loop;
	PFbyte* msgPtr;
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
	CHECK_NULL_PTR(msg);
#endif	// #if (PF_CAN1_DEBUG == 1)		
	
	msgPtr = (PFbyte*)msg;
	if(pfFifoIsEmpty(&can1RxFifo) == enBooleanTrue)
	{
		return enStatusError;
	}
	for(loop = 0; loop < CAN1_MESSAGE_SIZE; loop++)
	{
		*(msgPtr + loop) = pfFifoPop(&can1RxFifo);
	}
	return enStatusSuccess;
}

static PFEnStatus pfCan1PushTxMessage(PFpCanMessage msg)
{
	PFbyte loop;
	PFbyte* msgPtr;
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
	CHECK_NULL_PTR(msg);
#endif	// #if (PF_CAN1_DEBUG == 1)	

	msgPtr = (PFbyte*)msg;
	for(loop = 0; loop < CAN1_MESSAGE_SIZE; loop++)
	{
		pfFifoPush(&can1TxFifo, *(msgPtr + loop));
	}
	return enStatusSuccess;
}

static PFEnStatus pfCan1PopTxMessage(PFpCanMessage msg)
{
	PFbyte loop;
	PFbyte* msgPtr;
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
	CHECK_NULL_PTR(msg);
#endif	// #if (PF_CAN1_DEBUG == 1)	
	
	msgPtr = (PFbyte*)msg;
	if(pfFifoIsEmpty(&can1TxFifo) == enBooleanTrue)
	{
		return enStatusError;
	}
	for(loop = 0; loop < CAN1_MESSAGE_SIZE; loop++)
	{
		*(msgPtr + loop) = pfFifoPop(&can1TxFifo);
	}
	
	return enStatusSuccess;
}

PFEnStatus pfCan1GetRxBufferSize(PFdword* size)
{
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_CAN1_DEBUG == 1)    
    *size = (pfFifoLength(&can1RxFifo) / sizeof(PFCanMessage));
    return enStatusSuccess;
}

PFEnStatus pfCan1GetRxBufferCount(PFdword* count)
{
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
	CHECK_NULL_PTR(count);
#endif	// #if (PF_CAN1_DEBUG == 1)
	*count = (can1RxFifo.count/sizeof(PFCanMessage));
	return enStatusSuccess;
}

PFEnStatus pfCan1RxBufferFlush(void)
{
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
#endif	// #if (PF_CAN1_DEBUG == 1)    
	pfFifoFlush(&can1RxFifo);
	return enStatusSuccess;
}

PFEnStatus pfCan1GetTxBufferSize(PFdword* size)
{
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_CAN1_DEBUG == 1)    
    *size = (pfFifoLength(&can1TxFifo) / sizeof(PFCanMessage));
    return enStatusSuccess;
}

PFEnStatus pfCan1GetTxBufferCount(PFdword* count)
{
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
    CHECK_NULL_PTR(count);
#endif	// #if (PF_CAN1_DEBUG == 1)    
    *count = (can1TxFifo.count / sizeof(PFCanMessage));
    return enStatusSuccess;
}

PFEnStatus pfCan1TxBufferFlush(void)
{
#if (PF_CAN1_DEBUG == 1)
	CHECK_DEV_INIT(can1ChInit);
#endif	// #if (PF_CAN1_DEBUG == 1)    
	pfFifoFlush(&can1TxFifo);
	return enStatusSuccess;
}


static void pfCan1Buffer1TxDefaultCallback(void)
{
    PFCanMessage msg;
    pfCan1SetCommand(CAN_CMR_STB1, enBooleanFalse);
//    can1TxBuffFree[0] = enBooleanTrue;
	
    if(pfFifoIsEmpty(&can1TxFifo) == enBooleanFalse)
    {
		pfCan1PopTxMessage(&msg);
		pfCan1WriteMessage(&msg);
    }
}

static void pfCan1Buffer2TxDefaultCallback(void)
{
    PFCanMessage msg;
    pfCan1SetCommand(CAN_CMR_STB2, enBooleanFalse);
//    can1TxBuffFree[1] = enBooleanTrue;
	
    if(pfFifoIsEmpty(&can1TxFifo) == enBooleanFalse)
    {
		pfCan1PopTxMessage(&msg);
		pfCan1WriteMessage(&msg);
    }
}

static void pfCan1Buffer3TxDefaultCallback(void)
{
    PFCanMessage msg;
    pfCan1SetCommand(CAN_CMR_STB3, enBooleanFalse);
//    can1TxBuffFree[2] = enBooleanTrue;
	
    if(pfFifoIsEmpty(&can1TxFifo) == enBooleanFalse)
    {
		pfCan1PopTxMessage(&msg);
		pfCan1WriteMessage(&msg);
    }
}

static void pfCan1RxDefaultCallback(void)
{
    PFCanMessage msg;
    pfCan1ReadMessage(&msg);
    pfCan1PushRxMessage(&msg);
}
#endif	// #if(CAN1_USE_FIFO != 0)

#endif	// #if (PF_USE_CAN1 == 1)

