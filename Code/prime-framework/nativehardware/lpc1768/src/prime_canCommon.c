#include "prime_framework.h"
#if (PF_USE_CAN == 1)
#include "prime_utils.h"
#include "prime_canCommon.h"

static PFcallback canCallbackList[2][5];
static PFdword CAN_std_cnt = 0;
static PFdword CAN_ext_cnt = 0;
static PFdword intFlags;
static PFEnBoolean canChEnable[2] = {enBooleanFalse, enBooleanFalse};

PFEnStatus pfCanSetChannelStatus(PFbyte channel, PFEnBoolean status)
{
	if(channel > 2)
		return enStatusInvArgs;
	
	canChEnable[channel] = status;
	
	return enStatusSuccess;
}


void pfCanWriteAccFilter(PFbyte canController, PFdword msgId, PFEnCanFrameFormat frameFormat)  
{
    PFdword buf0, buf1;
    PFdword cnt1, cnt2, bound1;

	// Acceptance Filter Memory full
	if ((((CAN_std_cnt + 1) >> 1) + CAN_ext_cnt) >= 512)
		return;                                       // error: objects full

	// Setup Acceptance Filter Configuration 
    // Acceptance Filter Mode Register = Off
	PERIPH_CANAF->AFMR = 0x00000001;

	if (frameFormat == enCanFrameStandard)  	// Add mask for standard identifiers
	{              
		msgId |= (canController-1) << 13;                        // Add controller number
		msgId &= 0x0000F7FF;                            // Mask out 16-bits of ID

		// Move all remaining extended mask entries one place up                 
		// if new entry will increase standard ID filters list
		if ((CAN_std_cnt & 0x0001) == 0 && CAN_ext_cnt != 0) 
		{
			cnt1   = (CAN_std_cnt >> 1);
			bound1 = CAN_ext_cnt;
			buf0   = PERIPH_CANAF_RAM->mask[cnt1];
			while (bound1--)  
			{
				cnt1++;
				buf1 = PERIPH_CANAF_RAM->mask[cnt1];
				PERIPH_CANAF_RAM->mask[cnt1] = buf0;
				buf0 = buf1;
			}        
		}

		if(CAN_std_cnt == 0)  	        // For entering first  ID
		{
			PERIPH_CANAF_RAM->mask[0] = 0x0000FFFF | (msgId << 16);
		}  
		else if (CAN_std_cnt == 1) 		// For entering second ID
		{
			if ((PERIPH_CANAF_RAM->mask[0] >> 16) > msgId)
			{
				PERIPH_CANAF_RAM->mask[0] = (PERIPH_CANAF_RAM->mask[0] >> 16) | (msgId << 16);
			}
			else
			{
				PERIPH_CANAF_RAM->mask[0] = (PERIPH_CANAF_RAM->mask[0] & 0xFFFF0000) | msgId;
			}
		}  
		else  
		{
			// Find where to insert new ID
			cnt1 = 0;
			cnt2 = CAN_std_cnt;
			bound1 = (CAN_std_cnt - 1) >> 1;
			while (cnt1 <= bound1)  	     // Loop through standard existing IDs
			{
				if ((PERIPH_CANAF_RAM->mask[cnt1] >> 16) > msgId)  
				{
					cnt2 = cnt1 * 2;
					break;
				}
				if ((PERIPH_CANAF_RAM->mask[cnt1] & 0x0000FFFF) > msgId)  
				{
					cnt2 = cnt1 * 2 + 1;
					break;
				}
				cnt1++;               	// cnt1 = U32 where to insert new ID
			}                           // cnt2 = U16 where to insert new ID

			if (cnt1 > bound1)  		// Adding ID as last entry
			{                      
				if ((CAN_std_cnt & 0x0001) == 0)         // Even number of IDs exists
				{
					PERIPH_CANAF_RAM->mask[cnt1]  = 0x0000FFFF | (msgId << 16);
				}
				else                                     // Odd  number of IDs exists
				{
					PERIPH_CANAF_RAM->mask[cnt1]  = (PERIPH_CANAF_RAM->mask[cnt1] & 0xFFFF0000) | msgId;
				}
			}  
			else  
			{
				buf0 = PERIPH_CANAF_RAM->mask[cnt1];		// Remember current entry
				if ((cnt2 & 0x0001) == 0)               	// Insert new mask to even address
				{
					buf1 = (msgId << 16) | (buf0 >> 16);
				}
				else                                     	// Insert new mask to odd  address
				{
					buf1 = (buf0 & 0xFFFF0000) | msgId;
				}
				PERIPH_CANAF_RAM->mask[cnt1] = buf1;        // Insert mask
				bound1 = CAN_std_cnt >> 1;
				// Move all remaining standard mask entries one place up
				while (cnt1 < bound1)  
				{
					cnt1++;
					buf1  = PERIPH_CANAF_RAM->mask[cnt1];
					PERIPH_CANAF_RAM->mask[cnt1] = (buf1 >> 16) | (buf0 << 16);
					buf0  = buf1;
				}
				if ((CAN_std_cnt & 0x0001) == 0)         	// Even number of IDs exists
				{
					PERIPH_CANAF_RAM->mask[cnt1] = (PERIPH_CANAF_RAM->mask[cnt1] & 0xFFFF0000) | (0x0000FFFF);
				}
			}
		}
		CAN_std_cnt++;
	}  
	else										// Add mask for extended identifiers  
	{   
		msgId |= (canController-1) << 29;               	// Add controller number
		cnt1 = ((CAN_std_cnt + 1) >> 1);
		cnt2 = 0;
		while (cnt2 < CAN_ext_cnt)  			//Loop through extended existing masks
		{
			if (PERIPH_CANAF_RAM->mask[cnt1] > msgId)
			{
				break;
			}
			cnt1++;                                    	// cnt1 = U32 where to insert new mask
			cnt2++;
		}

		buf0 = PERIPH_CANAF_RAM->mask[cnt1];            // Remember current entry 
		PERIPH_CANAF_RAM->mask[cnt1] = msgId;              // Insert mask 

		CAN_ext_cnt++;

		bound1 = CAN_ext_cnt - 1;
		// Move all remaining extended mask entries one place up
		while (cnt2 < bound1)  
		{
			cnt1++;
			cnt2++;
			buf1 = PERIPH_CANAF_RAM->mask[cnt1];
			PERIPH_CANAF_RAM->mask[cnt1] = buf0;
			buf0 = buf1;
		}        
	}
  
	// Calculate std ID start address (buf0) and ext ID start address (buf1)
	buf0 = ((CAN_std_cnt + 1) >> 1) << 2;
	buf1 = buf0 + (CAN_ext_cnt << 2);

	// Setup acceptance filter pointers 
	PERIPH_CANAF->SFF_sa     = 0;
	PERIPH_CANAF->SFF_GRP_sa = buf0;
	PERIPH_CANAF->EFF_sa     = buf0;
	PERIPH_CANAF->EFF_GRP_sa = buf1;
	PERIPH_CANAF->ENDofTable = buf1;

	PERIPH_CANAF->AFMR = 0x00000000;                  /* Use acceptance filter */
}

PFEnStatus pfCanSetCallback(PFbyte canController, PFEnCanCallbackType callbackType, PFcallback callback)
{
#if (PF_CAN_DEBUG == 1)
    CHECK_NULL_PTR(callback);
#endif	// #if (PF_CAN_DEBUG == 1)
    canCallbackList[canController-1][callbackType] = callback;
    
    return enStatusSuccess;
}



PFEnStatus pfCanRemoveCallback(PFbyte canController, PFEnCanCallbackType callbackType)
{
    canCallbackList[canController-1][callbackType] = 0;
    return enStatusSuccess;
}

void CAN_IRQHandler(void)
{
#ifdef PF_USE_CAN1   
	if(canChEnable[0] == enBooleanTrue)
	{
		intFlags = PERIPH_CAN1->ICR;
		
		// Check CAN1 RX flag
		if((intFlags & CAN_ICR_RI) != 0)
		{
							if(canCallbackList[0][enCanRxCallback] != 0)
							{
									canCallbackList[0][enCanRxCallback]();
							}
		}
		
		// Check CAN1 TX flags
		if((intFlags & CAN_ICR_TI1) != 0)
		{
							if(canCallbackList[0][enCanBuff1TxCallback] != 0)
							{
									canCallbackList[0][enCanBuff1TxCallback]();
							}
		}
		if((intFlags & CAN_ICR_TI2) != 0)
		{
							if(canCallbackList[0][enCanBuff2TxCallback] != 0)
							{
									canCallbackList[0][enCanBuff2TxCallback]();
							}
		}
		if((intFlags & CAN_ICR_TI3) != 0)
		{
							if(canCallbackList[0][enCanBuff3TxCallback] != 0)
							{
									canCallbackList[0][enCanBuff3TxCallback]();
							}
		}
	}	//if(canChEnable[0] == enBooleanTrue)
#endif  // PF_USE_CAN1
#ifdef PF_USE_CAN2    	
	if(canChEnable[1] == enBooleanTrue)    
	{
		intFlags = PERIPH_CAN2->ICR;
		
		// Check CAN2 RX flag
		if((intFlags & CAN_ICR_RI) != 0)
		{
							if(canCallbackList[1][enCanRxCallback] != 0)
							{
									canCallbackList[1][enCanRxCallback]();
							}
		}
		
		// Check CAN2 TX flags
		if((intFlags & CAN_ICR_TI1) != 0)
		{
							if(canCallbackList[1][enCanBuff1TxCallback] != 0)
							{
									canCallbackList[1][enCanBuff1TxCallback]();
							}
		}
		if((intFlags & CAN_ICR_TI2) != 0)
		{
							if(canCallbackList[1][enCanBuff2TxCallback] != 0)
							{
									canCallbackList[1][enCanBuff2TxCallback]();
							}
		}
		if((intFlags & CAN_ICR_TI3) != 0)
		{
							if(canCallbackList[1][enCanBuff3TxCallback] != 0)
							{
									canCallbackList[1][enCanBuff3TxCallback]();
							}
		}
	}	//  if(canChEnable[1] == enBooleanTrue) 
#endif  // PF_USE_CAN2 	
}

#endif	// #if (PF_USE_CAN == 1)

