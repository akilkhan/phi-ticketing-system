#include "prime_framework.h"

#if (PF_USE_RTC == 1)

#include "prime_utils.h"
#include "prime_rtc.h"

static PFEnBoolean rtcIntStatus = enBooleanFalse;
static PFEnBoolean rtcChInit = enBooleanFalse;
static PFcallback rtcCallback = 0;

PFEnStatus pfRtcOpen(PFpCfgRtc config)
{
#if (PF_RTC_DEBUG == 1)	
	CHECK_NULL_PTR(config);
	if((config->intEnable == enBooleanTrue) && (config->callback == PF_NULL))
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_RTC_DEBUG == 1)		
	
	POWER_ON(RTC_CH);
	
	if ( RTC_CHANNEL->RTC_AUX & (BIT_MASK_4) )
	{
		RTC_CHANNEL->RTC_AUX |= (BIT_MASK_4);	
	}
	
	RTC_CHANNEL->AMR = 0;
	RTC_CHANNEL->CIIR = 0;
	RTC_CHANNEL->CCR = 0;
	
	rtcCallback = config->callback;
	
	RTC_CHANNEL->CIIR = config->countInt;
	if(config->alarmIntEnable == enBooleanTrue)
	{
		RTC_CHANNEL->AMR = 0x0F;
	}
	
	if(config->intEnable)
	{
		NVIC_EnableIRQ(IRQ_NUM(RTC_CH));	 //Enable RTC Interrupt
	}
	
	rtcIntStatus = enBooleanFalse;
	rtcChInit = enBooleanTrue;
	
	return enStatusSuccess;
}

void pfRtcClose(void)
{
	rtcChInit = enBooleanFalse;
	NVIC_DisableIRQ(IRQ_NUM(RTC_CH));
	POWER_OFF(RTC_CH);
}

PFEnStatus pfRtcSetTime(PFpRtcTime time)
{
#if (PF_RTC_DEBUG == 1)	
	CHECK_NULL_PTR(time);
	if(rtcChInit == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif	// #if (PF_RTC_DEBUG == 1)	
	RTC_CHANNEL->SEC = time->rtcSec;
	RTC_CHANNEL->MIN = time->rtcMin;
	RTC_CHANNEL->HOUR = time->rtcHour;
	RTC_CHANNEL->DOM = time->rtcDom;
	RTC_CHANNEL->DOW = time->rtcDow;
	RTC_CHANNEL->DOY = time->rtcDoy;
	RTC_CHANNEL->MONTH = time->rtcMon;
	RTC_CHANNEL->YEAR = time->rtcYear;
	
	return enStatusSuccess;
}

PFEnStatus pfRtcGetTime (PFpRtcTime time)
{
#if (PF_RTC_DEBUG == 1)	
	CHECK_NULL_PTR(time);
	if(rtcChInit == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif	// #if (PF_RTC_DEBUG == 1)		
	time->rtcSec = RTC_CHANNEL->SEC;
	time->rtcMin = RTC_CHANNEL->MIN;
	time->rtcHour = RTC_CHANNEL->HOUR;
	time->rtcDom = RTC_CHANNEL->DOM;
	time->rtcDow = RTC_CHANNEL->DOW;
	time->rtcDoy = RTC_CHANNEL->DOY;
	time->rtcMon= RTC_CHANNEL->MONTH;
	time->rtcYear = RTC_CHANNEL->YEAR;
	
	return enStatusSuccess;
}

PFEnStatus pfRtcSetAlarm(PFpRtcTime alarm)
{
#if (PF_RTC_DEBUG == 1)	
	CHECK_NULL_PTR(alarm);
	if(rtcChInit == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif	// #if (PF_RTC_DEBUG == 1)		
	
	RTC_CHANNEL->ALSEC = alarm->rtcSec;
	RTC_CHANNEL->ALMIN = alarm->rtcMin;
	RTC_CHANNEL->ALHOUR = alarm->rtcHour;
	RTC_CHANNEL->ALDOM = alarm->rtcDom;
	RTC_CHANNEL->ALDOW = alarm->rtcDow;
	RTC_CHANNEL->ALDOY = alarm->rtcDoy;
	RTC_CHANNEL->ALMON = alarm->rtcMon;
	RTC_CHANNEL->ALYEAR = alarm->rtcYear;
	
	return enStatusSuccess;
}
	
PFEnStatus pfRtcSetAlarmMask(PFdword rtcAlarmMask)
{
#if (PF_RTC_DEBUG == 1)	
	if(rtcChInit == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif	// #if (PF_RTC_DEBUG == 1)		
	
	RTC_CHANNEL->AMR = ~rtcAlarmMask;
	
	return enStatusSuccess;
}

PFEnStatus pfRtcIntSelect(PFdword rtcInterrupt)
{
#if (PF_RTC_DEBUG == 1)	
	if(rtcChInit == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif	// #if (PF_RTC_DEBUG == 1)		
	
	RTC_CHANNEL->CIIR = rtcInterrupt;
	
	return enStatusSuccess;
}

PFEnStatus pfRtcStart(void)
{
#if (PF_RTC_DEBUG == 1)	
	if(rtcChInit == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif	// #if (PF_RTC_DEBUG == 1)	
	
	RTC_CHANNEL->CCR |= 0x01;	//Enable Clock
	RTC_CHANNEL->ILR |= 0x01; //Clear Interrupt
	
	return enStatusSuccess;
}

PFEnStatus pfRtcStop(void)
{
#if (PF_RTC_DEBUG == 1)	
	if(rtcChInit == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif	// #if (PF_RTC_DEBUG == 1)	
	
	RTC_CHANNEL->CCR &= ~(0x01);
	
	return enStatusSuccess;
}

PFEnStatus pfRtcResetCTC(void)
{
#if (PF_RTC_DEBUG == 1)	
	if(rtcChInit == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif	// #if (PF_RTC_DEBUG == 1)	
	
	RTC_CHANNEL->CCR |= 0x02;
	RTC_CHANNEL->CCR &= ~0x02;
	
	return enStatusSuccess;
}

PFEnStatus pfRtcSetIRQCallback(PFcallback callback)
{
#if (PF_RTC_DEBUG == 1)	
	CHECK_NULL_PTR(callback);
	if(rtcChInit == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif	// #if (PF_RTC_DEBUG == 1)		
	rtcCallback = callback;
	
	return enStatusSuccess;	
}

void RTC_INT_HANDLER (void) 
{ 
	RTC_CHANNEL->ILR |= 0x01; //Clear Interrupt	
	rtcIntStatus = enBooleanTrue;
	if(rtcCallback != 0)
	{
		rtcCallback();
	}
}

#endif	// #if (PF_USE_RTC == 1)
