#include "prime_framework.h"

#if (PF_USE_I2C0 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_i2c0.h"

#if(I2C0_USE_FIFO != 0)
#include "prime_fifo.h"
	#warning I2C0 FIFO is enabled for interrupt based communication
	#if( (I2C0_BUFFER_SIZE == 0) || ((I2C0_BUFFER_SIZE & (I2C0_BUFFER_SIZE - 1)) != 0) )
		#error I2C0_BUFFER_SIZE cannot be zero. I2C0_BUFFER_SIZE should be power of 2
	#endif
#endif	// #if(I2C0_USE_FIFO != 0)

// I2C0 control bits	
#define I2C0_AA_FLAG				0x04			
#define I2C0_INT_FLAG				0x08
#define I2C0_STOP_FLAG				0x10
#define I2C0_START_FLAG				0x20
#define I2C0_EN_FLAG				0x40	

#define I2C0_SET_CTRL_BIT(BIT)		I2C0_CHANNEL->I2CONSET = BIT
#define I2C0_CLR_CTRL_BIT(BIT)		I2C0_CHANNEL->I2CONCLR = BIT
#define I2C0_GET_STATE()				(I2C0_CHANNEL->I2STAT >> 3)	
		
#define I2C0_SET_AA()				I2C0_SET_CTRL_BIT(I2C0_AA_FLAG)
#define I2C0_CLR_AA()				I2C0_CLR_CTRL_BIT(I2C0_AA_FLAG)
	
#define I2C0_SET_START()				I2C0_SET_CTRL_BIT(I2C0_START_FLAG)
#define I2C0_CLR_START()				I2C0_CLR_CTRL_BIT(I2C0_START_FLAG)

#define I2C0_SET_STOP()				I2C0_SET_CTRL_BIT(I2C0_STOP_FLAG)
#define I2C0_CLR_STOP()				I2C0_CLR_CTRL_BIT(I2C0_STOP_FLAG)	

#define I2C0_CLR_INT()				I2C0_CLR_CTRL_BIT(I2C0_INT_FLAG)
#define I2C0_CALL_SLAVE(ADDR, RW)	I2C0_CHANNEL->I2DAT = ADDR | RW


// State codes for I2C0 communication
// Master mode
#define I2C0_M_START					0x08
#define I2C0_M_REP_START				0x10
#define I2C0_M_ARB_LOST				0x38
// Master transmitter
#define I2C0_M_SLA_W_TX_ACK			0x18
#define I2C0_M_SLA_W_TX_NACK			0x20
#define I2C0_M_DATA_TX_ACK			0x28
#define I2C0_M_DATA_TX_NACK			0x30
// Master receiver
#define I2C0_M_SLA_R_ACK				0x40
#define I2C0_M_SLA_R_NACK			0x48
#define I2C0_M_DATA_RX_ACK			0x50
#define I2C0_M_DATA_RX_NACK			0x58

// Slave receiver
#define I2C0_S_SLA_W_RX_ACK			0x60
#define I2C0_S_ARB_LOST_SLA_W_RX		0x68
#define I2C0_S_GEN_CALL_RX_ACK		0x70								
#define	I2C0_S_ARB_LOST_GC_RX		0x78
#define I2C0_S_SLA_W_DATA_RX_ACK		0x80
#define I2C0_S_SLA_W_DATA_RX_NACK	0x88
#define I2C0_S_GC_DATA_RX_ACK		0x90
#define I2C0_S_GC_DATA_RX_NACK		0x98
#define I2C0_S_STOP_REP_START		0xA0
// Slave trasmiter
#define I2C0_S_SLA_R_RX_ACK			0xA8
#define I2C0_S_ARB_LOST_SLA_R_RX		0xB0
#define I2C0_S_DATA_TX_ACK			0xB8
#define I2C0_S_DATA_TX_NACK			0xC0
#define I2C0_S_LAST_DATA_TX_NACK		0xC8

static PFEnBoolean i2c0ChError = enBooleanFalse;		// transmission error flag
static PFbyte i2c0ChRWFlag = 0;						// read/write flag
static PFbyte i2c0SlaveAddr = 0;						// slave addr to call
static PFdword i2c0BytesToRead = 0;					// bytes to read from slave
static PFdword i2c0BytesRead = 0;					// bytes actually read from I2C0 bus
static PFEnBoolean i2c0ChInit = enBooleanFalse;		// initalize flag for the channel
static PFEnBoolean i2c0ChBusy = enBooleanFalse;		// busy flag for the channel
static PFbyte i2c0ChInt;								// interrupt flag for the channel
#if(I2C0_USE_FIFO != 0)
static PFbyte i2c0RxBuffer[I2C0_BUFFER_SIZE];			// I2C0 transmit buffer
static PFbyte i2c0TxBuffer[I2C0_BUFFER_SIZE];			// I2C0 receive buffer
static PFFifo i2c0TxFifo;							// I2C0 transmit fifo structure
static PFFifo i2c0RxFifo;							// I2C0 receive fifo structure
#else	
static PFcallback i2c0Callback = 0;					// transmit callback for the channel
#endif	// #if(I2C0_USE_FIFO != 0)
PFCfgI2c0 i2c0Cfg;	
	

PFEnStatus pfI2c0Open(PFpCfgI2c0 config)
{
	PFdword tHigh = 0, tLow = 0, tTot = 0;
#if (PF_I2C0_DEBUG == 1)
	// Validate config pointer
	CHECK_NULL_PTR(config);
	if( (config->clkDiv > 3) || (config->baudrate > I2C0_MAX_BAUDRATE) )
	{
		return enStatusInvArgs;
	}
#if(I2C0_USE_FIFO == 0)
	CHECK_NULL_PTR(config->callback);
#endif	
#endif	// #if (PF_I2C0_DEBUG == 1)	
	
	// power on I2C0 module
	POWER_ON(I2C0_CH);
	// set peripheral clock
	pfSysSetPclkDiv(PCLK_DIV(I2C0_CH), config->clkDiv);
	
	// set baudrate and duty cycle
	tTot = pfSysGetPclk(PCLK_DIV(I2C0_CH)) / config->baudrate;
	tHigh = (tTot * config->dutyCycle) / 100;
	tLow = tTot - tHigh;
	I2C0_CHANNEL->I2SCLH = tHigh;
	I2C0_CHANNEL->I2SCLL = tLow;
	
	// set own I2C0 addresses
	if(config->enableGenCall == enBooleanTrue)
	{
		config->ownAddress[0] |= 0x01;
	}
	I2C0_CHANNEL->I2ADR0 = config->ownAddress[0];
	I2C0_CHANNEL->I2ADR1 = config->ownAddress[1];
	I2C0_CHANNEL->I2ADR2 = config->ownAddress[2];
	I2C0_CHANNEL->I2ADR3 = config->ownAddress[3];
	
	// set address masks
	I2C0_CHANNEL->I2MASK0 = config->addrMask[0];
	I2C0_CHANNEL->I2MASK1 = config->addrMask[1];
	I2C0_CHANNEL->I2MASK2 = config->addrMask[2];
	I2C0_CHANNEL->I2MASK3 = config->addrMask[3];
	
	// set ack enable
	if(config->enableAck)
	{
		I2C0_SET_AA();
	}
	
	// initialize FIFO
#if(I2C0_USE_FIFO != 0)
	pfFifoInit(&i2c0TxFifo, i2c0TxBuffer, I2C0_BUFFER_SIZE);
	pfFifoInit(&i2c0RxFifo, i2c0RxBuffer, I2C0_BUFFER_SIZE);
#else
	i2c0Callback = config->callback;
#endif	// #if(I2C0_USE_FIFO != 0)

	if(config->intEnable)
	{
		i2c0ChInt = enBooleanTrue;
		NVIC_EnableIRQ(IRQ_NUM(I2C0_CH));
	}
	
	// enable I2C0 interface
	I2C0_SET_CTRL_BIT(I2C0_EN_FLAG);
	
	pfMemCopy(&i2c0Cfg, config, sizeof(PFCfgI2c0));
	i2c0ChInit = enBooleanTrue;
	return enStatusSuccess;
}	

PFEnStatus pfI2c0Close(void)
{
#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);
#endif	// #if (PF_I2C0_DEBUG == 1)	
	I2C0_CLR_CTRL_BIT(I2C0_EN_FLAG);
	POWER_OFF(I2C0_CH);
	i2c0ChInit = enBooleanFalse;
	return enStatusSuccess;
}

PFEnStatus pfI2c0IntEnable(void)
{
#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);
#endif	// #if (PF_I2C0_DEBUG == 1)	
	i2c0ChInt = enBooleanTrue;
	NVIC_EnableIRQ(IRQ_NUM(I2C0_CH));
	return enStatusSuccess;
}

PFEnStatus pfI2c0IntDisable(void)
{
#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);
#endif	// #if (PF_I2C0_DEBUG == 1)	
	NVIC_DisableIRQ(IRQ_NUM(I2C0_CH));
	i2c0ChInt = enBooleanFalse;
	return enStatusSuccess;
}

PFEnStatus pfI2c0SetControlFlag(PFbyte flag)
{
#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);
#endif	// #if (PF_I2C0_DEBUG == 1)	
	I2C0_SET_CTRL_BIT(flag);
	return enStatusSuccess;
}

PFEnStatus pfI2c0ClearControlFlag(PFbyte flag)
{
#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);
#endif	// #if (PF_I2C0_DEBUG == 1)	
	I2C0_CLR_CTRL_BIT(flag);
	return enStatusSuccess;
}

PFEnStatus pfI2c0GetState(PFbyte* state)
{
#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);
	CHECK_NULL_PTR(state);
#endif	// #if (PF_I2C0_DEBUG == 1)	
	*state = I2C0_GET_STATE();
	return enStatusSuccess;
}


PFEnStatus pfI2c0Start( void )
{

#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);

	if(i2c0ChBusy == enBooleanTrue)
		return enStatusBusy;	
#endif	// #if (PF_I2C0_DEBUG == 1)	
	
	I2C0_SET_START();
	return enStatusSuccess ;
}
PFEnStatus pfI2c0Stop( void )
{

#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);
#endif	// #if (PF_I2C0_DEBUG == 1)

	I2C0_SET_STOP();
	I2C0_CLR_INT();
	return enStatusSuccess ;
}



static PFEnStatus pfDataProcessing(PFbyte* data, PFword size)
{
	volatile PFbyte  status;
	PFword index=0;
	 
	while(1)
	{
		
		while(!(I2C0_CHANNEL->I2CONSET & I2C0_INT_FLAG));
		status = I2C0_CHANNEL->I2STAT;
		switch(status)
		{
//
// Master mode
//
				case I2C0_M_START:
				case I2C0_M_REP_START:
					I2C0_CALL_SLAVE(i2c0SlaveAddr, i2c0ChRWFlag);
					I2C0_CLR_START();
					break;
//
// Master transmitter
//
				case I2C0_M_SLA_W_TX_ACK:
				case I2C0_M_DATA_TX_ACK:
					if( index < size )
					{
							I2C0_CHANNEL->I2DAT = *(data + index);
							index++;
					}
					else
					{
						i2c0ChBusy = enBooleanFalse;
						return enStatusSuccess ;
					}										
					break;

//
// Master transmitter error conditions
//
				case I2C0_M_ARB_LOST:
				case I2C0_M_SLA_W_TX_NACK:
				case I2C0_M_DATA_TX_NACK:
					i2c0ChBusy = enBooleanFalse;
					I2C0_SET_STOP();
					return enStatusError ;
					//break;
//
// Master receiver
//
				case I2C0_M_SLA_R_NACK:
					i2c0ChBusy = enBooleanFalse;
					return enStatusError ;
					//break;
			
				case I2C0_M_SLA_R_ACK:
					// do nothing
					break;

				case I2C0_M_DATA_RX_ACK:
					*(data + index) = I2C0_CHANNEL->I2DAT;
					index++;
					i2c0BytesRead++;

					if(i2c0BytesRead >= i2c0BytesToRead -1)
					{
						I2C0_CLR_AA();
						return enStatusSuccess ;
					}
					break;


				case I2C0_M_DATA_RX_NACK:
					*(data + index) = I2C0_CHANNEL->I2DAT;
					i2c0BytesRead++;
					index++;
					i2c0ChBusy = enBooleanFalse;
					return enStatusSuccess ;
					//break;

//
// Slave receiver
//
				case I2C0_S_SLA_W_RX_ACK:
				case I2C0_S_ARB_LOST_SLA_W_RX:
				case I2C0_S_GEN_CALL_RX_ACK:
				case I2C0_S_ARB_LOST_GC_RX:
				case I2C0_S_STOP_REP_START:
					break;

				case I2C0_S_SLA_W_DATA_RX_ACK:
				case I2C0_S_GC_DATA_RX_ACK:
				case I2C0_S_SLA_W_DATA_RX_NACK:
				case I2C0_S_GC_DATA_RX_NACK:
					*(data + index) = I2C0_CHANNEL->I2DAT;
					index++;
					break;
			
//
// Slave transmitter
//
				case I2C0_S_SLA_R_RX_ACK:
				case I2C0_S_ARB_LOST_SLA_R_RX:
				case I2C0_S_DATA_TX_ACK:
					if( index < size )
					{
							I2C0_CHANNEL->I2DAT = *(data + index);
							index++;
					}
					else
					{
						I2C0_CHANNEL->I2DAT = 0;
						return enStatusSuccess ;
					}
					break;

				case I2C0_S_DATA_TX_NACK:
				case I2C0_S_LAST_DATA_TX_NACK:
					i2c0ChBusy = enBooleanFalse;
					return enStatusSuccess ;
					//break;

				default:
					i2c0ChBusy = enBooleanFalse;
					return enStatusError;							
					//	break;
			}
			
		I2C0_CLR_INT();
	}
}



PFEnStatus pfI2c0Write(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFdword size)
{
	PFdword index;
	PFEnStatus status;
	
#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);
	CHECK_NULL_PTR(data);
	if(size == 0)
	{
			return enStatusInvArgs;
	}
	if(i2c0ChBusy == enBooleanTrue)
		return enStatusBusy;	
#endif	// #if (PF_I2C0_DEBUG == 1)	

	i2c0ChBusy = enBooleanTrue;
	i2c0SlaveAddr = slaveAddr;
	i2c0ChRWFlag = 0;	
	
	// USING INTERRUPT ======================================================================================
	if( i2c0ChInt != enBooleanFalse)           
	{	
#if(I2C0_USE_FIFO != 0)
			
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&i2c0TxFifo, *(data + index));
		}
		if(master == enBooleanTrue)
		{
			I2C0_SET_START();
		}
		else
		{
			I2C0_SET_AA();
		}
		while(i2c0ChBusy == enBooleanTrue);
		
		if(i2c0ChError == enBooleanTrue)
		{
			i2c0ChError = enBooleanFalse;
			return enStatusError;
		}
#else
		return enStatusNotSupported;
#endif	// #if(I2C0_USE_FIFO != 0)	
	}
	
	// USING POLLING ======================================================================================
	else
	{
		status = pfDataProcessing(data,size);	
		if(status != enStatusSuccess)
				return status;
		
	}
	return enStatusSuccess;
}

PFEnStatus pfI2c0Read(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFdword size, PFdword* readBytes)
{
	PFdword index = 0;
	PFEnStatus status;
	
#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);
	CHECK_NULL_PTR(data);
	CHECK_NULL_PTR(readBytes);
	if(size == 0)
	{
		return enStatusInvArgs;
	}
	if(i2c0ChBusy == enBooleanTrue)
		return enStatusBusy;	
#endif	// #if (PF_I2C0_DEBUG == 1)	

	
	i2c0ChBusy = enBooleanTrue;
	i2c0SlaveAddr = slaveAddr;
	i2c0ChRWFlag = 1;		
	i2c0BytesToRead = size;
	
	// USING INTERRUPT ======================================================================================
	if( i2c0ChInt != enBooleanFalse)           
	{	
#if(I2C0_USE_FIFO != 0)
			if(master != enBooleanTrue)
			{
				for(index = 0; index < size; index++)
				{
					if(pfFifoIsEmpty(&i2c0RxFifo) == enBooleanTrue)
						break;
					*(data + index) = pfFifoPop(&i2c0RxFifo);
				}
				*readBytes = index;
				i2c0ChBusy == enBooleanFalse;
				return enStatusSuccess;
			}
			
			if(size > 1)
			{
				I2C0_SET_AA();
			}
			else
			{
				I2C0_CLR_AA();
			}
			I2C0_SET_START();
			
			while(i2c0ChBusy == enBooleanTrue);
		
			I2C0_CLR_AA();
			
			for(index = 0; index < i2c0BytesRead; index++)
			{
				*(data + index) = pfFifoPop(&i2c0RxFifo);
			}
			*readBytes = i2c0BytesRead;
			i2c0BytesRead = 0;
			
			if(i2c0ChError == enBooleanTrue)
			{
				i2c0ChError = enBooleanFalse;
				return enStatusError;
			}
#endif	// #if(I2C0_USE_FIFO != 0)	

	}
	
	// USING POLLING ======================================================================================
	else
	{
		status = pfDataProcessing(data,size);	
		if(status != enStatusSuccess)
				return status;
		
		I2C0_CLR_AA();		
		*readBytes = i2c0BytesRead;
		i2c0BytesRead = 0;
	}			
	return enStatusSuccess;
}

#if(I2C0_USE_FIFO != 0)

PFEnStatus pfI2c0GetRxBufferCount(PFdword* count)
{
#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);
	CHECK_NULL_PTR(count);
#endif	// #if (PF_I2C0_DEBUG == 1)	
	*count = i2c0RxFifo.count;
	return enStatusSuccess;
}

PFEnStatus pfI2c0TxBufferFlush(void)
{
#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);
#endif	// #if (PF_I2C0_DEBUG == 1)	
	pfFifoFlush(&i2c0TxFifo);
	return enStatusSuccess;
}

PFEnStatus pfI2c0RxBufferFlush(void)
{
#if (PF_I2C0_DEBUG == 1)
	CHECK_DEV_INIT(i2c0ChInit);
#endif	// #if (PF_I2C0_DEBUG == 1)	
	pfFifoFlush(&i2c0RxFifo);
	return enStatusSuccess;
}
#endif	// #if(I2C0_USE_FIFO != 0)	

void I2C0_INT_HANDLER(void)
{
#if(I2C0_USE_FIFO != 0)
	PFbyte status = I2C0_CHANNEL->I2STAT;
	switch(status)
	{
		//
		// Master mode
		//
		case I2C0_M_START:
		case I2C0_M_REP_START:
			I2C0_CALL_SLAVE(i2c0SlaveAddr, i2c0ChRWFlag);
			I2C0_CLR_START();
			break;
		//
		// Master transmitter
		//
		case I2C0_M_SLA_W_TX_ACK:
		case I2C0_M_DATA_TX_ACK:
			if(pfFifoIsEmpty(&i2c0TxFifo) != enBooleanTrue)
			{
				I2C0_CHANNEL->I2DAT = pfFifoPop(&i2c0TxFifo);
			}
			else
			{
				i2c0ChError= enBooleanFalse;
				i2c0ChBusy = enBooleanFalse;
				I2C0_SET_STOP();
			}
			break;
		
		//
		// Master transmitter error conditions
		//
		case I2C0_M_ARB_LOST:
		case I2C0_M_SLA_W_TX_NACK:
		case I2C0_M_DATA_TX_NACK:
			i2c0ChError= enBooleanTrue;
			i2c0ChBusy = enBooleanFalse;
			I2C0_SET_STOP();
			break;
		
		//
		// Master reciever
		//
		case I2C0_M_SLA_R_NACK:
			i2c0ChError= enBooleanTrue;
			i2c0ChBusy = enBooleanFalse;
			I2C0_SET_STOP();
			break;
		
		case I2C0_M_SLA_R_ACK:
			// do nothing
			break;

		case I2C0_M_DATA_RX_ACK:
			pfFifoPush(&i2c0RxFifo, I2C0_CHANNEL->I2DAT);
			i2c0BytesRead++;
			if(i2c0BytesRead >= i2c0BytesToRead -1)
			{
				I2C0_CLR_AA();
			}
			break;

		case I2C0_M_DATA_RX_NACK:
			pfFifoPush(&i2c0RxFifo, I2C0_CHANNEL->I2DAT);
			i2c0BytesRead++;
			i2c0ChError= enBooleanFalse;
			i2c0ChBusy = enBooleanFalse;
			I2C0_SET_STOP();
			break;
		
		//
		// Slave receiver
		//
		case I2C0_S_SLA_W_RX_ACK:
		case I2C0_S_ARB_LOST_SLA_W_RX:
		case I2C0_S_GEN_CALL_RX_ACK:
		case I2C0_S_ARB_LOST_GC_RX:
		case I2C0_S_STOP_REP_START:
			break;

		case I2C0_S_SLA_W_DATA_RX_ACK:
		case I2C0_S_GC_DATA_RX_ACK:
		case I2C0_S_SLA_W_DATA_RX_NACK:
		case I2C0_S_GC_DATA_RX_NACK:
			pfFifoPush(&i2c0RxFifo, I2C0_CHANNEL->I2DAT);
			break;
		
		//
		// Slave trasmitter
		//
		case I2C0_S_SLA_R_RX_ACK:
		case I2C0_S_ARB_LOST_SLA_R_RX:
		case I2C0_S_DATA_TX_ACK:
			if(pfFifoIsEmpty(&i2c0TxFifo) != enBooleanTrue)
			{
				I2C0_CHANNEL->I2DAT = pfFifoPop(&i2c0TxFifo);
			}
			else
			{
				I2C0_CHANNEL->I2DAT = 0;
			}
			break;

		case I2C0_S_DATA_TX_NACK:
		case I2C0_S_LAST_DATA_TX_NACK:
			i2c0ChError= enBooleanFalse;
			i2c0ChBusy = enBooleanFalse;
			break;

		default:
			i2c0ChError= enBooleanTrue;
			i2c0ChBusy = enBooleanFalse;
			break;
	}

	I2C0_CLR_INT();
#else
	if(i2c0Callback != 0)
	{
		i2c0Callback();
	}
#endif	// #if(I2C0_USE_FIFO != 0)
}

#endif	// #if (PF_USE_I2C0 == 1)
