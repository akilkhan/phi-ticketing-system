#include "prime_types.h"
#include "prime_tick.h"

static PFdword timeTick = 0;
static PFdword timerPeriodMs = 0;

void pfTickSetTimerPeriod(PFdword timerPeriod)
{
	timerPeriodMs = timerPeriod;
}

void pfTickUpdate(void)
{
	timeTick++;
}

void pfTickReset(void)
{
	timeTick = 0;
}

PFdword pfTickSetTimeoutMs(PFdword time)
{
#if (PF_UTILS_DEBUG == 1)
	if(timerPeriodMs == 0)
	{
		return 0;
	}
#endif	// #if (PF_UTILS_DEBUG == 1)
	if(time < timerPeriodMs)
	{
		time = timerPeriodMs;
	}
	time /= timerPeriodMs;
	
	return(timeTick + time);
}

PFEnBoolean pfTickCheckTimeout(PFdword timeoutTick)
{
#if (PF_UTILS_DEBUG == 1)
	if(timerPeriodMs == 0)
	{
		return enBooleanTrue;
	}
#endif	// #if (PF_UTILS_DEBUG == 1)
	if(timeoutTick > timeTick)
	{
		return enBooleanFalse;
	}
	else
	{
		return enBooleanTrue;
	}
}

void pfTickDelayMs(PFdword delayMs)
{
	PFdword delayTimeout;
#if (PF_UTILS_DEBUG == 1)
	if(timerPeriodMs == 0)
	{
		return;
	}
#endif	// #if (PF_UTILS_DEBUG == 1)
	delayTimeout = pfTickSetTimeoutMs(delayMs);
	while(pfTickCheckTimeout(delayTimeout) == enBooleanFalse);
}

