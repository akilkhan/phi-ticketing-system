#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_string.h"

static PFbyte tokString[256];
static PFword is,index,len;
PFbyte* pfStringTokenize(PFbyte* str, PFbyte* token)
{
    PFword index2 = 0, loop = 0, temp;
    PFbyte result = 0;
    if((token == PF_NULL_PTR))
    {
        return 0;
    }
		index++;
		is = index;
		if(str != PF_NULL_PTR)
		{
			len = 0;
			is = 0;
			index = 0;
			while(str[len] != 0)
			{
				tokString[len] = str[len];
				len++;
			}
			tokString[len] = 0;
		}
  
    while(tokString[index] != PF_NULL)
    {
        loop = 0;
        index2 = index;
        while(1)
        {
            if(tokString[index2] != token[loop])
            {
                break;
            }
            loop++;
            index2++;
            if(tokString[index2] == PF_NULL)
                break;
            if(token[loop] == PF_NULL)
            {
                result = 1;
                break;
            }
        }
        if(result == 1)
        {
            for(temp = 0; temp < loop; temp++)
            {
                tokString[index+temp] = 0;
            }
            return &tokString[is];
        }
        index++;
			if(index >= len)
				return &tokString[is];
    }
		
    return 0;
}

PFdword pfStrLen(const PFbyte* str)
{
	PFdword index = 0;
#if (PF_UTILS_DEBUG == 1)
	if(str == 0)
	{
		return 0;
	}	
#endif	// #if (PF_UTILS_DEBUG == 1)

	while(*(str + index) != 0)
	{
		index++;
	}
	
	return index;
}

PFdword pfStrCopy(PFbyte* dest, const PFbyte* src)
{
	PFdword count = 0;
#if (PF_UTILS_DEBUG == 1)
	if((dest == 0) || (src == 0))
	{
		return 0;
	}	
#endif	// #if (PF_UTILS_DEBUG == 1)
	while(*(src+count) != 0)
	{
		*(dest+count) = *(src+count);
		count++;
	}
	return count;
}

PFEnStatus pfStrReverse(PFbyte* str)
{
	PFbyte i, j,ch;
#if (PF_UTILS_DEBUG == 1)
	if(str == 0)
	{
		return enStatusInvArgs;
	}	
#endif	// #if (PF_UTILS_DEBUG == 1)	
	for (i = 0, j = pfStrLen(str)-1; i<j; i++, j--) 
	{
		ch = str[i];
		str[i] = str[j];
		str[j] = ch;
	}
	
	return enStatusSuccess;
}

PFEnStatus pfMemCopy(void* dest, const void* src, PFdword num)
{
	PFdword index;
#if (PF_UTILS_DEBUG == 1)
	if( (src == 0) || (dest == 0) || (num == 0) )
	{
		return enStatusInvArgs;
	}	
#endif	// #if (PF_UTILS_DEBUG == 1)

	for(index = 0; index < num; index++)	
	{
		*(((PFbyte*)dest) + index) = *(((PFbyte*)src) + index);
	}
	
	return enStatusSuccess;
}

PFEnStatus pfMemSet(void* ptr, PFbyte value, PFdword num)
{
	PFdword index;
#if (PF_UTILS_DEBUG == 1)
	if( (ptr == 0) || (num == 0) )
	{
		return enStatusInvArgs;
	}	
#endif	// #if (PF_UTILS_DEBUG == 1)

	for(index = 0; index < num; index++)	
	{
		*(((PFbyte*)ptr) + index) = value;
	}
	
	return enStatusSuccess;
}

PFEnBoolean pfMemCompare( const void* ptr1, const void* ptr2, PFdword num)
{
	PFdword index;
#if (PF_UTILS_DEBUG == 1)
	if( (ptr1 == 0) || (ptr2 == 0) || (num == 0) )
	{
		return enBooleanFalse;
	}	
#endif	// #if (PF_UTILS_DEBUG == 1)

	for(index = 0; index < num; index++)	
	{
		if( *(((PFbyte*)ptr1) + index) != *(((PFbyte*)ptr2) + index) )
		{
			return enBooleanFalse;
		}
	}
	
	return enBooleanTrue;
}

PFEnStatus pfFtoA(PFfloat x, PFbyte* p)
{
	int n, i=0, k=0;
#if (PF_UTILS_DEBUG == 1)
	if(p == 0)
	{
		return enStatusInvArgs;
	}	
#endif	// #if (PF_UTILS_DEBUG == 1)
	n=(int)x;
	while(n>0)
	{
		x/=10;
		n=(int)x;
		i++;
	}
	*(p+i) = '.';
	x *= 10;
	n = (int)x;
	x = x-n;
	while(n>0)
	{
		if(k == i)
		{
			k++;
		}
		*(p+k)=48+n;
		x *= 10;
		n = (int)x;
		x = x-n;
		k++;
	}
	*(p+k) = 0;
	
	return enStatusSuccess;
}

PFEnStatus pfAtoI(PFbyte* str, PFdword* num)
{
	PFdword k = 0;
#if (PF_UTILS_DEBUG == 1)
	if( (str == 0) || (num == 0) )
	{
		return enStatusInvArgs;
	}	
#endif	// #if (PF_UTILS_DEBUG == 1)
	
	while (*str) 
	{
			k = (k<<3)+(k<<1)+(*str)-'0';
			str++;
	}
	*num = k;
	
	return enStatusSuccess;
}

PFEnStatus pfAtoF(PFbyte* str, PFfloat* num)
{
	PFfloat result=0;
	PFword sLen = pfStrLen(str);
	PFbyte dotpos=0,i;
#if (PF_UTILS_DEBUG == 1)
	if( (str == 0) || (num == 0) )
	{
		return enStatusInvArgs;
	}	
#endif	// #if (PF_UTILS_DEBUG == 1)	
	if((str[0] == '-') || (str[0] == '+'))
	{
		i=1;
	}
	else
	{
		i=0;
	}
	for(;i<sLen;i++)
	{
		if(str[i]=='.')
		{
			dotpos = sLen-i-1;
		}
		else
		{
			result = (result * 10.0) + (str[i] - '0');
		}
	}
	while(dotpos--)
	{
		result /= 10.0;
	}
	if(str[0] == '-')
	{
		result *= -1;
	}
	
	*num = result;
	
	return enStatusSuccess;
}

PFEnStatus pfItoA(PFsdword num, PFbyte* str)
{
	PFsdword i=0, sign;
#if (PF_UTILS_DEBUG == 1)
	if( (str == 0) || (num == 0) )
	{
		return enStatusInvArgs;
	}	
#endif	// #if (PF_UTILS_DEBUG == 1)	
	
	sign = num;
	if (num < 0)  			// record sign
	{
		num = -num;         // make num positive
	}		
	do 
	{    
		// generate digits in reverse order
		str[i++] = num % 10 + '0';   // get next digit
	} while ((num /= 10) > 0);     	// delete it
			
	if (sign < 0)
	{
		str[i++] = '-';
	}
	str[i] = '\0';
	pfStrReverse(str);
	
	return enStatusSuccess;
}
