#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_gpio.h"
#include "prime_lcd.h"
#include "prime_tick.h"

#define LCD_HDELAY		50
#define LCD_MDELAY		30
#define LCD_LDELAY		10

#define DATA_PORT_WIDTH		8



static PFword CheckStatus1=0;
static PFword lcdInitFlag=0;
void pfILI9320Delay(PFdword t);
static PFword xMax = 0;
static PFword yMax = 0;
 
static PFEnLcdOrientation Orientation=0;

static PFCfgILI9320 ILI9320ConfigStruct;
PFEnStatus pfILI9320WriteCmd(PFword command)
{ 
	if((CheckStatus1!=0)|| (lcdInitFlag==1) )
	{					
		pfGpioPinsClear(ILI9320ConfigStruct.gpioRegSelect.port, ILI9320ConfigStruct.gpioRegSelect.pin);
		pfGpioPinsClear(ILI9320ConfigStruct.gpioChipSelect.port, ILI9320ConfigStruct.gpioChipSelect.pin);
		pfGpioPinsSet(ILI9320ConfigStruct.gpioRead.port,ILI9320ConfigStruct.gpioRead.pin);
		pfGpioPinsSet(ILI9320ConfigStruct.gpioWrite.port,ILI9320ConfigStruct.gpioWrite.pin);						
		pfGpioPortWriteByte(ILI9320ConfigStruct.gpioData[0].port,(command>>8),0);	
		pfGpioPinsClear(ILI9320ConfigStruct.gpioWrite.port,ILI9320ConfigStruct.gpioWrite.pin);
		pfGpioPinsSet(ILI9320ConfigStruct.gpioWrite.port,ILI9320ConfigStruct.gpioWrite.pin);				
		pfGpioPortWriteByte(ILI9320ConfigStruct.gpioData[0].port,command,0);
		pfGpioPinsClear(ILI9320ConfigStruct.gpioWrite.port,ILI9320ConfigStruct.gpioWrite.pin);
		pfGpioPinsSet(ILI9320ConfigStruct.gpioWrite.port,ILI9320ConfigStruct.gpioWrite.pin);
		pfGpioPinsClear(ILI9320ConfigStruct.gpioChipSelect.port, ILI9320ConfigStruct.gpioChipSelect.pin);
		return enStatusSuccess;
	}
	else
	{
		return enStatusNotConfigured;
	}				
}

PFEnStatus pfILI9320WriteData(PFword data)
{
	if((CheckStatus1!=0)|| (lcdInitFlag==1))
	{
		pfGpioPinsClear(ILI9320ConfigStruct.gpioChipSelect.port, ILI9320ConfigStruct.gpioChipSelect.pin);
		pfGpioPinsSet(ILI9320ConfigStruct.gpioRegSelect.port, ILI9320ConfigStruct.gpioRegSelect.pin);
		pfGpioPinsSet(ILI9320ConfigStruct.gpioRead.port, ILI9320ConfigStruct.gpioRead.pin);
		pfGpioPinsSet(ILI9320ConfigStruct.gpioWrite.port, ILI9320ConfigStruct.gpioWrite.pin);
		pfGpioPortWriteByte(ILI9320ConfigStruct.gpioData[0].port,(data>>8),0);
		pfGpioPinsClear(ILI9320ConfigStruct.gpioWrite.port, ILI9320ConfigStruct.gpioWrite.pin);
		pfGpioPinsSet(ILI9320ConfigStruct.gpioWrite.port, ILI9320ConfigStruct.gpioWrite.pin);
		pfGpioPortWriteByte(ILI9320ConfigStruct.gpioData[0].port,data,0);
		pfGpioPinsClear(ILI9320ConfigStruct.gpioWrite.port, ILI9320ConfigStruct.gpioWrite.pin);
		pfGpioPinsSet(ILI9320ConfigStruct.gpioWrite.port, ILI9320ConfigStruct.gpioWrite.pin);
		pfGpioPinsSet(ILI9320ConfigStruct.gpioChipSelect.port, ILI9320ConfigStruct.gpioChipSelect.pin);
		return enStatusSuccess;
	}
	else
	{
		return enStatusNotConfigured;
	}
}

PFEnStatus pfILI9320Command(PFword command, PFword data)
{
#if(PF_ILI9320_DEBUG==1)
	if(lcdInitFLag==0)
		return enStatusInvArgs;
#endif
	  pfILI9320WriteCmd(command);
	  pfILI9320WriteData(data);
	  return enStatusSuccess;
}

PFEnStatus pfILI9320Open(PFpCfgILI9320 lcdConfig)
{
	CheckStatus1=1;
	lcdInitFlag=0;
#if(PF_ILI9320_DEBUG==1)
	if(lcdConfig == NULL)
		return enStatusNotConfigured;
#endif
	Orientation = lcdConfig->orientation;
	xMax = lcdConfig->width;
	yMax = lcdConfig->height;
	pfMemCopy(&ILI9320ConfigStruct,lcdConfig,sizeof(PFCfgILI9320));		
	pfGpioPortWriteByte(ILI9320ConfigStruct.gpioData[0].port,0,0);
	pfGpioPinsSet(lcdConfig->gpioRead.port, lcdConfig->gpioRead.pin);
	pfGpioPinsSet(lcdConfig->gpioWrite.port, lcdConfig->gpioWrite.pin);
	pfGpioPinsSet(lcdConfig->gpioChipSelect.port, lcdConfig->gpioChipSelect.pin);
	pfGpioPinsSet(lcdConfig->gpioRegSelect.port, lcdConfig->gpioRegSelect.pin);
	pfGpioPinsClear(lcdConfig->gpioReset.port, lcdConfig->gpioReset.pin);
	pfTickDelayMs(LCD_LDELAY);
	pfGpioPinsSet(lcdConfig->gpioReset.port, lcdConfig->gpioReset.pin);
	pfILI9320Command(0x00FF,0x0001);
	pfILI9320Command(0x00F3,0x0008);
	pfILI9320WriteCmd(0x00F3);
	pfILI9320Command(0x0001, 0x0100);     // Driver Output Control Register (R01h)
	pfILI9320Command(0x0002, 0x0700);     // LCD Driving Waveform Control (R02h)
	pfILI9320Command(0x0003, 0x1030);

	pfILI9320Command(0x0008, 0x0202);  //0302
	pfILI9320Command(0x0009, 0x0000);
	pfILI9320Command(0x0010, 0x0000);     // Power Control 1 (R10h)
	pfILI9320Command(0x0011, 0x0007);     // Power Control 2 (R11h)  
	pfILI9320Command(0x0012, 0x0000);     // Power Control 3 (R12h)
	pfILI9320Command(0x0013, 0x0000);     // Power Control 4 (R13h)
	pfTickDelayMs(LCD_LDELAY);
	pfILI9320Command(0x0010, 0x14B0);     // Power Control 1 (R10h)  
	pfTickDelayMs(LCD_LDELAY);  
	pfILI9320Command(0x0011, 0x0007);     // Power Control 2 (R11h)  
	pfTickDelayMs(LCD_LDELAY);  
	pfILI9320Command(0x0012, 0x008E);     // Power Control 3 (R12h)
	pfILI9320Command(0x0013, 0x0C00);     // Power Control 4 (R13h)
	pfILI9320Command(0x0029, 0x0015);     // NVM read data 2 (R29h)
	pfTickDelayMs(LCD_LDELAY);
	pfILI9320Command(0x0030, 0x0000);     // Gamma Control 1
	pfILI9320Command(0x0031, 0x0107);     // Gamma Control 2
	pfILI9320Command(0x0032, 0x0000);     // Gamma Control 3
	pfILI9320Command(0x0035, 0x0203);     // Gamma Control 6
	pfILI9320Command(0x0036, 0x0402);     // Gamma Control 7
	pfILI9320Command(0x0037, 0x0000);     // Gamma Control 8
	pfILI9320Command(0x0038, 0x0207);     // Gamma Control 9
	pfILI9320Command(0x0039, 0x0000);     // Gamma Control 10
	pfILI9320Command(0x003C, 0x0203);     // Gamma Control 13
	pfILI9320Command(0x003D, 0x0403);     // Gamma Control 14
	pfILI9320Command(0x0050, 0x0000);     // Window Horizontal RAM Address Start (R50h)
	pfILI9320Command(0x0051, 0x00EF);     // Window Horizontal RAM Address End (R51h)
	pfILI9320Command(0x0052, 0X0000);     // Window Vertical RAM Address Start (R52h)
	pfILI9320Command(0x0053, 0x013F);     // Window Vertical RAM Address End (R53h)
	pfILI9320Command(0x0060, 0xA700);     // Driver Output Control (R60h)
	pfILI9320Command(0x0061, 0x0001);     // Driver Output Control (R61h)
	pfILI9320Command(0x006A, 0x0000);     // Driver Output Control (R61h)
	
	pfILI9320Command(0x0080, 0x0000);
	pfILI9320Command(0x0081, 0x0000);
	pfILI9320Command(0x0082, 0x0000);
	pfILI9320Command(0x0083, 0x0000);
	pfILI9320Command(0x0084, 0x0000);
	pfILI9320Command(0x0085, 0x0000);
	
	
	pfILI9320Command(0x0090, 0X0029);     // Panel Interface Control 1 (R90h) 
	pfILI9320Command(0x0092, 0x0000);
	pfILI9320Command(0x0093, 0x0003);
	pfILI9320Command(0x0095, 0x0110);
	pfILI9320Command(0x0097, 0x0000);
	pfILI9320Command(0x0098, 0x0000);
	
	// Display On
	pfILI9320Command(0x0007, 0x0173);     // Display Control (R07h)
	//pfTickDelayMs(LCD_LDELAY);
	//pfILI9320FillArea(0,0,239,319,WHITE);
	//pfILI9320WriteCmd(0x0022);
	CheckStatus1=0;
	lcdInitFlag=1;
	return enStatusSuccess;
}


PFEnStatus pfILI9320Home(void)
{ 
#if(PF_ILI9320_DEBUG==1)
	if(lcdInitFLag==0)
		return enStatusNotConfigured;
#endif
	
		pfILI9320Command(0x0020, 0X0000);     // GRAM Address Set (Horizontal Address) (R20h)
		pfILI9320Command(0x0021, 0X0000);     // GRAM Address Set (Vertical Address) (R21h)
		pfILI9320WriteCmd(0x0022);		// Write Data to GRAM (R22h)
		return enStatusSuccess;
}

PFEnStatus pfILI9320SetWindow(PFword x, PFword y, PFword x1, PFword y1)
{
	PFword xStartNew=0;
	PFword yStartNew=0;
	PFword xEndNew=0;
	PFword yEndNew=0;
#if(PF_ILI9320_DEBUG==1)
	if(lcdInitFLag==0)
		return enStatusNotConfigured;
#endif
// 	pfMapPoints(x,y,&xStartNew,&yStartNew);
// 	pfMapPoints(x1,y1,&xEndNew,&yEndNew);
// 	
// 	if(xStartNew >xEndNew)
// 	{
// 		xStartNew ^= xEndNew;
// 		xEndNew ^= xStartNew;
// 		xStartNew ^= xEndNew;
// 	}
// 	if(yStartNew > yEndNew)
// 	{
// 		yStartNew ^= yEndNew;
// 		yEndNew ^= yStartNew;
// 		yStartNew ^= yEndNew;
// 	}
	
	pfILI9320Command(0x0050, x-1);       // Window Horizontal RAM Address Start (R50h)
	pfILI9320Command(0x0051, x1-1);      // Window Horizontal RAM Address End (R51h)
	pfILI9320Command(0x0052, y);       	// Window Vertical RAM Address Start (R52h) )
	pfILI9320Command(0x0053, y1-1);      // Window Vertical RAM Address End (R53h)

	return enStatusSuccess;
}

PFEnStatus pfILI9320SetCursor(PFword x, PFword y)
{
	PFword xNew=0;
	PFword yNew=0;
#if(PF_ILI9320_DEBUG==1)
	if(lcdInitFLag==0)
		return enStatusNotConfigured;
#endif
//	pfMapPoints(x,y,&xNew,&yNew);

		pfILI9320Command(0x0020, x);       // GRAM Address Set (Horizontal Address) (R20h)
		pfILI9320Command(0x0021, y);  
		return enStatusSuccess;

}

PFEnStatus pfILI9320FillRGB(PFword data)
{
	PFdword i;
	PFdword pixelCnt =((ILI9320ConfigStruct.height)*(ILI9320ConfigStruct.width));
#if(PF_ILI9320_DEBUG==1)
	if(lcdInitFLag==0)
		return enStatusNotConfigured;
#endif

		pfILI9320Home();
		for ( i=0; i < pixelCnt; i++ )
		{
			pfILI9320WriteData(data);
			}		 
		return enStatusSuccess;		 
}

PFEnStatus pfILI9320DrawPixel(PFword x, PFword y, PFword color)
{
	PFword xdata=0;
	PFword ydata=0;
#if(PF_ILI9320_DEBUG==1)
	if(lcdInitFLag==0)
		return enStatusNotConfigured;
#endif
	    pfMapPoints(x,y,&xdata,&ydata);
		pfILI9320WriteCmd(0x0020); // GRAM Address Set (Horizontal Address) (R20h)
		pfILI9320WriteData(xdata);
		pfILI9320WriteCmd(0x0021); // GRAM Address Set (Vertical Address) (R21h)
		pfILI9320WriteData(ydata);
		pfILI9320WriteCmd(0x0022);  // Write Data to GRAM (R22h)
		pfILI9320WriteData(color);
		return enStatusSuccess;
}


PFEnStatus pfILI9320SetAreaMax(void)
{
#if(PF_ILI9320_DEBUG==1)
	if(lcdInitFLag==0)
		return enStatusNotConfigured;
#endif
	pfILI9320SetWindow(1, 1, 240, 320);
	return enStatusSuccess;
}


PFEnStatus pfILI9320FillArea(PFword xStart, PFword yStart, PFword xEnd, PFword yEnd, PFword clr)
{
	 PFsdword count, xCnt, yCnt;
#if(PF_ILI9320_DEBUG==1)
	if(lcdInitFLag==0)
		return enStatusNotConfigured;
#endif
	pfILI9320SetWindow(xStart, yStart, xEnd, yEnd);
	pfILI9320SetCursor(xStart, yStart);
	xCnt = (xEnd) - (xStart) + 1;	 
	yCnt = (yEnd) - (yStart) + 1;	 
	
/*    
	if((xStartNew < xEndNew) && (yStartNew < yEndNew))
	{
		pfILI9320SetWindow(xStartNew, yStartNew, xEndNew, yEndNew);
		pfILI9320SetCursor(xStartNew, yStartNew);
		xCnt = (xEndNew) - (xStartNew);	 
		yCnt = (yEndNew) - (yStartNew);	 
	}		
	else if((xStartNew > xEndNew) && (yStartNew < yEndNew))
	{
		
		pfILI9320SetWindow(xEndNew, yStartNew, xStartNew, yEndNew);
		pfILI9320SetCursor(xEndNew, yStartNew);
		xCnt = (xStartNew) - (xEndNew);
		yCnt = (yEndNew) - (yStartNew);	 
	}
	
	else if((xStartNew < xEndNew) && (yStartNew > yEndNew))
	{
		pfILI9320SetWindow(xStartNew, yEndNew, xEndNew, yStartNew);
		pfILI9320SetCursor(xStartNew, yEndNew);
		xCnt = (xEndNew) - (xStartNew);
		yCnt = (yStartNew) - (yEndNew);
	    
	}		
	else
	{
		pfILI9320SetWindow(xEndNew, yEndNew, xStartNew, yStartNew);
		pfILI9320SetCursor(xEndNew, yEndNew);
		xCnt = (xStartNew) - (xEndNew);
		yCnt = (yStartNew) - (yEndNew);
		
	}
*/	
	pfILI9320WriteCmd(0x0022); 	// Write Data to GRAM (R22h)
	count = (xCnt) * (yCnt);
	while(count--)
	{
		pfILI9320WriteData(clr);
	}
	pfILI9320SetAreaMax();
	return enStatusSuccess; 
}

PFEnStatus pfSetOrientation(PFEnLcdOrientation orient)
{
#if(PF_ILI9320_DEBUG==1)
	if(lcdInitFLag==0)
		return enStatusNotConfigured;
#endif
	 Orientation=orient;
	return enStatusSuccess;
}

PFEnStatus pfGetOrientation(PFEnLcdOrientation* orient)
{
#if(PF_ILI9320_DEBUG==1)
	if(lcdInitFLag==0)
		return enStatusNotConfigured;
#endif
	 *orient = Orientation;
	 return enStatusSuccess;
}
PFEnStatus pfMapPoints(PFword xOld,PFword yOld,PFword *xNew,PFword *yNew)
{ 
 switch(Orientation)
	{
		case enLcdOrientation_0:
									*xNew = xOld;
									*yNew = yOld;
									break;
		case enLcdOrientation_90:
									*xNew = xMax - yOld;
									*yNew = xOld;
									break;
		case enLcdOrientation_180:
									*xNew = xMax - xOld;
									*yNew = yMax - yOld;
									break;
		case enLcdOrientation_270:
									*xNew = yOld;
									*yNew = yMax - xOld;
									break;
		default :
									*xNew = xOld;
									*yNew = yOld;
	}
	return enStatusSuccess;
}


PFEnStatus pfILI9320Scroll(PFsdword data )
{
#if(PF_ILI9320_DEBUG==1)
	if(lcdInitFLag==0)
		return enStatusNotConfigured;
#endif
	pfILI9320Command(0x006A, data);     // Driver Output Control (R61h)	
	return enStatusSuccess;
}




