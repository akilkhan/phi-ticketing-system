#include "prime_framework.h"
#include "prime_mb1200.h"
#include "prime_delay.h"

static PFCfgUltrasonicMb1200 ultrasonicMb1200ConfigStruct[MAX_NO_OF_ULTRASONICMB1200_SUPPORTED]={0};  /** to store sensor configuration settings */             /** to store sensor configuration settings */
static PFbyte ultrasonicMb1200InitFlag[MAX_NO_OF_ULTRASONICMB1200_SUPPORTED]={0};        /** to check for initialization of sensor */
static PFbyte ultrasonicMb1200DeviceCount=0;

PFEnStatus pfUltrasonicMb1200Open(PFbyte* devId, PPfCfgUltrasonicMb1200 sensorConfig, PFbyte deviceCount)
{
	PFbyte i=0, j=0;
	
	#ifdef _PRIME_DEBUG_
	if(devId == NULL)
		return enStatusInvArgs;
	if(sensorConfig == NULL)
		return enStatusInvArgs;
	#endif	// _PRIME_DEBUG_
	
	for(i=0; i<deviceCount; ++i)
	{
		for(j=0; j<MAX_NO_OF_ULTRASONICMB1200_SUPPORTED; ++j)
		{
			if( ultrasonicMb1200InitFlag[j] == enBooleanFalse )
			{
				devId[i]=j;
				pfMemCopy(&ultrasonicMb1200ConfigStruct[j], &sensorConfig[i], sizeof(PFCfgUltrasonicMb1200));

				/** for continuous conversion mode with ADC */
	//			if(sensorConfig[i].mode == enUltrasonic_mb1200_modeADC)
	//			pfGpioPinsSet(sensorConfig[i].trigger.port, sensorConfig[i].trigger.pin);

				ultrasonicMb1200InitFlag[j] = enBooleanTrue;
				++ultrasonicMb1200DeviceCount;
				break;
			}
			if(j == MAX_NO_OF_ULTRASONICMB1200_SUPPORTED)
				return enStatusError;
		}
	}
    return enStatusSuccess;
}	

PFEnStatus pfUltrasonicMb1200GetDistance(PFbyte* devId, PFdword* distance)
{
	 PFEnStatus status;
	 PFdword delay=0,count=0,timerTicks=0,dist=0;
	 PFdword mVtg=0;

	/** check for initialization */
	if( ultrasonicMb1200InitFlag[*devId] != enBooleanTrue )
		return enStatusNotConfigured;
	
	#if (MB1200_INTERFACE == MB1200_USE_ADC)
	{
		status=ultrasonicMb1200ConfigStruct[*devId].pfAdcGetVoltageSingleConversion(ultrasonicMb1200ConfigStruct[*devId].channel, &mVtg);
		if(status!=enStatusSuccess)
			return status;
		
		if(ultrasonicMb1200ConfigStruct[*devId].refVtg == enUltrasonicMb1200RefVtg5V)
		{
			dist = ((PFdword)mVtg * 1000)/488;                        /** scaling for 5V reference to ADC */
			*distance = (PFword)dist;
		}
		else
		{
			dist = ((PFdword)mVtg * 1000)/322;                                /** scaling for 3V3 reference to ADC */
			*distance = (PFword)dist;
		}
	}
	#else
	{		
    /** Issue Trigger pulse of 30us (should be greater than 20us) to Trigger pin of Ultrasonic sensor */
        pfGpioPinsSet(ultrasonicMb1200ConfigStruct[*devId].trigger.port, ultrasonicMb1200ConfigStruct[*devId].trigger.pin);
        pfDelayMicroSec(30);
        pfGpioPinsClear(ultrasonicMb1200ConfigStruct[*devId].trigger.port, ultrasonicMb1200ConfigStruct[*devId].trigger.pin);

        delay = pfDelayGetTick() + 3500;
    /** wait for Echo pulse, start counting Timertick to measure pulse width */
        while( !(pfGpioPortRead(ultrasonicMb1200ConfigStruct[*devId].echo.port) & ultrasonicMb1200ConfigStruct[*devId].echo.pin) )
        {
            /** Timeout if no Echo for 100ms */
            if( delay < (pfDelayGetTick()) )
            return enStatusTimeout;
        }

        count = pfDelayGetTick();
        while( pfGpioPortRead(ultrasonicMb1200ConfigStruct[*devId].echo.port) & ultrasonicMb1200ConfigStruct[*devId].echo.pin );

        count = pfDelayGetTick() - count;

    /** calculate distance in mm */
        *distance = (PFword)((count * (pfDelayGetTimerPeriod()) * 100 )/58);
	}
	#endif
  return enStatusSuccess;
}

PFEnStatus pfUltrasonic_mb1200Close(PFbyte* devId)
{
	if(ultrasonicMb1200InitFlag[*devId] != enBooleanTrue)
		return enStatusNotConfigured;

	ultrasonicMb1200InitFlag[*devId] = enBooleanFalse;
	--ultrasonicMb1200DeviceCount;
	
	return enStatusSuccess;
}

