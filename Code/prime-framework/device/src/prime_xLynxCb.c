#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_gpio.h"
#include "prime_delay.h"
#include "prime_sysClk.h"
#include "prime_canCommon.h"
#include "prime_can1.h"
#include "prime_spi0.h"
#include "prime_uart0.h"
#include "prime_uart2.h"
#include "prime_a3977.h"
#include "prime_l6470.h"
#include "prime_servo.h"
#include "prime_xLynxCb.h"
#include "prime_stepper.h"

#if ( (XLYNXCB_USE_CAN == 0) && (XLYNXCB_USE_SPI == 0) && (XLYNXCB_USE_UART == 0) )
	#error "No peripheral is initialised for board communication"
#elif ( ((XLYNXCB_USE_CAN == 1) && (XLYNXCB_USE_SPI == 1)) || ((XLYNXCB_USE_UART == 1) && (XLYNXCB_USE_CAN == 1)) || ((XLYNXCB_USE_UART == 1) && (XLYNXCB_USE_SPI == 1)) )
	#error "Multiple peripherals are initialised for board communication"
#endif

#define XLYNXCB_OPEN_CAN_RCV_ACK_TIMEOUT           300000//in micro-seconds
#define XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT            300000//in micro-seconds

#define XLYNXCB_OPEN_UART_RCV_ACK_TIMEOUT          300000//in micro-seconds
#define XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT           300000//in micro-seconds

// delay after which baseboard read response of Xpansion board via SPI
#define XLYNXCB_LYNX_OPEN_SPI_RCV_ACK_TIMEOUT      (400+ _XLYNXCB_DEBUG_MSG_PRINTING_DELAY_)//in micro-seconds
#define XLYNXCB_SERVO_OPEN_SPI_RCV_ACK_TIMEOUT     (400+ _XLYNXCB_DEBUG_MSG_PRINTING_DELAY_)//in micro-seconds
#define XLYNXCB_SMARTLYNX_OPEN_SPI_RCV_ACK_TIMEOUT (1300+ _XLYNXCB_DEBUG_MSG_PRINTING_DELAY_)//in micro-seconds

#define XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT       (500+ _XLYNXCB_DEBUG_MSG_PRINTING_DELAY_)//in micro-seconds
#define XLYNXCB_LYNX_SLEEP_CMD_SPI_RCV_ACK_TIMEOUT (1100+ _XLYNXCB_DEBUG_MSG_PRINTING_DELAY_)//in micro-seconds
#define XLYNXCB_SERVO_CMD_SPI_RCV_ACK_TIMEOUT      (200+ _XLYNXCB_DEBUG_MSG_PRINTING_DELAY_)//in micro-seconds
#define XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT  (500+ _XLYNXCB_DEBUG_MSG_PRINTING_DELAY_)//in micro-seconds

static PFbyte boardId;
static PFbyte xlynxCbMotorCount = 0, xlynxCbServoCount=0, xlynxCbLynxCount=0, xlynxCbSmartLynxCount=0;
static PFbyte xlynxCbMotorInitFlag[XLYNXCB_MAX_MOTOR_SUPPORTED] = {0};
static PFxlynxCbMotor xlynxCbMotor[XLYNXCB_MAX_MOTOR_SUPPORTED] = {0};

static PFEnStatus pfReceiveAck(PFEnStatus* cmdStatus, PFdword timeout);
volatile PFCanMsgHeader msgHeader = { XLYNXCB_CAN_MSG_ID, enCanFrameStandard, enBooleanFalse };

PFEnStatus pfXlynxCbBoardOpen(PFpGpioPortPin chipSelect)
{
	volatile PFEnStatus status;
	
	// register xpansion board for communication
	status = pfSpi0RegisterDevice(&boardId, chipSelect);

	return status;
}

PFEnStatus pfXlynxCbGetStatus(void)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte packetData[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_GET_BOARD_STATUS, 0, 0, 0, XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_GET_BOARD_STATUS, 0, 0,0,XLYNXCB_ETX,0}};

	do{
#if(XLYNXCB_USE_CAN == 1)
			status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
			if(status != enStatusSuccess)
				return status;
//reply			
			status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
			pfSpi0ChipSelect(&boardId,0);
			pfSpi0Write(&boardId, packetData, 7);
			pfSpi0ChipSelect(&boardId,1);
//reply			
			status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);	
#else
			status = pfUart2Write(packetData, 7);
			if(status != enStatusSuccess)
				return status;
//reply			
			status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);		
#endif
			if( status != enStatusSuccess )
			{
				if(status == enStatusBoardErr)
					return enStatusBoardErr;
				
				++k;
				continue;
			}
			else
					return cmdStatus;

	}while(k<XLYNXCB_MAX_CMD_RETRY);
	
	if(k == XLYNXCB_MAX_CMD_RETRY)
		return enStatusBoardErr;
}

PFEnStatus pfXlynxCbBoardReset(void)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte packetData[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_BOARD_RESET, 0, 0, 0, XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_BOARD_RESET, 0, 0,0,XLYNXCB_ETX,0}};

	do{
#if(XLYNXCB_USE_CAN == 1)
			status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
			if(status != enStatusSuccess)
				return status;
//reply			
			status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
			pfSpi0ChipSelect(&boardId,0);
			pfSpi0Write(&boardId, packetData, 7);
			pfSpi0ChipSelect(&boardId,1);
//reply			
			status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);	
#else
			status = pfUart2Write(packetData, 7);
			if(status != enStatusSuccess)
				return status;
//reply			
			status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);		
#endif
			if( status != enStatusSuccess )
			{
				if(status == enStatusBoardErr)
					return enStatusBoardErr;
				
				++k;
				continue;
			}
			else
					return cmdStatus;
			
	}while(k<XLYNXCB_MAX_CMD_RETRY);
	
	if(k == XLYNXCB_MAX_CMD_RETRY)
		return enStatusTimeout;
}

PFEnStatus pfXlynxCbSmartlynxOpen(PFbyte* id, PFpCfgXlynxCbStepperSmartlynx config, PFbyte deviceCount)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte i=0, j=0, k=0, *ptr=NULL;
	volatile PFbyte packetMetadata[7]={XLYNXCB_STX, 0, CMD_XLYNXCB_SMARTLYNX_OPEN, enXlynxCbMotorSelectSmartLynx, 0, 0, XLYNXCB_ETX};
	volatile PFbyte dataSize=0,checksum=0,packets=0;
	volatile PFdword count=0,timeout=0;
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x08, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX,0, CMD_XLYNXCB_SMARTLYNX_OPEN, enXlynxCbMotorSelectSmartLynx, 0,0,0,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(id == NULL)
		return enStatusInvArgs;
	if(config == NULL)
		return enStatusInvArgs;
	if( (xlynxCbSmartLynxCount+deviceCount) > XLYNXCB_MAX_SMARTLYNX_SUPPORTED )
		return enStatusInvArgs;
	if( (xlynxCbMotorCount+deviceCount) > XLYNXCB_MAX_MOTOR_SUPPORTED )
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG
	
	while( k < XLYNXCB_MAX_CMD_RETRY )
	{
		dataSize = deviceCount*(sizeof(PFCfgXlynxCbStepperSmartlynx));
		ptr = (PFbyte*)&config[0];
		checksum = 0;

		for(i=0; i<dataSize; ++i)
		{
			checksum ^= *(ptr+i);
		}

#if(XLYNXCB_USE_CAN == 1)
		dataSize -= 3;
		msg.data[0]=XLYNXCB_STX;
		msg.data[1]=dataSize+10; //first frame(8 bytes) + checksum + ETX
		msg.data[2]=CMD_XLYNXCB_SMARTLYNX_OPEN;
		msg.data[3]=enXlynxCbMotorSelectSmartLynx;
		msg.data[4] = deviceCount;		//for multipl device
		msg.data[5] = *ptr;
		msg.data[6] = *(ptr+1);
		msg.data[7] = *(ptr+2);
		
		msg.length = 8;
		status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
		if(status != enStatusSuccess)
		return status;

		ptr = ptr+3;
		j=0;
		for(i=0; i<dataSize; ++i)
		{
			msg.data[j] = *(ptr+j);
			if(j==7)
			{
				j=0;
				ptr+=8;
		
				pfDelayMicroSec(2000);
				
				status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
				if(status != enStatusSuccess)
					return status;
			}
			else
				++j;
		}

		msg.data[j]= checksum;
		++j;
		
		if(j==8)
		{
		pfDelayMicroSec(2000);
			
			status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
			if(status != enStatusSuccess)
				return status;

			j=0;
			msg.length=1;
		}
		else
		{
			msg.length=j+1;
		}

		msg.data[j]= XLYNXCB_ETX;
		
//		pfDelayMicroSec(20000);
		
		status =  COMM_CAN_WRITE(&msgHeader, msg.data, msg.length);
		if(status != enStatusSuccess)
			return status;

//reply
		status = pfReceiveAck(&cmdStatus,XLYNXCB_OPEN_CAN_RCV_ACK_TIMEOUT);		
#endif

#if(XLYNXCB_USE_SPI == 1)
		packetMetadata[0]=XLYNXCB_STX;
		packetMetadata[1] = dataSize+7;
		packetMetadata[2]=CMD_XLYNXCB_SMARTLYNX_OPEN;
		packetMetadata[3]=enXlynxCbMotorSelectSmartLynx;
		packetMetadata[4] = deviceCount;
		packetMetadata[5] = checksum;

		pfSpi0ChipSelect(&boardId,0);
		pfSpi0Write(&boardId, packetMetadata, 5);
		pfSpi0ChipSelect(&boardId,1);
		
		pfDelayMicroSec(1);
		
		pfSpi0ChipSelect(&boardId,0);
		pfSpi0Write(&boardId, ptr, (PFdword)dataSize);
		pfSpi0ChipSelect(&boardId,1);
		
		pfDelayMicroSec(1);
		
		pfSpi0ChipSelect(&boardId,0);
		pfSpi0Write(&boardId, &packetMetadata[5], 2);
		pfSpi0ChipSelect(&boardId,1);
		
//reply
		status = pfReceiveAck(&cmdStatus,(deviceCount * XLYNXCB_SMARTLYNX_OPEN_SPI_RCV_ACK_TIMEOUT));
#endif

#if(XLYNXCB_USE_UART == 1)
		packetMetadata[0]=XLYNXCB_STX;
		packetMetadata[1] = dataSize+7;
		packetMetadata[2]=CMD_XLYNXCB_SMARTLYNX_OPEN;
		packetMetadata[3]=enXlynxCbMotorSelectSmartLynx;
		packetMetadata[4] = deviceCount;
		packetMetadata[5] = checksum;

		status = pfUart2Write(packetMetadata, 5);
		if(status != enStatusSuccess)
		return status;

		status = pfUart2Write(ptr, (PFdword)dataSize);
		if(status != enStatusSuccess)
		return status;

		status = pfUart2Write(&packetMetadata[5], 2);
		if(status != enStatusSuccess)
		return status;

//reply
		status = pfReceiveAck(&cmdStatus,XLYNXCB_OPEN_UART_RCV_ACK_TIMEOUT);
#endif

		if( status != enStatusSuccess )
		{
			if(status == enStatusBoardErr)
				return enStatusBoardErr;
			
			++k;
			continue;
		}
		else
		{
			if( cmdStatus != enStatusSuccess )
				return cmdStatus;
			else
				break;
		}
	}

	if( k == XLYNXCB_MAX_CMD_RETRY )
		return enStatusTimeout;
	
	for(i=0; i<deviceCount; ++i)
	{
		for(j=0; j<XLYNXCB_MAX_MOTOR_SUPPORTED; ++j)
		{
			if(xlynxCbMotorInitFlag[j] == enBooleanFalse)
			{
				*(id+i) = j;
				++xlynxCbMotorCount;
				++xlynxCbSmartLynxCount;
				xlynxCbMotor[j].channel = config[i].channel;
				xlynxCbMotor[j].motorType = enXlynxCbMotorSelectSmartLynx;
				xlynxCbMotorInitFlag[j] = enBooleanTrue;
				break;
			}
		}
	}
	
	return cmdStatus;
}

PFEnStatus pfXlynxCbLynxOpen(PFbyte* id, PFpCfgXlynxCbStepperLynx config, PFbyte deviceCount)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFdword count=0;
	volatile PFbyte i=0, j=0, k=0,l=0, *ptr=NULL;
	volatile PFbyte packetMetadata[7]={XLYNXCB_STX, 0, CMD_XLYNXCB_LYNX_OPEN, enXlynxCbMotorSelectLynx, 0, 0, XLYNXCB_ETX};
	volatile PFbyte dataSize=0, checksum=0;
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x08, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 0, CMD_XLYNXCB_LYNX_OPEN, enXlynxCbMotorSelectLynx, 0,0,0,0}};


#ifdef PF_XLYNXCB_DEBUG
	if(id == NULL)
		return enStatusInvArgs;
	if(config == NULL)
		return enStatusInvArgs;
	if( (xlynxCbLynxCount+deviceCount) > XLYNXCB_MAX_LYNX_SUPPORTED )
		return enStatusInvArgs;
	if( (xlynxCbMotorCount+deviceCount) > XLYNXCB_MAX_MOTOR_SUPPORTED )
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG
	
	for(l=0; l<deviceCount; ++l)
	{
		k=0;
		while( k < XLYNXCB_MAX_CMD_RETRY )
		{
			dataSize = sizeof(PFCfgXlynxCbStepperLynx);
			ptr = (PFbyte*)&config[l];
			checksum = 0;

			for(i=0; i<dataSize; ++i)
			{
				checksum ^= *(ptr+i);
			}
			
#if(XLYNXCB_USE_CAN == 1)			
			dataSize -= 3;
			msg.data[0]=XLYNXCB_STX;
			msg.data[1]=dataSize+10; //first frame(8 bytes) + checksum + ETX
			msg.data[2]=CMD_XLYNXCB_LYNX_OPEN;
			msg.data[3]=enXlynxCbMotorSelectLynx;
			msg.data[4] = 1;		//for single device
			msg.data[5] = *ptr;
			msg.data[6] = *(ptr+1);
			msg.data[7] = *(ptr+2);
			
			msg.length = 8;
			status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
			if(status != enStatusSuccess)
			return status;
			
			ptr = ptr+3;
			j=0;
			for(i=0; i<dataSize; ++i)
			{
				msg.data[j] = *(ptr+j);
				if(j==7)
				{
					j=0;
					ptr+=8;
					
//					pfDelayMicroSec(1000);
					
					status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
					if(status != enStatusSuccess)
						return status;
				}
				else
					++j;
			}

			msg.data[j]= checksum;
			++j;
			if(j==8)
			{
				status =  COMM_CAN_WRITE(&msgHeader, msg.data, 8);
				if(status != enStatusSuccess)
					return status;

				j=0;
				msg.length=1;
			}
			else
			{
				msg.length=j+1;
			}

			msg.data[j]= XLYNXCB_ETX;
			
			status = COMM_CAN_WRITE(&msgHeader, msg.data, msg.length);
			if(status != enStatusSuccess)
				return status;
		
//reply			
			status = pfReceiveAck(&cmdStatus,XLYNXCB_OPEN_CAN_RCV_ACK_TIMEOUT);//must be in order of 2/3ms
#endif

#if(XLYNXCB_USE_SPI == 1)
		packetMetadata[0] = XLYNXCB_STX;
		packetMetadata[1] = dataSize+7;
		packetMetadata[2] = CMD_XLYNXCB_LYNX_OPEN;
		packetMetadata[3] = enXlynxCbMotorSelectLynx;
		packetMetadata[4] = 1;
		packetMetadata[5] = checksum;
		
		pfSpi0ChipSelect(&boardId,0);
		pfSpi0Write(&boardId, packetMetadata, 5);
		pfSpi0ChipSelect(&boardId,1);
		
		pfDelayMicroSec(1);
		
		pfSpi0ChipSelect(&boardId,0);
		pfSpi0Write(&boardId, ptr, (PFdword)dataSize);
		pfSpi0ChipSelect(&boardId,1);
		
		pfDelayMicroSec(1);
		
		pfSpi0ChipSelect(&boardId,0);
		pfSpi0Write(&boardId, &packetMetadata[5], 2);
		pfSpi0ChipSelect(&boardId,1);
		
//reply			
		status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_OPEN_SPI_RCV_ACK_TIMEOUT);//must be in order of 2/3ms
#endif

#if(XLYNXCB_USE_UART == 1)
		packetMetadata[0] = XLYNXCB_STX;
		packetMetadata[1] = dataSize+7;
		packetMetadata[2] = CMD_XLYNXCB_LYNX_OPEN;
		packetMetadata[3] = enXlynxCbMotorSelectLynx;
		packetMetadata[4] = 1;
		packetMetadata[5] = checksum;

		status = pfUart2Write(packetMetadata, 5);
		if(status != enStatusSuccess)
		return status;

		status = pfUart2Write(ptr, (PFdword)dataSize);
		if(status != enStatusSuccess)
		return status;

		status = pfUart2Write(&packetMetadata[5], 2);
		if(status != enStatusSuccess)
		return status;

//reply
		status = pfReceiveAck(&cmdStatus,XLYNXCB_OPEN_UART_RCV_ACK_TIMEOUT);
#endif

			if( status != enStatusSuccess )
			{
				if(status == enStatusBoardErr)
					return enStatusBoardErr;
			
				++k;
				continue;
			}
			else
			{
				if( cmdStatus != enStatusSuccess )
					return cmdStatus;
				else
					break;
			}
		}
		
		if( k == XLYNXCB_MAX_CMD_RETRY )
			return enStatusTimeout;
	}

	for(i=0; i<deviceCount; ++i)
	{
		for(j=0; j<XLYNXCB_MAX_MOTOR_SUPPORTED; ++j)
		{
			if(xlynxCbMotorInitFlag[j] == enBooleanFalse)
			{
				*(id+i) = j;
				++xlynxCbMotorCount;
				++xlynxCbLynxCount;
				xlynxCbMotor[j].channel = config[i].channel;
				xlynxCbMotor[j].motorType = enXlynxCbMotorSelectLynx;
				xlynxCbMotorInitFlag[j] = enBooleanTrue;
				break;
			}
		}
	}

	return cmdStatus;
}

PFEnStatus pfXlynxCbServoOpen(PFbyte* id, PFpCfgXlynxCbServo config, PFbyte deviceCount)
{
	volatile PFEnStatus status, cmdStatus;
	volatile PFbyte i=0, j=0, k=0, l=0, *ptr=NULL;
	volatile PFbyte dataSize=0, checksum=0;
	volatile PFdword count=0;
	volatile PFbyte packetMetadata[7]={XLYNXCB_STX, 0, CMD_XLYNXCB_SERVO_OPEN, enXlynxCbMotorSelectServo, 0, 0, XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x08, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 0, CMD_XLYNXCB_SERVO_OPEN, enXlynxCbMotorSelectServo, 0,0,0,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(id == NULL)
		return enStatusInvArgs;
	if(config == NULL)
		return enStatusInvArgs;
	if( (xlynxCbServoCount+deviceCount) > XLYNXCB_MAX_SERVO_SUPPORTED )
		return enStatusInvArgs;
	if( (xlynxCbMotorCount+deviceCount) > XLYNXCB_MAX_MOTOR_SUPPORTED )
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG
	
	for(l=0; l<deviceCount; ++l)
	{
		k=0;
		while( k < XLYNXCB_MAX_CMD_RETRY )
		{
			dataSize = sizeof(PFCfgXlynxCbServo);
			ptr = (PFbyte*)&config[l];
			checksum = 0;

			for(i=0; i<dataSize; ++i)
			{
				checksum ^= *(ptr+i);
			}

#if(XLYNXCB_USE_CAN == 1)	
			dataSize -= 3;
			msg.data[0]=XLYNXCB_STX;
			msg.data[1]=dataSize+10; //first frame(8 bytes) + checksum + ETX
			msg.data[2]=CMD_XLYNXCB_SERVO_OPEN;
			msg.data[3]=enXlynxCbMotorSelectServo;
			msg.data[4] = 1;		//for single device
			msg.data[5] = *ptr;
			msg.data[6] = *(ptr+1);
			msg.data[7] = *(ptr+2);
			
			msg.length = 8;
			status =  COMM_CAN_WRITE(&msgHeader, msg.data, 8);
			if(status != enStatusSuccess)
			return status;
			
			ptr = ptr+3;
			j=0;
			for(i=0; i<dataSize; ++i)
			{
				msg.data[j] = *(ptr+j);
				if(j==7)
				{
					j=0;
					ptr+=8;
					status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
					if(status != enStatusSuccess)
						return status;
				}
				else
					++j;
			}

			msg.data[j]= checksum;
			++j;
			if(j==8)
			{
				status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
				if(status != enStatusSuccess)
					return status;

				j=0;
				msg.length=1;
			}
			else
			{
				msg.length=j+1;
			}

			msg.data[j]= XLYNXCB_ETX;
			
			status = COMM_CAN_WRITE(&msgHeader, msg.data, msg.length);
			if(status != enStatusSuccess)
				return status;

//reply			
			status = pfReceiveAck(&cmdStatus,XLYNXCB_OPEN_CAN_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_SPI == 1)
			packetMetadata[0] = XLYNXCB_STX;
			packetMetadata[1] = dataSize+7;
			packetMetadata[2] = CMD_XLYNXCB_SERVO_OPEN;
			packetMetadata[3] = enXlynxCbMotorSelectServo;
			packetMetadata[4] = 1;
			packetMetadata[5] = checksum;
			
			pfSpi0ChipSelect(&boardId,0);
			pfSpi0Write(&boardId, packetMetadata, 5);
			pfSpi0ChipSelect(&boardId,1);
			
			pfDelayMicroSec(1);
			
			pfSpi0ChipSelect(&boardId,0);
			pfSpi0Write(&boardId, ptr, (PFdword)dataSize);
			pfSpi0ChipSelect(&boardId,1);
			
			pfDelayMicroSec(1);
			
			pfSpi0ChipSelect(&boardId,0);
			pfSpi0Write(&boardId, &packetMetadata[5], 2);
			pfSpi0ChipSelect(&boardId,1);

//reply			
			status = pfReceiveAck(&cmdStatus,XLYNXCB_SERVO_OPEN_SPI_RCV_ACK_TIMEOUT);//must be in order of 2/3ms
#endif

#if(XLYNXCB_USE_UART == 1)
			packetMetadata[0] = XLYNXCB_STX;
			packetMetadata[1] = dataSize+7;
			packetMetadata[2] = CMD_XLYNXCB_SERVO_OPEN;
			packetMetadata[3] = enXlynxCbMotorSelectLynx;
			packetMetadata[4] = 1;
			packetMetadata[5] = checksum;

			status = pfUart2Write(packetMetadata, 5);
			if(status != enStatusSuccess)
			return status;

			status = pfUart2Write(ptr, (PFdword)dataSize);
			if(status != enStatusSuccess)
			return status;

			status = pfUart2Write(&packetMetadata[5], 2);
			if(status != enStatusSuccess)
			return status;

//reply
			status = pfReceiveAck(&cmdStatus,XLYNXCB_OPEN_UART_RCV_ACK_TIMEOUT);
#endif

			if( status != enStatusSuccess )
			{
				if(status == enStatusBoardErr)
					return enStatusBoardErr;
			
				++k;
				continue;
			}
			else
			{
				if( cmdStatus != enStatusSuccess )
					return cmdStatus;
				else
					break;
			}
		}
		
		if( k == XLYNXCB_MAX_CMD_RETRY )
			return enStatusTimeout;
	}

	for(i=0; i<deviceCount; ++i)
	{
		for(j=0; j<XLYNXCB_MAX_MOTOR_SUPPORTED; ++j)
		{
			if(xlynxCbMotorInitFlag[j] == enBooleanFalse)
			{
				*(id+i) = j;
				++xlynxCbMotorCount;
				++xlynxCbServoCount;
				xlynxCbMotor[j].channel = config[i].channel;
				xlynxCbMotor[j].motorType = enXlynxCbMotorSelectServo;
				xlynxCbMotorInitFlag[j] = enBooleanTrue;
				break;
			}
		}
	}
	
	return cmdStatus;
}

//close function for lynx, smartlynx and servo
PFEnStatus pfXlynxCbMotorClose(PFbyte* deviceId)
{
	volatile PFEnStatus status, cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte packetData[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_CLOSE, 0, 0, 0, XLYNXCB_ETX};
	volatile PFbyte dataSize=0;
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX,7, CMD_XLYNXCB_MOTOR_CLOSE, 0, 0,0,XLYNXCB_ETX,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

#if(XLYNXCB_USE_CAN == 1)
	msg.data[4]=xlynxCbMotor[*deviceId].channel;
#else
	packetData[4]=xlynxCbMotor[*deviceId].channel;
#endif

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectServo;
#else
					packetData[3]=enXlynxCbMotorSelectServo;
#endif
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status =  COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SERVO_CMD_SPI_RCV_ACK_TIMEOUT);
#else
							status =  pfUart2Write(packetData, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
							{
								if( cmdStatus != enStatusSuccess )
									return cmdStatus;
								else
								{
									--xlynxCbServoCount;
									break;
								}
							}
					}while(k<XLYNXCB_MAX_CMD_RETRY);
			
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;
					
					--xlynxCbMotorCount;
					xlynxCbMotorInitFlag[j] = enBooleanFalse;
					break;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectLynx;
#else
					packetData[3]=enXlynxCbMotorSelectLynx;
#endif

					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#else
							status =  pfUart2Write(packetData, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif

							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
							{
								if( cmdStatus != enStatusSuccess )
									return cmdStatus;
								else
								{
									--xlynxCbLynxCount;
									break;
								}
							}
					}while(k<XLYNXCB_MAX_CMD_RETRY);
			
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;
					
					--xlynxCbMotorCount;
					xlynxCbMotorInitFlag[j] = enBooleanFalse;
					break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectSmartLynx;
#else
					packetData[3]=enXlynxCbMotorSelectSmartLynx;
#endif

					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#else
							status =  pfUart2Write(packetData, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif

							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
							{
								if( cmdStatus != enStatusSuccess )
									return cmdStatus;
								else
								{
									--xlynxCbSmartLynxCount;
									break;
								}
							}
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;
					
					--xlynxCbMotorCount;
					xlynxCbMotorInitFlag[j] = enBooleanFalse;
					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
	
	return cmdStatus;
}

//function calls for servo, lynx and smartlynx
PFEnStatus pfXlynxCbMotorResetDevice(PFbyte *deviceId)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte packetData[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_RESET_DEVICE, 0, 0, 0, XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_RESET_DEVICE, 0, 0,0,XLYNXCB_ETX,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG
	
#if(XLYNXCB_USE_CAN == 1)
	msg.data[4]=xlynxCbMotor[*deviceId].channel;
#else
	packetData[4]=xlynxCbMotor[*deviceId].channel;
#endif

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
					return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectLynx;
#else
					packetData[3]=enXlynxCbMotorSelectLynx;
#endif
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);	
#else
							status = pfUart2Write(packetData, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);		
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
									return cmdStatus;
							
					}while(k<XLYNXCB_MAX_CMD_RETRY);
					
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectSmartLynx;
#else
					packetData[3]=enXlynxCbMotorSelectSmartLynx;
#endif
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#else
							pfUart2Write(packetData, 7);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
								return cmdStatus;
								
					}while(k<XLYNXCB_MAX_CMD_RETRY);
					
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorResetPosition(PFbyte *deviceId)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte packetData[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_RESET_POSITION, 0, 0, 0, XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_RESET_POSITION, 0, 0,0,XLYNXCB_ETX,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG
	
#if(XLYNXCB_USE_CAN == 1)
	msg.data[4]=xlynxCbMotor[*deviceId].channel;
#else
	packetData[4]=xlynxCbMotor[*deviceId].channel;
#endif

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectServo;
#else
					packetData[3]=enXlynxCbMotorSelectServo;
#endif
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SERVO_CMD_SPI_RCV_ACK_TIMEOUT);	
#else
							pfUart2Write(packetData, 7);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);		
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
								return cmdStatus;
						
					}while(k<XLYNXCB_MAX_CMD_RETRY);
					
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectLynx;
#else
					packetData[3]=enXlynxCbMotorSelectLynx;
#endif
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);	
#else
							pfUart2Write(packetData, 7);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);		
#endif

							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
									return cmdStatus;
						
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectSmartLynx;
#else
					packetData[3]=enXlynxCbMotorSelectSmartLynx;
#endif
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);	
#else
							pfUart2Write(packetData, 7);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);		
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
								return cmdStatus;
						
					}while(k<XLYNXCB_MAX_CMD_RETRY);
					
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorGoHomePosition(PFbyte *deviceId)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte packetData[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_GOHOME, 0, 0, 0, XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_GOHOME, 0, 0,0,XLYNXCB_ETX,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG
	
#if(XLYNXCB_USE_CAN == 1)
	msg.data[4]=xlynxCbMotor[*deviceId].channel;
#else
	packetData[4]=xlynxCbMotor[*deviceId].channel;
#endif

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectLynx;
#else
					packetData[3]=enXlynxCbMotorSelectLynx;
#endif
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status =COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#else
							pfUart2Write(packetData, 7);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);		
#endif

							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
								return cmdStatus;
						
					}while(k<XLYNXCB_MAX_CMD_RETRY);
					
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectSmartLynx;
#else
					packetData[3]=enXlynxCbMotorSelectSmartLynx;
#endif
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);	
#else
							pfUart2Write(packetData, 7);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);		
#endif
						
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
								return cmdStatus;
							
					}while(k<XLYNXCB_MAX_CMD_RETRY);
					
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorSoftStop(PFbyte *deviceId)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte packetData[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_SOFTSTOP, 0, 0, 0, XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_SOFTSTOP, 0, 0,0,XLYNXCB_ETX,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG
	
#if(XLYNXCB_USE_CAN == 1)
	msg.data[4]=xlynxCbMotor[*deviceId].channel;
#else
	packetData[4]=xlynxCbMotor[*deviceId].channel;
#endif

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectLynx;
#else
					packetData[3]=enXlynxCbMotorSelectLynx;
#endif
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#else
							pfUart2Write(packetData, 7);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif

							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
								return cmdStatus;
							
					}while(k<XLYNXCB_MAX_CMD_RETRY);
					
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectSmartLynx;
#else
					packetData[3]=enXlynxCbMotorSelectSmartLynx;
#endif
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);	
#else
							pfUart2Write(packetData, 7);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);		
#endif

							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
								return cmdStatus;
						
					}while(k<XLYNXCB_MAX_CMD_RETRY);
					
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorHardStop(PFbyte *deviceId)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte packetData[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_HARDSTOP, 0, 0, 0, XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_HARDSTOP, 0, 0,0,XLYNXCB_ETX,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG
	
#if(XLYNXCB_USE_CAN == 1)
	msg.data[4]=xlynxCbMotor[*deviceId].channel;
#else
	packetData[4]=xlynxCbMotor[*deviceId].channel;
#endif

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectLynx;
#else
					packetData[3]=enXlynxCbMotorSelectLynx;
#endif
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);	
#else
							pfUart2Write(packetData, 7);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);		
#endif

							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
								return cmdStatus;
							
					}while(k<XLYNXCB_MAX_CMD_RETRY);
					
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
					msg.data[3]=enXlynxCbMotorSelectSmartLynx;
#else
					packetData[3]=enXlynxCbMotorSelectSmartLynx;
#endif
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);	
#else
							pfUart2Write(packetData, 7);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);		
#endif

							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
								return cmdStatus;
							
					}while(k<XLYNXCB_MAX_CMD_RETRY);
					
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorMove(PFbyte *deviceId, PFbyte dir, PFdword steps, PFfloat* speed)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte i=0,j=0,k=0,checksum=0;
	volatile PFbyte data[16]={XLYNXCB_STX, 16, CMD_XLYNXCB_MOTOR_MOVE,0, 0,0,0,0, 0,0,0,0, 0,0,0,XLYNXCB_ETX};
	volatile PFbyte dataSize=0;
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x08, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 16, CMD_XLYNXCB_MOTOR_MOVE, 0, 0,0,0,0}};

	union spd{
		float speed;
		PFbyte arr[4];
	}speedVar;
	
#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

//	DEBUG_UART_WRITE("\r\n in move", 1);
	
	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				do{
						checksum=0;
#if(XLYNXCB_USE_CAN == 1)
						msg.data[0]=XLYNXCB_STX;
						msg.data[1]=8;
						msg.data[2]=CMD_XLYNXCB_MOTOR_MOVE;
						msg.data[3]=enXlynxCbMotorSelectServo;
						msg.data[4]=xlynxCbMotor[*deviceId].channel;
						msg.data[5]=(PFbyte)steps;
						msg.data[7]=XLYNXCB_ETX;

						status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
						if(status != enStatusSuccess)
							return status;
	//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_SPI == 1)
						data[0]=XLYNXCB_STX;
						data[1]=8;
						data[2]=CMD_XLYNXCB_MOTOR_MOVE;
						data[3]=enXlynxCbMotorSelectServo;
						data[4]=xlynxCbMotor[*deviceId].channel;
						data[5]=(PFbyte)steps;
						data[7]=XLYNXCB_ETX;
						
						pfSpi0ChipSelect(&boardId,0);
						pfSpi0Write(&boardId, data, 8);
						pfSpi0ChipSelect(&boardId,1);
						
	//reply
						status = pfReceiveAck(&cmdStatus,XLYNXCB_SERVO_CMD_SPI_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_UART == 1)
						data[0]=XLYNXCB_STX;
						data[1]=8;
						data[2]=CMD_XLYNXCB_MOTOR_MOVE;
						data[3]=enXlynxCbMotorSelectServo;
						data[4]=xlynxCbMotor[*deviceId].channel;
						data[5]=(PFbyte)steps;
						data[7]=XLYNXCB_ETX;

						pfUart2Write(data, 8);
						
	//reply
						status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
						
						
						if( status != enStatusSuccess )
						{
							if(status == enStatusBoardErr)
								return enStatusBoardErr;
							
							++k;
							continue;
						}
						else
							return cmdStatus;
						
				}while(k<XLYNXCB_MAX_CMD_RETRY);
			
				if(k == XLYNXCB_MAX_CMD_RETRY)
					return enStatusTimeout;
					
				break;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
				do{
						checksum=0;
#if(XLYNXCB_USE_CAN == 1)
						msg.data[0]=XLYNXCB_STX;
						msg.data[1]=16;
						msg.data[2]=CMD_XLYNXCB_MOTOR_MOVE;
						msg.data[3]=enXlynxCbMotorSelectLynx;
						msg.data[4]=xlynxCbMotor[*deviceId].channel;
						msg.data[5]=dir;
						msg.data[6]=(PFbyte)steps;
						msg.data[7]=(PFbyte)(steps>>8);
						msg.length=8;
						
						checksum ^= msg.data[5] ^ (msg.data[6]^msg.data[7]);
						
						status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
						if(status != enStatusSuccess)
							return status;
					
						speedVar.speed = *speed;
						
						msg.data[0]=(PFbyte)(steps>>16);
						msg.data[1]=(PFbyte)(steps>>24);
						msg.data[2]=speedVar.arr[0];
						msg.data[3]=speedVar.arr[1];
						msg.data[4]=speedVar.arr[2];
						msg.data[5]=speedVar.arr[3];
						
						for(j=0; j<6; ++j)
							checksum ^= msg.data[j];
						
						msg.data[6]=checksum;	
						msg.data[7]=XLYNXCB_ETX;
						msg.length=8;

						status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
						if(status != enStatusSuccess)
							return status;

//reply					
					status = pfReceiveAck(&cmdStatus,500000);
#endif

#if(XLYNXCB_USE_SPI == 1)
					speedVar.speed = *speed;

					data[3]=enXlynxCbMotorSelectLynx;
					data[4]=xlynxCbMotor[*deviceId].channel;
					data[5]=dir;
					data[6]=(PFbyte)steps;
					data[7]=(PFbyte)(steps>>8);
					data[8]=(PFbyte)(steps>>16);
					data[9]=(PFbyte)(steps>>24);
					data[10]=speedVar.arr[0];
					data[11]=speedVar.arr[1];
					data[12]=speedVar.arr[2];
					data[13]=speedVar.arr[3];
					
					for(i=5; i<=13; ++i)
						checksum ^= data[i];
					
					data[14]=checksum;

					pfSpi0ChipSelect(&boardId,0);
					pfSpi0Write(&boardId, data, 16);
					pfSpi0ChipSelect(&boardId,1);		

//reply					
					status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);			
#endif

#if(XLYNXCB_USE_UART == 1)
					speedVar.speed = *speed;

					data[3]=enXlynxCbMotorSelectLynx;
					data[4]=xlynxCbMotor[*deviceId].channel;
					data[5]=dir;
					data[6]=(PFbyte)steps;
					data[7]=(PFbyte)(steps>>8);
					data[8]=(PFbyte)(steps>>16);
					data[9]=(PFbyte)(steps>>24);
					data[10]=speedVar.arr[0];
					data[11]=speedVar.arr[1];
					data[12]=speedVar.arr[2];
					data[13]=speedVar.arr[3];
					
					for(i=5; i<=13; ++i)
						checksum ^= data[i];
					
					data[14]=checksum;

					pfUart2Write(data, 16);		

//reply					
					status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);			
#endif
					if( status != enStatusSuccess )
					{
						if(status == enStatusBoardErr)
							return enStatusBoardErr;

						++k;
						continue;
					}
					else
					{
						return cmdStatus;
					}
						
				}while( k < XLYNXCB_MAX_CMD_RETRY );
			
				if(k == XLYNXCB_MAX_CMD_RETRY)
					return enStatusTimeout;
				
				break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
				do{
						checksum=0;
#if(XLYNXCB_USE_CAN == 1)
						msg.data[0]=XLYNXCB_STX;
						msg.data[1]=16;
						msg.data[2]=CMD_XLYNXCB_MOTOR_MOVE;
						msg.data[3]=enXlynxCbMotorSelectSmartLynx;
						msg.data[4]=xlynxCbMotor[*deviceId].channel;
						msg.data[5]=dir;
						msg.data[6]=(PFbyte)steps;
						msg.data[7]=(PFbyte)(steps>>8);
						msg.length=8;

						checksum ^= msg.data[5] ^ (msg.data[6]^msg.data[7]);
						
						status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
						if(status != enStatusSuccess)
							return status;
					
						speedVar.speed = *speed;
						
						msg.data[0]=(PFbyte)(steps>>16);
						msg.data[1]=(PFbyte)(steps>>24);
						msg.data[2]=speedVar.arr[0];
						msg.data[3]=speedVar.arr[1];
						msg.data[4]=speedVar.arr[2];
						msg.data[5]=speedVar.arr[3];
						
						for(j=0; j<6; ++j)
							checksum ^= msg.data[j];
						
						msg.data[6]=checksum;	
						msg.data[7]=XLYNXCB_ETX;
						msg.length=8;

						status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
						if(status != enStatusSuccess)
							return status;
					
//reply			
						status = pfReceiveAck(&cmdStatus,300000);
#endif

#if(XLYNXCB_USE_SPI == 1)
						speedVar.speed = *speed;

						data[3]=enXlynxCbMotorSelectSmartLynx;
						data[4]=xlynxCbMotor[*deviceId].channel;
						data[5]=dir;
						data[6]=(PFbyte)steps;
						data[7]=(PFbyte)(steps>>8);
						data[8]=(PFbyte)(steps>>16);
						data[9]=(PFbyte)(steps>>24);
						data[10]=speedVar.arr[0];
						data[11]=speedVar.arr[1];
						data[12]=speedVar.arr[2];
						data[13]=speedVar.arr[3];
						
						for(i=5; i<=13; ++i)
							checksum ^= data[i];
						
						data[14]=checksum;

						pfSpi0ChipSelect(&boardId,0);
						pfSpi0Write(&boardId, data, 16);
						pfSpi0ChipSelect(&boardId,1);
					
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_UART == 1)
						speedVar.speed = *speed;

						data[3]=enXlynxCbMotorSelectSmartLynx;
						data[4]=xlynxCbMotor[*deviceId].channel;
						data[5]=dir;
						data[6]=(PFbyte)steps;
						data[7]=(PFbyte)(steps>>8);
						data[8]=(PFbyte)(steps>>16);
						data[9]=(PFbyte)(steps>>24);
						data[10]=speedVar.arr[0];
						data[11]=speedVar.arr[1];
						data[12]=speedVar.arr[2];
						data[13]=speedVar.arr[3];
						
						for(i=5; i<=13; ++i)
							checksum ^= data[i];
						
						data[14]=checksum;

						pfUart2Write(data, 16);
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
						if( status != enStatusSuccess )
						{
							if(status == enStatusBoardErr)
								return enStatusBoardErr;

							++k;
							continue;
						}
						else
						{
							return cmdStatus;
						}
							
				}while( k < XLYNXCB_MAX_CMD_RETRY );
			
				if(k == XLYNXCB_MAX_CMD_RETRY)
					return enStatusTimeout;

				break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorSetFullStepSpeed(PFbyte* deviceId, PFfloat* speed)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0, checksum=0;
	volatile PFbyte data[11]={XLYNXCB_STX, 11, CMD_XLYNXCB_MOTOR_SET_FULLSTEP_SPEED, enXlynxCbMotorSelectSmartLynx, 0, 0, 0,0,0,0, XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x08, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 11, CMD_XLYNXCB_MOTOR_SET_FULLSTEP_SPEED, 0, 0,0,0,0}};

	union spd{
		float speed;
		PFbyte arr[4];
	}speedVar;

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
					speedVar.speed = *speed;
				 
					do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=11;
							msg.data[2]=CMD_XLYNXCB_MOTOR_SET_FULLSTEP_SPEED;
							msg.data[3]=enXlynxCbMotorSelectSmartLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=speedVar.arr[0];
							msg.data[6]=speedVar.arr[1];
							msg.data[7]=speedVar.arr[2];
							msg.length=8;

							status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
							if(status != enStatusSuccess)
								return status;

							msg.data[0]=speedVar.arr[3];
							msg.data[1]= checksum ^ (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]);//checksum
							msg.data[2]=XLYNXCB_ETX;
							msg.length=3;

							status = COMM_CAN_WRITE(&msgHeader, msg.data, 3);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_SPI == 1)
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=speedVar.arr[0];
							data[6]=speedVar.arr[1];
							data[7]=speedVar.arr[2];
							data[8]=speedVar.arr[3];
							
							checksum ^= ( (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]) );
							data[9]=checksum;

							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, data, 11);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_UART == 1)
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=speedVar.arr[0];
							data[6]=speedVar.arr[1];
							data[7]=speedVar.arr[2];
							data[8]=speedVar.arr[3];
							
							checksum ^= ( (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]) );
							data[9]=checksum;

							pfUart2Write(data, 11);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
								return cmdStatus;
						
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorRun(PFbyte* deviceId, PFbyte dir, PFfloat* speed)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte i=0,j=0,k=0,checksum=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte data[12]={XLYNXCB_STX, 12, CMD_XLYNXCB_MOTOR_RUN,0, 0,0,0,0, 0,0,0,XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x08, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 12, CMD_XLYNXCB_MOTOR_RUN, 0, 0,0,0,0}};

	union spd{
		float speed;
		PFbyte arr[4];
	}speedVar;

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
					speedVar.speed=*speed;
				 
					do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=12;
							msg.data[2]=CMD_XLYNXCB_MOTOR_RUN;
							msg.data[3]=enXlynxCbMotorSelectLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=dir;
							msg.data[6]=speedVar.arr[0];
							msg.data[7]=speedVar.arr[1];
							msg.length=8;
							
							checksum ^= msg.data[5] ^ (msg.data[6] ^ msg.data[7]);
							
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
							if(status != enStatusSuccess)
								return status;
						
							msg.data[0]=speedVar.arr[2];
							msg.data[1]=speedVar.arr[3];
							
							checksum ^= msg.data[0] ^ msg.data[1];
								
							msg.data[2]=checksum;
							msg.data[3]=XLYNXCB_ETX;
							msg.length=4;

							status = COMM_CAN_WRITE(&msgHeader, msg.data, 4);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_SPI == 1)
							data[3]=enXlynxCbMotorSelectLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=dir;
							data[6]=speedVar.arr[0];
							data[7]=speedVar.arr[1];
							data[8]=speedVar.arr[2];
							data[9]=speedVar.arr[3];
							
							for(i=5; i<=9; ++i)
								checksum ^= data[i];
							
							data[10]=checksum;

							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, data, 12);
							pfSpi0ChipSelect(&boardId,1);

//reply					
							status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);			
#endif

#if(XLYNXCB_USE_UART == 1)
							data[3]=enXlynxCbMotorSelectLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=dir;
							data[6]=speedVar.arr[0];
							data[7]=speedVar.arr[1];
							data[8]=speedVar.arr[2];
							data[9]=speedVar.arr[3];
							
							for(i=5; i<=9; ++i)
								checksum ^= data[i];
							
							data[10]=checksum;

							pfUart2Write(data, 12);
//reply					
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);			
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							return cmdStatus;
						
					}while( k < XLYNXCB_MAX_CMD_RETRY );
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
					speedVar.speed=*speed;
				 
					do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=12;
							msg.data[2]=CMD_XLYNXCB_MOTOR_RUN;
							msg.data[3]=enXlynxCbMotorSelectSmartLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=dir;
							msg.data[6]=speedVar.arr[0];
							msg.data[7]=speedVar.arr[1];
							msg.length=8;
							
							checksum ^= msg.data[5] ^ (msg.data[6] ^ msg.data[7]);
							
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
							if(status != enStatusSuccess)
								return status;
						
							msg.data[0]=speedVar.arr[2];
							msg.data[1]=speedVar.arr[3];
							
							checksum ^= msg.data[0] ^ msg.data[1];
								
							msg.data[2]=checksum;
							msg.data[3]=XLYNXCB_ETX;
							msg.length=4;

							status = COMM_CAN_WRITE(&msgHeader, msg.data, 4);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_SPI == 1)
							data[3]=enXlynxCbMotorSelectSmartLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=dir;
							data[6]=speedVar.arr[0];
							data[7]=speedVar.arr[1];
							data[8]=speedVar.arr[2];
							data[9]=speedVar.arr[3];
							
							for(i=5; i<=9; ++i)
								checksum ^= data[i];
							
							data[10]=checksum;

							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, data, 12);
							pfSpi0ChipSelect(&boardId,1);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_UART == 1)
							data[3]=enXlynxCbMotorSelectSmartLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=dir;
							data[6]=speedVar.arr[0];
							data[7]=speedVar.arr[1];
							data[8]=speedVar.arr[2];
							data[9]=speedVar.arr[3];
							
							for(i=5; i<=9; ++i)
								checksum ^= data[i];
							
							data[10]=checksum;

							pfUart2Write(data, 12);
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
								return cmdStatus;
						
					}while( k < XLYNXCB_MAX_CMD_RETRY );
			
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorDeviceReady(PFbyte* deviceId, PFEnBoolean* status)
{
	volatile PFEnStatus stat;
	volatile PFbyte i=0,j=0,k=0;
	volatile PFdword count=0,timeout=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte packetData[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_READY, 0, 0, 0, XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_READY, 0, 0,0,0,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
				 do{
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=7;
							msg.data[2]=CMD_XLYNXCB_MOTOR_READY;
							msg.data[3]=enXlynxCbMotorSelectLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=0;//no need of checksum
							msg.data[6]=XLYNXCB_ETX;

							msg.length = 7;
							stat = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(stat != enStatusSuccess)
								return stat;
							
							timeout = pfDelaySetTimeout(XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
							
							do{
									COMM_CAN_GET_RX_BUFFER_COUNT(&count);
										
									if(pfDelayCheckTimeout(timeout))
										break;
							}while(count<1);

							if(count<1)
							{
								++k;
								continue;
							}

							stat = COMM_CAN_READ(&msg);
							if(stat != enStatusSuccess)
								return stat;

							if( (msg.data[0]!=XLYNXCB_STX) || (msg.data[2]!=enStatusSuccess))
							{
								if(msg.data[2]==enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								COMM_CAN_RX_BUFFER_FLUSH();
								continue;
							}
							else
							{
								if(msg.data[3]==enStatusSuccess)
								{
									*status = (PFEnBoolean)msg.data[4];
									return enStatusSuccess;
								}
								else
								{
									COMM_CAN_RX_BUFFER_FLUSH();
									return msg.data[3];
								}
							}
#endif

#if(XLYNXCB_USE_SPI == 1)
							packetData[0]=XLYNXCB_STX;
							packetData[1]=7;
							packetData[2]=CMD_XLYNXCB_MOTOR_READY;
							packetData[3]=enXlynxCbMotorSelectLynx;
							packetData[4]=xlynxCbMotor[*deviceId].channel;
							packetData[5]=0;//no need of checksum
							packetData[6]=XLYNXCB_ETX;

							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
							
							pfDelayMicroSec(XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);

							for(i=0; i<7; ++i)
							{
									pfSpi0ChipSelect(&boardId,0);
									pfSpi0ExchangeByte(&boardId, 0, (PFword*)&packetData[i]);
									pfSpi0ChipSelect(&boardId,1);
								
									pfDelayMicroSec(30);
							}

							if( (packetData[0]!=XLYNXCB_STX) || (packetData[2]!=enStatusSuccess))
							{
								if(packetData[2]==enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
							{
								if(packetData[3]==enStatusSuccess)
								{
									*status = (PFEnBoolean)packetData[4];
									return enStatusSuccess;
								}
								else
								{
									return packetData[3];
								}
							}
#endif

#if(XLYNXCB_USE_UART == 1)
							packetData[0]=XLYNXCB_STX;
							packetData[1]=7;
							packetData[2]=CMD_XLYNXCB_MOTOR_READY;
							packetData[3]=enXlynxCbMotorSelectLynx;
							packetData[4]=xlynxCbMotor[*deviceId].channel;
							packetData[5]=0;//no need of checksum
							packetData[6]=XLYNXCB_ETX;

							pfUart2Write(packetData, 7);
							
							timeout = pfDelaySetTimeout(XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
							
							do{
									pfUart2GetRxBufferCount(&count);
										
									if(pfDelayCheckTimeout(timeout))
										break;
							}while(count<7);

							if(count<7)
							{
								++k;
								continue;
							}

							pfUart2Read(packetData, 7, &count);

							if( (packetData[0]!=XLYNXCB_STX) || (packetData[2]!=enStatusSuccess))
							{
								if(packetData[2]==enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
							{
								if(packetData[3]==enStatusSuccess)
								{
									*status = (PFEnBoolean)packetData[4];
									return enStatusSuccess;
								}
								else
								{
									return packetData[3];
								}
							}
#endif
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
					do{
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=7;
							msg.data[2]=CMD_XLYNXCB_MOTOR_READY;
							msg.data[3]=enXlynxCbMotorSelectSmartLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=0;//no need of checksum
							msg.data[6]=XLYNXCB_ETX;

							msg.length = 7;
							stat = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(stat != enStatusSuccess)
								return stat;
							
							timeout = pfDelaySetTimeout(XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
							
							do{
									COMM_CAN_GET_RX_BUFFER_COUNT(&count);
										
									if(pfDelayCheckTimeout(timeout))
										break;
							}while(count<1);

							if(count<1)
							{
								++k;
								continue;
							}

							stat = COMM_CAN_READ(&msg);
							if(stat != enStatusSuccess)
								return stat;

							if( (msg.data[0]!=XLYNXCB_STX) || (msg.data[2]!=enStatusSuccess))
							{
								if(msg.data[2]==enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								COMM_CAN_RX_BUFFER_FLUSH();
								continue;
							}
							else
							{
								if(msg.data[3]==enStatusSuccess)
								{
									*status = (PFEnBoolean)msg.data[4];
									return enStatusSuccess;
								}
								else
								{
									COMM_CAN_RX_BUFFER_FLUSH();
									return msg.data[3];
								}
							}
#endif

#if(XLYNXCB_USE_SPI == 1)
							packetData[0]=XLYNXCB_STX;
							packetData[1]=7;
							packetData[2]=CMD_XLYNXCB_MOTOR_READY;
							packetData[3]=enXlynxCbMotorSelectSmartLynx;
							packetData[4]=xlynxCbMotor[*deviceId].channel;
							packetData[5]=0;//no need of checksum
							packetData[6]=XLYNXCB_ETX;

							pfSpi0ChipSelect(&boardId,0);
							pfSpi0Write(&boardId, packetData, 7);
							pfSpi0ChipSelect(&boardId,1);
							
							pfDelayMicroSec(XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);

							for(i=0; i<7; ++i)
							{
									pfSpi0ChipSelect(&boardId,0);
									pfSpi0ExchangeByte(&boardId, 0, (PFword*)&packetData[i]);
									pfSpi0ChipSelect(&boardId,1);
								
									pfDelayMicroSec(30);
							}

							if( (packetData[0]!=XLYNXCB_STX) || (packetData[2]!=enStatusSuccess) )
							{
								if(packetData[2]==enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
							{
								if(packetData[3]==enStatusSuccess)
								{
									*status = (PFEnBoolean)packetData[4];
									return enStatusSuccess;
								}
								else
								{
									return packetData[3];
								}
							}
#endif

#if(XLYNXCB_USE_UART == 1)
							packetData[0]=XLYNXCB_STX;
							packetData[1]=7;
							packetData[2]=CMD_XLYNXCB_MOTOR_READY;
							packetData[3]=enXlynxCbMotorSelectSmartLynx;
							packetData[4]=xlynxCbMotor[*deviceId].channel;
							packetData[5]=0;//no need of checksum
							packetData[6]=XLYNXCB_ETX;

							pfUart2Write(packetData, 7);
							
							timeout = pfDelaySetTimeout(XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
							
							do{
									pfUart2GetRxBufferCount(&count);
										
									if(pfDelayCheckTimeout(timeout))
										break;
							}while(count<7);

							if(count<7)
							{
								++k;
								continue;
							}

							pfUart2Read(packetData, 7, &count);

							if( (packetData[0]!=XLYNXCB_STX) || (packetData[2]!=enStatusSuccess) )
							{
								if(packetData[2]==enStatusBoardErr)
									return enStatusBoardErr;
								
								++k;
								continue;
							}
							else
							{
								if(packetData[3]==enStatusSuccess)
								{
									*status = (PFEnBoolean)packetData[4];
									return enStatusSuccess;
								}
								else
								{
									return packetData[3];
								}
							}
#endif
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorSetAccelerationProfile(PFbyte* deviceId, PFfloat* acceleration, PFfloat* deceleration)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0,checksum=0;
	volatile PFbyte data[15]={XLYNXCB_STX, 15, CMD_XLYNXCB_MOTOR_SET_ACC_PROFILE,0, 0,0,0,0, 0,0,0,0, 0,0,XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x08, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 15, CMD_XLYNXCB_MOTOR_SET_ACC_PROFILE, 0, 0,0,0,0}};

	union spd{
		float speed;
		PFbyte arr[4];
	}speedVar,tempVar;
	
#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
				 speedVar.speed = *acceleration;
				 tempVar.speed = *deceleration;
				 
				 do{
							checksum =0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=15;
							msg.data[2]=CMD_XLYNXCB_MOTOR_SET_ACC_PROFILE;
							msg.data[3]=enXlynxCbMotorSelectLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=speedVar.arr[0];
							msg.data[6]=speedVar.arr[1];
							msg.data[7]=speedVar.arr[2];
							msg.length=8;
				
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
							if(status != enStatusSuccess)
								return status;
							
							msg.data[0]=speedVar.arr[3];
							msg.data[1]=tempVar.arr[0];
							msg.data[2]=tempVar.arr[1];
							msg.data[3]=tempVar.arr[2];
							msg.data[4]=tempVar.arr[3];
							
							checksum ^= (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]);
							checksum ^= (tempVar.arr[0] ^ tempVar.arr[1]) ^ (tempVar.arr[2] ^ tempVar.arr[3]);
							
							msg.data[5]=checksum;
							msg.data[6]=XLYNXCB_ETX;
							msg.length=7;

							status =COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#endif
							
#if(XLYNXCB_USE_SPI == 1)
							data[3]=enXlynxCbMotorSelectLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=speedVar.arr[0];
							data[6]=speedVar.arr[1];
							data[7]=speedVar.arr[2];
							data[8]=speedVar.arr[3];
							data[9]=tempVar.arr[0];
							data[10]=tempVar.arr[1];
							data[11]=tempVar.arr[2];
							data[12]=tempVar.arr[3];
							
							checksum ^= (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]);
							checksum ^= (tempVar.arr[0] ^ tempVar.arr[1]) ^ (tempVar.arr[2] ^ tempVar.arr[3]);
							
							data[13]=checksum;

							pfSpi0ChipSelect(&boardId,0);
							status = pfSpi0Write(&boardId, data, 15);
							pfSpi0ChipSelect(&boardId,1);
							
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#endif
		
#if(XLYNXCB_USE_UART == 1)
							data[3]=enXlynxCbMotorSelectLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=speedVar.arr[0];
							data[6]=speedVar.arr[1];
							data[7]=speedVar.arr[2];
							data[8]=speedVar.arr[3];
							data[9]=tempVar.arr[0];
							data[10]=tempVar.arr[1];
							data[11]=tempVar.arr[2];
							data[12]=tempVar.arr[3];
							
							checksum ^= (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]);
							checksum ^= (tempVar.arr[0] ^ tempVar.arr[1]) ^ (tempVar.arr[2] ^ tempVar.arr[3]);
							
							data[13]=checksum;

							status = pfUart2Write(data, 15);
							
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
								return cmdStatus;
						
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
				 speedVar.speed = *acceleration;
				 tempVar.speed = *deceleration;
				 
				 do{
							checksum =0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=15;
							msg.data[2]=CMD_XLYNXCB_MOTOR_SET_ACC_PROFILE;
							msg.data[3]=enXlynxCbMotorSelectSmartLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=speedVar.arr[0];
							msg.data[6]=speedVar.arr[1];
							msg.data[7]=speedVar.arr[2];
							msg.length=8;
				
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
							if(status != enStatusSuccess)
								return status;
							
							msg.data[0]=speedVar.arr[3];
							msg.data[1]=tempVar.arr[0];
							msg.data[2]=tempVar.arr[1];
							msg.data[3]=tempVar.arr[2];
							msg.data[4]=tempVar.arr[3];
							
							checksum ^= (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]);
							checksum ^= (tempVar.arr[0] ^ tempVar.arr[1]) ^ (tempVar.arr[2] ^ tempVar.arr[3]);
							
							msg.data[5]=checksum;
							msg.data[6]=XLYNXCB_ETX;
							msg.length=7;

							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#endif
							
#if(XLYNXCB_USE_SPI == 1)
							data[3]=enXlynxCbMotorSelectSmartLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=speedVar.arr[0];
							data[6]=speedVar.arr[1];
							data[7]=speedVar.arr[2];
							data[8]=speedVar.arr[3];
							data[9]=tempVar.arr[0];
							data[10]=tempVar.arr[1];
							data[11]=tempVar.arr[2];
							data[12]=tempVar.arr[3];
							
							checksum ^= (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]);
							checksum ^= (tempVar.arr[0] ^ tempVar.arr[1]) ^ (tempVar.arr[2] ^ tempVar.arr[3]);
							
							data[13]=checksum;

							pfSpi0ChipSelect(&boardId,0);
							status = pfSpi0Write(&boardId, data, 15);
							pfSpi0ChipSelect(&boardId,1);
							
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_UART == 1)
							data[3]=enXlynxCbMotorSelectSmartLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=speedVar.arr[0];
							data[6]=speedVar.arr[1];
							data[7]=speedVar.arr[2];
							data[8]=speedVar.arr[3];
							data[9]=tempVar.arr[0];
							data[10]=tempVar.arr[1];
							data[11]=tempVar.arr[2];
							data[12]=tempVar.arr[3];
							
							checksum ^= (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]);
							checksum ^= (tempVar.arr[0] ^ tempVar.arr[1]) ^ (tempVar.arr[2] ^ tempVar.arr[3]);
							
							data[13]=checksum;

							status = pfUart2Write(data, 15);
							
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
								return cmdStatus;
						
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorGetAccelerationProfile(PFbyte* deviceId, PFfloat* acceleration, PFfloat* deceleration)
{
	volatile PFEnStatus status;
	volatile PFbyte i=0,j=0,k=0,checksum=0;
	volatile PFdword count=0,timeout=0;
	volatile PFbyte dataSize=0,argData[14]={0};
	volatile PFbyte data[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_GET_ACC_PROFILE,0, 0,0,XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_GET_ACC_PROFILE, 0, 0,0,0,0}};

	union spd{
		float speed;
		PFbyte arr[4];
	}speedVar,tempVar;
	
#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
					do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=7;
							msg.data[2]=CMD_XLYNXCB_MOTOR_GET_ACC_PROFILE;
							msg.data[3]=enXlynxCbMotorSelectLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=0;//no need of checksum
							msg.data[6]=XLYNXCB_ETX;

							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;

							timeout = pfDelaySetTimeout(XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
							
							do{
									if(pfDelayCheckTimeout(timeout))
										break;

									COMM_CAN_GET_RX_BUFFER_COUNT(&count);
							}while(count<2);
							
							if(count<2)
							{
								++k;
								continue;
							}

							status = COMM_CAN_READ(&msg);
							if(status != enStatusSuccess)
								return status;

							if( (msg.data[0]!=XLYNXCB_STX) || (msg.data[2]!= enStatusSuccess) )
							{
								if(msg.data[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								COMM_CAN_RX_BUFFER_FLUSH();
								continue;
							}
							else
							{
								if(msg.data[3] != enStatusSuccess)
								{
									COMM_CAN_RX_BUFFER_FLUSH();
									return msg.data[3];
								}
								else
								{
									checksum ^= (msg.data[4] ^ msg.data[5]) ^ (msg.data[6] ^ msg.data[7]);
									speedVar.arr[0] = msg.data[4];
									speedVar.arr[1] = msg.data[5];
									speedVar.arr[2] = msg.data[6];
									speedVar.arr[3] = msg.data[7];
									*acceleration = speedVar.speed;
									
									status = COMM_CAN_READ(&msg);
									if(status != enStatusSuccess)
										return status;

									checksum ^= (msg.data[0] ^ msg.data[1]) ^ (msg.data[2] ^ msg.data[3]);
									if( (msg.data[4]!=checksum) || (msg.data[5]!=XLYNXCB_ETX) )
									{
										++k;
										COMM_CAN_RX_BUFFER_FLUSH();
										continue;
									}
									
									speedVar.arr[0] = msg.data[0];
									speedVar.arr[1] = msg.data[1];
									speedVar.arr[2] = msg.data[2];
									speedVar.arr[3] = msg.data[3];
									*deceleration = speedVar.speed;
									
									return enStatusSuccess;
								}
							}
#endif

#if(XLYNXCB_USE_SPI == 1)
							data[3]=enXlynxCbMotorSelectLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;

							pfSpi0ChipSelect(&boardId,0);
							status = pfSpi0Write(&boardId, data, 7);
							pfSpi0ChipSelect(&boardId,1);

							pfDelayMicroSec(XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);

							for(i=0; i<14; ++i)
							{
									pfSpi0ChipSelect(&boardId,0);
									pfSpi0ExchangeByte(&boardId, 0, (PFword*)&argData[i]);
									pfSpi0ChipSelect(&boardId,1);
								
									pfDelayMicroSec(30);
							}

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[12]!=checksum) || (argData[13]!=XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									checksum ^= (argData[4] ^ argData[5]) ^ (argData[6] ^ argData[7]);
									speedVar.arr[0] = argData[4];
									speedVar.arr[1] = argData[5];
									speedVar.arr[2] = argData[6];
									speedVar.arr[3] = argData[7];
									*acceleration = speedVar.speed;
									
									checksum ^= (argData[8] ^ argData[9]) ^ (argData[10] ^ argData[11]);
									speedVar.arr[0] = argData[8];
									speedVar.arr[1] = argData[9];
									speedVar.arr[2] = argData[10];
									speedVar.arr[3] = argData[11];
									*deceleration = speedVar.speed;
									
									return enStatusSuccess;
								}
							}
#endif

#if(XLYNXCB_USE_UART == 1)
							data[3]=enXlynxCbMotorSelectLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;

							status = pfUart2Write(data, 7);

							timeout = pfDelaySetTimeout(XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
							
							do{
									if(pfDelayCheckTimeout(timeout))
										break;

									pfUart2GetRxBufferCount(&count);
							}while(count<14);
							
							if(count<14)
							{
								++k;
								continue;
							}

							pfUart2Read(argData, 14, &count);

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[12]!=checksum) || (argData[13]!=XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									checksum ^= (argData[4] ^ argData[5]) ^ (argData[6] ^ argData[7]);
									speedVar.arr[0] = argData[4];
									speedVar.arr[1] = argData[5];
									speedVar.arr[2] = argData[6];
									speedVar.arr[3] = argData[7];
									*acceleration = speedVar.speed;
									
									checksum ^= (argData[8] ^ argData[9]) ^ (argData[10] ^ argData[11]);
									speedVar.arr[0] = argData[8];
									speedVar.arr[1] = argData[9];
									speedVar.arr[2] = argData[10];
									speedVar.arr[3] = argData[11];
									*deceleration = speedVar.speed;
									
									return enStatusSuccess;
								}
							}
#endif
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
					do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=7;
							msg.data[2]=CMD_XLYNXCB_MOTOR_GET_ACC_PROFILE;
							msg.data[3]=enXlynxCbMotorSelectSmartLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=0;//no need of checksum
							msg.data[6]=XLYNXCB_ETX;

							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
					
							timeout = pfDelaySetTimeout(XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
							
							do{
									COMM_CAN_GET_RX_BUFFER_COUNT(&count);
									
									if(pfDelayCheckTimeout(timeout))
										break;
							}while(count<2);
							
							if(count<2)
							{
								++k;
								continue;
							}
				
							status = COMM_CAN_READ(&msg);
							if(status != enStatusSuccess)
								return status;

							if( (msg.data[0]!=XLYNXCB_STX) || (msg.data[2]!= enStatusSuccess) )
							{
								if(msg.data[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								COMM_CAN_RX_BUFFER_FLUSH();
								continue;
							}
							else
							{
								if(msg.data[3] != enStatusSuccess)
								{
									COMM_CAN_RX_BUFFER_FLUSH();
									return msg.data[3];
								}
								else
								{
									checksum ^= (msg.data[4] ^ msg.data[5]) ^ (msg.data[6] ^ msg.data[7]);
									speedVar.arr[0] = msg.data[4];
									speedVar.arr[1] = msg.data[5];
									speedVar.arr[2] = msg.data[6];
									speedVar.arr[3] = msg.data[7];
									*acceleration = speedVar.speed;
									
									status = COMM_CAN_READ(&msg);
									if(status != enStatusSuccess)
										return status;

									checksum ^= (msg.data[0] ^ msg.data[1]) ^ (msg.data[2] ^ msg.data[3]);
									if( (msg.data[4]!=checksum) || (msg.data[5]!=XLYNXCB_ETX) )
									{
										++k;
										COMM_CAN_RX_BUFFER_FLUSH();
										continue;
									}
									
									speedVar.arr[0] = msg.data[0];
									speedVar.arr[1] = msg.data[1];
									speedVar.arr[2] = msg.data[2];
									speedVar.arr[3] = msg.data[3];
									*deceleration = speedVar.speed;
									
									return enStatusSuccess;
								}
							}
#endif

#if(XLYNXCB_USE_SPI == 1)
							data[3]=enXlynxCbMotorSelectSmartLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;

							pfSpi0ChipSelect(&boardId,0);
							status = pfSpi0Write(&boardId, data, 7);
							pfSpi0ChipSelect(&boardId,1);

							pfDelayMicroSec(XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);

							for(i=0; i<14; ++i)
							{
									pfSpi0ChipSelect(&boardId,0);
									pfSpi0ExchangeByte(&boardId, 0, (PFword*)&argData[i]);
									pfSpi0ChipSelect(&boardId,1);
								
									pfDelayMicroSec(30);
							}

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[12]!=checksum) || (argData[13]!=XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									checksum ^= (argData[4] ^ argData[5]) ^ (argData[6] ^ argData[7]);
									speedVar.arr[0] = argData[4];
									speedVar.arr[1] = argData[5];
									speedVar.arr[2] = argData[6];
									speedVar.arr[3] = argData[7];
									*acceleration = speedVar.speed;
									
									checksum ^= (argData[8] ^ argData[9]) ^ (argData[10] ^ argData[11]);
									speedVar.arr[0] = argData[8];
									speedVar.arr[1] = argData[9];
									speedVar.arr[2] = argData[10];
									speedVar.arr[3] = argData[11];
									*deceleration = speedVar.speed;
									
									return enStatusSuccess;
								}
							}
#endif

#if(XLYNXCB_USE_UART == 1)
							data[3]=enXlynxCbMotorSelectSmartLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;

							status = pfUart2Write(data, 7);

							timeout = pfDelaySetTimeout(XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
							
							do{
									if(pfDelayCheckTimeout(timeout))
										break;

									pfUart2GetRxBufferCount(&count);
							}while(count<14);
							
							if(count<14)
							{
								++k;
								continue;
							}

							pfUart2Read(argData, 14, &count);

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[12]!=checksum) || (argData[13]!=XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									checksum ^= (argData[4] ^ argData[5]) ^ (argData[6] ^ argData[7]);
									speedVar.arr[0] = argData[4];
									speedVar.arr[1] = argData[5];
									speedVar.arr[2] = argData[6];
									speedVar.arr[3] = argData[7];
									*acceleration = speedVar.speed;
									
									checksum ^= (argData[8] ^ argData[9]) ^ (argData[10] ^ argData[11]);
									speedVar.arr[0] = argData[8];
									speedVar.arr[1] = argData[9];
									speedVar.arr[2] = argData[10];
									speedVar.arr[3] = argData[11];
									*deceleration = speedVar.speed;
									
									return enStatusSuccess;
								}
							}
#endif
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorGetPosition(PFbyte* deviceId, PFsdword* position)
{
	volatile PFEnStatus status;
	volatile PFbyte i=0,j=0,k=0,checksum=0;
	volatile PFdword count=0, timeout=0;
	volatile PFbyte dataSize=0,argData[10]={0};
	volatile PFsword tempWord=0;
	volatile PFbyte data[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_GET_POSITION,0, 0,0,XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_GET_POSITION, 0, 0,0,0,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
					do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=7;
							msg.data[2]=CMD_XLYNXCB_MOTOR_GET_POSITION;
							msg.data[3]=enXlynxCbMotorSelectServo;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=0;//no need of checksum
							msg.data[6]=XLYNXCB_ETX;

							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
					
							timeout = pfDelaySetTimeout(XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
							
							do{
									COMM_CAN_GET_RX_BUFFER_COUNT(&count);
									
									if(pfDelayCheckTimeout(timeout))
										break;
							}while(count<1);
							
							if(count<1)
							{
								++k;
								continue;
							}
				
							status = COMM_CAN_READ(&msg);
							if(status != enStatusSuccess)
								return status;

							if( (msg.data[0]!=XLYNXCB_STX) || (msg.data[2]!= enStatusSuccess) || (msg.data[6]!=XLYNXCB_ETX) )
							{
								if(msg.data[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								COMM_CAN_RX_BUFFER_FLUSH();
								continue;
							}
							else
							{
								if(msg.data[3] != enStatusSuccess)
								{
									return msg.data[3];
								}
								else
								{
									*position = (PFsdword)msg.data[4];
									break;
								}
							}
#endif
							
#if(XLYNXCB_USE_SPI == 1)
							data[3]=enXlynxCbMotorSelectServo;
							data[4]=xlynxCbMotor[*deviceId].channel;

							pfSpi0ChipSelect(&boardId,0);
							status = pfSpi0Write(&boardId, data, 7);
							pfSpi0ChipSelect(&boardId,1);
					
							pfDelayMicroSec(XLYNXCB_SERVO_CMD_SPI_RCV_ACK_TIMEOUT);

							for(i=0; i<7; ++i)
							{
									pfSpi0ExchangeByte(&boardId, 0, (PFword*)&argData[i]);
									pfSpi0ChipSelect(&boardId,1);
								
									pfDelayMicroSec(30);
							}

							checksum ^= argData[4] ^ argData[5];

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[6]!= XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									*position = (PFsdword)argData[4];
									break;
								}
							}
#endif

#if(XLYNXCB_USE_UART == 1)
							data[3]=enXlynxCbMotorSelectServo;
							data[4]=xlynxCbMotor[*deviceId].channel;

							status = pfUart2Write(data, 7);

							timeout = pfDelaySetTimeout(XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
							
							do{
									if(pfDelayCheckTimeout(timeout))
										break;

									pfUart2GetRxBufferCount(&count);
							}while(count<7);
							
							if(count<7)
							{
								++k;
								continue;
							}

							pfUart2Read(argData, 7, &count);

							checksum ^= argData[4] ^ argData[5];

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[6]!= XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									*position = (PFsdword)argData[4];
									break;
								}
							}
#endif
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
					do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=7;
							msg.data[2]=CMD_XLYNXCB_MOTOR_GET_POSITION;
							msg.data[3]=enXlynxCbMotorSelectLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=0;//no need of checksum
							msg.data[6]=XLYNXCB_ETX;

							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
					
							timeout = pfDelaySetTimeout(XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
							
							do{
									COMM_CAN_GET_RX_BUFFER_COUNT(&count);
									
									if(pfDelayCheckTimeout(timeout))
										break;
							}while(count<2);
							
							if(count<2)
							{
								++k;
								COMM_CAN_RX_BUFFER_FLUSH();
								continue;
							}
				
							status = COMM_CAN_READ(&msg);
							if(status != enStatusSuccess)
								return status;

							if( (msg.data[0]!=XLYNXCB_STX) || (msg.data[2]!= enStatusSuccess) )
							{
								if(msg.data[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								COMM_CAN_RX_BUFFER_FLUSH();
								continue;
							}
							else
							{
								if(msg.data[3] != enStatusSuccess)
								{
									COMM_CAN_RX_BUFFER_FLUSH();
									return msg.data[3];
								}
								else
								{
									checksum ^= (msg.data[4] ^ msg.data[5]) ^ (msg.data[6] ^ msg.data[7]);
									*position = msg.data[4];
									*position |= (msg.data[5]<<8);
									*position |= (msg.data[6]<<16);
									*position |= (msg.data[7]<<24);
									
									status = COMM_CAN_READ(&msg);
									if(status != enStatusSuccess)
										return status;

									if( (msg.data[0]!=checksum) || (msg.data[1]!=XLYNXCB_ETX) )
									{
										++k;
										COMM_CAN_RX_BUFFER_FLUSH();
										continue;
									}
									else
										return enStatusSuccess;
								}
							}
#endif
							
#if(XLYNXCB_USE_SPI == 1)
							data[3]=enXlynxCbMotorSelectLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;

							pfSpi0ChipSelect(&boardId,0);
							status = pfSpi0Write(&boardId, data, 7);
							pfSpi0ChipSelect(&boardId,1);
					
							pfDelayMicroSec(XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);
							
							for(i=0; i<10; ++i)
							{
									pfSpi0ChipSelect(&boardId,0);
									pfSpi0ExchangeByte(&boardId, 0, (PFword*)&argData[i]);
									pfSpi0ChipSelect(&boardId,1);
								
									pfDelayMicroSec(30);
							}
				
							checksum ^= argData[4] ^ argData[5] ^ argData[6] ^ argData[7];

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[8] != checksum) || (argData[9]!= XLYNXCB_ETX) )
							{	
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									*position = argData[4];
									*position |= (argData[5]<<8);
									*position |= (argData[6]<<16);
									*position |= (argData[7]<<24);
									break;
								}
							}
#endif

#if(XLYNXCB_USE_UART == 1)
							data[3]=enXlynxCbMotorSelectLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;

							status = pfUart2Write(data, 7);

							timeout = pfDelaySetTimeout(XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
							
							do{
									if(pfDelayCheckTimeout(timeout))
										break;

									pfUart2GetRxBufferCount(&count);
							}while(count<10);
							
							if(count<10)
							{
								++k;
								continue;
							}

							pfUart2Read(argData, 10, &count);
				
							checksum ^= argData[4] ^ argData[5] ^ argData[6] ^ argData[7];

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[8] != checksum) || (argData[9]!= XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									*position = argData[4];
									*position |= (argData[5]<<8);
									*position |= (argData[6]<<16);
									*position |= (argData[7]<<24);
									break;
								}
							}
#endif
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
					{
						return enStatusTimeout;
					}

					break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
					do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=7;
							msg.data[2]=CMD_XLYNXCB_MOTOR_GET_POSITION;
							msg.data[3]=enXlynxCbMotorSelectSmartLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=0;//no need of checksum
							msg.data[6]=XLYNXCB_ETX;

							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
					
							timeout = pfDelaySetTimeout(XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
							
							do{
									COMM_CAN_GET_RX_BUFFER_COUNT(&count);
									
									if(pfDelayCheckTimeout(timeout))
										break;
							}while(count<2);
							
							if(count<2)
							{
								++k;
								continue;
							}
				
							status = COMM_CAN_READ(&msg);
							if(status != enStatusSuccess)
								return status;

							if( (msg.data[0]!=XLYNXCB_STX) || (msg.data[2]!= enStatusSuccess) )
							{
								if(msg.data[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								COMM_CAN_RX_BUFFER_FLUSH();
								continue;
							}
							else
							{
								if(msg.data[3] != enStatusSuccess)
								{
									COMM_CAN_RX_BUFFER_FLUSH();
									return msg.data[3];
								}
								else
								{
									checksum ^= (msg.data[4] ^ msg.data[5]) ^ (msg.data[6] ^ msg.data[7]);
									*position = msg.data[4];
									*position |= (msg.data[5]<<8);
									*position |= (msg.data[6]<<16);
									*position |= (msg.data[7]<<24);
									
									status = COMM_CAN_READ(&msg);
									if(status != enStatusSuccess)
										return status;

									if( (msg.data[0]!=checksum) || (msg.data[1]!=XLYNXCB_ETX) )
									{
										++k;
										COMM_CAN_RX_BUFFER_FLUSH();
										continue;
									}
									else
										return enStatusSuccess;
								}
							}
#endif
							
#if(XLYNXCB_USE_SPI == 1)
							data[3]=enXlynxCbMotorSelectSmartLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;

							pfSpi0ChipSelect(&boardId,0);
							status = pfSpi0Write(&boardId, data, 7);
							pfSpi0ChipSelect(&boardId,1);
					
							pfDelayMicroSec(XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
							
							for(i=0; i<10; ++i)
							{
									pfSpi0ChipSelect(&boardId,0);
									pfSpi0ExchangeByte(&boardId, 0, (PFword*)&argData[i]);
									pfSpi0ChipSelect(&boardId,1);
								
									pfDelayMicroSec(30);
							}
				
							checksum ^= argData[4] ^ argData[5] ^ argData[6] ^ argData[7];

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[8] != checksum) || (argData[9]!= XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									*position = argData[4];
									*position |= (argData[5]<<8);
									*position |= (argData[6]<<16);
									*position |= (argData[7]<<24);
									break;
								}
							}
#endif

#if(XLYNXCB_USE_UART == 1)
							data[3]=enXlynxCbMotorSelectSmartLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;

							status = pfUart2Write(data, 7);

							timeout = pfDelaySetTimeout(XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
							
							do{
									if(pfDelayCheckTimeout(timeout))
										break;

									pfUart2GetRxBufferCount(&count);
							}while(count<10);
							
							if(count<10)
							{
								++k;
								continue;
							}

							pfUart2Read(argData, 10, &count);
				
							checksum ^= argData[4] ^ argData[5] ^ argData[6] ^ argData[7];

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[8] != checksum) || (argData[9]!= XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									*position = argData[4];
									*position |= (argData[5]<<8);
									*position |= (argData[6]<<16);
									*position |= (argData[7]<<24);
									break;
								}
							}
#endif
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
	
	return enStatusSuccess;
}

PFEnStatus pfXlynxCbMotorSleep(PFbyte* deviceId, PFEnBoolean sleepState)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte data[8]={XLYNXCB_STX, 8, CMD_XLYNXCB_MOTOR_SLEEP,enXlynxCbMotorSelectLynx, 0,0,0,XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x08, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 8, CMD_XLYNXCB_MOTOR_SLEEP, enXlynxCbMotorSelectLynx, 0,0,0,XLYNXCB_ETX}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
				msg.data[4]=xlynxCbMotor[*deviceId].channel;
				msg.data[5]=sleepState;
#else
				data[4]=xlynxCbMotor[*deviceId].channel;
				data[5]=sleepState;
#endif
				do{
#if(XLYNXCB_USE_CAN == 1)
						msg.length = 8;
						status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
						if(status != enStatusSuccess)
							return status;
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
						pfSpi0ChipSelect(&boardId,0);
						status = pfSpi0Write(&boardId, data, 8);
						pfSpi0ChipSelect(&boardId,1);

						if(status != enStatusSuccess)
							return status;
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_LYNX_SLEEP_CMD_SPI_RCV_ACK_TIMEOUT);
#else
						status = pfUart2Write(data, 8);

						if(status != enStatusSuccess)
							return status;
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
						if( status != enStatusSuccess )
						{
							if(status == enStatusBoardErr)
								return enStatusBoardErr;
							
							++k;
							continue;
						}
						else
							return cmdStatus;
						
				}while(k<XLYNXCB_MAX_CMD_RETRY);
				
				if(k == XLYNXCB_MAX_CMD_RETRY)
					return enStatusTimeout;

				break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
				return enStatusNotSupported;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorSetStepClockMode(PFbyte* deviceId, PFbyte dir)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte packetData[8]={XLYNXCB_STX, 8, CMD_XLYNXCB_MOTOR_SET_STEPCLOCK_MODE, enXlynxCbMotorSelectSmartLynx, 0, 0,0, XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x08, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 8, CMD_XLYNXCB_MOTOR_SET_STEPCLOCK_MODE, enXlynxCbMotorSelectSmartLynx, 0,0,0,XLYNXCB_ETX}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
				msg.data[4]=xlynxCbMotor[*deviceId].channel;
				msg.data[5]=dir;
#else
				packetData[4]=xlynxCbMotor[*deviceId].channel;
				packetData[5]=dir;
#endif

				do{
#if(XLYNXCB_USE_CAN == 1)
						msg.length = 8;
						status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
						if(status != enStatusSuccess)
							return status;
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
						pfSpi0ChipSelect(&boardId,0);
						status = pfSpi0Write(&boardId, packetData, 8);
						pfSpi0ChipSelect(&boardId,1);

						if(status != enStatusSuccess)
							return status;
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#else
						status = pfUart2Write(packetData, 8);

						if(status != enStatusSuccess)
							return status;
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
						if( status != enStatusSuccess )
						{
							if(status == enStatusBoardErr)
								return enStatusBoardErr;
							
							++k;
							continue;
						}
						else
							return cmdStatus;
						
				}while(k<XLYNXCB_MAX_CMD_RETRY);
				
				if(k == XLYNXCB_MAX_CMD_RETRY)
					return enStatusTimeout;

				break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorTakeSingleStep(PFbyte* deviceId)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte packetData[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_TAKE_SINGLE_STEP, enXlynxCbMotorSelectSmartLynx, 0, 0, XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_TAKE_SINGLE_STEP, enXlynxCbMotorSelectSmartLynx, 0,0,XLYNXCB_ETX,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
				msg.data[4]=xlynxCbMotor[*deviceId].channel;
#else
				packetData[4]=xlynxCbMotor[*deviceId].channel;
#endif

				do{
#if(XLYNXCB_USE_CAN == 1)
						msg.length = 7;
						status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
						if(status != enStatusSuccess)
							return status;
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
						pfSpi0ChipSelect(&boardId,0);
						status = pfSpi0Write(&boardId, packetData, 7);
						pfSpi0ChipSelect(&boardId,1);

						if(status != enStatusSuccess)
							return status;
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#else
						status = pfUart2Write(packetData, 7);

						if(status != enStatusSuccess)
							return status;
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
						
						if( status != enStatusSuccess )
						{
							if(status == enStatusBoardErr)
								return enStatusBoardErr;
							
							++k;
							continue;
						}
						else
							return cmdStatus;
						
				}while(k<XLYNXCB_MAX_CMD_RETRY);
				
				if(k == XLYNXCB_MAX_CMD_RETRY)
					return enStatusTimeout;

				break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorGetSpeed(PFbyte* deviceId, PFfloat* speed)
{
	volatile PFEnStatus status;
	volatile PFbyte i=0,j=0,k=0;
	volatile PFdword count=0,timeout=0;
	volatile PFbyte dataSize=0,checksum=0,argData[10]={0};
	volatile PFbyte data[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_GET_SPEED,0, 0,0,XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_GET_SPEED, 0, 0,0,0,0}};

	union spd{
		float speed;
		PFbyte arr[4];
	}speedVar;
	
#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
					do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=7;
							msg.data[2]=CMD_XLYNXCB_MOTOR_GET_SPEED;
							msg.data[3]=enXlynxCbMotorSelectLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=0;//no need of checksum
							msg.data[6]=XLYNXCB_ETX;

							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
					
							timeout = pfDelaySetTimeout(XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
							
							do{
									COMM_CAN_GET_RX_BUFFER_COUNT(&count);
									
									if(pfDelayCheckTimeout(timeout))
										break;
							}while(count<2);
							
							if(count<2)
							{
								++k;
								continue;
							}
				
							status = COMM_CAN_READ(&msg);
							if(status != enStatusSuccess)
								return status;

							if( (msg.data[0]!=XLYNXCB_STX) || (msg.data[2]!= enStatusSuccess) )
							{
								if(msg.data[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								COMM_CAN_RX_BUFFER_FLUSH();
								continue;
							}
							else
							{
								if(msg.data[3] != enStatusSuccess)
								{
									COMM_CAN_RX_BUFFER_FLUSH();
									return msg.data[3];
								}
								else
								{
									checksum ^= (msg.data[4] ^ msg.data[5]) ^ (msg.data[6] ^ msg.data[7]);
									speedVar.arr[0] = msg.data[4];
									speedVar.arr[1] = msg.data[5];
									speedVar.arr[2] = msg.data[6];
									speedVar.arr[3] = msg.data[7];
									*speed = speedVar.speed;
									
									status = COMM_CAN_READ(&msg);
									if(status != enStatusSuccess)
										return status;

									if( (msg.data[0]!=checksum) || (msg.data[1]!=XLYNXCB_ETX) )
									{
										++k;
										COMM_CAN_RX_BUFFER_FLUSH();
										continue;
									}
									else
										return enStatusSuccess;
								}
							}
#endif
							
#if(XLYNXCB_USE_SPI == 1)
							data[3]=enXlynxCbMotorSelectLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;

							pfSpi0ChipSelect(&boardId,0);
							status = pfSpi0Write(&boardId, data, 7);
							pfSpi0ChipSelect(&boardId,1);
					
							pfDelayMicroSec(XLYNXCB_LYNX_CMD_SPI_RCV_ACK_TIMEOUT);

							pfSpi0ChipSelect(&boardId,0);
							for(i=0; i<10; ++i)
							{
									pfSpi0ExchangeByte(&boardId, 0, (PFword*)&argData[i]);
								
									pfDelayMicroSec(40);
							}
							pfSpi0ChipSelect(&boardId,1);

							checksum ^= argData[4] ^ argData[5] ^ argData[6] ^ argData[7];
							
							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[8] != checksum) || (argData[9]!= XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									speedVar.arr[0] = argData[4];
									speedVar.arr[1] = argData[5];
									speedVar.arr[2] = argData[6];
									speedVar.arr[3] = argData[7];
									*speed = speedVar.speed;
									break;
								}
							}
#endif

#if(XLYNXCB_USE_UART == 1)
							data[3]=enXlynxCbMotorSelectLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;

							status = pfUart2Write(data, 7);

							timeout = pfDelaySetTimeout(XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
							
							do{
									if(pfDelayCheckTimeout(timeout))
										break;

									pfUart2GetRxBufferCount(&count);
							}while(count<10);
							
							if(count<10)
							{
								++k;
								continue;
							}

							pfUart2Read(argData, 10, &count);

							checksum ^= argData[4] ^ argData[5] ^ argData[6] ^ argData[7];

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[8] != checksum) || (argData[9]!= XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									speedVar.arr[0] = argData[4];
									speedVar.arr[1] = argData[5];
									speedVar.arr[2] = argData[6];
									speedVar.arr[3] = argData[7];
									*speed = speedVar.speed;
									break;
								}
							}
#endif
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
					do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=7;
							msg.data[2]=CMD_XLYNXCB_MOTOR_GET_SPEED;
							msg.data[3]=enXlynxCbMotorSelectSmartLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=0;//no need of checksum
							msg.data[6]=XLYNXCB_ETX;

							msg.length = 7;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
							if(status != enStatusSuccess)
								return status;
					
							timeout = pfDelaySetTimeout(XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
							
							do{
									COMM_CAN_GET_RX_BUFFER_COUNT(&count);
									
									if(pfDelayCheckTimeout(timeout))
										break;
							}while(count<2);
							
							if(count<2)
							{
								++k;
								continue;
							}
				
							status = COMM_CAN_READ(&msg);
							if(status != enStatusSuccess)
								return status;

							if( (msg.data[0]!=XLYNXCB_STX) || (msg.data[2]!= enStatusSuccess) )
							{
								if(msg.data[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								COMM_CAN_RX_BUFFER_FLUSH();
								continue;
							}
							else
							{
								if(msg.data[3] != enStatusSuccess)
								{
									COMM_CAN_RX_BUFFER_FLUSH();
									return msg.data[3];
								}
								else
								{
									checksum ^= (msg.data[4] ^ msg.data[5]) ^ (msg.data[6] ^ msg.data[7]);
									speedVar.arr[0] = msg.data[4];
									speedVar.arr[1] = msg.data[5];
									speedVar.arr[2] = msg.data[6];
									speedVar.arr[3] = msg.data[7];
									*speed = speedVar.speed;
									
									status = COMM_CAN_READ(&msg);
									if(status != enStatusSuccess)
										return status;

									if( (msg.data[0]!=checksum) || (msg.data[1]!=XLYNXCB_ETX) )
									{
										++k;
										COMM_CAN_RX_BUFFER_FLUSH();
										continue;
									}
									else
										return enStatusSuccess;
								}
							}
#endif
							
#if(XLYNXCB_USE_SPI == 1)
							data[3]=enXlynxCbMotorSelectSmartLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;

							pfSpi0ChipSelect(&boardId,0);
							status = pfSpi0Write(&boardId, data, 7);
							pfSpi0ChipSelect(&boardId,1);
					
							pfDelayMicroSec(XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);

							for(i=0; i<10; ++i)
							{
									pfSpi0ChipSelect(&boardId,0);
									pfSpi0ExchangeByte(&boardId, 0, (PFword*)&argData[i]);
									pfSpi0ChipSelect(&boardId,1);
								
									pfDelayMicroSec(30);
							}

							checksum ^= argData[4] ^ argData[5] ^ argData[6] ^ argData[7];

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[8] != checksum) || (argData[9]!= XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									speedVar.arr[0] = argData[4];
									speedVar.arr[1] = argData[5];
									speedVar.arr[2] = argData[6];
									speedVar.arr[3] = argData[7];
									*speed = speedVar.speed;
									break;
								}
							}
#endif

#if(XLYNXCB_USE_UART == 1)
							data[3]=enXlynxCbMotorSelectSmartLynx;
							data[4]=xlynxCbMotor[*deviceId].channel;

							status = pfUart2Write(data, 7);

							timeout = pfDelaySetTimeout(XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
							
							do{
									if(pfDelayCheckTimeout(timeout))
										break;

									pfUart2GetRxBufferCount(&count);
							}while(count<10);
							
							if(count<10)
							{
								++k;
								continue;
							}

							pfUart2Read(argData, 10, &count);

							checksum ^= argData[4] ^ argData[5] ^ argData[6] ^ argData[7];

							if( (argData[0]!=XLYNXCB_STX) || (argData[2]!= enStatusSuccess) || (argData[8] != checksum) || (argData[9]!= XLYNXCB_ETX) )
							{
								if(argData[2] == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
							{
								if(argData[3] != enStatusSuccess)
								{
									return argData[3];
								}
								else
								{
									speedVar.arr[0] = argData[4];
									speedVar.arr[1] = argData[5];
									speedVar.arr[2] = argData[6];
									speedVar.arr[3] = argData[7];
									*speed = speedVar.speed;
									break;
								}
							}
#endif
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
	
	return enStatusSuccess;
}

PFEnStatus pfXlynxCbMotorGoMark(PFbyte* deviceId)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte data[7]={XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_GOMARK,enXlynxCbMotorSelectSmartLynx, 0,0,XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x07, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 7, CMD_XLYNXCB_MOTOR_GOMARK, enXlynxCbMotorSelectSmartLynx, 0,0,XLYNXCB_ETX,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
#if(XLYNXCB_USE_CAN == 1)
				msg.data[4]=xlynxCbMotor[*deviceId].channel;
#else
				data[4]=xlynxCbMotor[*deviceId].channel;
#endif

				do{
#if(XLYNXCB_USE_CAN == 1)
						msg.length = 7;
						status = COMM_CAN_WRITE(&msgHeader, msg.data, 7);
						if(status != enStatusSuccess)
							return status;
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#elif(XLYNXCB_USE_SPI == 1)
						pfSpi0ChipSelect(&boardId,0);
						status = pfSpi0Write(&boardId, data, 7);
						pfSpi0ChipSelect(&boardId,1);
						
						if(status != enStatusSuccess)
							return status;
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#else
						status = pfUart2Write(data, 7);
						
						if(status != enStatusSuccess)
							return status;
//reply			
						status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
						if( status != enStatusSuccess )
						{
							if(status == enStatusBoardErr)
								return enStatusBoardErr;
							
							++k;
							continue;
						}
						else
							return cmdStatus;
						
				}while(k<XLYNXCB_MAX_CMD_RETRY);
				
				if(k == XLYNXCB_MAX_CMD_RETRY)
					return enStatusTimeout;

				break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
	
	return enStatusSuccess;
}

PFEnStatus pfXlynxCbMotorGoUntil(PFbyte* deviceId, PFEnL6470actionForSwitchPress act, PFbyte dir, PFfloat* speed)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0,checksum=0;
	volatile PFbyte data[13]={XLYNXCB_STX, 13, CMD_XLYNXCB_MOTOR_GOUNTIL,enXlynxCbMotorSelectSmartLynx, 0,0,0,0 ,0,0,0,0 ,XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x08, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 13, CMD_XLYNXCB_MOTOR_GOUNTIL, 0, 0,0,0,0}};

	union spd{
		float speed;
		PFbyte arr[4];
	}speedVar;

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
					speedVar.speed=*speed;
				 
				  do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0] = XLYNXCB_STX;
							msg.data[1]=13;
							msg.data[2]=CMD_XLYNXCB_MOTOR_GOUNTIL;
							msg.data[3]=enXlynxCbMotorSelectSmartLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=act;
							msg.data[6]=dir;
							msg.data[7]=speedVar.arr[0];
							
							msg.length=8;
							status =COMM_CAN_WRITE(&msgHeader, msg.data, 8);
							if(status != enStatusSuccess)
								return status;
				
							msg.data[0]=speedVar.arr[1];
							msg.data[1]=speedVar.arr[2];
							msg.data[2]=speedVar.arr[3];
							
							checksum ^= msg.data[5] ^ msg.data[6];
							checksum ^= (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]);
							
							msg.data[3]=checksum;
							msg.data[4]=XLYNXCB_ETX;

							msg.length=5;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 5);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#endif
							
#if(XLYNXCB_USE_SPI == 1)
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=act;
							data[6]=dir;
							data[7]=speedVar.arr[0];				
							data[8]=speedVar.arr[1];
							data[9]=speedVar.arr[2];
							data[10]=speedVar.arr[3];
							
							checksum ^= data[5] ^ data[6];
							checksum ^= (data[7] ^ data[8]) ^ (data[9] ^ data[10]);
							
							data[11]=checksum;

							pfSpi0ChipSelect(&boardId,0);
							status = pfSpi0Write(&boardId, data, 13);
							pfSpi0ChipSelect(&boardId,1);
							
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_UART == 1)
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=act;
							data[6]=dir;
							data[7]=speedVar.arr[0];				
							data[8]=speedVar.arr[1];
							data[9]=speedVar.arr[2];
							data[10]=speedVar.arr[3];
							
							checksum ^= data[5] ^ data[6];
							checksum ^= (data[7] ^ data[8]) ^ (data[9] ^ data[10]);
							
							data[11]=checksum;

							status = pfUart2Write(data, 13);
							
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
								return cmdStatus;
						
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorReleseSW(PFbyte* deviceId, PFEnL6470actionForSwitchPress act, PFbyte dir)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0;
	volatile PFbyte dataSize=0,checksum=0;
	volatile PFbyte data[9]={XLYNXCB_STX, 9, CMD_XLYNXCB_MOTOR_RELEASE_SW,enXlynxCbMotorSelectSmartLynx, 0,0,0,0 ,XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x08, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 9, CMD_XLYNXCB_MOTOR_RELEASE_SW, 0, 0,0,0,0}};

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
					do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0] = XLYNXCB_STX;
							msg.data[1]=9;
							msg.data[2]=CMD_XLYNXCB_MOTOR_RELEASE_SW;
							msg.data[3]=enXlynxCbMotorSelectSmartLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=act;
							msg.data[6]=dir;
							checksum ^= msg.data[5] ^ msg.data[6];
							msg.data[7]=checksum;
							
							msg.length=8;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
							if(status != enStatusSuccess)
								return status;
				
							msg.data[0]=XLYNXCB_ETX;
							msg.length=1;
							status = COMM_CAN_WRITE(&msgHeader, msg.data, 1);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_SPI == 1)
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=act;
							data[6]=dir;
							checksum ^= data[5] ^ data[6];
							data[7]=checksum;

							pfSpi0ChipSelect(&boardId,0);
							status = pfSpi0Write(&boardId, data, 9);
							pfSpi0ChipSelect(&boardId,1);

							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_UART == 1)
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=act;
							data[6]=dir;
							checksum ^= data[5] ^ data[6];
							data[7]=checksum;

							status = pfUart2Write(data, 9);

							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
								return cmdStatus;
						
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

PFEnStatus pfXlynxCbMotorSetMinimumSpeed(PFbyte* deviceId, PFfloat* speed)
{
	volatile PFEnStatus status,cmdStatus;
	volatile PFbyte j=0,k=0,checksum=0;
	volatile PFbyte dataSize=0;
	volatile PFbyte data[11]={XLYNXCB_STX, 11, CMD_XLYNXCB_MOTOR_SET_MIN_SPEED,enXlynxCbMotorSelectSmartLynx, 0,0,0,0, 0,0,XLYNXCB_ETX};
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x08, enCanFrameStandard, enBooleanFalse, {XLYNXCB_STX, 11, CMD_XLYNXCB_MOTOR_SET_MIN_SPEED, 0, 0,0,0,0}};

	union spd{
		float speed;
		PFbyte arr[4];
	}speedVar;

#ifdef PF_XLYNXCB_DEBUG
	if(deviceId == NULL)
		return enStatusInvArgs;
	if(xlynxCbMotorInitFlag[*deviceId] == enBooleanFalse)
		return enStatusNotConfigured;
	if(*deviceId >= XLYNXCB_MAX_MOTOR_SUPPORTED)
		return enStatusInvArgs;
#endif	// PF_XLYNXCB_DEBUG

	switch(xlynxCbMotor[*deviceId].motorType)
	{
		case enXlynxCbMotorSelectServo : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectLynx : 
			 {
				return enStatusNotSupported;
			 }

		case enXlynxCbMotorSelectSmartLynx : 
			 {
					speedVar.speed = *speed;
				 
					do{
							checksum=0;
#if(XLYNXCB_USE_CAN == 1)
							msg.data[0]=XLYNXCB_STX;
							msg.data[1]=11;
							msg.data[2]=CMD_XLYNXCB_MOTOR_SET_MIN_SPEED;
							msg.data[3]=enXlynxCbMotorSelectSmartLynx;
							msg.data[4]=xlynxCbMotor[*deviceId].channel;
							msg.data[5]=speedVar.arr[0];
							msg.data[6]=speedVar.arr[1];
							msg.data[7]=speedVar.arr[2];
							msg.length=8;

							status = COMM_CAN_WRITE(&msgHeader, msg.data, 8);
							if(status != enStatusSuccess)
								return status;

							msg.data[0]=speedVar.arr[3];
							msg.data[1]= checksum ^ (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]);//checksum
							msg.data[2]=XLYNXCB_ETX;
							msg.length=3;

							status = COMM_CAN_WRITE(&msgHeader, msg.data, 3);
							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_CAN_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_SPI == 1)
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=speedVar.arr[0];
							data[6]=speedVar.arr[1];
							data[7]=speedVar.arr[2];
							data[8]=speedVar.arr[3];
							data[9]= checksum ^ (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]);//checksum

							pfSpi0ChipSelect(&boardId,0);
							status = pfSpi0Write(&boardId, data, 11);
							pfSpi0ChipSelect(&boardId,1);

							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT);
#endif

#if(XLYNXCB_USE_UART == 1)
							data[4]=xlynxCbMotor[*deviceId].channel;
							data[5]=speedVar.arr[0];
							data[6]=speedVar.arr[1];
							data[7]=speedVar.arr[2];
							data[8]=speedVar.arr[3];
							data[9]= checksum ^ (speedVar.arr[0] ^ speedVar.arr[1]) ^ (speedVar.arr[2] ^ speedVar.arr[3]);//checksum

							status = pfUart2Write(data, 11);

							if(status != enStatusSuccess)
								return status;
//reply			
							status = pfReceiveAck(&cmdStatus,XLYNXCB_CMD_UART_RCV_ACK_TIMEOUT);
#endif
							if( status != enStatusSuccess )
							{
								if(status == enStatusBoardErr)
									return enStatusBoardErr;
							
								++k;
								continue;
							}
							else
								return cmdStatus;
						
					}while(k<XLYNXCB_MAX_CMD_RETRY);
				
					if(k == XLYNXCB_MAX_CMD_RETRY)
						return enStatusTimeout;

					break;
			 }
			 
		default :
			return enStatusInvArgs;
	}
}

static PFEnStatus pfReceiveAck(PFEnStatus* cmdStatus, PFdword timeout)
{
	volatile PFdword j=0,temp=0;
	volatile PFEnStatus status;
	volatile PFbyte i=0, k=0, data[6]=0;
	volatile PFdword count=0;
	volatile PFCanMessage msg = {XLYNXCB_CAN_MSG_ID, 0x06, enCanFrameStandard, enBooleanFalse, {0, 0, 0, 0, 0,0,0,0}};

#if(XLYNXCB_USE_CAN == 1)
	temp = pfDelaySetTimeout(timeout);
	
	do{
		COMM_CAN_GET_RX_BUFFER_COUNT(&count);
					
		if(pfDelayCheckTimeout(temp))
			break;
	}while(count<1);
	
	if(count<1)
		return enStatusError;

	status = COMM_CAN_READ(&msg);
	if(status != enStatusSuccess)
		return status;

	if( (msg.data[0]!=XLYNXCB_STX) || (msg.data[5]!=XLYNXCB_ETX) || (msg.data[2]!=enStatusSuccess) )
	{
		COMM_CAN_RX_BUFFER_FLUSH();
		if(msg.data[2] == enStatusBoardErr)
			return enStatusBoardErr;
		else
			return enStatusError;
	}
	else
	{
		*cmdStatus = msg.data[3];
		return enStatusSuccess;
	}
#endif

#if(XLYNXCB_USE_SPI == 1)
	pfDelayMicroSec(timeout);

	for(i=0; i<6; ++i)
	{
			pfSpi0ChipSelect(&boardId,0);
			pfSpi0ExchangeByte(&boardId, 0, (PFword*)&data[i]);
			pfSpi0ChipSelect(&boardId,1);

 			pfDelayMicroSec(30);
	}

	if( (data[0]!=XLYNXCB_STX) || (data[5]!=XLYNXCB_ETX) || (data[2]!=enStatusSuccess) )
	{
		if(data[2] == enStatusBoardErr)
			return enStatusBoardErr;
		else
			return enStatusError;
	}
	else
	{
		*cmdStatus = data[3];
		return enStatusSuccess;
	}
#endif

#if(XLYNXCB_USE_UART == 1)

	temp = pfDelaySetTimeout(timeout);
	
	do{
			if(pfDelayCheckTimeout(temp))
				break;

			pfUart2GetRxBufferCount(&count);
	}while(count<6);
	
	if(count<6)
		return enStatusError;

	pfUart2Read(data, 6, &count);
	
	if( (data[0]!=XLYNXCB_STX) || (data[5]!=XLYNXCB_ETX) || (data[2]!=enStatusSuccess) )
	{
		if(data[2] == enStatusBoardErr)
			return enStatusBoardErr;
		else
			return enStatusError;
	}
	else
	{
		*cmdStatus = data[3];
		return enStatusSuccess;
	}
#endif
	
}

