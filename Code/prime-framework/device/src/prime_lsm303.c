#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_lsm303.h"
#include <math.h>

static PFEnStatus pfLSM303_ACC_MAG_WriteReg(PFbyte* devId,PFbyte slaveAddr, PFbyte regAddr, PFbyte writeValue);
static PFEnStatus pfLSM303_ACC_MAG_ReadReg(PFbyte* devId,PFbyte slaveAddr, PFbyte regAddr, PFbyte* readValue);


static PFfloat lsm303_acc_mag_scaleMagArr_M[]={ 0.9478, 1.2578, 1.5748, 2.3255, 2.6667, 3.125, 4.3478 };
static PFbyte  lsm303_acc_mag_gainIndex_M=0;
static PFEnLSM303_ACC_MAG_mode_M     lsm303_acc_mag_mode_M;
static PFEnLSM303_ACC_MAG_range_A    lsm303_acc_mag_accelerometerRang;

static PFEnBoolean lsm303InitFlag[PF_MAX_LSM303_SUPPORTED] = {enBooleanFalse};  /** array to store initialization status */
static PFCfgLSM303_ACC_MAG lsm303Config[PF_MAX_LSM303_SUPPORTED];      /** array to store configuration structures of VCNL4000 devices connected */
static PFbyte lsm303DeviceCount=0;

PFEnStatus pfLSM303_ACC_MAG_open(PFbyte* devId,PPFCfgLSM303_ACC_MAG sensorConfig,PFbyte deviceCount)
{
    PFbyte regVal=0,i,j;
    PFEnStatus stat;

	#if (PF_LSM303_DEBUG == 1)
		if(devId == NULL)
			return enStatusInvArgs;
		if(sensorConfig == NULL)
			return enStatusInvArgs;
		if(deviceCount == 0 || deviceCount > PF_MAX_LSM303_SUPPORTED)
			return enStatusInvArgs;
	#endif //#if (PF_LSM303_DEBUG == 1)
	
	for(i=0; i<deviceCount; ++i)
	{
		for(j=0; j<PF_MAX_LSM303_SUPPORTED; ++j)
		{
			if( lsm303InitFlag[j] == enBooleanFalse )
			{
				devId[i]=j;
				pfMemCopy(&lsm303Config[j], &sensorConfig[i], sizeof(PFCfgLSM303_ACC_MAG));	
			/** magnetometer initialization */
				if( (sensorConfig[i].bias_M>enLSM303_ACC_MAG_NegetiveBias)  || (sensorConfig[i].dataRate_M>enLSM303_ACC_MAG_DataRate_75_0))
					return enStatusInvArgs;
				else
				{
					regVal= sensorConfig[i].bias_M | (sensorConfig[i].dataRate_M<<2);
					stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_MAG, LSM303_ACC_MAG_CRA_REG_M, regVal);
					if(stat!=enStatusSuccess)
						return stat;
				}

				if(sensorConfig[i].gain_M>7)
					return enStatusInvArgs;
				else
				{
					regVal = sensorConfig[i].gain_M<<5;
					stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_MAG, LSM303_ACC_MAG_CRB_REG_M,regVal);
					if(stat!=enStatusSuccess)
						return stat;

					lsm303_acc_mag_gainIndex_M=sensorConfig[i].gain_M-1;
				}

				if( sensorConfig[i].mode_M>enLSM303_ACC_MAG_ModeIdle )
					return enStatusInvArgs;
				else
				{
					regVal = sensorConfig[i].mode_M;
					stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_MAG, LSM303_ACC_MAG_MR_REG_M,regVal);
					if(stat!=enStatusSuccess)
						return stat;

					lsm303_acc_mag_mode_M=sensorConfig[i].mode_M;
				}

			/** Accelerometer initialization */
				if( (sensorConfig[i].powerMode_A>enLSM303_ACC_MAG_lowPowerMode_ODR_10Hz) ||	(sensorConfig[i].dataRate_A>enLSM303_ACC_MAG_dataRate_1000Hz) || (sensorConfig[i].compSelec_A>enLSM303_ACC_MAG_Component_XYZ) )
					return enStatusInvArgs;
				else
				{
					regVal = (sensorConfig[i].powerMode_A<<5) | (sensorConfig[i].dataRate_A<<3) | (sensorConfig[i].compSelec_A);

					stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_CTRL_REG1_A,regVal);
					if(stat!=enStatusSuccess)
						return stat;
				}

				if( (sensorConfig[i].hpfMode_A>enLSM303_ACC_MAG_hpfMode_Ref) || \
						(sensorConfig[i].cutOff_Freq_A>enLSM303_ACC_MAG_hpfCutOffFreq_64) )
					return enStatusInvArgs;
				else
				{
					regVal = (sensorConfig[i].hpfMode_A<<5) | (sensorConfig[i].cutOff_Freq_A);

					if(LSM303_ACC_MAG_RebootInternalRegisters_A__)
					regVal |= 0x80;

					if(LSM303_ACC_MAG_FitleredDataSelection_A__)
					regVal |= 0x10;

					if(LSM303_ACC_MAG_HighPassFilterForInt2_A__)
					regVal |= 0x08;

					if(LSM303_ACC_MAG_HighPassFilterForInt1_A__)
					regVal |= 0x04;

					stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_CTRL_REG2_A,regVal);
					if(stat!=enStatusSuccess)
					return stat;
				}

				if( (sensorConfig[i].sourceINT1_A>enLSM303_ACC_MAG_int1_BootRunning) || \
						(sensorConfig[i].sourceINT2_A>enLSM303_ACC_MAG_int2_BootRunning) )
					return enStatusInvArgs;
				else
				{
					regVal = sensorConfig[i].sourceINT1_A | (sensorConfig[i].sourceINT2_A<<3);

					if(LSM303_ACC_MAG_ConfigIntActiveLow_A__)
					regVal |= 0x80;

					if(LSM303_ACC_MAG_LatchInterruptRequestOnInt2_A__)
					regVal |= 0x20;

					if(LSM303_ACC_MAG_LatchInterruptRequestOnInt1_A__)
					regVal |= 0x04;

					stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_CTRL_REG3_A,regVal);
					if(stat!=enStatusSuccess)
					return stat;
				}

				if(sensorConfig[i].range_A>enLSM303_ACC_MAG_range_8g) 
					return enStatusInvArgs;
				else
				{
					regVal = sensorConfig[i].range_A<<4;

					lsm303_acc_mag_accelerometerRang = sensorConfig[i].range_A;

					if(LSM303_ACC_MAG_BlockDataUpdate_A__)
					regVal |= 0x80;

					stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_CTRL_REG4_A,regVal);
					if(stat!=enStatusSuccess)
					return stat;
				}

				if(sensorConfig[i].configInt1_Source_A>enLSM303_ACC_MAG_sixDirPosRecogntion)
					return enStatusInvArgs;
				else
				{
					regVal = sensorConfig[i].configInt1_Source_A<<6;

					regVal |= sensorConfig[i].intEnable_onInt1_A;

					stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_INT1_CFG_A,regVal);
					if(stat!=enStatusSuccess)
						return stat;
				}

				stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_INT1_THS_A, LSM303_ACC_MAG_Int1ThresholdValue_A__);
				if(stat!=enStatusSuccess)
				return stat;

				stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_INT1_DURATION_A, LSM303_ACC_MAG_Int1ValidationDuration_A__);
				if(stat!=enStatusSuccess)
					return stat;

				if( (sensorConfig[i].configInt2_Source_A>enLSM303_ACC_MAG_sixDirPosRecogntion) )
					return enStatusInvArgs;
				else
				{
					regVal = sensorConfig[i].configInt2_Source_A<<6;

					regVal |= sensorConfig[i].intEnable_onInt2_A;

			stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_INT2_CFG_A,regVal);
					if(stat!=enStatusSuccess)
						return stat;
				}

				stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_INT2_THS_A, LSM303_ACC_MAG_Int2ThresholdValue_A__);
				if(stat!=enStatusSuccess)
					return stat;

				stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_INT2_DURATION_A, LSM303_ACC_MAG_Int2ValidationDuration_A__);
				if(stat!=enStatusSuccess)
					return stat;

				stat=pfLSM303_ACC_MAG_WriteReg(&devId[i],LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_REFERENCE_A, LSM303_ACC_MAG_HighPassFilterReferenceSelection_A__);
				if(stat!=enStatusSuccess)
					return stat;

				lsm303InitFlag[j]=enBooleanTrue;
				++lsm303DeviceCount;
					break;
			}
			
		if(j == PF_MAX_LSM303_SUPPORTED)
			return enStatusError;
		}
	}			
    return enStatusSuccess;
}

PFEnStatus pfLSM303_ACC_MAG_close(PFbyte* devId)
{
	#if (PF_LSM303_DEBUG == 1)
		if(*devId > PF_MAX_LSM303_SUPPORTED)
			return enStatusInvArgs;
		if(lsm303InitFlag[*devId] != enBooleanTrue)
			return enStatusNotConfigured;
	#endif  //#if (PF_LSM303_DEBUG == 1)
	
	lsm303InitFlag[*devId] = enBooleanFalse;
	--lsm303DeviceCount;

	return enStatusSuccess;
}


PFEnStatus pfLSM303_ACC_MAG_getID(PFbyte* devId,PFbyte* ida, PFbyte* idb, PFbyte* idc)
{
    PFEnStatus ret;
		
		#if (PF_LSM303_DEBUG == 1)
			if(ida == NULL)
				return enStatusInvArgs;
			if(idb ==  NULL)
				return enStatusInvArgs;
			if(idc ==  NULL)
				return enStatusInvArgs;
		#endif //#if (PF_LSM303_DEBUG == 1)
	
    if( lsm303InitFlag[*devId] == enBooleanFalse )
		return enStatusNotConfigured;

    ret=pfLSM303_ACC_MAG_ReadReg(devId,LSM303_ACC_MAG_SLAVE_ADDR_MAG, LSM303_ACC_MAG_IRA_REG_M, ida);
    if(ret!=enStatusSuccess)
		return ret;

    ret=pfLSM303_ACC_MAG_ReadReg(devId,LSM303_ACC_MAG_SLAVE_ADDR_MAG, LSM303_ACC_MAG_IRB_REG_M, idb);
    if(ret!=enStatusSuccess)
		return ret;

    ret=pfLSM303_ACC_MAG_ReadReg(devId,LSM303_ACC_MAG_SLAVE_ADDR_MAG, LSM303_ACC_MAG_IRC_REG_M, idc);
    if(ret!=enStatusSuccess)
		return ret;

    return enStatusSuccess;
}

PFEnStatus pfLSM303_ACC_MAG_readAccelerometer(PFbyte* devId,PFword* Xcomp, PFword* Ycomp, PFword* Zcomp)
{
    PFEnStatus stat;
    PFbyte dataBuf[6]={0};
    PFbyte temp=0,i=0;

		#if (PF_LSM303_DEBUG == 1)
			if(devId == NULL)
				return enStatusInvArgs;
			if(Xcomp == NULL)
				return enStatusInvArgs;
			if(Ycomp ==  NULL)
				return enStatusInvArgs;
			if(Zcomp ==  NULL)
				return enStatusInvArgs;
		#endif //#if (PF_LSM303_DEBUG == 1)

    if( lsm303InitFlag[*devId] == enBooleanFalse )
		return enStatusNotConfigured;

    /** poll for data ready bit set */
    stat=pfLSM303_ACC_MAG_ReadReg(devId,LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_STATUS_REG_A, &temp);
    if(stat!=enStatusSuccess)
		return stat;

    while( !(temp & 0x0f) )
    {
        pfLSM303_ACC_MAG_ReadReg(devId,LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_STATUS_REG_A, &temp);
    }

    while(i<6)
    {
        stat=pfLSM303_ACC_MAG_ReadReg(devId,LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_OUT_X_L_A+i, &dataBuf[i]);
        if(stat!=enStatusSuccess)
			return stat;

        ++i;
    }

/** as result is 12bit left justifies data need to shift right by 4  */
    *Xcomp= (PFword)( ( (dataBuf[1]<<8) | dataBuf[0] )>>4 );        //copy higher and lower data register of X
    *Ycomp= (PFword)( ( (dataBuf[3]<<8) | dataBuf[2] )>>4 );        //copy higher and lower data register of Y
    *Zcomp= (PFword)( ( (dataBuf[5]<<8) | dataBuf[4] )>>4 );        //copy higher and lower data register of Z

    return enStatusSuccess;
}

PFEnStatus pfLSM303_ACC_MAG_getINT1source(PFbyte* devId,PFEnLSM303_ACC_MAG_intComponent_A intSource[])
{
     PFEnStatus ret;
     PFEnLSM303_ACC_MAG_intComponent_A i=enLSM303_ACC_MAG_int_X_L,j=enLSM303_ACC_MAG_intNone,source=enLSM303_ACC_MAG_intNone;
	
	#if (PF_LSM303_DEBUG == 1)
		if(devId == NULL)
			return enStatusInvArgs;
	#endif //#if (PF_LSM303_DEBUG == 1)
		
    if( lsm303InitFlag[*devId] == enBooleanFalse )
		return enStatusNotConfigured;

    ret=pfLSM303_ACC_MAG_ReadReg(devId,LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_INT1_SRC_A, (PFbyte*)&source);
    if(ret!=enStatusSuccess)
		return ret;

    /** to find int source */
    while( i<=6 )
    {
        if( source & (1 << (i-1) ) )
        {
            intSource[j] = enLSM303_ACC_MAG_int_X_L << (enLSM303_ACC_MAG_int_X_L-1);
            ++j;
        }

        ++i;
    }

    while(j<6)
    {
        intSource[j]=enLSM303_ACC_MAG_intNone;
        ++j;
    }

    return enStatusSuccess;
}

PFEnStatus pfLSM303_ACC_MAG_getINT2source(PFbyte* devId,PFEnLSM303_ACC_MAG_intComponent_A intSource[])
{
    PFEnStatus ret;
    PFEnLSM303_ACC_MAG_intComponent_A i=enLSM303_ACC_MAG_int_X_L,j=enLSM303_ACC_MAG_intNone,source=enLSM303_ACC_MAG_intNone;
	
		#if (PF_LSM303_DEBUG == 1)
			if(devId == NULL)
				return enStatusInvArgs;
	#endif //#if (PF_LSM303_DEBUG == 1)
  
    if( lsm303InitFlag[*devId] == enBooleanFalse )
		return enStatusNotConfigured;

    ret=pfLSM303_ACC_MAG_ReadReg(devId,LSM303_ACC_MAG_SLAVE_ADDR_ACC, LSM303_ACC_MAG_INT2_SRC_A, (PFbyte*)&source);
    if(ret!=enStatusSuccess)
    return ret;

    /** to find int source */
    while( i<=6 )
    {
        if( source & (1 << (i-1) ) )
        {
            intSource[j] = enLSM303_ACC_MAG_int_X_L << (i-enLSM303_ACC_MAG_int_X_L);
            ++j;
        }

        ++i;
    }

    while(j<6)
    {
        intSource[j]=enLSM303_ACC_MAG_intNone;
        ++j;
    }

    return enStatusSuccess;
}

PFEnStatus pfLSM303_ACC_MAG_getAcceleration(PFbyte* devId,PFword* Xcomp, PFword* Ycomp, PFword* Zcomp)
{
    PFEnStatus stat;
    PFword xcomp=0,ycomp=0,zcomp=0;
    PFfloat scaleFactor;
	
	#if (PF_LSM303_DEBUG == 1)
		if(devId == NULL)
			return enStatusInvArgs;
		if(Xcomp == NULL)
			return enStatusInvArgs;
		if(Ycomp ==  NULL)
			return enStatusInvArgs;
		if(Zcomp ==  NULL)
			return enStatusInvArgs;
		#endif //#if (PF_LSM303_DEBUG == 1)

    if( lsm303InitFlag[*devId] == enBooleanFalse )
		return enStatusNotConfigured;

    if(lsm303_acc_mag_accelerometerRang == enLSM303_ACC_MAG_range_2g)
        scaleFactor=1.0;
    else
    {
        if(lsm303_acc_mag_accelerometerRang == enLSM303_ACC_MAG_range_4g)
            scaleFactor=2.0;
        else
            scaleFactor=3.9;
    }

    stat=pfLSM303_ACC_MAG_readAccelerometer(devId,&xcomp,&ycomp,&zcomp);
    if(stat!=enStatusSuccess)
    return stat;

    *Xcomp = (PFword)( (PFfloat)(xcomp) * scaleFactor);
    *Ycomp = (PFword)( (PFfloat)(ycomp) * scaleFactor);
    *Zcomp = (PFword)( (PFfloat)(zcomp) * scaleFactor);

    return enStatusSuccess;
}

PFEnStatus pfLSM303_ACC_MAG_readMagnetometer(PFbyte* devId,PFword* Xcomp, PFword* Ycomp, PFword* Zcomp)
{
    PFEnStatus stat;
    PFbyte dataBuf[6]={0};
    PFbyte temp=0;
    PFword i,j,readBytes=0;

	#if (PF_LSM303_DEBUG == 1)
		if(devId == NULL)
			return enStatusInvArgs;
		if(Xcomp == NULL)
			return enStatusInvArgs;
		if(Ycomp ==  NULL)
			return enStatusInvArgs;
		if(Zcomp ==  NULL)
			return enStatusInvArgs;
	#endif //#if (PF_LSM303_DEBUG == 1)


    if( lsm303InitFlag[*devId] == enBooleanFalse )
		return enStatusNotConfigured;

    if(lsm303_acc_mag_mode_M == enLSM303_ACC_MAG_ModeSingle)
    {
        stat=pfLSM303_ACC_MAG_WriteReg(devId,LSM303_ACC_MAG_SLAVE_ADDR_MAG, LSM303_ACC_MAG_MR_REG_M,lsm303_acc_mag_mode_M);
        if(stat!=enStatusSuccess)
        return stat;
    }

/** delay to allow conversion complete */
    for(j=0;j<6;++j)
    {    for(i=0;i<325;++i); }

    stat=pfLSM303_ACC_MAG_ReadReg(devId,LSM303_ACC_MAG_SLAVE_ADDR_MAG, LSM303_ACC_MAG_SR_REG_M, &temp);
    if(stat!=enStatusSuccess)
		return stat;

    while( !(temp & 0x01) )
    {
        pfLSM303_ACC_MAG_ReadReg(devId,LSM303_ACC_MAG_SLAVE_ADDR_MAG, LSM303_ACC_MAG_SR_REG_M, &temp);
    }

    /** point to data reg X msb */
    temp=LSM303_ACC_MAG_OUT_X_H_M;

    /** 0x3C regAddr  to set pointer to reg with addresses regAddr */
    stat=lsm303Config[*devId].pfI2cWrite(&temp, enBooleanTrue, LSM303_ACC_MAG_SLAVE_ADDR_MAG, 1);
    if(stat!=enStatusSuccess)
    return stat;

    /** read 6 data registers */
    stat=lsm303Config[*devId].pfI2cRead(dataBuf, enBooleanTrue, LSM303_ACC_MAG_SLAVE_ADDR_MAG, 6, &readBytes);
    if(stat!=enStatusSuccess)
    return stat;

    if(readBytes!=6)
        return enStatusError;

    *Xcomp= (dataBuf[0]<<8) | dataBuf[1];    //copy higher and lower data register of X
    *Ycomp= (dataBuf[2]<<8) | dataBuf[3];    //copy higher and lower data register of Y
    *Zcomp= (dataBuf[4]<<8) | dataBuf[5];    //copy higher and lower data register of Z

    return enStatusSuccess;
}

PFEnStatus pfLSM303_ACC_MAG_getMagnetism(PFbyte* devId,PFword* Xcomp, PFword* Ycomp, PFword* Zcomp)
{
    PFEnStatus stat;
    PFword xcomp=0,ycomp=0,zcomp=0;
	
		#if (PF_LSM303_DEBUG == 1)
			if(devId == NULL)
				return enStatusInvArgs;
			if(Xcomp == NULL)
				return enStatusInvArgs;
			if(Ycomp ==  NULL)
				return enStatusInvArgs;
			if(Zcomp ==  NULL)
				return enStatusInvArgs;
		#endif //#if (PF_LSM303_DEBUG == 1)

    if( lsm303InitFlag[*devId] == enBooleanFalse )
		return enStatusNotConfigured;

    stat=pfLSM303_ACC_MAG_readMagnetometer(devId,&xcomp, &ycomp, &zcomp);
    if(stat!=enStatusSuccess)
    return stat;

    *Xcomp = (PFword)( (PFfloat)xcomp * lsm303_acc_mag_scaleMagArr_M[lsm303_acc_mag_gainIndex_M] );
    *Ycomp = (PFword)( (PFfloat)ycomp * lsm303_acc_mag_scaleMagArr_M[lsm303_acc_mag_gainIndex_M] );
    *Zcomp = (PFword)( (PFfloat)zcomp * lsm303_acc_mag_scaleMagArr_M[lsm303_acc_mag_gainIndex_M] );

    return enStatusSuccess;
}

PFEnStatus pfLSM303_ACC_MAG_getHeadingAngle(PFbyte* devId,PFword* headingAngle)
{
    PFEnStatus stat;
    PFword xcomp=0,ycomp=0,zcomp=0;
    double cal=0.0;
	
		#if (PF_LSM303_DEBUG == 1)
			if(devId == NULL)
				return enStatusInvArgs;
		#endif //#if (PF_LSM303_DEBUG == 1)
	
	
    if( lsm303InitFlag[*devId] == enBooleanFalse )
		return enStatusNotConfigured;
    stat=pfLSM303_ACC_MAG_readMagnetometer(devId,&xcomp, &ycomp, &zcomp);
    if(stat!=enStatusSuccess)
    return stat;

    cal= atan2( (double)(xcomp), (double)(ycomp) );
    cal= ( ( cal * 180.0 ) / M_PI ) + 180;
    *headingAngle=(PFword)cal;

    return enStatusSuccess;
}

static PFEnStatus pfLSM303_ACC_MAG_WriteReg(PFbyte* devId,PFbyte slaveAddr, PFbyte regAddr, PFbyte writeValue)
{
    PFEnStatus ret;
    PFbyte writeBuf[2]={0};

		#if (PF_LSM303_DEBUG == 1)
			if(devId == NULL)
				return enStatusInvArgs;
		#endif //#if (PF_LSM303_DEBUG == 1)

    writeBuf[0]=regAddr;
    writeBuf[1]=writeValue;

    ret=lsm303Config[*devId].pfI2cWrite(writeBuf, enBooleanTrue, slaveAddr, 2);
    if(ret!=enStatusSuccess)
        return ret;

    return enStatusSuccess;
}

static PFEnStatus pfLSM303_ACC_MAG_ReadReg(PFbyte* devId,PFbyte slaveAddr, PFbyte regAddr, PFbyte* readValue)
{
    PFEnStatus ret;
    PFbyte writeVal=0;
    PFword readBytes=0;

		#if (PF_LSM303_DEBUG == 1)
			if(devId == NULL)
				return enStatusInvArgs;
		#endif //#if (PF_LSM303_DEBUG == 1)
	
    writeVal=regAddr;

    /** 0x3C regAddr to set pointer to reg with addresses regAddr */
    ret=lsm303Config[*devId].pfI2cWrite(&writeVal, enBooleanTrue, slaveAddr, 1);
    if(ret!=enStatusSuccess)
    return ret;

    /** read register value */
    ret=lsm303Config[*devId].pfI2cRead(readValue, enBooleanTrue, slaveAddr, 1, &readBytes);
    if(ret!=enStatusSuccess)
    return ret;

    return enStatusSuccess;
}


