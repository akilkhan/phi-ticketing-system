#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_gps.h"

PFbyte* gpsUpdateRateCmd[] = 
{
	(PFbyte*)"$PMTK220,1000*1F", 
	(PFbyte*)"$PMTK220,500*2B", 
	(PFbyte*)"$PMTK220,250*29", 
	(PFbyte*)"$PMTK220, 200*2C", 
	(PFbyte*)"$PMTK220,125*28", 
	(PFbyte*)"$PMTK220,100*2F"
};

PFbyte* gpsBaudCmd[] = 
{
	(PFbyte*)"$PMTK251,4800*14", 
	(PFbyte*)"$PMTK251,19200*22", 
	(PFbyte*)"$PMTK251,38400*27", 
	(PFbyte*)"$PMTK251,57600*2C", 
	(PFbyte*)"$PMTK251,115200*1F"
};

PFbyte* gpsSeqCmd[] = 
{
	(PFbyte*)"$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29", 
	(PFbyte*)"$PMTK314,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29", 
	(PFbyte*)"$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29", 
	(PFbyte*)"$PMTK314,-1*04"
};
	
	
static PFbyte gpsMsg[256];
static PFbyte temp_buf[80];
static PFbyte* gpsMsgPtr;
static PFbyte gpsChSum=0;
static PFbyte gpsInitFlag=0;
static PFbyte retryCount=0;
PFEnStatus pfGpsGetData(void);
PFfloat pfTrunc(PFfloat v);

static PFCfgGps gpsCfg;

PFEnStatus pfGpsOpen(PFpCfgGps config)
{
	PFEnStatus status;
#if (PF_GPS_DEBUG == 1)
	if((config->comWrite == NULL) || (config->comRead == NULL))
		return enStatusInvArgs;
#endif	// #if (PF_GPS_DEBUG == 1)
	pfMemCopy(&gpsCfg, config, sizeof(PFCfgGps));
	status = pfGpsSetSequence(config->gpsSeq);
	if(status != enStatusSuccess)
		return status;
	status = pfGpsSetUpdateRate(config->rateHz);
	if(status != enStatusSuccess)
		return status;
	status = pfGpsSetBaudrate(config->gpsBaudrate);
	if(status != enStatusSuccess)
		return status;
	gpsInitFlag = 1;
	return enStatusSuccess;
}
PFuint pfFloor(PFfloat n)
{
	if (n >= 0)
    {
        return (PFuint)n;
    }
    else
    {
        PFuint y = (PFuint)n;
        return ((PFfloat)y == n) ? y : y - 1;
    }
}

PFfloat pfTrunc(PFfloat v) {
    if(v < 0.0) {
        v*= -1.0;
        v = pfFloor(v);
        v*=-1.0;
    } else {
        v = pfFloor(v);
    }
    return v;
}
	
PFEnStatus pfGpsGetData(void)
{
	PFdword rnos,totalCount;
    PFEnBoolean timeStatus;
    PFdword timeOutValue;
#if (PF_GPS_DEBUG == 1)
	if(gpsInitFlag == 0)
		return enStatusNotConfigured;
#endif	// #if (PF_GPS_DEBUG == 1)
    totalCount = 0;
#if (PF_GPS_DEBUG == 1)    
    timeOutValue = pfTickSetTimeoutMs(PF_GPS_TIMEOUT);
#endif	// #if (PF_GPS_DEBUG == 1)
    while(totalCount<=100)
    {
        rnos = 0;
        gpsCfg.comRead(&gpsMsg[totalCount],100,&rnos);
        totalCount += rnos;
#if (PF_GPS_DEBUG == 1)
        timeStatus = pfTickCheckTimeout(timeOutValue);
        if(timeStatus == enBooleanTrue)
            return enStatusTimeout;
#endif	// #if (PF_GPS_DEBUG == 1)
    }
        gpsMsgPtr = &gpsMsg[0];
        while(*gpsMsgPtr != '$')
            gpsMsgPtr++;
        gpsMsgPtr++;
	return enStatusSuccess;
}

PFEnStatus pfGpsProcessGGA(PFpGpsDataGGA dataGGA)
{
	PFEnStatus ret;
	PFbyte x,y;
	PFbyte *mem,*temp;
    PFEnBoolean timeStatus;
    PFdword timeOutValue;
    
#if (PF_GPS_DEBUG == 1)	
	if(dataGGA == PF_NULL_PTR)
		return enStatusInvArgs;
#endif	// #if (PF_GPS_DEBUG == 1)
	pfGpsSetSequence(enGpsSeqGGA);
	ret = pfGpsGetData();
#if (PF_GPS_DEBUG == 1)
	if(ret!=enStatusSuccess)
		return ret;
    timeOutValue = pfTickSetTimeoutMs(PF_GPS_TIMEOUT);
#endif	// #if (PF_GPS_DEBUG == 1)
    
	mem = pfStringTokenize(gpsMsgPtr,(PFbyte*)",");
	while(pfMemCompare(mem,(PFbyte*)"GPGGA",pfStrLen((PFbyte*)"GPGGA")) != 1 )
	{
		ret = pfGpsGetData();
#if (PF_GPS_DEBUG == 1)
        if(ret!=enStatusSuccess)
            return ret;
        timeStatus = pfTickCheckTimeout(timeOutValue);
        if(timeStatus == enBooleanTrue)
            return enStatusTimeout;
#endif	// #if (PF_GPS_DEBUG == 1)
        mem = pfStringTokenize(gpsMsgPtr,(PFbyte*)",");
	}
	temp = gpsMsgPtr;
	gpsChSum = 0;
	while (*temp != '*')
	{
		gpsChSum ^= *temp;
		temp++;
	}
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
    pfAtoF(temp, &(dataGGA->utcTime));
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoF(temp, &dataGGA->latitude);
    
	temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	if(*temp == 'N')
		dataGGA->NSIndicator = enGpsDirNorth;
	else
		dataGGA->NSIndicator = enGpsDirSouth;
	
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoF(temp, &dataGGA->longitude);
    
	temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	if(*temp == 'E')
		dataGGA->EWIndicator = enGpsDirEast;
	else
		dataGGA->EWIndicator = enGpsDirWest;
    
	temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
    pfAtoI(temp, (PFdword*)&dataGGA->validData);
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoI(temp, (PFdword*)&dataGGA->usedSatellites);
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoF(temp, &dataGGA->HDOP);
	
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
    pfAtoF(temp, &dataGGA->mslAltitude);
    
	pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
    
	temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
    pfAtoF(temp, &dataGGA->geoidalSeparation);
    
	pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoF(temp, &dataGGA->ageGiffCorr);
    
	temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)"*");
	while(*temp != 0)
		temp++;
	temp++;
	x = *temp > '9' ? ((*temp)|32) - 'a' + 10 : *temp - '0';
	temp++;
	y = *temp > '9' ? ((*temp)|32) - 'a' + 10 : *temp - '0';
	
	dataGGA->checksum				=	(x<<4) | y;
	if(gpsChSum == dataGGA->checksum)
		dataGGA->validChecksum = enBooleanTrue;
	else
		dataGGA->validChecksum = enBooleanFalse;
return enStatusSuccess;
}

PFEnStatus pfGpsProcessRMC(PFpGpsDataRMC dataRMC)
{
	PFEnStatus ret;
	PFbyte x,y;
	PFbyte *mem,*temp;
	PFEnBoolean timeStatus;
    PFdword timeOutValue;
    
	pfGpsSetSequence(enGpsSeqRMC);
	ret = pfGpsGetData();
#if (PF_GPS_DEBUG == 1)
	if(ret!=enStatusSuccess)
		return ret;
    timeOutValue = pfTickSetTimeoutMs(PF_GPS_TIMEOUT);
#endif	// #if (PF_GPS_DEBUG == 1)
    
	mem = pfStringTokenize(gpsMsgPtr,(PFbyte*)",");
	while(pfMemCompare(mem,(PFbyte*)"GPRMC",pfStrLen((PFbyte*)"GPRMC")) != 1 )
	{
		ret = pfGpsGetData();
#if (PF_GPS_DEBUG == 1)
        if(ret!=enStatusSuccess)
            return ret;
        timeStatus = pfTickCheckTimeout(timeOutValue);
        if(timeStatus == enBooleanTrue)
            return enStatusTimeout;
#endif	// #if (PF_GPS_DEBUG == 1)
        mem = pfStringTokenize(gpsMsgPtr,(PFbyte*)",");
	}
	temp = gpsMsgPtr;
	gpsChSum = 0;
	while (*temp != '*')
	{
		gpsChSum ^= *temp;
		temp++;
	}
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoF(temp, &dataRMC->utcTime);
    
	temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	dataRMC->validData		= *temp;
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoF(temp, &dataRMC->latitude);
    
	temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	if(*temp == 'N')
		dataRMC->NSIndicator = enGpsDirNorth;
	else
		dataRMC->NSIndicator = enGpsDirSouth;
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoF(temp, &dataRMC->longitude);
    
	temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	if(*temp == 'E')
		dataRMC->EWIndicator = enGpsDirEast;
	else
		dataRMC->EWIndicator = enGpsDirWest;
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoF(temp, &dataRMC->speedOverGnd);
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoF(temp, &dataRMC->courseOverGnd);
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoI(temp, (PFdword*)&dataRMC->date);
    
	pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)"*");
	dataRMC->mode = *temp;
	while(*temp != 0)
		temp++;
	temp++;
	x = *temp > '9' ? ((*temp)|32) - 'a' + 10 : *temp - '0';
	temp++;
	y = *temp > '9' ? ((*temp)|32) - 'a' + 10 : *temp - '0';
	
	dataRMC->checksum				=	(x<<4) | y;
	if(gpsChSum == dataRMC->checksum)
		dataRMC->validChecksum = enBooleanTrue;
	else
		dataRMC->validChecksum = enBooleanFalse;
return enStatusSuccess;
}


PFEnStatus pfGpsProcessVTG(PFpGpsDataVTG dataVTG)
{
	PFEnStatus ret;
	PFbyte x,y;
	PFbyte *mem,*temp;
    PFEnBoolean timeStatus;
    PFdword timeOutValue;
	
	pfGpsSetSequence(enGpsSeqVTG);
	ret = pfGpsGetData();
#if (PF_GPS_DEBUG == 1)
	if(ret!=enStatusSuccess)
		return ret;
    timeOutValue = pfTickSetTimeoutMs(PF_GPS_TIMEOUT);
#endif	// #if (PF_GPS_DEBUG == 1)

	mem = pfStringTokenize(gpsMsgPtr,(PFbyte*)",");
	while(pfMemCompare(mem,(PFbyte*)"GPVTG",pfStrLen((PFbyte*)"GPVTG")) != 1 )
	{
		ret = pfGpsGetData();
#if (PF_GPS_DEBUG == 1)
        if(ret!=enStatusSuccess)
            return ret;
        timeStatus = pfTickCheckTimeout(timeOutValue);
        if(timeStatus == enBooleanTrue)
            return enStatusTimeout;
#endif	// #if (PF_GPS_DEBUG == 1)
        mem = pfStringTokenize(gpsMsgPtr,(PFbyte*)",");
	}
	temp = gpsMsgPtr;
	gpsChSum = 0;
	while (*temp != '*')
	{
		gpsChSum ^= *temp;
		temp++;
	}
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoF(temp, &dataVTG->course1);
    
	temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	dataVTG->reference1		= *temp;
	
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
    pfAtoF(temp, &dataVTG->course2);
    
	temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	dataVTG->reference2		= *temp;
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoF(temp, &dataVTG->speedKnot);
    
	pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
    
    temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	pfAtoF(temp, &dataVTG->speedKmhr);
    
	pfStringTokenize(PF_NULL_PTR,(PFbyte*)",");
	temp = pfStringTokenize(PF_NULL_PTR,(PFbyte*)"*");
	dataVTG->mode 			= *temp;
	while(*temp != 0)
		temp++;
	temp++;
	x = *temp > '9' ? ((*temp)|32) - 'a' + 10 : *temp - '0';
	temp++;
	y = *temp > '9' ? ((*temp)|32) - 'a' + 10 : *temp - '0';
	
	dataVTG->checksum				=	(x<<4) | y;
	if(gpsChSum == dataVTG->checksum)
		dataVTG->validChecksum = enBooleanTrue;
	else
		dataVTG->validChecksum = enBooleanFalse;
return enStatusSuccess;
}


PFEnStatus pfGpsConvertDD(PFpGpsDataConvert data, PFfloat* latitude,PFfloat* longitude)
{
	PFfloat degrees,minutes;
#if (PF_GPS_DEBUG == 1)
	if((data == NULL) || (latitude == NULL) || (longitude == NULL))
		return enStatusInvArgs;
#endif	// #if (PF_GPS_DEBUG == 1)
	if(data->NSIndicator == enGpsDirSouth) {*latitude  *= -1.0; }
	if(data->EWIndicator == enGpsDirWest)  {*longitude *= -1.0; }
	degrees = pfTrunc(data->latitude / 100);
	minutes = data->latitude - (degrees * 100);
	*latitude = degrees + (minutes / 60);    
	degrees = pfTrunc(data->longitude / 100);
	minutes = data->longitude - (degrees * 100);
	*longitude = degrees + (minutes / 60);
	return enStatusSuccess;
}

PFEnStatus pfGpsSetBaudrate(PFEnGpsBaudrate baud)
{
	PFbyte len=0;
#if (PF_GPS_DEBUG == 1)
	if(baud > 4)
		return enStatusInvArgs;
#endif	// #if (PF_GPS_DEBUG == 1)
	
	len = pfStrCopy(temp_buf, gpsBaudCmd[baud]);
	
	temp_buf[len++] = 0x0D;
	temp_buf[len++] = 0x0A;
	gpsCfg.comWrite(temp_buf, len);
	return enStatusSuccess;
}

PFEnStatus pfGpsSetSequence(PFEnGpsSequence seq)
{
	PFbyte len=0,len1;
#if (PF_GPS_DEBUG == 1)
	if(seq >3)
		return enStatusInvArgs;
#endif	// #if (PF_GPS_DEBUG == 1)
	len = pfStrCopy(temp_buf, gpsSeqCmd[seq]);

	temp_buf[len++] = 0x0D;
	temp_buf[len++] = 0x0A;
	gpsCfg.comWrite(temp_buf, len);
	return enStatusSuccess;
}	


PFEnStatus pfGpsSetUpdateRate(PFEnGpsUpdateRate rate)
{
	PFbyte len=0;
#if (PF_GPS_DEBUG == 1)
	if(rate >5)
		return enStatusInvArgs;
#endif	// #if (PF_GPS_DEBUG == 1)
	
	len = pfStrCopy(temp_buf, gpsUpdateRateCmd[rate]);	
	temp_buf[len++] = 0x0D;
	temp_buf[len++] = 0x0A;
	gpsCfg.comWrite(temp_buf, len);
	return enStatusSuccess;
}


