/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework L6470 Stepper Motor Driver.
 * 
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_L6470_STEPPER_DRIVER_API L6470 STEPPER DRIVER API
 * @{
 */

#define L6470_MAX_DAISY_CHAIN_DEVICES_SUPPORTED       8         /** max number of SmartLynx devices in Daisy chain */

/** L6470 driver's Register addresses **/
#define L6470_ABS_POS                        0x01
#define L6470_EL_POS                         0x02
#define L6470_MARK                           0x03
#define L6470_SPEED                          0x04
#define L6470_ACC                            0x05
#define L6470_DEC                            0x06
#define L6470_MAX_SPEED                      0x07
#define L6470_MIN_SPEED                      0x08
#define L6470_FS_SPD                         0x15
#define L6470_KVAL_HOLD                      0x09
#define L6470_KVAL_RUN                       0x0A
#define L6470_KVAL_ACC                       0x0B
#define L6470_KVAL_DEC                       0x0C
#define L6470_INT_SPD                        0x0D
#define L6470_ST_SLP                         0x0E
#define L6470_FN_SLP_ACC                     0x0F
#define L6470_FN_SLP_DEC                     0x10
#define L6470_K_THERM                        0x11
#define L6470_ADC_OUT                        0x12
#define L6470_OCD_TH                         0x13
#define L6470_STALL_TH                       0x14
#define L6470_STEP_MODE                      0x16
#define L6470_ALARM_EN                       0x17
#define L6470_CONFIG                         0x18
#define L6470_STATUS                         0x19

/** L6470 driver's command **/
#define L6470_NOP                            0x00
#define L6470_SET_PARAM                      0x00
#define L6470_GET_PARAM                      0x20
#define L6470_RUN                            0x50
#define L6470_STEP_CLOCK                     0x58
#define L6470_MOVE                           0x40
#define L6470_GOTO                           0x60
#define L6470_GOTO_DIR                       0x68
#define L6470_GO_UNTIL                       0x82
#define L6470_RELEASE_SW                     0x92
#define L6470_GO_HOME                        0x70
#define L6470_GO_MARK                        0x78
#define L6470_RESET_POS                      0xD8
#define L6470_RESET_DEVICE                   0xC0
#define L6470_SOFT_STOP                      0xB0
#define L6470_HARD_STOP                      0xB8
#define L6470_SOFT_HIZ                       0xA0
#define L6470_HARD_HIZ                       0xA8
#define L6470_GET_STATUS                     0xD0

#define L6470_MAX_SPEED_SUPPORTED            15600     /** maximum speed(in steps/sec) supported by driver **/
#define L6470_MAX_SUPPORTED_MIN_SPEED        976       /** maximum speed(in steps/sec) limit of min speed register **/
#define L6470_MIN_SPEED_OF_ROTATION          0         /** min speed limit in steps/sec **/
#define L6470_FULL_STEP_THRESHOLD_SPEED      400       /** threshold speed(in steps/sec) after which stepper leaves microstepping 
																													 mode and inter into full stepping mode    **/
#define L6470_SYNC_SELECT_ENABLE             0         /** enable BUSY\SYNC output, 1: BUSY\SYNC pin provide SYNC signal
																																										0: BUSY\SYNC pin provide BYSY signal   **/
#define L6470_HARD_STOP_DISABLE              0         /** disable hard stop for external switch press **/
#define L6470_VTG_COMPENSATION_ENABLE        0         /** enable/disable motor supply voltage compensation **/
#define L6470_OVERCURRENT_SHUTDOWN_ENABLE    0         /** Shutdown on overcurrent detection **/

/** Voltage=(VS*KVAL_x/256)volt, different voltage values for different motor state **/
#define L6470_VTG_KVAL_HOLD                  29        /** assigned to the PWM modulators when the motor is stopped **/
#define L6470_VTG_KVAL_RUN                   100       /** assigned to the PWM modulators when the motor is running at constant speed **/
#define L6470_VTG_KVAL_ACC                   100       /** assigned to the PWM modulators during acceleration **/
#define L6470_VTG_KVAL_DEC                   100       /** assigned to the PWM modulators during deceleration **/

/** Enumeration to set the current level at which an overcurrent event occurs. **/
typedef enum{
	enL6470overCurrentThreshold_375mA=0x00,              /**< select overcurrent threshold to 375mA */
	enL6470overCurrentThreshold_750mA,                   /**< select overcurrent threshold to 750mA */
	enL6470overCurrentThreshold_1125mA,                  /**< select overcurrent threshold to 1125mA */
	enL6470overCurrentThreshold_1500mA,                  /**< select overcurrent threshold to 1500mA */
	enL6470overCurrentThreshold_1875mA,                  /**< select overcurrent threshold to 1875mA */
	enL6470overCurrentThreshold_2250mA,                  /**< select overcurrent threshold to 2250mA */
	enL6470overCurrentThreshold_2625mA,                  /**< select overcurrent threshold to 2625mA */
	enL6470overCurrentThreshold_3000mA,                  /**< select overcurrent threshold to 3000mA */
	enL6470overCurrentThreshold_3375mA,                  /**< select overcurrent threshold to 3375mA */
	enL6470overCurrentThreshold_3750mA,                  /**< select overcurrent threshold to 3750mA */
	enL6470overCurrentThreshold_4125mA,                  /**< select overcurrent threshold to 4125mA */
	enL6470overCurrentThreshold_4500mA,                  /**< select overcurrent threshold to 4500mA */
	enL6470overCurrentThreshold_4875mA,                  /**< select overcurrent threshold to 4875mA */
	enL6470overCurrentThreshold_5250mA,                  /**< select overcurrent threshold to 5250mA */
	enL6470overCurrentThreshold_5625mA,                  /**< select overcurrent threshold to 5625mA */
	enL6470overCurrentThreshold_6000mA                   /**< select overcurrent threshold to 6000mA */
}PFEnL6470overCurrentThreshold;

/** Enumeration for step size selection */
typedef enum{
	enL6470stepSelect_1 = 0x00,      /**< stepper move by full stepangle in single step **/
	enL6470stepSelect_1_2,           /**< stepper move by half of stepangle for single step **/
	enL6470stepSelect_1_4,           /**< stepper move by quarter of stepangle for single step **/
	enL6470stepSelect_1_8,           /**< stepper move by 1/8th of stepangle for single step **/
	enL6470stepSelect_1_16,          /**< stepper move by 1/16th of stepangle for single step **/
	enL6470stepSelect_1_32,          /**< stepper move by 1/32th of stepangle for single step **/
	enL6470stepSelect_1_64,          /**< stepper move by 1/64th of stepangle for single step **/
	enL6470stepSelect_1_128          /**< stepper move by 1/128th of stepangle for single step **/
}PFEnL6470stepSelect;

/** Enumeration for system(driver) clock source selection */
typedef enum{
/** internal 16MHz oscillator as clock source **/
	enL6470oscSelect_int_16MHz_oscOut_none   = 0x0000, /**< pins OSCIN and OSCOUT are unused **/
	enL6470oscSelect_int_16MHz_oscOut_2MHz   = 0x0008, /**< pin OSCIN unused and pin OSCOUT provides 2MHz **/
	enL6470oscSelect_int_16MHz_oscOut_4MHz   = 0x0009, /**< pin OSCIN unused and pin OSCOUT provides 4MHz **/
	enL6470oscSelect_int_16MHz_oscOut_8MHz   = 0x000A, /**< pin OSCIN unused and pin OSCOUT provides 8MHz **/
	enL6470oscSelect_int_16MHz_oscOut_16MHz  = 0x000B, /**< pin OSCIN unused and pin OSCOUT provides 16MHz **/
/** external crystal/resonator, pins OSCIN and OSCOUT drives Crystal/resonator **/
	enL6470oscSelect_ext_8MHz_XTAL_Drive     = 0x0004, /**< 8MHz external crystal/resonator **/
	enL6470oscSelect_ext_16MHz_XTAL_Drive    = 0x0005, /**< 16MHz external crystal/resonator **/
	enL6470oscSelect_ext_24MHz_XTAL_Drive    = 0x0006, /**< 24MHz external crystal/resonator **/
	enL6470oscSelect_ext_32MHz_XTAL_Drive    = 0x0007, /**< 32MHz external crystal/resonator **/
/**< external clock source, crystal/ resonator disabled, OSCIN pin provide clock while OSCOUT pin supplies inverted OSCIN signal **/
	enL6470oscSelect_ext_8MHz_oscOut_Invert  = 0x000C, /**< 8MHz external clock source **/
	enL6470oscSelect_ext_16MHz_oscOut_Invert = 0x000D, /**< 16MHz external clock source **/
	enL6470oscSelect_ext_24MHz_oscOut_Invert = 0x000E, /**< 24MHz external clock source **/
	enL6470oscSelect_ext_32MHz_oscOut_Invert = 0x000F, /**< 32MHz external clock source **/
}PFEnL6470oscSelect;

/** Enumeration for slew rate selection */
typedef enum{
	enL6470slewRate_320 = 0x00,       /**< select slew rate 320volts/micro-second **/
	enL6470slewRate_75,               /**< select slew rate 75volts/micro-second **/
	enL6470slewRate_110,              /**< select slew rate 110volts/micro-second **/
	enL6470slewRate_260               /**< select slew rate 260volts/micro-second **/
}PFEnL6470slewRate;

/** Enumeration for selection of Multiplier for the PWM sinewave frequency generation **/
typedef enum{
	enL6470pwmMultiplier_0_625 = 0x00, /**< select multiplication factor 0.625 **/
	enL6470pwmMultiplier_0_75,         /**< select multiplication factor 0.75 **/
	enL6470pwmMultiplier_0_875,        /**< select multiplication factor 0.875 **/
	enL6470pwmMultiplier_1,            /**< select multiplication factor 1.0 **/
	enL6470pwmMultiplier_1_25,         /**< select multiplication factor 1.25 **/
	enL6470pwmMultiplier_1_5,          /**< select multiplication factor 1.5 **/
	enL6470pwmMultiplier_1_75,         /**< select multiplication factor 1.75 **/
	enL6470pwmMultiplier_2             /**< select multiplication factor 2.0 **/
}PFEnL6470pwmMultiplier;

/** Enumeration for selection of Divisor for the PWM sinewave frequency generation **/
typedef enum{
	enL6470pwmDivisor_1 = 0x00,        /**< select division factor 1 **/
	enL6470pwmDivisor_2,               /**< select division factor 2 **/
	enL6470pwmDivisor_3,               /**< select division factor 3 **/
	enL6470pwmDivisor_4,               /**< select division factor 4 **/
	enL6470pwmDivisor_5,               /**< select division factor 5 **/
	enL6470pwmDivisor_6,               /**< select division factor 6 **/
	enL6470pwmDivisor_7                /**< select division factor 7 **/
}PFEnL6470pwmDivisor;

/** Enumeration for selection of Alarm Enable for **/
typedef enum{
	enL6470overcurrentAlarmEnable       = 0x01,  /**< alarm enable for overcurrent detection **/
	enL6470thermalShutdownAlarmEnable   = 0x02,  /**< alarm enable for thermal shutdown event **/
	enL6470thermalWarningAlarmEnable    = 0x04,  /**< alarm enable for thermal warning **/
	enL6470underVoltageAlarmEnable      = 0x08,  /**< alarm enable for undervoltage detection **/
	enL6470stallDetection_A_AlarmEnable = 0x10,  /**< alarm enable for stall detection on Bridge A **/
	enL6470stallDetection_B_AlarmEnable = 0x20,  /**< alarm enable for stall detection on Bridge B **/
	enL6470switchTurnOnEventAlarmEnable = 0x40,  /**< alarm enable for switch turn on event **/
	enL6470wrongCommandAlarmEnable      = 0x80,  /**< alarm enable for wrong command pass **/
}PFEnL6470AlarmEnable;

/** Enumeration for action selection for switch press**/
typedef enum{
	enL6470resetABS_POS      = 0x00,   /**< reset Absolute Position register for switch press **/
	enL6470copyABS_POStoMark = 0x08    /**< reset Absolute Position register to Mark register for switch press **/
}PFEnL6470actionForSwitchPress;

/** structure for configuration of L6470 stepper driver **/
typedef struct{
	PFbyte devId;                                                            /**< stepper id for spi **/
	PFGpioPortPin chipSelectPortPin;                                         /**< port & pin for spi chip select  **/
	PFGpioPortPin stepPortPin;                                               /**< stepper step port pin  **/
	PFGpioPortPin resetPortPin;                                              /**< driver reset port pin **/
	PFEnL6470pwmMultiplier multiplier;                                       /**< multiplier for PWM frequency generation  **/
	PFEnL6470pwmDivisor divisor;                                             /**< divisor for PWM frequency generation  **/
	PFEnL6470slewRate slewRate;                                              /**< slew rate selection **/
	PFEnL6470oscSelect oscSelect;                                            /**< clock selection **/
	PFEnL6470overCurrentThreshold  overCurrentThreshold;                     /**< threshold for overcurrent detection **/
	PFEnL6470AlarmEnable alarmEnable;                                        /**< selection of alarm signals used to generate the FLAG output **/
	PFEnL6470stepSelect stepSize;                                            /**< stepsize selection **/
	PFdword stepAngle;					                                             /**< Step angle in degrees. This value must be the actual step angle multiplied by 10 */
	PFfloat acceleration;                                                    /**< acceleration profile in steps/sec^2 */
	PFfloat deceleration;                                                    /**< deceleration profile in steps/sec^2 */
	PFEnStatus (*pfSpiChipSelect)(PFbyte* id, PFbyte pinStatus);             /**< function pointer to Spi Chip Select */
	PFEnStatus (*pfSpiExchangeByte)(PFbyte *id, PFword data, PFword* rxData);/**< function pointer to spi exchange byte */
	PFEnStatus (*pfSpiRegisterDevice)(PFbyte* id, PFpGpioPortPin chipSelect);/**< function pointer to register spi divice */
	PFEnStatus (*pfSpiUnregisterDevice)(PFbyte* id);                         /**< function pointer to unregister spi device */
}PFCfgStepperL6470;

/** printer to structure  PFCfgStepperL6470 **/
typedef PFCfgStepperL6470* PFpCfgStepperL6470;

/**
 * To initialize stepper drivers L6470
 *
 * \param deviceId pointer to copy Id's assigned to steppers
 *
 * \param sensorConfig pointer to array of structures to configure steppers
 *
 * \param deviceCount number of devices to be initialised
 *
 * \return status of initialization
 *
 * \note Function needs SPI to be initialized before calling this function
 *       this function should be called only once for all devices
 */
PFEnStatus pfL6470open(PFbyte* deviceId, PFpCfgStepperL6470 sensorConfig, PFbyte deviceCount);

/**
 * to close all steppers
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of stepper close operation
 */
PFEnStatus pfL6470close(PFbyte* devId);

/**
 * to close all steppers
 *
 * \return status of stepper close operation
 */
PFEnStatus pfL6470closeAll(void);

/**
 * The Run command produces a motion at spd speed; the direction is selected by the dir bit: '1' forward or '0' reverse.
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param dir to select the direction of motion; dir bit: '1' anticlockwise or '0' clockwise.
 *
 * \param spd speed(in steps/sec) at which stepper shuold run (ranging 15.25 to 15610 step/s).
 *
 * \return status of run operation
 * \note Run keeps the BUSY flag low until the target speed is reached.
 *       This command can be given anytime and is immediately executed.
 *       speed set is multiple of 15.25 step/s.
 */
PFEnStatus pfL6470run(PFbyte* devId, PFbyte dir, PFfloat spd);

/**
 * to move stepper by specified number of steps as per stepsize selection in specified direction
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param dir direction of rotation  0: for clockwise 1: for anticlockwise
 *
 * \param steps number of steps stepper shuold move, as per stepsize selection
 *
 * \param spd speed(in steps/sec) at which stepper shuold run (ranging 15.25 to 15610 step/s).
 *
 * \return status of move operation
 * \note command keeps the BUSY flag low until the target number of steps is performed
 *       This command can only be performed when the motor is stopped.
 *       speed set is multiple of 15.25 step/s.
 */
PFEnStatus pfL6470move(PFbyte* devId, PFbyte dir, PFdword steps, float spd);

/**
 * Puts the device into Step-clock mode and imposes dir direction
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param dir direction of rotation
 *
 * \return status of step clock operation
 * \note This command can only be given when the motor is stopped.
 */
PFEnStatus pfL6470setStepClockMode(PFbyte* devId, PFbyte dir);

/**
 * issue single step pulse(single step is in selected stepmode)
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of step clock operation
 * \note before calling this function pfL6470setStepClockMode function should be called
 */
PFEnStatus pfL6470takeSingleStep(PFbyte* devId);

/**
 * The GoTo command produces a motion to pos absolute position through the shortest path.
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param pos position by which stepper shuold move, as per stepsize selection
 *
 * \return status of goto operation
 * \note The GoTo command keeps the BUSY flag low until the target position is reached.
 *       This command can be given only when the previous motion command has been completed
 */
PFEnStatus pfL6470goTo(PFbyte* devId, PFdword pos);

/**
 * Brings motor into pos position in dir direction
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param dir direction of rotation
 *
 * \param pos position by which stepper shuold move, as per stepsize selection
 *
 * \return status of goto with direction operation
 * \note This command keeps the BUSY flag low until the target position is reached.
 *       This command can be given only when the previous motion command has been completed
 */
PFEnStatus pfL6470goTo_DIR(PFbyte* devId, PFbyte dir, PFdword pos);

/**
 * The GoMark command produces a motion to the MARK(position stored in MARK register) position performing the minimum path.
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of gomark operation
 * \note This command keeps the BUSY flag low until the MARK position is reached.
 *       This command can be given only when the previous motion command has been completed
 */
PFEnStatus pfL6470goMark(PFbyte* devId);

/**
 * produces a motion at spd speed imposing specified direction till external switch turn-on event occurs
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param act action on external switch turn-on event occurs 	enL6470resetABS_POS : current position is resetted,	
																															enL6470copyABS_POStoMark : current position is copied into the MARK register
 *
 * \param dir direction of rotation  0: for clockwise 1: for anticlockwise
 *
 * \param spd speed(in steps/sec) at which stepper shuold run untill switch get pressed
 *
 * \return status of go until operation
 * \note This command keeps the BUSY flag low until the switch turn-on event occurs and the motor is stopped
 *			 This command can be given anytime and is immediately executed.
 */
PFEnStatus pfL6470goUntil(PFbyte* devId, PFEnL6470actionForSwitchPress act, PFbyte dir, PFfloat spd);

/**
 * The ReleaseSW command produces a motion at minimum speed until the switch input is released
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param act action on external switch turn-on event occurs 	
 *        enL6470resetABS_POS : current position is resetted,	
 *		  enL6470copyABS_POStoMark : current position is copied into the MARK register
 *
 * \param dir direction of rotation  0: for clockwise 1: for anticlockwise
 *
 * \return status of release Switch operation
 * \note This command keeps the BUSY flag low until the switch input is released and the motor is stopped
 *	     This command can be given anytime and is immediately executed.
 */
PFEnStatus pfL6470releseSW(PFbyte* devId, PFEnL6470actionForSwitchPress act, PFbyte dir);

/**
 * to test wheather device is ready for next command(only check busy flag)
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param test pointer to copy busy/ready status of driver. True if device is ready(not accelerating or deceleration)
 *
 * \return status of device 
 */
PFEnStatus pfL6470deviceReady(PFbyte* devId, PFEnBoolean* test);

/**
 * The GoHome command produces a motion to the HOME position (zero position) via the shortest path.
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of gohome operation
 * \note The GoHome command keeps the BUSY flag low until the home position is reached. 
 *       Thiscommand can be given only when the previous motion command has been completed
 */
PFEnStatus pfL6470GoHomePosition(PFbyte* devId);

/**
 * to reset home position of stepper to zero
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of resetting home position
 * \note This commmand is performed only when motor is stopped
 */
PFEnStatus pfL6470resetPosition(PFbyte* devId);

/**
 * to reset device, all values are set to power on reset values
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of resetDev operation
 */
PFEnStatus pfL6470resetDevice(PFbyte* devId);

/**
 * Puts the bridges into high impedance status after a deceleration phase
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of softHiZ operation
 */
PFEnStatus pfL6470softHiZ(PFbyte* devId);

/**
 * Puts the bridges into high impedance status immediately
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of hardHiZ operation
 */
PFEnStatus pfL6470hardHiZ(PFbyte* devId);

/**
 * to perform soft stop(immediate deceleration) operation 
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of softStop operation
 */
PFEnStatus pfL6470softStop(PFbyte* devId);

/**
 * to perform hard stop(immediate stop) operation 
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of hardStop operation
 */
PFEnStatus pfL6470hardStop(PFbyte* devId);

/**
 * to do nothing 
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of nop operation
 */
PFEnStatus pfL6470nop(PFbyte* devId);

/**
 * to read driver status register 
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param status pointer to copy status register value
 *
 * \return status of getStatus operation
 */
PFEnStatus pfL6470getStatus(PFbyte* devId, PFword *status);

/**
 * to read current position of stepper
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param pos pointer to copy current position of stepper, +ve for anticlockwise while -ve for clockwise direction
 *
 * \return status of read Current Position value
 */
PFEnStatus pfL6470getCurrentPosition(PFbyte* devId, PFsdword* pos);

/**
 * to read acceleration value 
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param acceleration pointer to copy current acceleration value in steps/sec^2
 *
 * \param deceleration pointer to copy current deceleration value in steps/sec^2
 *
 * \return status of read Current Acceleration value
 */
PFEnStatus pfL6470getAccelerationProfile(PFbyte* devId, PFfloat* acceleration, PFfloat* deceleration);

/**
 * to set new acceleration and deceleration profile for motor
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param acceleration new acceleration value in steps/sec^2 to be set (ranging 14.55 to 59590 steps/sec^2)
 *
 * \param deceleration new deceleration value in steps/sec^2 to be set (ranging 14.55 to 59590 steps/sec^2)
 *
 * \return status of set Acceleration value
 * \note This command is performed only when motor is stopped
 *       Acceleration/deceleration values set are in multiple of 14.55steps/sec^2
 */
PFEnStatus pfL6470setAccelerationProfile(PFbyte* devId, PFfloat acceleration, PFfloat deceleration);

/**
 * to set minimum speed of rotation(motion start from this speed otherwise start from zero speed)
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param speed is minimum speed of rotation in step/s
 *
 * \return status of set minimum speed of rotation
 * \note This command is performed only when motor is stopped
 */
PFEnStatus pfL6470setMinimumSpeed(PFbyte* devId, PFfloat speed);

/**
 * to set full step speed limit above this speed stepper leaves selected step mode and enter into full step mode 
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param speed is full step speed limit in step/s
 *
 * \return status of set full step speed limit
 * \note speed setting range is from 7.63 to 15625 step/s which is multiple of 15.25 step/s.
 */
PFEnStatus pfL6470setFullStepSpeed(PFbyte* devId, PFfloat speed);

/**
 * to get current speed of stepper 
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param speed pointer to copy current speed of stepper in step/s
 *
 * \return status of read Current Speed value
 */
PFEnStatus pfL6470getSpeed(PFbyte* devId, PFfloat* speed);
/** @} */
