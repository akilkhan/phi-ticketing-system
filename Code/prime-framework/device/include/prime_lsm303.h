/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework LSM303 driver.
 *
 * 
 * Review status: NO
 *
 */ 
#pragma once

#define M_PI	3.14159265358979323846

#define PF_MAX_LSM303_SUPPORTED 	2   		/** Max number of devices supported */

 /** 
 * \defgroup LSM303DLH_Register_Addresses LSM303DLH Register Addresses 
 * @{
 */

#define LSM303_ACC_MAG_SLAVE_ADDR_ACC          0x30    /** Address of Accelerometer in LSM303 as SA0 pin tied to GND */
#define LSM303_ACC_MAG_SLAVE_ADDR_MAG          0x3C    /** Address of Magnetometer in LSM303 */

#define LSM303_ACC_MAG_CTRL_REG1_A             0x20    /** Control Register 1 of Accelerometer */
#define LSM303_ACC_MAG_CTRL_REG2_A             0x21    /** Control Register 2 of Accelerometer */
#define LSM303_ACC_MAG_CTRL_REG3_A             0x22    /** Control Register 3 of Accelerometer */
#define LSM303_ACC_MAG_CTRL_REG4_A             0x23    /** Control Register 4 of Accelerometer */
#define LSM303_ACC_MAG_CTRL_REG5_A             0x24    /** Control Register 5 of Accelerometer */
#define LSM303_ACC_MAG_HP_FILTER_RESET_A       0x25    /** High Pass filter reset register */
#define LSM303_ACC_MAG_REFERENCE_A             0x26    /** reference value for High Pass filter */
#define LSM303_ACC_MAG_STATUS_REG_A            0x27    /** status register of Accelerometer */

#define LSM303_ACC_MAG_OUT_X_L_A               0x28    /** lower data register of X component of Accelerometer */
#define LSM303_ACC_MAG_OUT_X_H_A               0x29    /** higher data register of X component of Accelerometer */
#define LSM303_ACC_MAG_OUT_Y_L_A               0x2A    /** lower data register of X component of Accelerometer */
#define LSM303_ACC_MAG_OUT_Y_H_A               0x2B    /** higher data register of X component of Accelerometer */
#define LSM303_ACC_MAG_OUT_Z_L_A               0x2C    /** lower data register of X component of Accelerometer */
#define LSM303_ACC_MAG_OUT_Z_H_A               0x2D    /** higher data register of X component of Accelerometer */

#define LSM303_ACC_MAG_INT1_CFG_A              0x30    /** INT1, interrupt configuration register of accelerometer */
#define LSM303_ACC_MAG_INT1_SRC_A              0x31    /** INT1, interrupt source from accelerometer */
#define LSM303_ACC_MAG_INT1_THS_A              0x32    /** threshold value for INT1 generation */
#define LSM303_ACC_MAG_INT1_DURATION_A         0x33    /** duration to validate INT1 */
#define LSM303_ACC_MAG_INT2_CFG_A              0x34    /** INT2 interrupt configuration register of accelerometer */
#define LSM303_ACC_MAG_INT2_SRC_A              0x35    /** INT2, interrupt source from accelerometer */
#define LSM303_ACC_MAG_INT2_THS_A              0x36    /** threshold value for INT2 generation */
#define LSM303_ACC_MAG_INT2_DURATION_A         0x37    /** duration to validate INT2 */

#define LSM303_ACC_MAG_CRA_REG_M               0x00    /** control register A of magnetometer */
#define LSM303_ACC_MAG_CRB_REG_M               0x01    /** control register B of magnetometer */
#define LSM303_ACC_MAG_MR_REG_M                0x02    /** mode control register of magnetometer */

#define LSM303_ACC_MAG_OUT_X_H_M               0x03    /** higher data register of X component of magnetometer */
#define LSM303_ACC_MAG_OUT_X_L_M               0x04    /** lower data register of X component of magnetometer */
#define LSM303_ACC_MAG_OUT_Y_H_M               0x05    /** higher data register of Y component of magnetometer */
#define LSM303_ACC_MAG_OUT_Y_L_M               0x06    /** lower data register of Y component of magnetometer */
#define LSM303_ACC_MAG_OUT_Z_H_M               0x07    /** higher data register of Z component of magnetometer */
#define LSM303_ACC_MAG_OUT_Z_L_M               0x08    /** lower data register of Z component of magnetometer */

#define LSM303_ACC_MAG_SR_REG_M                0x09    /** status register of magnetometer */

#define LSM303_ACC_MAG_IRA_REG_M               0x0A    /** Identification Register A of magnetometer hold fix value 0x48 */
#define LSM303_ACC_MAG_IRB_REG_M               0x0B    /** Identification Register B of magnetometer hold fix value 0x32 */
#define LSM303_ACC_MAG_IRC_REG_M               0x0C    /** Identification Register C of magnetometer hold fix value 0x33 */



/** 
 * \defgroup LSM303DLH_Accelerometer_with_Magnetometer_Control_Functions LSM303DLH Accelerometer with Magnetometer Control Functions
 * @{
 */

#define LSM303_ACC_MAG_RebootInternalRegisters_A__                 0        /** setting BOOT bit cause refresh content of Internal Registers stored in Flash memory block */
#define LSM303_ACC_MAG_FitleredDataSelection_A__                   1        /** data output from internal filter */
#define LSM303_ACC_MAG_HighPassFilterForInt2_A__                   1        /** High-pass filter enabled for interrupt 2 source */
#define LSM303_ACC_MAG_HighPassFilterForInt1_A__                   1        /** High-pass filter enabled for interrupt 1 source */
#define LSM303_ACC_MAG_ConfigIntActiveLow_A__                      1        /** Interrupt configuration */
#define LSM303_ACC_MAG_LatchInterruptRequestOnInt2_A__             1        /** Latch interrupt request on INT2_SRC register */
#define LSM303_ACC_MAG_LatchInterruptRequestOnInt1_A__             1        /** Latch interrupt request on INT1_SRC register */
#define LSM303_ACC_MAG_BlockDataUpdate_A__                         0        /** data registers updated in block */
#define LSM303_ACC_MAG_HighPassFilterReferenceSelection_A__        0x01     /** Reference selection for HighPass filter */
#define LSM303_ACC_MAG_Int2ThresholdValue_A__                      0x02     /** threshold value to validate Interrupt */
#define LSM303_ACC_MAG_Int1ThresholdValue_A__                      0x02     /** threshold value to validate Interrupt */
#define LSM303_ACC_MAG_Int2ValidationDuration_A__                  0x20     /** Duration to validate Interrupt 2 */
#define LSM303_ACC_MAG_Int1ValidationDuration_A__                  0x20     /** Duration to validate Interrupt 1 */


/* Enumeration for power mode with output data rate select for Accelerometer */

typedef enum{
    enLSM303_ACC_MAG_powerDownMode=0,
    enLSM303_ACC_MAG_powerModeNormal,
    enLSM303_ACC_MAG_lowPowerMode_ODR_0_5Hz,
    enLSM303_ACC_MAG_lowPowerMode_ODR_1Hz,
    enLSM303_ACC_MAG_lowPowerMode_ODR_2Hz,
    enLSM303_ACC_MAG_lowPowerMode_ODR_5Hz,
    enLSM303_ACC_MAG_lowPowerMode_ODR_10Hz
}PFEnLSM303_ACC_MAG_powerMode_A;

/* Enumeration for data rate selection of Accelerometer */

typedef enum{
    enLSM303_ACC_MAG_dataRate_50Hz,
    enLSM303_ACC_MAG_dataRate_100Hz,
    enLSM303_ACC_MAG_dataRate_400Hz,
    enLSM303_ACC_MAG_dataRate_1000Hz
}PFEnLSM303_ACC_MAG_dataRate_A;

/* Enumeration for High-pass filter mode selection of Accelerometer */

typedef enum{
    enLSM303_ACC_MAG_hpfMode_Normal=0,
    enLSM303_ACC_MAG_hpfMode_Ref
}PFEnLSM303_ACC_MAG_hpfMode_A;

/* Enumeration for High-pass filter cut-off frequency of Accelerometer */

typedef enum{
    enLSM303_ACC_MAG_hpfCutOffFreq_8=0,
    enLSM303_ACC_MAG_hpfCutOffFreq_16,
    enLSM303_ACC_MAG_hpfCutOffFreq_32,
    enLSM303_ACC_MAG_hpfCutOffFreq_64
}PFEnLSM303_ACC_MAG_hpfCutOffFreq_A;

/* Enumeration for data signal on INT2 control of Accelerometer */

typedef enum{
    enLSM303_ACC_MAG_int2_src=0,
    enLSM303_ACC_MAG_int2_or_int1_src,
    enLSM303_ACC_MAG_int2_dataReady,
    enLSM303_ACC_MAG_int2_BootRunning
}PFEnLSM303_ACC_MAG_int2_source_A;

/* Enumeration for data signal on INT1 control  of Accelerometer */

typedef enum{
    enLSM303_ACC_MAG_int1_src=0,
    enLSM303_ACC_MAG_int1_or_int2_src,
    enLSM303_ACC_MAG_int1_dataReady,
    enLSM303_ACC_MAG_int1_BootRunning
}PFEnLSM303_ACC_MAG_int1_source_A;

/* Enumeration for measurement range selection of Accelerometer */

typedef enum{
    enLSM303_ACC_MAG_range_2g=0,
    enLSM303_ACC_MAG_range_4g,
    enLSM303_ACC_MAG_range_8g=0x03
}PFEnLSM303_ACC_MAG_range_A;

/* Enumeration for interrupt enable on individual axes Higher and Lower component of Acceleration */

typedef enum{
    enLSM303_ACC_MAG_intNone=0,
    enLSM303_ACC_MAG_int_X_L=0x01,
    enLSM303_ACC_MAG_int_X_H=0x02,
    enLSM303_ACC_MAG_int_Y_L=0x04,
    enLSM303_ACC_MAG_int_Y_H=0x08,
    enLSM303_ACC_MAG_int_Z_L=0x10,
    enLSM303_ACC_MAG_int_Z_H=0x20
}PFEnLSM303_ACC_MAG_intComponent_A;

/* Enumeration for configuration of Interrupts */

typedef enum{
    enLSM303_ACC_MAG_orCombinationOfInt=0,
    enLSM303_ACC_MAG_sixDirMovRecogntion,
    enLSM303_ACC_MAG_andCombinationOfInt,
    enLSM303_ACC_MAG_sixDirPosRecogntion
}PFEnLSM303_ACC_MAG_configIntSource;

/* Enumeration for participant axes in measurement of Acceleration */

typedef enum{
    enLSM303_ACC_MAG_ComponentNone=0,
    enLSM303_ACC_MAG_Component_Z,
    enLSM303_ACC_MAG_Component_Y,
    enLSM303_ACC_MAG_Component_ZY,
    enLSM303_ACC_MAG_Component_X,
    enLSM303_ACC_MAG_Component_ZX,
    enLSM303_ACC_MAG_Component_XY,
    enLSM303_ACC_MAG_Component_XYZ
}PFEnLSM303_ACC_MAG_component_A;

/* Enumeration for Measurement mode of magnetometer */

typedef enum{
    enLSM303_ACC_MAG_NormalBias=0,                                          /**< (default) Normal Measurement configuration mode */
    enLSM303_ACC_MAG_PositiveBias,                                          /**< force Positive Bias current through load Resistor */
    enLSM303_ACC_MAG_NegetiveBias                                           /**< force Negative Bias current through load Resistor */
}PFEnLSM303_ACC_MAG_bias_M;

/* Enumeration for Data Output Rate of Magnetometer */

typedef enum{
    enLSM303_ACC_MAG_DataRate_0_75=0,
    enLSM303_ACC_MAG_DataRate_1_5,
    enLSM303_ACC_MAG_DataRate_3_0,
    enLSM303_ACC_MAG_DataRate_7_5,
    enLSM303_ACC_MAG_DataRate_15_0,
    enLSM303_ACC_MAG_DataRate_30_0,
    enLSM303_ACC_MAG_DataRate_75_0
}PFEnLSM303_ACC_MAG_dataRate_M;

/* Enumeration for Gain selection of magnetometer */

typedef enum{
    enLSM303_ACC_MAG_Gain_None=0,
    enLSM303_ACC_MAG_Gain_1055,
    enLSM303_ACC_MAG_Gain_795,
    enLSM303_ACC_MAG_Gain_635,
    enLSM303_ACC_MAG_Gain_430,
    enLSM303_ACC_MAG_Gain_375,
    enLSM303_ACC_MAG_Gain_320,
    enLSM303_ACC_MAG_Gain_230
}PFEnLSM303_ACC_MAG_gain_M;

/* Enumeration for Device measurement Mode selection of Magnetometer */

typedef enum{
    enLSM303_ACC_MAG_ModeCont=0,
    enLSM303_ACC_MAG_ModeSingle,
    enLSM303_ACC_MAG_ModeIdle=0x03
}PFEnLSM303_ACC_MAG_mode_M;

/* LSM303DLH configuration structure */

typedef struct{
    PFEnLSM303_ACC_MAG_bias_M             bias_M;                           /**< bias current polarity through load resistance */
    PFEnLSM303_ACC_MAG_dataRate_M         dataRate_M;                       /**< output data rate selection of magnetometer  */
    PFEnLSM303_ACC_MAG_gain_M             gain_M;                           /**< gain selection of Magnetometer */
    PFEnLSM303_ACC_MAG_mode_M             mode_M;                           /**< mode selection of magnetometer */
    PFEnLSM303_ACC_MAG_powerMode_A        powerMode_A;                      /**< power mode selection */
    PFEnLSM303_ACC_MAG_dataRate_A         dataRate_A;                       /**< output data rate selection of Accelerometer  */
    PFEnLSM303_ACC_MAG_component_A        compSelec_A;                      /**< component selection for Acceleration measurement */
    PFEnLSM303_ACC_MAG_hpfMode_A          hpfMode_A;                        /**< gain selection of high-pass filter for Accelerometer */
    PFEnLSM303_ACC_MAG_configIntSource    configInt1_Source_A;              /**< configuration of Interrupt source */
    PFEnLSM303_ACC_MAG_configIntSource    configInt2_Source_A;              /**< configuration of Interrupt source */
    PFEnLSM303_ACC_MAG_hpfCutOffFreq_A    cutOff_Freq_A;                    /**< cut-off frequency selection of Accelerometer */
    PFEnLSM303_ACC_MAG_int2_source_A      sourceINT2_A;                     /**< data signal on INT2 control of Accelerometer */
    PFEnLSM303_ACC_MAG_int1_source_A      sourceINT1_A;                     /**< data signal on INT1 control of Accelerometer */
    PFEnLSM303_ACC_MAG_range_A            range_A;                          /**< Acceleration measurement range selection */
    PFEnLSM303_ACC_MAG_intComponent_A     intEnable_onInt1_A;               /**< interrupt enable for individual axis component on int1 */
    PFEnLSM303_ACC_MAG_intComponent_A     intEnable_onInt2_A;               /**< interrupt enable for individual axis component on int2 */
		PFEnStatus (*pfI2cWrite)(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size);
		PFEnStatus (*pfI2cRead)(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size, PFword* readBytes);
}PFCfgLSM303_ACC_MAG;

/* pointer to PFCfgLSM303_ACC_MAG structure */

typedef PFCfgLSM303_ACC_MAG* PPFCfgLSM303_ACC_MAG;

/**
 * To initialize LSM303DLH Accelerometer with Magnetometer module
 *
 * \param devId, pointer to copy id/id's assigned by driver to lsm303
 *
 * \param sensorConfig, structure to config LSM303DLH Accelerometer with Magnetometer
 *
 * \return status of initialization
 *
 * \note Function needs TWI/I2C to be initialized before calling this function
 */
PFEnStatus pfLSM303_ACC_MAG_open(PFbyte *devId,PPFCfgLSM303_ACC_MAG sensorConfig,PFbyte deviceCount);


/**
 * to close lsm303 with id devId
 *
 * \param devId, specify id allocated to driver
 *
 * \return status of device close
 *
 */
PFEnStatus pfLSM303_ACC_MAG_close(PFbyte* devId);

/**
 * To read raw values from LSM303DLH's Accelerometer in Three Axis
 *
 * \param devId, pointer to specify id assigned to driver
 *
 * \param Xcomp, pointer to copy acceleration in X-axis 
 *
 * \param Ycomp, pointer to copy acceleration in Y-axis
 *
 * \param Zcomp, pointer to copy acceleration in Z-axis
 *
 * \return status of Read Accelerometer
 */
PFEnStatus pfLSM303_ACC_MAG_readAccelerometer(PFbyte* devId,PFword* Xcomp, PFword* Ycomp, PFword* Zcomp);
/**
 * To read Acceleration of LSM303DLH's Accelerometer in Three Axis scaled in mg (milliG)
 *
 * \param devId, pointer to specify id assigned to driver
 *
 * \param Xcomp, pointer to copy acceleration in X-axis 
 *
 * \param Ycomp, pointer to copy acceleration in Y-axis
 *
 * \param Zcomp, pointer to copy acceleration in Z-axis
 *
 * \return status of get Acceleration
 */
PFEnStatus pfLSM303_ACC_MAG_getAcceleration(PFbyte* devId,PFword* Xcomp, PFword* Ycomp, PFword* Zcomp);

/**
 * To get INT1 interrupt sources by LSM303DLH's Accelerometer in Three Axis 
 *
 * \param devId, pointer to specify id assigned to driver
 *
 * \param intSource, array to copy sources of INT1 interrupts
 * 
 * \return status of get Interrupt source
 */
PFEnStatus pfLSM303_ACC_MAG_getINT1source(PFbyte* devId,PFEnLSM303_ACC_MAG_intComponent_A intSource[]);

/**
 * To get INT2 interrupt sources by LSM303DLH's Accelerometer in Three Axis 
 *
 * \param devId, pointer to specify id assigned to driver
 *
 * \param intSource, array to copy sources of INT2 interrupts
 * 
 * \return status of get Interrupt source
 */
PFEnStatus pfLSM303_ACC_MAG_getINT2source(PFbyte* devId,PFEnLSM303_ACC_MAG_intComponent_A intSource[]);
/**
 * To get magnetism(static/dynamic magnetic field) from LSM303DLH's Magnetometer in milliGauss
 *
 * \param devId, pointer to specify id assigned to driver
 *
 * \param Xcomp, pointer to copy magnetism with Respect to X-axis
 *
 * \param Ycomp, pointer to copy magnetism with Respect to Y-axis
 *
 * \param Zcomp, pointer to copy magnetism with Respect to z-axis
 *
 * \return status of read magnetism
 */
PFEnStatus pfLSM303_ACC_MAG_getMagnetism(PFbyte* devId,PFword* Xcomp, PFword* Ycomp, PFword* Zcomp);

/**
 * To get raw values from LSM303DLH's Magnetometer
 *
 * \param devId, pointer to specify id assigned to driver
 *
 * \param Xcomp, pointer to copy raw value from magnetometer with Respect to X-axis
 *
 * \param Ycomp, pointer to copy raw value from magnetometer with Respect to Y-axis
 *
 * \param Zcomp, pointer to copy raw value from magnetometer with Respect to z-axis
 *
 * \return status of read magnetometer
 */
PFEnStatus pfLSM303_ACC_MAG_readMagnetometer(PFbyte* devId,PFword* Xcomp, PFword* Ycomp, PFword* Zcomp);

/**
 * To get heading Angle in horizontal plane from LSM303dlh's Magnetometer
 *
 * \param devId, pointer to specify id assigned to driver
 *
 * \param headingAngle, to copy heading Angle
 *
 * \return status of get headingAngle
 */
PFEnStatus pfLSM303_ACC_MAG_getHeadingAngle(PFbyte* devId,PFword* headingAngle);

/**
 * To read Identification Registers A, B & C of LSM303DLH's Magnetometer
 *
 * \param devId, pointer to specify id assigned to driver
 *
 * \param ida, to copy Identification Register A
 *
 * \param idb, to copy Identification Register B
 *
 * \param idc, to copy Identification Register C
 *
 * \return status of ID read operation
 */
PFEnStatus pfLSM303_ACC_MAG_getID(PFbyte* devId,PFbyte* ida, PFbyte* idb, PFbyte* idc);


/** @} */


