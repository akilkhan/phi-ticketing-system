/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework A3977 Stepper Motor Driver
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_A3977_API STEPPER MOTOR DRIVER A3977 API
 * @{
 */

#define A3977_MAX_SPEED_LIMIT                  10000   /** maximum speed limit(in steps/sec) for A3977 */
#define A3977_MAX_NO_OF_LYNX_CONNECTED         3     /** number of lynx connected */

/**
 * callback for all used timers ISR for motor control
 * this is common for all motor
 * \note user should not try to overwrite these function
 */
void stepperControlFunction(void);

/** Enumeration for selection of stepsize of stepper with A3977 */
typedef enum
{
	enA3977StepResolutionFull = 0,   /**< stepper run in full step mode */
	enA3977StepResolutionHalf,       /**< stepper run in half step mode */
	enA3977StepResolutionQuarter,    /**< stepper run in 1/4th step mode */
	enA3977StepResolutionEighth      /**< stepper run in 1/8th step mode */
}PFEnA3977StepResolution;

/** Enumeration for selection direction of stepper with A3977 */
typedef enum
{
	enA3977dirCW = 0,   /**< rotate in clockwise direction */
	enA3977dirCCW       /**< rotate in anti-clockwise direction */
}PFEnA3977dir;

/** Configuration structure for A3977 Stepper Motor Driver		*/
typedef struct
{
	PFGpioPortPin homeGpio;		                                               /**< Assign gpio pin for A3977 home output				*/
	PFGpioPortPin dirGpio;			                                             /**< Assign gpio pin for A3977 direction input			*/
	PFGpioPortPin ms1Gpio;					                                         /**< Assign gpio pin for A3977 MS1 input				*/
	PFGpioPortPin ms2Gpio;					                                         /**< Assign gpio pin for A3977 MS2 input				*/
	PFGpioPortPin resetGpio;				                                         /**< Assign gpio pin for A3977 active low reset input	*/
	PFGpioPortPin stepGpio;					                                         /**< Assign gpio pin for A3977 step input				*/
	PFGpioPortPin sleepGpio;				                                         /**< Assign gpio pin for A3977 active low sleep input	*/
	PFEnA3977StepResolution stepResolution;	                                 /**< Step resolution for the stepper motor				*/
	PFdword stepAngle;					                                             /**< Step angle in degrees. This value must be the actual angle multiplied by 10 */
	PFfloat acceleration;                                                    /**< acceleration profile in steps/s^2 (corresponds to full step)*/
	PFfloat deceleration;                                                    /**< deceleration profile in steps/s^2 (corresponds to full step)*/
	PFEnStatus (*pfTimerReset)(void);                                        /** function pointer to timer reset */
	PFEnStatus (*pfTimerStart)(void);                                        /** function pointer to timer start */
	PFEnStatus (*pfTimerStop)(void);                                         /** function pointer to timer stop */
	PFEnStatus (*pfTimerGetTickFreq)(PFdword* tickClk);                      /** function pointer to timer get timer frequency */
	PFEnStatus (*pfTimerClearIntStatus)(PFdword intStatus);                  /** function pointer to timer clear intterupt status  */
	PFEnStatus (*pfTimerGetIntStatus)(PFdword* status);                      /** function pointer to timer intterupt status read */
	PFEnStatus (*pfTimerUpdateMatchRegister)(PFbyte regNum, PFdword regVal); /** function pointer to timer update match register */
}PFCfgStepperA3977;

/** Pointer to PFCfgStepperA3977 structure		*/
typedef PFCfgStepperA3977*		PFpCfgStepperA3977;

/**
 * To initialize stepper driver A3977
 *
 * \param devId pointer to copy id/id's assigned by driver to motor/motor's
 *
 * \param config pointer to structure to configure stepper driver A3977
 *
 * \param deviceCount number of stepper driver A3977 to be initialised 
 *
 * \return status of initialization
 *
 * \note timer should be initialized with isr stepperControlFunction before calling the function
 */
PFEnStatus pfA3977Open(PFbyte* id, PFpCfgStepperA3977 config, PFbyte deviceCount);

/**
 * to close stepper with id devId
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of stepper close operation
 */
PFEnStatus pfA3977Close(PFbyte* devId);

/**
 * to close all steppers
 *
 * \return status of stepper close operation
 */
PFEnStatus pfA3977CloseAll(void);

/**
 * to reset driver A3977
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of A3977 reset operation
 */
PFEnStatus pfA3977ResetDevice(PFbyte* devId);

/**
 * to reset home position of stepper by reseting ABS_POS register
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of resetPos operation
 */
PFEnStatus pfA3977ResetPosition(PFbyte* devId);

/**
 * to test weather device is ready(stepper not running, not in sleep mode)
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param test pointer to copy busy/ready status of driver. True if device is ready
 *
 * \return status of device 
 */
PFEnStatus pfA3977DeviceReady(PFbyte* devId, PFEnBoolean* test);

/**
 * to put A3977 in sleep mode
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param sleepState true: to put in sleep state.
 *									 false: to remove from sleep state
 *
 * \return status of A3977 sleep operation
 */
PFEnStatus pfA3977Sleep(PFbyte* devId, PFEnBoolean sleepState);

/**
 * To set stepper in continuous running mode with specified speed
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param dir directio of rotation  0: for clockwise 1: for anticlockwise
 *
 * \param speed speed(in steps/s) at which stepper shuold run 
 *
 * \return status of run operation
 */
PFEnStatus pfA3977run(PFbyte* devId, PFbyte dir, PFfloat speed);

/**
 * to perform soft stop(decelerate to zero speed) operation 
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of soft Stop operation
 */
PFEnStatus pfA3977softStop(PFbyte* devId);

/**
 * to perform hard stop(immediate stop) operation
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of hard Stop operation
 */
PFEnStatus pfA3977hardStop(PFbyte* devId);
 
/**
 * to move stepper by specified number of steps as per stepsize selection in specified direction
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param dir direction of rotation  0: for clockwise 1: for anticlockwise
 *
 * \param steps number of steps stepper shuold move, as per stepsize selection
 *
 * \param speed value specify speed of rotation of stepper in steps/sec(always in full step)
 *
 * \return status of move operation
 * \note command keeps the BUSY flag low until the target number of steps is performed
 *       This command can only be performed when the motor is stopped.
 */
PFEnStatus pfA3977Move(PFbyte* devId, PFbyte dir, PFdword steps, PFfloat speed);

/**
 * to return stepper to its initial home position
 *
 * \param devId pointer specify id allocated to driver
 *
 * \return status of go home operation
 */
PFEnStatus pfA3977GoHomePosition(PFbyte* devId);

/**
 * to set new acceleration profile
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param acceleration new acceleration value in steps/s2 to be set
 *
 * \param deceleration new deceleration value in steps/s2 to be set
 *
 * \return status of set Acceleration profile
 */
PFEnStatus pfA3977SetAccelerationProfile(PFbyte* devId, PFfloat acceleration, PFfloat deceleration);

/**
 * to read current position of stepper
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param position pointer to copy current position of stepper in microsteps, +ve for anticlockwise while -ve for clockwise direction 
 *
 * \return status of read Current Position value
 */
PFEnStatus pfA3977GetCurrentPosition(PFbyte* devId, PFsdword* position);

/**
 * to read current speed of stepper
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param speed pointer to copy current speed of stepper
 *
 * \return status of read Current Position value
 */ 
PFEnStatus pfA3977getSpeed(PFbyte* devId, PFfloat* speed);
 
 /**
 * to read acceleration profile 
 *
 * \param devId pointer specify id allocated to driver
 *
 * \param acceleration pointer to copy current acceleration value in steps/s2
 *
 * \param deceleration pointer to copy current deceleration value in steps/s2
 *
 * \return status of read Current Acceleration profile
 */
PFEnStatus pfA3977getAccelerationProfile(PFbyte* devId, PFfloat* acceleration, PFfloat* deceleration);

/** @} */ 

