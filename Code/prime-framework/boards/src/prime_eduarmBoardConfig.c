#include "prime_framework.h"
#ifdef MCU_CHIP_lpc1768
#include "prime_gpio.h"
#include "prime_eduarmBoardConfig.h"

PFCfgGpio pfEduArmLEDGpioCfg[3] = 
{
	// LED UL1
	{HYDRA_LED_UL1_PORT, HYDRA_LED_UL1_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED UL2
	{HYDRA_LED_UL2_PORT, HYDRA_LED_UL2_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED UL3
	{HYDRA_LED_UL3_PORT, HYDRA_LED_UL3_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio}
};
