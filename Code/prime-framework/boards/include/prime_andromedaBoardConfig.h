#pragma once
/**	LED GPIO Configuration Macros	*/

#define		ANDROMEDA_LED_UL1_PORT		GPIO_PORTD
#define		ANDROMEDA_LED_UL1_PIN			BIT_4

#define		ANDROMEDA_LED_UL2_PORT		GPIO_PORTD
#define		ANDROMEDA_LED_UL2_PIN			BIT_5

#define		ANDROMEDA_LED_UL3_PORT		GPIO_PORTD
#define		ANDROMEDA_LED_UL3_PIN			BIT_6

#define		ANDROMEDA_LED_UL4_PORT		GPIO_PORTD
#define		ANDROMEDA_LED_UL4_PIN			BIT_7


/**	Push Button GPIO Configuration Macros  */

#define		ANDROMEDA_PUSHBUTTON_PB1_PORT		GPIO_PORTK
#define		ANDROMEDA_PUSHBUTTON_PB1_PIN		BIT_4

#define		ANDROMEDA_PUSHBUTTON_PB2_PORT		GPIO_PORTK
#define		ANDROMEDA_PUSHBUTTON_PB2_PIN		BIT_5

#define		ANDROMEDA_PUSHBUTTON_PB3_PORT		GPIO_PORTK
#define		ANDROMEDA_PUSHBUTTON_PB3_PIN		BIT_6

#define		ANDROMEDA_PUSHBUTTON_PB4_PORT		GPIO_PORTK
#define		ANDROMEDA_PUSHBUTTON_PB4_PIN		BIT_7

/**	USART GPIO Configuration Macros		*/

/**	USART0	*/

#define		ANDROMEDA_J4_2_USART0_RXD0_PORT		GPIO_PORTE
#define		ANDROMEDA_J4_2_USART0_RXD0_PIN		BIT_0

#define		ANDROMEDA_J4_3_USART0_TXD0_PORT		GPIO_PORTE
#define		ANDROMEDA_J4_3_USART0_TXD0_PIN		BIT_1	

#define		ANDROMEDA_J4_4_USART0_XCK0_PORT		GPIO_PORTE
#define		ANDROMEDA_J4_4_USART0_XCK0_PIN		BIT_2	

/**	USART1  */

#define		ANDROMEDA_EXP_17_USART1_RXD1_PORT		GPIO_PORTD	
#define		ANDROMEDA_EXP_17_USART1_RXD1_PIN		BIT_2

#define		ANDROMEDA_EXP_18_USART1_TXD1_PORT		GPIO_PORTD
#define		ANDROMEDA_EXP_18_USART1_TXD1_PIN		BIT_3

#define		ANDROMEDA_J7_11_USART1_XCK1_PORT		GPIO_PORTD
#define		ANDROMEDA_J7_11_USART1_XCK1_PIN		BIT_5	

/** USART2  */

#define		ANDROMEDA_J4_12_USART2_RXD2_PORT		GPIO_PORTH
#define		ANDROMEDA_J4_12_USART2_RXD2_PIN		BIT_0

#define		ANDROMEDA_J4_13_USART2_TXD2_PORT		GPIO_PORTH
#define		ANDROMEDA_J4_13_USART2_TXD2_PIN		BIT_1	

#define		ANDROMEDA_J4_14_USART2_XCK2_PORT		GPIO_PORTH
#define		ANDROMEDA_J4_14_USART2_XCK2_PIN		BIT_2	

/**  External Interrupt Source GPIO Configuration Macros */

/**	INT0	*/

#define		ANDROMEDA_EXP_11_INT0_PORT		GPIO_PORTD
#define		ANDROMEDA_EXP_11_INT0_PIN			BIT_0

/**INT1		*/

#define		ANDROMEDA_EXP_12_INT1_PORT		GPIO_PORTD
#define		ANDROMEDA_EXP_12_INT1_PIN			BIT_1

/**	INT2	*/

#define		ANDROMEDA_EXP_17_INT2_PORT		GPIO_PORTD				         
#define		ANDROMEDA_EXP_17_INT2_PIN			BIT_2

/**	INT3	*/

#define		ANDROMEDA_EXP_18_INT3_PORT		GPIO_PORTD						
#define		ANDROMEDA_EXP_18_INT3_PIN			BIT_3

/**	INT4	*/

#define		ANDROMEDA_J4_6_INT4_PORT			GPIO_PORTE						
#define		ANDROMEDA_J4_6_INT4_PIN			BIT_4

/**	INT5	*/

#define 	ANDROMEDA_J4_7_INT5_PORT			GPIO_PORTE						
#define 	ANDROMEDA_J4_7_INT5_PIN			BIT_5

/**	INT6	*/

#define 	ANDROMEDA_J4_8_INT6_PORT			GPIO_PORTE						
#define 	ANDROMEDA_J4_8_INT6_PIN			BIT_6

/**	INT7	*/

#define 	ANDROMEDA_EXP_10_INT7_PORT			GPIO_PORTE						
#define 	ANDROMEDA_EXP_10_INT7_PIN			BIT_7


/**	Timer GPIO Configuration Macros	*/

/**	Timer0   */

/** OC0A pin used for SD card interface */

#define 	ANDROMEDA_J7_13_TIMER0_T0_PORT		GPIO_PORTD	
#define 	ANDROMEDA_J7_13_TIMER0_T0_PIN			BIT_7

/**	Timer1	*/

/** OC1A pin used for SPI to CAN Interface */
/** OC1B and OC1C pins are used for SD card interface */

#define 	ANDROMEDA_J7_10_TIMER1_ICP1_PORT		GPIO_PORTD	
#define 	ANDROMEDA_J7_10_TIMER1_ICP1_PIN		BIT_4

#define 	ANDROMEDA_J7_12_TIMER1_T1_PORT		GPIO_PORTD	
#define 	ANDROMEDA_J7_12_TIMER1_T1_PIN			BIT_6

/**	Timer2	*/

/** OC2A pin used for SPI to CAN interface */

#define 	ANDROMEDA_J4_18_TIMER2_OC2B_PORT		GPIO_PORTH
#define 	ANDROMEDA_J4_18_TIMER2_OC2B_PIN		BIT_6

/**	Timer3	*/

#define 	ANDROMEDA_J4_5_TIMER3_OC3A_PORT		GPIO_PORTE
#define 	ANDROMEDA_J4_5_TIMER3_OC3A_PIN		BIT_3

#define 	ANDROMEDA_J4_6_TIMER3_OC3B_PORT		GPIO_PORTE
#define 	ANDROMEDA_J4_6_TIMER3_OC3B_PIN		BIT_4

#define 	ANDROMEDA_J4_7_TIMER3_OC3C_PORT		GPIO_PORTE
#define 	ANDROMEDA_J4_7_TIMER3_OC3C_PIN		BIT_5

#define 	ANDROMEDA_J4_8_TIMER3_T3_PORT			GPIO_PORTE
#define 	ANDROMEDA_J4_8_TIMER3_T3_PIN			BIT_6

#define 	ANDROMEDA_EXP_10_TIMER3_ICP3_PORT		GPIO_PORTE
#define 	ANDROMEDA_EXP_10_TIMER3_ICP3_PIN		BIT_7

/**	Timer4	*/

#define 	ANDROMEDA_J4_15_TIMER4_OC4A_PORT		GPIO_PORTH
#define 	ANDROMEDA_J4_15_TIMER4_OC4A_PIN		BIT_3

#define 	ANDROMEDA_J4_16_TIMER4_OC4B_PORT		GPIO_PORTH
#define 	ANDROMEDA_J4_16_TIMER4_OC4B_PIN		BIT_4

#define 	ANDROMEDA_J4_17_TIMER4_OC4C_PORT		GPIO_PORTH
#define 	ANDROMEDA_J4_17_TIMER4_OC4C_PIN		BIT_5

#define 	ANDROMEDA_J4_19_TIMER4_T4_PORT		GPIO_PORTH
#define 	ANDROMEDA_J4_19_TIMER4_T4_PIN			BIT_7

#define 	ANDROMEDA_J7_2_TIMER4_ICP4_PORT		GPIO_PORTL
#define 	ANDROMEDA_J7_2_TIMER4_ICP4_PIN		BIT_0

/**	Timer5	*/

#define 	ANDROMEDA_J7_5_TIMER5_OC5A_PORT		GPIO_PORTL
#define 	ANDROMEDA_J7_5_TIMER5_OC5A_PIN		BIT_3

#define 	ANDROMEDA_J7_6_TIMER5_OC5B_PORT		GPIO_PORTL
#define 	ANDROMEDA_J7_6_TIMER5_OC5B_PIN		BIT_4

#define 	ANDROMEDA_J7_7_TIMER5_OC5C_PORT		GPIO_PORTL
#define 	ANDROMEDA_J7_7_TIMER5_OC5C_PIN		BIT_5

#define 	ANDROMEDA_J7_4_TIMER5_T5_PORT		GPIO_PORTL
#define 	ANDROMEDA_J7_4_TIMER5_T5_PIN			BIT_2

#define 	ANDROMEDA_J7_3_TIMER5_ICP5_PORT		GPIO_PORTL
#define 	ANDROMEDA_J7_3_TIMER5_ICP5_PIN		BIT_1

/**	ADC GPIO Configuration Macros	*/

/** ADC0 */

#define 	ANDROMEDA_J5_19_ADC0_PORT		GPIO_PORTF
#define 	ANDROMEDA_J5_19_ADC0_PIN		BIT_0

/** ADC1 */

#define 	ANDROMEDA_J5_18_ADC1_PORT		GPIO_PORTF
#define 	ANDROMEDA_J5_18_ADC1_PIN		BIT_1

/** ADC2 */

#define 	ANDROMEDA_J5_17_ADC2_PORT		GPIO_PORTF
#define 	ANDROMEDA_J5_17_ADC2_PIN		BIT_2

/** ADC3 */

#define 	ANDROMEDA_J5_16_ADC3_PORT		GPIO_PORTF
#define 	ANDROMEDA_J5_16_ADC3_PIN		BIT_3

/** ADC4 */

#define 	ANDROMEDA_J5_15_ADC4_PORT		GPIO_PORTF
#define 	ANDROMEDA_J5_15_ADC4_PIN		BIT_4

/** ADC5 */

#define 	ANDROMEDA_J5_14_ADC5_PORT		GPIO_PORTF
#define 	ANDROMEDA_J5_14_ADC5_PIN		BIT_5

/** ADC6 */

#define 	ANDROMEDA_J5_13_ADC6_PORT		GPIO_PORTF
#define 	ANDROMEDA_J5_13_ADC6_PIN		BIT_6

/** ADC7 */

#define 	ANDROMEDA_J5_12_ADC7_PORT		GPIO_PORTF
#define 	ANDROMEDA_J5_12_ADC7_PIN		BIT_7

/**	SPI GPIO Configuration Macros	*/

#define 	ANDROMEDA_EXP_9_SPI_MOSI_PORT		GPIO_PORTB
#define 	ANDROMEDA_EXP_9_SPI_MOSI_PIN		BIT_2

#define 	ANDROMEDA_EXP_8_SPI_MISO_PORT		GPIO_PORTB
#define 	ANDROMEDA_EXP_8_SPI_MISO_PIN		BIT_3

#define 	ANDROMEDA_EXP_6_SPI_SCK_PORT		GPIO_PORTB
#define 	ANDROMEDA_EXP_6_SPI_SCK_PIN		BIT_1

#define		ANDROMEDA_SPI_SS_PORT				GPIO_PORTB
#define		ANDROMEDA_SPI_SS_PIN				BIT_0

/** I2C GPIO Configuration Macros*/

#define 	ANDROMEDA_EXP_12_I2C_SDA_PORT		GPIO_PORTD
#define 	ANDROMEDA_EXP_12_I2C_SDA_PIN		BIT_1

#define 	ANDROMEDA_EXP_11_I2C_SCL_PORT		GPIO_PORTD
#define 	ANDROMEDA_EXP_11_I2C_SCL_PIN		BIT_0
 
/**	Analog Comparator GPIO Configuration Macros	 */
 
#define 	ANDROMEDA_J4_4_ACOMP_AIN0_PORT	GPIO_PORTE
#define 	ANDROMEDA_J4_4_ACOMP_AIN0_PIN		BIT_2

#define 	ANDROMEDA_J4_5_ACOMP_AIN1_PORT	GPIO_PORTE
#define 	ANDROMEDA_J4_5_ACOMP_AIN1_PIN		BIT_3

 
/**	Divided System Clock GPIO Configuration Macros	
 *\note note When the CKOUT Fuse is programmed, the system Clock will be output on CLKO.
 */ 

#define 	ANDROMEDA_EXP_10_CLKO_PORT		GPIO_PORTE
#define 	ANDROMEDA_EXP_10_CLKO_PIN			BIT_7



#define		ANDROMEDA_J5_18_SI_PORT						GPIO_PORTF
#define		ANDROMEDA_J5_18_SI_PIN						BIT_1
 
#define		ANDROMEDA_J5_17_CLK_PORT						GPIO_PORTF
#define		ANDROMEDA_J5_17_CLK_PIN						BIT_2

#define		ANDROMEDA_J5_19_AOUT_PORT						GPIO_PORTF
#define		ANDROMEDA_J5_19_AOUT_PIN						BIT_0

#define 	ANDROMEDA_CAMERA_ADC_CHANNEL		0
 

void andromedaBoardInit(void);

/** LED gpio pin configuration */
extern PFCfgGpio pfAndromedaLEDGpioCfg[4];

/** Pushbutton gpio pin configuration */
extern PFCfgGpio pfAndromedaPushButtonGpioCfg[4];

extern PFCfgGpio pfAndromedaUsart0MasterGpioCfg[3];		 /** For Synchronous master Mode of USART0 */
extern PFCfgGpio pfAndromedaUsart0SlaveGpioCfg[3]; 		 /** For Synchronous slave Mode of USART0 */

extern PFCfgGpio pfAndromedaUart0GpioCfg[2];		     /** UART0 gpio pin configuration */
extern PFCfgGpio pfAndromedaUart1GpioCfg[2];		     /** UART1 gpio pin configuration */
extern PFCfgGpio pfAndromedaUart2GpioCfg[2];		     /** UART2 gpio pin configuration */

extern PFCfgGpio pfAndromedaInt0GpioCfg;                 /** EINT0 gpio pins configuration */
extern PFCfgGpio pfAndromedaInt1GpioCfg;                 /** EINT1 gpio pins configuration */
extern PFCfgGpio pfAndromedaInt2GpioCfg;                 /** EINT2 gpio pins configuration */
extern PFCfgGpio pfAndromedaInt3GpioCfg;                 /** EINT3 gpio pins configuration */
extern PFCfgGpio pfAndromedaInt4GpioCfg;                 /** EINT4 gpio pins configuration */
extern PFCfgGpio pfAndromedaInt5GpioCfg;                 /** EINT5 gpio pins configuration */
extern PFCfgGpio pfAndromedaInt6GpioCfg;                 /** EINT6 gpio pins configuration */
extern PFCfgGpio pfAndromedaInt7GpioCfg;                 /** EINT7 gpio pins configuration */

extern PFCfgGpio pfAndromedaADC1GpioCfg;                 /** ADC1 gpio pins configuration */
extern PFCfgGpio pfAndromedaADC2GpioCfg;                 /** ADC2 gpio pins configuration */
extern PFCfgGpio pfAndromedaADC3GpioCfg;                 /** ADC3 gpio pins configuration */
extern PFCfgGpio pfAndromedaADC4GpioCfg;                 /** ADC4 gpio pins configuration */
extern PFCfgGpio pfAndromedaADC5GpioCfg;                 /** ADC5 gpio pins configuration */
extern PFCfgGpio pfAndromedaADC6GpioCfg;                 /** ADC6 gpio pins configuration */

extern PFCfgGpio pfAndromedaSPIMasterGpioCfg[4];         /** SSP in master mode gpio pin configuration */
extern PFCfgGpio pfAndromedaSPISlaveGpioCfg[4];          /** SSP in slave mode gpio pin configuration */
extern PFCfgGpio pfAndromedaI2CGpioCfg[2];               /** I2C gpio pin configuration */
extern PFCfgGpio pfAndromedaAnlgCompGpioCfg[2];          /** Analog comparator gpio pin configuration */


