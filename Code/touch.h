/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Touch Driver
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup TOUCH_DRIVER_API TOUCH Driver API
 * @{
 */
/**		Enumeration for the coordinates of the touch Driver		*/
 typedef enum{
	enCoordinate_X = 0,
	enCoordinate_Y = 1
}PFEnCoordinate;

/** Touch Configuration structure */
typedef struct{
	PFdword power_down;
	PFdword conversion_mode;
	PFdword conversion_width;
	PFdword pressure_threshold;
	PFdword precision;
	PFdword max_x;
	PFdword max_y;
	PFdword margin_x;
	PFdword margin_y;
	PFdword appllication_mode;
}PFCfgTouch;

/** pointer to structure PFCgfTouch */
typedef PFCfgTouch* PFpCfgTouch;

/**
 * To initialize touch module
 *
 * \param TouchConfig, pointer to config structure to configure touch driver 
 *
 * \return status of initialize
 * 
 */
PFEnStatus pfTouchOpen(PFpCfgTouch touchConfig);
/**
 * To get the coordinates from touch module
 *
 * \param pointer to x and y to store the coordinates from touch driver
 *
 * \return status of whether the user got coordinates from touch driver
 * 
 */
PFEnStatus pfTouchGetCoordinates(PFdword* x_pos, PFdword* y_pos);