#include "prime_framework.h"
#include "prime_sysclk.h"
#include "prime_utils.h"
#include "prime_types.h"
#include "prime_i2s.h"
#include "prime_fifo.h"


PFCfgI2s i2sCfg;
PFEnBoolean ConfigInit=enBooleanFalse;
//static PFbyte i2sChInt;

PFcallback txCallback;
PFcallback rxCallback;

PFEnStatus pfI2sOpen(PFpCfgI2s config)
{

	PFdword regtxConfig = 0;
	PFdword regrxConfig = 0;
	PFdword txbps=0;
	PFdword rxbps=0;
	PFdword i2sMclk=0;
	PFdword bitrate;
//	PFdword datacheck=0;
#if(PF_I2S_DEBUG == 1)
	{
	CHECK_NULL_PTR(config);
	 if( (config->txChannelConfig->txWordWidth > 3)
		return enStatusInvArgs;
	}
#endif
	POWER_ON(I2S_CH);
	pfSysSetPclkDiv(PCLK_DIV(I2S_CH),config->clkDiv);
    I2S_CHANNEL->I2SDAI = I2S_CHANNEL->I2SDAI = 0x00;
	
	txbps = (config->txChannelConfig->txWordWidth +1)*8;
	rxbps = (config->rxChannelConfig->rxWordWidth+1)*8;
	
	regtxConfig = (txbps - 1)<<6 | (config->txChannelConfig->txMode)<<5 | (config->txChannelConfig->txReset)<<4 |
		(config->txChannelConfig->txStop)<<3 | (config->txChannelConfig->txFormat)<<2 | (config->txChannelConfig->txWordWidth);
	I2S_CHANNEL->I2SDAO = regtxConfig;
	

	regrxConfig = (rxbps - 1)<<6 | (config->rxChannelConfig->rxMode)<<5 | (config->rxChannelConfig->rxReset)<<4 |
		(config->rxChannelConfig->rxStop)<<3 | (config->rxChannelConfig->rxFormat)<<2 | (config->rxChannelConfig->rxWordWidth);
	
	I2S_CHANNEL->I2SDAI = regrxConfig;
		
    I2S_CHANNEL->I2STXMODE &= ~0x0F; //clear bit 3:0 in I2STXMODE register
	I2S_CHANNEL->I2SRXMODE &= ~0x0F; //clear bit 3:0 in I2SRXMODE register
		if (config->txChannelConfig->txClkSel == enI2sClkSel_MCLK) {
			I2S_CHANNEL->I2STXMODE |= 0x02;
		}
		if (config->txChannelConfig->txFpin == 1) {
			I2S_CHANNEL->I2STXMODE |= (1 << 2);
		}
		if (config->txChannelConfig->txMcena == 1) {
			I2S_CHANNEL->I2STXMODE |= (1 << 3);
		}
	
	
		if (config->rxChannelConfig->rxClkSel == enI2sClkSel_MCLK) {
			I2S_CHANNEL->I2SRXMODE |= 0x02;
		}
		if (config->rxChannelConfig->rxFpin == 1)
		{
			I2S_CHANNEL->I2SRXMODE |= (1 << 2);
		}
		if (config->rxChannelConfig->rxMcena == 1) 
		{
			I2S_CHANNEL->I2SRXMODE |= (1 << 3);
		}
		
		
	i2sMclk = pfSysGetPclk(PCLK_DIV(I2S_CH));
	I2S_CHANNEL->I2STXRATE = 0x1019; //X= 0x10 and Y=0x19
	I2S_CHANNEL->I2SRXRATE = 0x1019; //X= 0x10 and Y=0x19
		//datacheck= I2S_CHANNEL->I2STXRATE;
	i2sMclk = i2sMclk/3;
	bitrate = (i2sMclk/(config->txChannelConfig->frequency)) - 1;
	I2S_CHANNEL->I2STXBITRATE = bitrate;
	I2S_CHANNEL->I2SRXBITRATE = bitrate;
	if(config->interrupt != enI2sIntNone)
	{
	 if(config->interrupt == enI2sIntTx)
	 {
		I2S_CHANNEL->I2SIRQ |= (1<<1);
		I2S_CHANNEL->I2SIRQ |= (config->txChannelConfig->txInterruptFifoLevel << 8); 
		 txCallback = config->txcallback;
	 }
	 else if(config->interrupt == enI2sIntRx)
	 {
		I2S_CHANNEL->I2SIRQ |= (1<<0);
	    I2S_CHANNEL->I2SIRQ |= (config->txChannelConfig->txInterruptFifoLevel << 16); 
            rxCallback = config->rxcallback;		 
	 }
	 else
	 {
		I2S_CHANNEL->I2SIRQ |= (1<<1);
		I2S_CHANNEL->I2SIRQ |= (config->txChannelConfig->txInterruptFifoLevel << 8); 
		I2S_CHANNEL->I2SIRQ |= (1<<0);
	    I2S_CHANNEL->I2SIRQ |= (config->txChannelConfig->txInterruptFifoLevel << 16);  
     }		 
	}	
	else
	{ 
		txCallback =0;
		rxCallback =0;
	}
	pfMemCopy(&i2sCfg, config, sizeof(PFCfgI2s));
	ConfigInit= enBooleanTrue;
	return enStatusSuccess;
	}

PFEnStatus I2sClose(void)
{ 
	#if(PF_I2S_DEBUG == 1)
	if(ConfigInit==0)
		return enStatusNotConfigured;
	
#endif
	ConfigInit=enBooleanFalse;
	POWER_OFF(I2S_CH);
	 return enStatusSuccess;
}


PFEnStatus pfI2sStart(void)
{ 
#if(PF_I2S_DEBUG == 1)
	{
		if(ConfigInit==0)
		return enStatusNotConfigured;
	}
#endif
	I2S_CHANNEL->I2SDAO &= ~(i2sCfg.txChannelConfig->txReset);
	I2S_CHANNEL->I2SDAI &= ~(i2sCfg.rxChannelConfig->rxReset);
	I2S_CHANNEL->I2SDAO &= ~(i2sCfg.txChannelConfig->txStop);
	I2S_CHANNEL->I2SDAI &= ~(i2sCfg.rxChannelConfig->rxStop);
	I2S_CHANNEL->I2SDAO &= ~(i2sCfg.txChannelConfig->txMute);
 return enStatusSuccess;	
}

PFEnStatus pfI2sIntEnable()
{
#if(PF_I2S_DEBUG == 1)
	
		if(ConfigInit==0)
		return enStatusNotConfigured;
	
#endif
	NVIC_EnableIRQ(I2S_IRQn);
	return enStatusSuccess; 
}
PFEnStatus pfI2sIntDisable()
{
#if(PF_I2S_DEBUG == 1)
	
		if(ConfigInit==0)
		return enStatusNotConfigured;
	
#endif
	NVIC_DisableIRQ(I2S_IRQn);
	return enStatusSuccess;
}

PFEnStatus pfI2sWrite(PFbyte *data,PFdword size) 
{
	PFdword index;
#if(PF_I2S_DEBUG == 1)
	{
		 if(ConfigInit==0)
		return enStatusNotConfigured;	
	}
#endif
for(index=0; index <=size;index++)
	{
		while(pfI2SGetFifoLevel(enI2sModeTx)>6);
		//I2S_CHANNEL->I2STXFIFO = *(data);
	    I2S_CHANNEL->I2STXFIFO = *(data+index);
	}
		return enStatusSuccess;
}


PFEnStatus pfI2sRead(PFbyte *data,PFdword size,PFdword *readBytes) 
{
	PFdword index=0;
	
#if(PF_I2S_DEBUG == 1)
		if(ConfigInit==0)
		return enStatusNotConfigured;	
#endif
	for(index = 0; index < size; index++)
		{
			*(data + index) = I2S_CHANNEL->I2STXFIFO;
			*readBytes += 1;
		}
		return enStatusSuccess;
}


PFEnStatus pfI2sPause(PFEnI2sMode mode)
{
#if(PF_I2S_DEBUG == 1)
	{
		if(ConfigInit==0)
			return enStatusNotConfigured;
	}
#endif
	
	if (mode == enI2sModeTx) //Transmit mode
	{
		I2S_CHANNEL->I2SDAO |= (1<<3);
	} else //Receive mode
	{
		I2S_CHANNEL->I2SDAI |= (1<<3);
	}
	return enStatusSuccess;
}	


PFEnStatus pfI2sMute(PFEnI2sMode mode)
{
#if(PF_I2S_DEBUG == 1)
	
		if(ConfigInit==0)
		return enStatusNotConfigured;
#endif
	 if (mode == enI2sModeTx) //Transmit mode
	{
		I2S_CHANNEL->I2SDAO |= (1<<15);
	} else //Receive mode
	{
		I2S_CHANNEL->I2SDAI |= (1<<15);
	}
	return enStatusSuccess;
}

PFEnStatus pfI2SGetIRQDepth(PFEnI2sMode mode,PFdword *depth)
{
#if(PF_I2S_DEBUG == 1)
	
	 if(ConfigInit==0)
	return enStatusNotConfigured;
	
#endif
	if(mode == enI2sModeTx)
		*depth = (((I2S_CHANNEL->I2SIRQ)>>16)&0xFF);
	else
		*depth = (((I2S_CHANNEL->I2SIRQ)>>8)&0xFF);
	return enStatusSuccess;
}


PFword pfI2SGetFifoLevel(PFEnI2sMode mode)
{
	PFword level1=0;
#if(PF_I2S_DEBUG == 1)
	
		 if(ConfigInit==0)
		return enStatusNotConfigured;
#endif	
	if(mode == enI2sModeTx)
	{
	 level1 =((I2S_CHANNEL->I2SSTATE >> 16) & 0x0F);
	}
	else
	{
		level1 = ((I2S_CHANNEL->I2SSTATE >> 8) & 0x0F);
	}
	return level1;

}

PFEnStatus pfI2sGetIRQStatus(PFEnI2sMode mode,PFbyte *checkStatus)
{
#if(PF_I2S_DEBUG == 1)
	if(ConfigInit==0)
	return enStatusNotConfigured;
	
#endif	
	if(mode == enI2sModeTx)
		*checkStatus = ((I2S_CHANNEL->I2SIRQ >> 1)&0x01);
	else
		*checkStatus =((I2S_CHANNEL->I2SIRQ)&0x01);
	return enStatusSuccess;
}

void I2S_IRQHandler(void)
{ 
	PFbyte intState = 0;
	PFdword rxLevel=0;
	PFdword txLevel=0;
	intState = (I2S_CHANNEL->I2SIRQ);
	if(intState &(1<<0)) //RX
	{	
	rxLevel = (I2S_CHANNEL->I2SSTATE >> 8);
	 if(i2sCfg.rxChannelConfig->rxInterruptFifoLevel == rxLevel)
	 {
		 rxCallback();				 
	}
	if(intState &(1<<1)) //RX
	{	
	 rxLevel = (I2S_CHANNEL->I2SSTATE >> 16);
	 if(i2sCfg.rxChannelConfig->rxInterruptFifoLevel == txLevel)
	 {
 		 txCallback();				 
      }
     }

	}
}
